package br.com.mercadodemusica.controller;

import org.springframework.stereotype.Controller;

@Controller
public class LojaOnlineController {
	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private PesquisaProdutoService pesquisaProdutoService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@RequestMapping(value = "/loja/{idUsuario}")
//	public ModelAndView obterLojaOnline(HttpServletRequest request, @PathVariable BigInteger idUsuario) throws UnknownHostException, MalformedURLException, UsuarioException, IOException {
////		Verificacao de usuario pelo ip ou online
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		UsuarioDTO usuarioDonoDaLojaDto = usuarioService.obterInfosUsuarioPorId(idUsuario);
//		
//		List<InstrumentoAcessorioLojaOnlineDTO> listaInstrumentoAcessorioLojaOnlineDto = pesquisaProdutoService.obterProdutoPorUsuario(usuarioDonoDaLojaDto);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		modelAndView.addObject("titulo", usuarioService.obterNomeDaLojaOnline(usuarioDonoDaLojaDto, listaInstrumentoAcessorioLojaOnlineDto));
//		modelAndView.addObject("h3", usuarioService.obterNomeDaLojaOnline(usuarioDonoDaLojaDto, listaInstrumentoAcessorioLojaOnlineDto));
//		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
//		modelAndView.addObject("pagina", "loja/produtos_loja_online");		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		modelAndView.addObject("listaInstrumentoAcessorioLojaOnlineDto", listaInstrumentoAcessorioLojaOnlineDto);
//		
//		return modelAndView;
//	}
//	
//	@RequestMapping(value = "/lojas_online/{quantidadeRegistros}/{pagina}")
//	public ModelAndView lojasOnline(HttpServletRequest request, @PathVariable("quantidadeRegistros") BigInteger quantidadeRegistros, @PathVariable("pagina") BigInteger pagina) throws UnknownHostException, MalformedURLException, UsuarioException, IOException, EnderecoException {
////		Verificacao de usuario pelo ip ou online
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
////		UsuarioDTO usuarioDonoDaLojaDto = usuarioService.obterInfosUsuarioPorId(idUsuario);
//		
////		List<InstrumentoAcessorioLojaOnlineDTO> listaInstrumentoAcessorioLojaOnlineDto = pesquisaProdutoService.obterProdutoPorUsuario(usuarioDonoDaLojaDto);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		modelAndView.addObject("titulo", "- Lojas Virtuais");
//		modelAndView.addObject("h3", "Lojas Virtuais");
//		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
//		modelAndView.addObject("pagina", "loja/lista_de_lojas_on_line");		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		modelAndView.addObject("listaUsuarioDto", usuarioService.obterListaDeLojasOnLine(usuarioDto, quantidadeRegistros, pagina));
//		return modelAndView;
//	}
//	
//	@RequestMapping(value = "/bucar_loja_virtual")
//	@ResponseBody
//	public List<UsuarioDTO> buscarLojaVirtual(HttpServletRequest request) throws UnknownHostException, MalformedURLException, UsuarioException, IOException, EnderecoException {
////		Verificacao de usuario pelo ip ou online
//		
//		String nome = request.getParameter("q");
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		return usuarioService.obterLojasOnlinePorNome(usuarioDto, nome);
//	}
}
