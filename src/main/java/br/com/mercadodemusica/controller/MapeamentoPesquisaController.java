package br.com.mercadodemusica.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.mercadodemusica.dto.MapeamentoPesquisaDetalheDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.MapeamentoPesquisaFacade;

@Controller
public class MapeamentoPesquisaController {
	
	@Autowired
	private MapeamentoPesquisaFacade mapeamentoPesquisaFacade;
	
	@RequestMapping(value = "/inserir_pesquisa_detalhe", method = RequestMethod.POST)
	@ResponseBody
	public void inserirPesquisaDetalhe(HttpServletRequest request, @RequestBody MapeamentoPesquisaDetalheDTO mapeamentoPesquisaDetalheDto) throws RegraDeNegocioException {
		mapeamentoPesquisaDetalheDto.setIp(request.getRemoteAddr());
		mapeamentoPesquisaDetalheDto.setData(Calendar.getInstance());
		
		mapeamentoPesquisaFacade.salvarPesquisaDetalhe(mapeamentoPesquisaDetalheDto);
	}
}
