package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.MapeamentoPesquisaDTO;
import br.com.mercadodemusica.dto.PesquisaDeProdutosDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.dto.SubCategoriaDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.enums.TipoPesquisa;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.CategoriaFacade;
import br.com.mercadodemusica.facade.MapeamentoPesquisaFacade;
import br.com.mercadodemusica.facade.PesquisaDeProdutoFacade;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@Controller
public class PesquisaProdutosController {
	
	@Autowired
	private UsuarioFacade usuarioFacade;
	
	@Autowired
	private PesquisaDeProdutoFacade pesquisaProdutoFacade;
	
	@Autowired
	private CategoriaFacade categoriaFacade;
	
	@Autowired
	private MapeamentoPesquisaFacade mapeamentoPesquisaFacade;
	
	@RequestMapping(value = "/pesquisa_produtos/{pagina}/{pesquisa}/{tipoPesquisa}/{produtosPorPagina}/{precoInicial}/{precoFinal}/{categoriasJson}", method = RequestMethod.GET)
	public ModelAndView pesquisaProdutos(HttpServletRequest request, @PathVariable("pagina") BigInteger pagina, 
			@PathVariable("pesquisa") String pesquisa, @PathVariable("tipoPesquisa") TipoPesquisa tipoPesquisa, 
			@PathVariable("produtosPorPagina") Integer produtosPorPagina, @PathVariable("precoInicial") BigDecimal precoInicial, 
			@PathVariable("precoFinal") BigDecimal precoFinal, @PathVariable("categoriasJson") JSONArray categoriasJson) throws IOException, RegraDeNegocioException, JSONException, ParseException, URISyntaxException {
		
		return this.montarTelaDePesquisa(request, pagina, pesquisa, tipoPesquisa, produtosPorPagina, precoInicial, precoFinal, categoriasJson);
	}
		
	private ModelAndView montarTelaDePesquisa(HttpServletRequest request, BigInteger pagina, String pesquisa, TipoPesquisa tipoPesquisa, Integer produtosPorPagina, BigDecimal precoInicial, BigDecimal precoFinal, JSONArray categoriasJson) throws JSONException, ParseException, IOException, RegraDeNegocioException, URISyntaxException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		ProdutoDTO produtoDto = new ProdutoDTO();
		produtoDto.setNome(pesquisa);
		
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("pagina", "pesquisa_produtos/retorno_pesquisa");
		
		
		MapeamentoPesquisaDTO mapeamentoPesquisaDto = new MapeamentoPesquisaDTO();
		mapeamentoPesquisaDto.setUsuario(usuarioDto);
		mapeamentoPesquisaDto.setPesquisa(pesquisa);
		
		mapeamentoPesquisaFacade.salvarPesquisa(mapeamentoPesquisaDto);
		
		PesquisaDeProdutosDTO pesquisaDeProdutosDto = pesquisaProdutoFacade.pesquisaDeProdutos(usuarioDto, produtoDto, produtosPorPagina, pagina, tipoPesquisa, precoInicial, precoFinal, categoriasJson);
		pesquisaDeProdutosDto.setTipoPesquisa(tipoPesquisa);
		
		modelAndView.addObject("listaDeTipoPesquisa", TipoPesquisa.values());
		modelAndView.addObject("produtoPesquisado", pesquisa);
		modelAndView.addObject("pesquisaDeProdutos", pesquisaDeProdutosDto);
		
		if(pesquisaDeProdutosDto.getProdutosEncontrados().equals(Boolean.FALSE)) {
			modelAndView.addObject("titulo", "Pesquisa por " + (pesquisa.equals("null") ? pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getCategoria().getNome() + " -> " + pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getNome() : pesquisa.toUpperCase()) + " não encontrou resultados.");
			modelAndView.addObject("h3", "Pesquisa por " + (pesquisa.equals("null") ? pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getCategoria().getNome() + " -> " + pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getNome() : pesquisa.toUpperCase()) + " não encontrou resultados. Talvez você possa gostar destas ofertas:");
		} else {
			modelAndView.addObject("titulo", "Pesquisa por " + (pesquisa.equals("null") ? pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getCategoria().getNome() + " -> " + pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getNome() : pesquisa.toUpperCase()));
			modelAndView.addObject("h3", "Encontramos " + pesquisaDeProdutosDto.getContagem() + " resultados para sua busca por " + (pesquisa.equals("null") ? pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getCategoria().getNome() + " -> " + pesquisaDeProdutosDto.getListaDeSubcategoriasEscolhidas().get(0).getNome() : pesquisa.toUpperCase()));
		}
		
		
		
		
		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
		
		
		return modelAndView;
		
	}
	
	
	@RequestMapping(value = "/obter_subcategorias_por_categoria", method = RequestMethod.GET)
	@ResponseBody
	public List<SubCategoriaDTO> obterSubcategoriasPorCategoria(@RequestParam("idCategoria") String idCategoriaString, @RequestParam("pesquisa") String pesquisa) {
		
		
		return categoriaFacade.obterSubcategoriasPorCategoria(idCategoriaString, pesquisa);
		
	}
	
}
