package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.mercadodemusica.dto.CompraDTO;
import br.com.mercadodemusica.dto.ProdutoCompraDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.enums.StatusPagamentoEnum;
import br.com.mercadodemusica.enums.StatusTransacaoEnum;
import br.com.mercadodemusica.enums.TipoPagamentoEnum;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.CompraFacade;
import br.com.mercadodemusica.facade.ProdutoFacade;

@Controller
public class CarrinhoDeComprasController {
	
	@Autowired
	private CompraFacade compraFacade;
	
	@Autowired
	private ProdutoFacade produtoFacade;
	
	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private CompraVendaService compraVendaService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private PesquisaProdutoService pesquisaProdutoService;
//	
//	@Autowired
//	private PrecoPrazoCorreiosService precoPrazoCorreiosService;
//	
//	@Autowired
//	private CorreiosService correiosService;
//	
//	@Autowired
//	private GatewayDePagamentoService gatewayDePagamentoService;
//	
//	@Autowired
//	private DetalhamentoProdutoService detalhamentoProdutoService;
//	
//	@Autowired
//	private RastreamentoService rastreamentoService;
//		
//	/*
//	 * Por uma questão dos servicos estarem em modulos diferentes
//	 * foi salvo primeiramente a entidade de compra com seu respectivo comprador
//	 * para depois salvar os produtos comprados
//	 * 
//	 */
//
//	@RequestMapping(value="/inserir_carrinho_de_compras")
//	public void inserirCarrinhoDeCompra(HttpServletRequest request, BigInteger idProduto) throws UnknownHostException, MalformedURLException, IOException, CompraVendaException, UsuarioException {
//		
//		usuarioService.obterInfosUsuarioVerificacaoOnLine(request);
//	
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		if(compraDto == null) {
//			compraDto = new CompraDTO();
//			compraDto.setComprador(usuarioDto);
//		}
//	
//		List<ProdutoDTO> listaDeProdutoDto = new ArrayList<ProdutoDTO>();
//		listaDeProdutoDto.add(infosGeraisProdutoUsuarioDto);
//		
//		compraDto.setProdutosComprados(listaDeProdutoDto);
//		
//		compraVendaService.inserirProdutosNaCompraFeitaRealizada(compraDto);
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/mostrar_carrinho_de_compras")
//	public ModelAndView mostrarCarrinhoDeCompra(HttpServletRequest request, Model model) throws UnknownHostException, MalformedURLException, IOException, UsuarioException, CarrinhoDeCompraException, CorreiosException, CompraVendaException, EnderecoException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		
//		modelAndView.addObject("titulo", "Carrinho de compras");
//		modelAndView.addObject("h3", "Carrinho de compras");
//		modelAndView.addObject("pagina", "compras/carrinho_de_compras");
//		
//		CompraDTO compraDoDiaDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		/*
//		 * Aqui é lancado uma excessao caso a compra esteja nula
//		 */
//		compraVendaService.verificarCarrinhoVazio(compraDoDiaDto);
//		
//		/*
//		 * metodo usado para colocar os preços e prazos dos correios
//		 */
//		correiosService.obterPrecosPrazosCorreios(compraDoDiaDto);
//		
//		List<ApresentacaoProdutoDTO> listaDeApresentacaoDto = new ArrayList<ApresentacaoProdutoDTO>();
//		for(ProdutoDTO produtoComprado : compraDoDiaDto.getProdutosComprados()) {	
//			produtoComprado = produtoService.atualizarDadosDosCorreiosVendedorComprador(produtoComprado);
//			
//			
//			
//			List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = produtoService.obterApresentacaoPorProduto(produtoComprado);
//			listaDeApresentacaoDto.add(pesquisaProdutoService.setarApresentacaoComFotoDto(listaDeApresentacoesDto));
//		}
//		
//		
//		/*
//		 * nova chamada porque os atributos viram nulos
//		 * 
//		 */
//		compraDoDiaDto = compraVendaService.obterCompraCompleta(compraDoDiaDto);
//		String valorTotal = compraVendaService.obterValorTotal(compraDoDiaDto);
//		
//		
//		
//		
//		modelAndView.addObject("listaApresentacoesDoDia", listaDeApresentacaoDto);
//		modelAndView.addObject("valorTotal", valorTotal);
//		
//		modelAndView.addObject("compraDto", compraDoDiaDto);
//		
//		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addAttribute("compraDtoModel", new CompraDTO());
//		
//		return modelAndView;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/produtos_com_aviso_de_entrega")
//	@ResponseBody
//	public List<ProdutoDTO> produtosComAvisoDeEntrega(HttpServletRequest request) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, CarrinhoDeCompraException, JSONException, EnderecoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		CompraDTO compraDoDia = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		if(compraDoDia == null) {
//			return null;
//		} else {
//			
//			List<ApresentacaoProdutoDTO> listaDeApresentacaoDto = new ArrayList<ApresentacaoProdutoDTO>();
//			for(ProdutoDTO produtoComprado : compraDoDia.getProdutosComprados()) {
//				listaDeApresentacaoDto.addAll(produtoService.obterApresentacaoPorProduto(produtoComprado));
//			}
//			List<ProdutoDTO> listaDeProdutosComAvisoDeEntrega = precoPrazoCorreiosService.verificarTipoDeEntregaDistanciaEntreUsuarios(listaDeApresentacaoDto, usuarioDto);
//			return listaDeProdutosComAvisoDeEntrega;
//		}
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/estou_ciente_da_distancia")
//	public void estouCienteDaDistancia(HttpServletRequest request) throws UnknownHostException, MalformedURLException, IOException, UsuarioException, JSONException, EnderecoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		CompraDTO compraDoDia = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		if(compraDoDia != null) {	
//			
//			List<ApresentacaoProdutoDTO> listaDeApresentacaoDto = new ArrayList<ApresentacaoProdutoDTO>();
//			for(ProdutoDTO produtoComprado : compraDoDia.getProdutosComprados()) {
//				listaDeApresentacaoDto.addAll(produtoService.obterApresentacaoPorProduto(produtoComprado));
//			}
//			List<ProdutoDTO> listaDeProdutosComAvisoDeEntrega = precoPrazoCorreiosService.verificarTipoDeEntregaDistanciaEntreUsuarios(listaDeApresentacaoDto, usuarioDto);
//			
//			compraVendaService.updateNaPoliticaDeCompra(compraDoDia, listaDeProdutosComAvisoDeEntrega);
//		}
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/remover_produto_do_carrinho_de_compras")
//	@ResponseBody
//	public String removerCarrinhoDeCompra(HttpServletRequest request, BigInteger idProduto) throws UnknownHostException, MalformedURLException, IOException, CarrinhoDeCompraException, UsuarioException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		
//		produtoService.retirarCompraDoProduto(infosGeraisProdutoUsuarioDto);
//		
//		CompraDTO compraDoDia = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		compraDoDia = compraVendaService.removerCompraSemProduto(compraDoDia);
//		
//		
//		if(compraDoDia == null) {
//			return null;
//		} else {
//			return compraVendaService.obterValorTotal(compraDoDia);
//		}
//		
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/insercao_de_dados_para_pagamento", method=RequestMethod.POST)
//	public ModelAndView insercaoDeDadosParaPagamento(HttpServletRequest request, @ModelAttribute("compraDtoModel") CompraDTO compraDto) throws UnknownHostException, MalformedURLException, IOException, CompraVendaException, PesquisaDeProdutosException, NaoEncontradoException, FotosException, UsuarioException, GatewayDePagamentoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		CompraDTO compraDoDiaDto = compraVendaService.obterCompraCompleta(compraDto);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//
//		modelAndView.addObject("titulo", "Pagamento");
//		modelAndView.addObject("h3", "Pagamento");
//		modelAndView.addObject("pagina", "compras/insercao_de_dados");
//		
//		modelAndView.addObject("compraDoDiaDto", compraDoDiaDto);
//		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
//		modelAndView.addObject("listaDeAnosDeVencimentoCartaoDeCredito", compraVendaService.obterAnosDeVencimentoDoCartaoDeCredito());
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		/*
//		 * parte de parcelamento, igual ao detalhamento de produtos. 
//		 * Neste caso é usado apenas para deixar claro os valores de compra por parte do cliente
//		 */
//		String valorTotal = compraVendaService.obterValorTotal(compraDoDiaDto);
//		modelAndView.addObject("parcelamentoTotalDaCompraDto", this.parcelasTotaisCartaoCredito(compraDoDiaDto, ParcelamentoEnum.UM, null));
//		modelAndView.addObject("valorTotalString", valorTotal);
//		modelAndView.addObject("valorTotalBigDecimal", new Precos().padraoBrasileiroParaBigDecimal(valorTotal).doubleValue());
//		
//		return modelAndView;
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/verificacao_parcelamento_total", method=RequestMethod.GET)
//	@ResponseBody
//	public List<BandeiraValoresParcelasGatewayDePagamentoDTO> verificacaoParcelamentoTotal(HttpServletRequest request, BigInteger parcela, String bandeiraCartao) throws UnknownHostException, MalformedURLException, IOException, CompraVendaException, UsuarioException, PesquisaDeProdutosException, NaoEncontradoException, FotosException, GatewayDePagamentoException{
//		
//		InstituicaoEnum instituicaoEnum = gatewayDePagamentoService.verificarBandeiraCartaoDeCredito(bandeiraCartao);
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);	
//		CompraDTO compraDoDiaDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		return this.parcelasTotaisCartaoCredito(compraDoDiaDto, Parcelamentos.retornoEnum(parcela), instituicaoEnum);
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/compra_cartao_de_credito", method = RequestMethod.POST)
//	public ModelAndView enviarCompraCartaoDeCredito(HttpServletRequest request, @ModelAttribute("pagamentoCartoesDto") PagamentoCartoesDTO pagamentoCartoesDto) throws UnknownHostException, MalformedURLException, IOException, UsuarioException, PesquisaDeProdutosException, NaoEncontradoException, NumberFormatException, CorreiosException, CompraVendaException, EnderecoException, GatewayDePagamentoException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, MessagingException, FotosException, JSONException, CodigoDeEntregaException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		gatewayDePagamentoService.verificarBandeiraCartaoDeCreditoNula(pagamentoCartoesDto.getInstituicao());
//		
//		List<BandeiraValoresParcelasGatewayDePagamentoDTO> parcelasValoresGatewayDePagamentoDto = this.parcelasTotaisCartaoCredito(compraDto, Parcelamentos.retornoEnum(BigInteger.valueOf(pagamentoCartoesDto.getParcelas())), pagamentoCartoesDto.getInstituicao());
//			
//		/*
//		 * 
//		 */
//		gatewayDePagamentoService.efetuarPagamentoComCartaoDeCredito(pagamentoCartoesDto, compraDto, parcelasValoresGatewayDePagamentoDto, request);
//		/*
//		 * 
//		 */
//	
//	
//		for( ProdutoDTO produtoComprado : compraDto.getProdutosComprados() ) {
//			produtoService.updateProdutoParaInativo(produtoComprado);
//		}
//		
//		return this.pagamentoConcluído(request);
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/compra_boleto")
//	public ModelAndView enviarCompraBoleto(HttpServletRequest request) throws UnknownHostException, MalformedURLException, IOException, UsuarioException, CompraVendaException, EnderecoException, GatewayDePagamentoException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, MessagingException, NumberFormatException, CorreiosException, PesquisaDeProdutosException, FotosException, NaoEncontradoException, JSONException, CodigoDeEntregaException{
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//	
//		gatewayDePagamentoService.efetuarPagamentoComBoleto(compraDto, request);
//		
//		for( ProdutoDTO produtoComprado : compraDto.getProdutosComprados() ) {
//			produtoService.updateProdutoParaInativo(produtoComprado);
//		}
//		
//		return this.pagamentoConcluído(request);
//	}
//	
//	
//	private ModelAndView pagamentoConcluído(HttpServletRequest request) throws UnknownHostException, MalformedURLException, IOException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraFinalizadaDoDia(usuarioDto);
//		
//		
//		/*
//		 * verificacao caso a compra tenha sido feita por boleto, enviamos ele no html para ter o link para a página de impressão do mesmo
//		 */
//		CompraBoletoDTO compraBoletoDto = compraVendaService.obterCompraPorBoleto(compraDto);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		
//		
////		enviar email para comprador com os dados da compra
//		
//		/*
//		 * verificar o comp farei para enviar este email
//		 * 
//		 */
//		
//		
//		modelAndView.addObject("titulo", "Carrinho de compras");
//		modelAndView.addObject("h3", "Pagamento concluído");
//		modelAndView.addObject("pagina", "compras/pagamento_concluido");
//		
//		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		
//		modelAndView.addObject("compraDto", compraDto);
//		modelAndView.addObject("compraBoletoDto", compraBoletoDto);
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		
//		return modelAndView;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/mudanca_frete")
//	@ResponseBody
//	public Map<String, Object> mudancaDeFrete(HttpServletRequest request, BigInteger idFrete) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, CarrinhoDeCompraException, CompraVendaException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		ValorFreteCorreioDTO valorFreteCorreiosDto = new ValorFreteCorreioDTO();
//		valorFreteCorreiosDto.setId(idFrete);
//		
//		rastreamentoService.atualizarFreteEscolhido(valorFreteCorreiosDto);
//		
//		CompraDTO compraDoDiaDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		
//		List<ApresentacaoProdutoDTO> listaDeApresentacaoDto = new ArrayList<ApresentacaoProdutoDTO>();
//		for(ProdutoDTO produtoComprado : compraDoDiaDto.getProdutosComprados()) {		
//			
//			List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = produtoService.obterApresentacaoPorProduto(produtoComprado);
//			listaDeApresentacaoDto.add(pesquisaProdutoService.setarApresentacaoComFotoDto(listaDeApresentacoesDto));
//		}
//	
//		String valorTotal = compraVendaService.obterValorTotal(compraDoDiaDto);
//		
//		
//		Map<String, Object> hashRetorno = new HashMap<String, Object>();
//
//		hashRetorno.put("listaApresentacoesDoDia", listaDeApresentacaoDto);
//		hashRetorno.put("valorTotal", valorTotal);
//		return hashRetorno;
//	}
//	
//	
//	private List<BandeiraValoresParcelasGatewayDePagamentoDTO> parcelasTotaisCartaoCredito(CompraDTO compraDoDiaDto, ParcelamentoEnum parcelamentoEnum, InstituicaoEnum instituicaoEnum) throws PesquisaDeProdutosException, NaoEncontradoException, FotosException, GatewayDePagamentoException, UsuarioException {
//		List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaParcelamentoTotalDto = new ArrayList<BandeiraValoresParcelasGatewayDePagamentoDTO>();
//		for(ProdutoDTO produtoComprado : compraDoDiaDto.getProdutosComprados()) {
//			
//			PesquisaDeProdutosDTO pesquisaDeProdutosDto = detalhamentoProdutoService.detalhamentoProdutoId(produtoComprado);
//			List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaBandeiraValoresParcelasGatewayDePagamentoDto = gatewayDePagamentoService.obterValorDoParcelamento(parcelamentoEnum, pesquisaDeProdutosDto.getListaApresentacaoProduto().get(0).getProduto(), TipoPagamentoEnum.CartaoCredito, instituicaoEnum);
//								
////			lista nas vezes pedidas
//			listaParcelamentoTotalDto = gatewayDePagamentoService.parcelamentoTotal(listaParcelamentoTotalDto, listaBandeiraValoresParcelasGatewayDePagamentoDto);
//			
//		}
//		
//		return listaParcelamentoTotalDto;
//	}
	
	@RequestMapping(value = "/insercao_compra_ir_para_loja")
	@ResponseBody
	public String insercaoCompraIrParaLoja(String idCrypt, HttpServletRequest request) throws IOException, RegraDeNegocioException {
		
		ProdutoDTO produtoDto = produtoFacade.obterProdutoPorId(idCrypt);
		
		CompraDTO compraDto = new CompraDTO();
		compraDto.setAceitePoliticaDeCompra(Boolean.TRUE);
		compraDto.setCriacao(Calendar.getInstance());
		compraDto.setFinalizado(Boolean.TRUE);
		compraDto.setStatusPagamento(StatusPagamentoEnum.Sucesso);
		compraDto.setStatusTransacao(StatusTransacaoEnum.APROVADO);
		compraDto.setTipoPagamento(TipoPagamentoEnum.RedirecionamentoLoja);
		compraDto.setIpComprador(request.getRemoteAddr());
		
		
		ProdutoCompraDTO produtoCompraDto = new ProdutoCompraDTO();
		produtoCompraDto.setProduto(produtoDto);
		produtoCompraDto.setCompra(compraDto);
		produtoCompraDto.setDataDaCompra(Calendar.getInstance());
		
		
		compraFacade.insercaoCompra(produtoCompraDto);
		
		return produtoDto.getUrlAcesso();
	}
	
}
