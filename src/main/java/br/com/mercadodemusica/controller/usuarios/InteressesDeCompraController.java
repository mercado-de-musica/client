package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class InteressesDeCompraController {
	
	
//	@Autowired
//	UsuarioFacade usuarioService;
//	
//	@Autowired
//	ProdutoService produtoService;
//	
//	@Autowired
//	InteresseDeCompraService interesseDeCompraService;
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/interesses_de_compra")
//	public ModelAndView cadastroDeInteresseDeCompras(HttpServletRequest request, Model model) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		model.addAttribute("interessesDeCompra", new InteressesDeCompraDTO());
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("pagina", "area_usuario/interesses_compra/interesses_compra");
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		modelAndView.addObject("listaDistanciaInteresseDeCompraEnum", DistanciaInteresseDeCompraEnum.values());
//		modelAndView.addObject("listaInteressesDeCompraUsuarioDto", interesseDeCompraService.obterInteressesDeCompraPorUsuario(usuarioDto));
//		
//		return modelAndView;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/interesses_de_compra_cadastrado")
//	public ModelAndView cadastroDeInteresseDeComprasCadastrado(HttpServletRequest request, @ModelAttribute("interessesDeCompra") InteressesDeCompraDTO interesseDeCompraDto) throws RegraDeNegocioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		interesseDeCompraDto.setUsuario(usuarioDto);
//		
//		interesseDeCompraService.cadastrarInteresseDeCompras(interesseDeCompraDto);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		modelAndView.addObject("mensagem", "Interesse de compra cadastrado com sucesso!");
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("url", request.getHeader("Referer"));
//		modelAndView.addObject("usuarioDto", usuarioDto);
//		
//		modelAndView.addObject("pagina", "area_usuario/interesses_compra/cadastro_interesses_compra_cadastrado");
//		
//		return modelAndView;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/obter_marca_por_instrumento_acessorio")
//	@ResponseBody
//	public Set<MarcaDTO> obterMarcaPorInstrumentoAcessorio(BigInteger idInstrumentoAcessorio) {		
//		
//		CategoriaProdutoDTO infosGeraisProdutoDto = new CategoriaProdutoDTO();
//		InstrumentoAcessorioDTO instrumentoAcessorioDto = new InstrumentoAcessorioDTO();
//		
//		instrumentoAcessorioDto.setId(idInstrumentoAcessorio);
//		infosGeraisProdutoDto.setInstrumentoAcessorio(instrumentoAcessorioDto);
//		
//		return produtoService.obterMarcaPorInstrumentoAcessorio(infosGeraisProdutoDto);
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/obter_produto_por_instrumento_acessorio_marca")
//	@ResponseBody
//	public Set<ProdutoDTO> obterProdutoPorInstrumentoAcessorioMarca(BigInteger idInstrumentoAcessorio, BigInteger idMarca) {		
//		
//		CategoriaProdutoDTO infosGeraisProdutoDto = new CategoriaProdutoDTO();
//		InstrumentoAcessorioDTO instrumentoAcessorioDto = new InstrumentoAcessorioDTO();
//		MarcaDTO marcaDto = new MarcaDTO();
//		
//		instrumentoAcessorioDto.setId(idInstrumentoAcessorio);
//		marcaDto.setId(idMarca);
//		
//		infosGeraisProdutoDto.setInstrumentoAcessorio(instrumentoAcessorioDto);
//		infosGeraisProdutoDto.setMarca(marcaDto);
//		
//		
//		return produtoService.obterProdutoPorInstrumentoAcessorioMarca(infosGeraisProdutoDto);
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/obter_modelo_por_instrumento_acessorio_marca_produto")
//	@ResponseBody
//	public Set<ModeloDTO> obterModeloPorInstrumentoAcessorioMarcaProduto(BigInteger idInstrumentoAcessorio, BigInteger idMarca, BigInteger idProduto) {		
//		
//		CategoriaProdutoDTO infosGeraisProdutoDto = new CategoriaProdutoDTO();
//		InstrumentoAcessorioDTO instrumentoAcessorioDto = new InstrumentoAcessorioDTO();
//		MarcaDTO marcaDto = new MarcaDTO();
//		ProdutoDTO produtoDto = new ProdutoDTO();
//		
//		instrumentoAcessorioDto.setId(idInstrumentoAcessorio);
//		marcaDto.setId(idMarca);
//		produtoDto.setId(idProduto);
//		
//		infosGeraisProdutoDto.setInstrumentoAcessorio(instrumentoAcessorioDto);
//		infosGeraisProdutoDto.setMarca(marcaDto);
//		infosGeraisProdutoDto.setProduto(produtoDto);
//		
//		return produtoService.obterModeloPorInstrumentoAcessorioMarcaProduto(infosGeraisProdutoDto);
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/exclusao_de_interesse_de_compra")
//	public void exclusaoDeInteresseDeCompra(HttpServletRequest request, BigInteger interesseDeCompra) throws RegraDeNegocioException {		
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		InteressesDeCompraDTO interesseDeCompraDto = new InteressesDeCompraDTO();
//		interesseDeCompraDto.setId(interesseDeCompra);
//		
//		InteressesDeCompraDTO interesseVerificacaoUsuarioDto = interesseDeCompraService.obterInteresseDeCompra(interesseDeCompraDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, interesseVerificacaoUsuarioDto.getUsuario());
//		
//		interesseDeCompraService.excluirInteresseDeCompra(interesseDeCompraDto);
//		
//	}
	
}
