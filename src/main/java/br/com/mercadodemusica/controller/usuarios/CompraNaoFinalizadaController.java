package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class CompraNaoFinalizadaController {

//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private CompraVendaService compraVendaService;	
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private PesquisaProdutoService pesquisaProdutoService;
//		
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/compras_nao_finalizadas")
//	public ModelAndView comprasNaoFinalizadas(HttpServletRequest request) throws CompraVendaException, UnknownHostException, MalformedURLException, IOException, CorreiosException, UsuarioException {
//			
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		List<CompraDTO> arrayCompraNaoFinalizada = compraVendaService.obterListaDeComprasNaoFinalizadas(usuarioDto);
//		
//			
//		List<ComprasNaoFinalizadasDTO> arrayComprasNaoFinalizadasDto = new ArrayList<ComprasNaoFinalizadasDTO>();
//		/*
//		 * metodo usado para colocar os preços e prazos dos correios
//		 */
//		for(CompraDTO compraDto : arrayCompraNaoFinalizada) {
//			if(compraDto.getProdutosComprados() != null && compraDto.getProdutosComprados().size() > 0) {
//				
//				List<ApresentacaoProdutoDTO> listaDeApresentacaoDto = new ArrayList<ApresentacaoProdutoDTO>();
//				
//				for(ProdutoDTO produtoComprado : compraDto.getProdutosComprados()) {	
//					
//					List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = produtoService.obterApresentacaoPorProduto(produtoComprado);
//					if(listaDeApresentacoesDto != null && !listaDeApresentacoesDto.isEmpty()) {
//						listaDeApresentacaoDto.add(pesquisaProdutoService.setarApresentacaoComFotoDto(listaDeApresentacoesDto));
//						pesquisaProdutoService.setarPrecoPadraoBrasil(listaDeApresentacaoDto);
//					}
//					
//				}
//			
//				/*
//				 * nova chamada porque os atributos viram nulos
//				 * 
//				 */
//				compraDto = compraVendaService.obterCompraCompleta(compraDto);
//				String valorTotal = compraVendaService.obterValorTotalSemFrete(compraDto);
//			
//				ComprasNaoFinalizadasDTO comprasNaoFinalizadasDto = new ComprasNaoFinalizadasDTO();
//				comprasNaoFinalizadasDto.setCompraDto(compraDto);
//				comprasNaoFinalizadasDto.setListaApresentacoes(listaDeApresentacaoDto);
//				comprasNaoFinalizadasDto.setValorTotal(valorTotal);
//				
//				arrayComprasNaoFinalizadasDto.add(comprasNaoFinalizadasDto);
//				
//			}
//		}
//		
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		modelAndView.addObject("h3", "Compras não finalizadas");
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("pagina", "area_usuario/produtos/compra_nao_finalizada");
//		modelAndView.addObject("listaDeComprasNaoFinalizadas", arrayComprasNaoFinalizadasDto);
//		
//		return modelAndView;
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/usuario/remover_produto_de_compras_nao_finalizadas")
//	@ResponseBody
//	public List<ComprasNaoFinalizadasDTO> removerDeComprasNaoFinalizadas(HttpServletRequest request, BigInteger idProduto) throws UnknownHostException, MalformedURLException, IOException, CarrinhoDeCompraException, UsuarioException, ProdutoException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
////		ProdutoDTO infosGeraisProdutoUsuarioVerificacaoDto = produtoService.obterProdutoPorId(idProduto);
//		
//		/*
//		 * verificacao de usuarios
//		 */
////		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioVerificacaoDto.getUsuario());
//		
//		
//		produtoService.retirarCompraDoProduto(infosGeraisProdutoUsuarioDto);
//		
//		List<CompraDTO> arrayCompraDto = compraVendaService.obterListaDeComprasNaoFinalizadas(usuarioDto);
//		
//		List<ComprasNaoFinalizadasDTO> arrayComprasNaoFinalizadasDto = new ArrayList<ComprasNaoFinalizadasDTO>();
//		for(CompraDTO compraDto : arrayCompraDto) {
//			compraDto = compraVendaService.removerCompraSemProduto(compraDto);
//			
//			if(compraDto != null) {
//				
//				List<ApresentacaoProdutoDTO> listaDeApresentacaoDto = new ArrayList<ApresentacaoProdutoDTO>();
//				for(ProdutoDTO produtoComprado : compraDto.getProdutosComprados()) {	
//					
//					List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = produtoService.obterApresentacaoPorProduto(produtoComprado);
//					listaDeApresentacaoDto.add(pesquisaProdutoService.setarApresentacaoComFotoDto(listaDeApresentacoesDto));
//				}
//				
//				
//				String valorTotal = compraVendaService.obterValorTotalSemFrete(compraDto);
//				
//				ComprasNaoFinalizadasDTO comprasNaoFinalizadasDto = new ComprasNaoFinalizadasDTO();
//				comprasNaoFinalizadasDto.setCompraDto(compraDto);
//				comprasNaoFinalizadasDto.setListaApresentacoes(listaDeApresentacaoDto);
//				comprasNaoFinalizadasDto.setValorTotal(valorTotal);
//				
//				arrayComprasNaoFinalizadasDto.add(comprasNaoFinalizadasDto);
//			} 	
//		}
//		
//		return arrayComprasNaoFinalizadasDto;
//		
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/usuario/remover_compra_nao_finalizada")
//	@ResponseBody
//	public List<ComprasNaoFinalizadasDTO> removerCompraNaoFinalizada(HttpServletRequest request, BigInteger idCompra) throws UnknownHostException, MalformedURLException, IOException, CarrinhoDeCompraException, UsuarioException, CompraVendaException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//
//		CompraDTO compraRecebidaDto = new CompraDTO();
//		compraRecebidaDto.setId(idCompra);
//		compraRecebidaDto = compraVendaService.obterCompraCompleta(compraRecebidaDto);
//		
//		if(! compraRecebidaDto.getProdutosComprados().isEmpty()) {
//			/*
//			 * verificacao de usuarios
//			 */
//			VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, compraRecebidaDto.getProdutosComprados().get(0).getUsuario());
//		}
//		
//		for (ProdutoDTO infosGeraisProdutoUsuarioDto : compraRecebidaDto.getProdutosComprados()) {
//			produtoService.retirarCompraDoProduto(infosGeraisProdutoUsuarioDto);
//		}
//		
//		
//		List<CompraDTO> arrayCompraDto = compraVendaService.obterListaDeComprasNaoFinalizadas(usuarioDto);
//		
//		List<ComprasNaoFinalizadasDTO> arrayComprasNaoFinalizadasDto = new ArrayList<ComprasNaoFinalizadasDTO>();
//		for(CompraDTO compraDto : arrayCompraDto) {
//			compraDto = compraVendaService.removerCompraSemProduto(compraDto);
//			
//			if(compraDto != null) {
//				
//				List<ApresentacaoProdutoDTO> listaDeApresentacaoDto = new ArrayList<ApresentacaoProdutoDTO>();
//				for(ProdutoDTO produtoComprado : compraDto.getProdutosComprados()) {	
//					
//					List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = produtoService.obterApresentacaoPorProduto(produtoComprado);
//					listaDeApresentacaoDto.add(pesquisaProdutoService.setarApresentacaoComFotoDto(listaDeApresentacoesDto));
//				}
//				
//				
//				String valorTotal = compraVendaService.obterValorTotalSemFrete(compraDto);
//				
//				ComprasNaoFinalizadasDTO comprasNaoFinalizadasDto = new ComprasNaoFinalizadasDTO();
//				comprasNaoFinalizadasDto.setCompraDto(compraDto);
//				comprasNaoFinalizadasDto.setListaApresentacoes(listaDeApresentacaoDto);
//				comprasNaoFinalizadasDto.setValorTotal(valorTotal);
//				
//				arrayComprasNaoFinalizadasDto.add(comprasNaoFinalizadasDto);
//			} 	
//		}
//		
//		return arrayComprasNaoFinalizadasDto;
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/usuario/verificar_compra_no_dia")
//	@ResponseBody
//	public Boolean verificarCompraNoDia(HttpServletRequest request) throws UnknownHostException, MalformedURLException, IOException, CarrinhoDeCompraException, UsuarioException, CompraVendaException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//
//		CompraDTO compraDoDiaDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		if(compraDoDiaDto == null) {
//			return false;
//		} else {
//			return true;
//		}
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/usuario/finalizar_compra_nao_finalizada")
//	public void finalizarCompraNaoFinalizada(HttpServletRequest request, BigInteger idCompra) throws UnknownHostException, MalformedURLException, IOException, CarrinhoDeCompraException, UsuarioException, CompraVendaException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		CompraDTO compraParaFinalizarDto = new CompraDTO();
//		compraParaFinalizarDto.setId(idCompra);
//				
//		CompraDTO compraDoDiaDto = compraVendaService.obterCompraNaoFinalizadaDoDia(usuarioDto);
//		
//		
//		/*
//		 * nao teve jeito, tive que fazer IF no controller =O
//		 */
//		
//		if(compraDoDiaDto != null && !compraDoDiaDto.getProdutosComprados().isEmpty()) {
//			/*
//			 * verificacao de usuarios
//			 */
//			VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, compraDoDiaDto.getComprador());
//		
//		} else {
//			compraParaFinalizarDto = compraVendaService.obterCompraCompleta(compraParaFinalizarDto);
//			VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, compraParaFinalizarDto.getComprador());
//		}
//		
//		
//		
//		compraVendaService.updateCompraNaoRealizadaComCompraDoDia(compraDoDiaDto, compraParaFinalizarDto);		
//	}
}
