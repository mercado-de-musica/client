package br.com.mercadodemusica.controller.usuarios;

import java.io.Serializable;

import org.springframework.stereotype.Controller;

@Controller
public class ContaBancariaController implements Serializable {

//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 5126066137924524111L;
//
//	@Autowired
//	UsuarioFacade usuarioService;
//	
//	@Autowired
//	GatewayDePagamentoService gatewayDePagamentoService;
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/conta_bancaria")
//	public ModelAndView contaBancaria(HttpServletRequest request) throws GatewayDePagamentoException, EnderecoException, IOException, InterruptedException, ParseException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//	
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		model.addObject("h3", "Contas Bancárias Cadastradas");
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("pagina", "area_usuario/gateway_de_pagamento/lista_contas_bancarias");
//		model.addObject("listaDeContas", gatewayDePagamentoService.obterContasBancariasPorUsuario(usuarioDto));
//		model.addObject("listaDeBancos", BancoEnum.values());
//		
//		return model;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/cadastro_nova_conta_bancaria")
//	public ModelAndView cadastroNovaContaBancaria(HttpServletRequest request, Model model ) throws GatewayDePagamentoException, EnderecoException, IOException, InterruptedException, ParseException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		model.addAttribute("contaBancariaGatewayDePagamentoDto", new ContaBancariaDTO());
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		modelAndView.addObject("listaDeBancos", BancoEnum.values());
//		modelAndView.addObject("h3", "Cadastro nova conta bancária");
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("pagina", "area_usuario/gateway_de_pagamento/cadastro_nova_conta_bancaria");
//		
//		return modelAndView;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/cadastrar_nova_conta_bancaria", method = RequestMethod.POST)
//	public ModelAndView cadastrarNovaContaBancaria(HttpServletRequest request,  @ModelAttribute("contaBancariaGatewayDePagamentoDto") ContaBancariaDTO contaBancariaDto) throws GatewayDePagamentoException, EnderecoException, IOException, InterruptedException, ParseException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		contaBancariaDto.setUsuario(usuarioDto);
//		
//		gatewayDePagamentoService.cadastrarContaBancaria(contaBancariaDto);
//		
//		return new ModelAndView("redirect:conta_bancaria");
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/excluir_conta_bancaria")
//	public void excluirContaBancaria(HttpServletRequest request,  BigInteger idContaBancaria) throws UsuarioException, GatewayDePagamentoException, JSONException, IOException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		ContaBancariaDTO contaBancariaGatewayDePagamentoDto = new ContaBancariaDTO();
//		contaBancariaGatewayDePagamentoDto.setId(idContaBancaria);
//				
//		contaBancariaGatewayDePagamentoDto = gatewayDePagamentoService.obterContaBancaria(contaBancariaGatewayDePagamentoDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, contaBancariaGatewayDePagamentoDto.getUsuario());	
//		gatewayDePagamentoService.excluirContaBancaria(contaBancariaGatewayDePagamentoDto);
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/buscar_conta_bancaria")
//	@ResponseBody
//	public ContaBancariaDTO buscarContaBancaria(BigInteger idContaBancaria) throws GatewayDePagamentoException, JSONException, IOException {
//		
//		ContaBancariaDTO contaBancariaGatewayDePagamentoDto = new ContaBancariaDTO();
//		contaBancariaGatewayDePagamentoDto.setId(idContaBancaria);
//		
//		contaBancariaGatewayDePagamentoDto = gatewayDePagamentoService.obterContaBancaria(contaBancariaGatewayDePagamentoDto);
//		
//		/*
//		 * parametro colocado para trazer a lista de bancos para update
//		 */
//		contaBancariaGatewayDePagamentoDto.setListaDeBancos(BancoEnum.values());
//		return contaBancariaGatewayDePagamentoDto;
//	}
}
