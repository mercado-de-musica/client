package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class InfosPessoaisController {
	
//	@Autowired
//	UsuarioFacade usuarioService;
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/informacoes_pessoais")
//	public ModelAndView infosPessoais(HttpServletRequest request ) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		usuarioService.transformacaoDeData(usuarioDto);
//		
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("usuarioDto", usuarioDto);
//		model.addObject("tiposTelefone", TipoDeTelefoneEnum.values());
//		model.addObject("estados", EstadosEnum.values());
//		if(usuarioDto instanceof UsuarioPessoaJuridicaDTO) {
//			model.addObject("h3", "Alteração de dados Pessoa Jurídica");
//			model.addObject("pagina", "area_usuario/infos_pessoais/formulario_pj");
//		} else {
//			model.addObject("h3", "Alteração de dados Pessoa Física");
//			model.addObject("pagina", "area_usuario/infos_pessoais/formulario_pf");
//		}
//		return model;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/alteracao_cadastro_usuario_pf")
//	public ModelAndView alteracaoCadastroPf(HttpServletRequest request , @ModelAttribute("usuarioDto") UsuarioPessoaFisicaDTO usuarioDto) throws Exception {
//		
//		
//		UsuarioDTO usuarioRemoteDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioRemoteDto, usuarioDto);
//		
//		usuarioService.cadastrarUsuarioPessoaFisica(usuarioDto);
//		
//		
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		model.addObject("titulo", "- Alteração de usuário");
//		model.addObject("h3", "Alteração de usuário");
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("cadastroAlteracaoTitulo", "Alteração efetuada com sucesso!");
//		model.addObject("cadastroAlteracaoFrase", "");
//		model.addObject("pagina", "area_usuario/infos_pessoais/cadastro_ok");
//		
//		return model;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/alteracao_cadastro_usuario_pj")
//	public ModelAndView alteracaoCadastroPj(HttpServletRequest request , @ModelAttribute("usuarioDto") UsuarioPessoaJuridicaDTO usuarioDto) throws Exception {
//		
//		UsuarioDTO usuarioRemoteDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioRemoteDto, usuarioDto);
//		
//		usuarioService.cadastrarUsuarioPessoaJuridica(usuarioDto);
//		
//		
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		model.addObject("titulo", "- Alteração de usuário");
//		model.addObject("h3", "Alteração de usuário");
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("cadastroAlteracaoTitulo", "Alteração efetuada com sucesso!");
//		model.addObject("cadastroAlteracaoFrase", "");
//		model.addObject("pagina", "area_usuario/infos_pessoais/cadastro_ok");
//		
//		return model;
//	}
//	
//	@RequestMapping(value = "/usuario/verificar_email")
//	@ResponseBody
//	public Boolean verificarEmail (HttpServletRequest request, String email) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		usuarioDto.setEmail(email);
//		return usuarioService.verificarEmail(usuarioDto);
//	}
	
}
