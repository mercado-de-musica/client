package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class DesistenciaDeProdutoController {
	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private CompraVendaService compraVendaService;
//	
//	@Autowired
//	private DesistenciaService desistenciaService;
//	
//	@Autowired
//	private RastreamentoService rastreamentoService;
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/desistencia_de_produto/{idProduto}")
//	public ModelAndView desistenciaDeproduto(HttpServletRequest request, @PathVariable BigInteger idProduto) throws UsuarioException, ProdutoException, RemoteException, CorreiosException, RastreamentoException, DesistenciaException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraPorProduto(infosGeraisProdutoUsuarioDto);
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, compraDto.getComprador());
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		modelAndView.addObject("pagina", "area_usuario/produtos/desistencia_de_produto");
//		modelAndView.addObject("h3", "Desistencia do produto: " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getInstrumentoAcessorio().getNome() + " " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getMarca().getNome() + 
//				" " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getProduto().getNome() + " " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getModelo().getNome());
//		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		
//		/*
//		 * verificar jsp
//		 */
//		modelAndView.addObject("desistenciaDeProdutoDto", desistenciaService.obterDesistenciaPorProduto(infosGeraisProdutoUsuarioDto));
//		
//		return modelAndView;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/desistencia_de_produto_vendedor/{idProduto}")
//	public ModelAndView desistenciaDeprodutoVendedor(HttpServletRequest request, @PathVariable BigInteger idProduto) throws UsuarioException, ProdutoException, RemoteException, CorreiosException, RastreamentoException, DesistenciaException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//																										
//		modelAndView.addObject("pagina", "area_usuario/produtos/desistencia_de_produto_vendedor");
//		modelAndView.addObject("h3", "Desistencia do produto: " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getInstrumentoAcessorio().getNome() + " " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getMarca().getNome() + 
//				" " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getProduto().getNome() + " " + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getModelo().getNome());
//		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("desistenciaProdutoDto", desistenciaService.obterDesistenciaPorProduto(infosGeraisProdutoUsuarioDto));
//		
//		return modelAndView;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value= "/usuario/cadastro_de_codigo_de_vendedor_direto_desistencia")
//	public void cadastroDeCodigoDeVendedorDiretoDesistencia(HttpServletRequest request, BigInteger idProduto, String codigoVendedor) throws CodigoDeEntregaException, UsuarioException, CompraVendaException, ContaBancariaGatewayDePagamentoException, RastreamentoException, DesistenciaException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getCompra().getComprador());
//		
//		DesistenciaProdutoDTO desistenciaDto = desistenciaService.obterDesistenciaPorProduto(infosGeraisProdutoUsuarioDto);
//		rastreamentoService.inserirCodigoVendedorPorComprador(desistenciaDto.getRastreamento(), codigoVendedor);
//		
//		desistenciaDto.getRastreamento().setFinalizado(Boolean.TRUE);
//		produtoService.retirarCompraDoProduto(desistenciaDto.getRastreamento().getProduto());
//		compraVendaService.removerCompraSemProduto(desistenciaDto.getRastreamento().getProduto().getCompra());
//		
//		desistenciaService.finalizarDesistencia(desistenciaDto);
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value= "/usuario/cadastro_de_codigo_de_comprador_direto_desistencia")
//	public void cadastroDeCodigoDeCompradorDiretoDesistencia(HttpServletRequest request, BigInteger idProduto, String codigoComprador) throws CodigoDeEntregaException, UsuarioException, CompraVendaException, ContaBancariaGatewayDePagamentoException, RastreamentoException, DesistenciaException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		DesistenciaProdutoDTO desistenciaDto = desistenciaService.obterDesistenciaPorProduto(infosGeraisProdutoUsuarioDto);
//		rastreamentoService.inserirCodigoCompradorPorVendedor(desistenciaDto.getRastreamento(), codigoComprador);
//	}
}
