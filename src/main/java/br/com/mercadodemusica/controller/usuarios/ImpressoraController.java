package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class ImpressoraController {
	
//	@Autowired
//	private PDFService pdfService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private RastreamentoService rastreamentoService;
//
//	@Autowired
//	private ContratoCorreiosService contratoCorreiosService;
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/gerar_codigo_em_PDF")
//	@ResponseBody
//	public String enviarParaImpressao(String codigo, BigInteger idProduto, String tipoDeComercianteDoProduto) throws JRException, FileNotFoundException {
//		
//		
//		return pdfService.gerarCodigoEmPDF(codigo, idProduto, tipoDeComercianteDoProduto);
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/excluir_PDF")
//	public void excluirPDF(String path) {
//		pdfService.excluirPDF(path);
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/imprimir_chancela_correios")
//	@ResponseBody
//	public String imprimirChancelaCorreios(HttpServletRequest request, BigInteger idProduto, Boolean desistencia) throws RastreamentoException, EnderecoException, ConfigurationException, BarcodeException, IOException, JRException, CorreiosException, NumberFormatException, DesistenciaException {
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		RastreamentoDTO rastreamentoDto = rastreamentoService.obterRastreamentoPorProduto(infosGeraisProdutoUsuarioDto, desistencia);
//		
//		/*
//		 * sempre eh retornado um rastreamento por causa do metodo de cima
//		 */
//		rastreamentoDto = rastreamentoService.verificarRastreamentoVencidoCorreios(rastreamentoDto, infosGeraisProdutoUsuarioDto);
//	
//		rastreamentoService.inserirRastreamentoDeProdutoPorRastreamento(rastreamentoDto);
//	
//		ContratoCorreiosDTO contratoCorreios = contratoCorreiosService.obterInfosContratoCorreios();
//		
//		
//		String codigoBarcodePostNet = pdfService.gerarCodigoBarcodePostNet(rastreamentoDto, desistencia);
//		String chancelaPostNet = pdfService.gerarChancelaPostNet(rastreamentoDto, codigoBarcodePostNet);
//		
////		String codigoDeBarras = pdfService.gerarCodigoDeBarras(rastreamentoProdutoCorreiosDto, contratoCorreios);
//		String chancelaCodigoDeBarras = pdfService.gerarChancelaCodigoDeBarras(rastreamentoDto);
//		
//		String codigoBarcodeDatamatrix = pdfService.gerarCodigoBarcodeDatamatrix(rastreamentoDto, contratoCorreios);
//		String chancelaDatamatrix = pdfService.gerarChancelaQrCode(rastreamentoDto, codigoBarcodeDatamatrix);
//		
//		/*
//		 * usado o mesmo codigo do postnet mas apenas para fazer o código de barras do cep
//		 */
//		String chancelaCodigoDeBarrasCEP = pdfService.gerarChancelaCodigoDeBarrasCEP(rastreamentoDto, codigoBarcodePostNet);
//		
//		
//		String impressaoCorreios = pdfService.gerarImpressaoCorreios(rastreamentoDto, contratoCorreios, chancelaPostNet, chancelaDatamatrix, chancelaCodigoDeBarras, chancelaCodigoDeBarrasCEP);
//		
//		return impressaoCorreios;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/excluir_chancela_correios")
//	public void excluirChancelaCorreios(String path) {
//		pdfService.excluirChancelaCorreios(path);
//	}
}
