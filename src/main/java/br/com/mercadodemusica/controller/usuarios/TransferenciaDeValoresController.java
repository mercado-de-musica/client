package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class TransferenciaDeValoresController {
	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private CompraVendaService compraVendaService;
//	
//	@Autowired
//	private TransferenciaDeValoresService transferenciaDeValoresService;
//	
//	@Autowired
//	private PesquisaProdutoService pesquisaProdutoService;
//	
//	@Autowired
//	private DesistenciaService desistenciaService;
//
//	@Autowired
//	private RastreamentoService rastreamentoService;
//	
//	/*
//	 * liberacao de produtos para transferencia
//	 */
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/verificar_conta_bancaria_de_insercao")
//	@ResponseBody
//	public List<ContaBancariaDTO> verificarContaBancariaDeInsercao(HttpServletRequest request) throws UsuarioException, ContaBancariaGatewayDePagamentoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		List<ContaBancariaDTO> listaContaBancariaGatewayDePagamentoDto = transferenciaDeValoresService.verificarContaBancariaTransferenciaDeValores(usuarioDto);
//		
//		return listaContaBancariaGatewayDePagamentoDto;
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/produtos_liberados_para_pagamento")
//	public ModelAndView produtosLiberadosParaPagamento(HttpServletRequest request ) throws AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, RegraDeNegocioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//	
//		
//		/*
//		 * para produtos que podem ser inseridos na transferencia
//		 */
//		
//		List<ProdutoDTO> listaDeProdutosInativosDto = pesquisaProdutoService.pesquisaDeProdutosInativosPorUsuario(usuarioDto);
//		List<CompraDTO> listaDeComprasFinalizadasDto = compraVendaService.verificarProdutosComComprasFinalizadas(listaDeProdutosInativosDto);
//		
//		listaDeComprasFinalizadasDto = desistenciaService.retirarProdutosComDesistenciaEmAberto(listaDeComprasFinalizadasDto);
//		
//		List<ProdutoDTO> listaDeProdutosFinalizados = transferenciaDeValoresService.verificarComprasFinalizadasComTransferencias(listaDeComprasFinalizadasDto);
//		
//
////		for maldito, tive que fazer no controller pra nao ter que abrir outro metodo
//		for(ProdutoDTO produtoFinalizado : listaDeProdutosFinalizados) {
//			RastreamentoDTO rastreamentoDto = rastreamentoService.obterRastreamentoPorProduto(produtoFinalizado, Boolean.FALSE);
//			produtoFinalizado.setRastreamento(rastreamentoDto);
//		}
//		
//		rastreamentoService.verificacaoDataDeCancelamentoDeCompra(listaDeComprasFinalizadasDto);
//		
//				
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		model.addObject("h3", "Produtos liberados para pagamento");
//		model.addObject("titulo", "- Produtos liberados para pagamento");
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("listaDeProdutosLiberados", compraVendaService.obterComprasLiberadasParaPagamento(listaDeProdutosFinalizados));
//		model.addObject("pagina", "area_usuario/gateway_de_pagamento/produtos_liberados_para_pagamento");
//		model.addObject("transferenciAtiva", transferenciaDeValoresService.verificarTransferenciaAtiva(usuarioDto));
//		return model;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/produtos_indisponiveis_para_pagamento")
//	public ModelAndView produtosIndisponiveisParaPagamento(HttpServletRequest request ) throws RastreamentoException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, UsuarioException, CorreiosException, DesistenciaException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		List<ProdutoDTO> listaDeProdutosInativosDto = pesquisaProdutoService.pesquisaDeProdutosInativosPorUsuario(usuarioDto);
//		List<CompraDTO> listaDeComprasFinalizadasDto = compraVendaService.verificarProdutosComComprasFinalizadas(listaDeProdutosInativosDto);
//		
//		listaDeComprasFinalizadasDto = desistenciaService.retirarProdutosComDesistenciaEmAberto(listaDeComprasFinalizadasDto);
//		
//		rastreamentoService.obterRastreamentoPorListaDeCompras(listaDeComprasFinalizadasDto, Boolean.FALSE);
//		rastreamentoService.verificacaoDataDeCancelamentoDeCompra(listaDeComprasFinalizadasDto);
//		
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		model.addObject("h3", "Produtos indisponíveis para pagamento");
//		model.addObject("titulo", "- Produtos liberados para pagamento");
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("listaDeProdutosIndisponiveis", compraVendaService.obterComprasIndisponiveisParaPagamento(listaDeComprasFinalizadasDto));
//		model.addObject("pagina", "area_usuario/gateway_de_pagamento/produtos_indisponiveis_para_pagamento");
//		return model;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/produtos_pagos")
//	public ModelAndView produtosPagos(HttpServletRequest request ) throws RastreamentoException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//			
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		model.addObject("h3", "Produtos indisponíveis para pagamento");
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("hashProdutosPagosDto", transferenciaDeValoresService.obterProdutosPagos(usuarioDto));
//		model.addObject("pagina", "area_usuario/gateway_de_pagamento/produtos_pagos");
//		return model;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/incluir_carrinho_transferencia_de_valores")
//	public void incluirCarrinho(HttpServletRequest request, BigInteger idProduto, BigInteger idContaTransferencia, Boolean vendedor) throws UsuarioException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException, TransferenciaValoresUsuarioException {
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		ContaBancariaDTO contaBancariaGatewayDePagamentoDto = new ContaBancariaDTO();
//		contaBancariaGatewayDePagamentoDto.setId(idContaTransferencia);
//		
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		if(vendedor) {
//			VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//			
//			RastreamentoDTO rastreamentoDto = rastreamentoService.obterRastreamentoPorProduto(infosGeraisProdutoUsuarioDto, Boolean.FALSE);
//			transferenciaDeValoresService.inserirProdutoVendidoNaTransferenciaDeValores(rastreamentoDto, contaBancariaGatewayDePagamentoDto, Boolean.FALSE);
//		} else {
////			desistencia
//			RastreamentoDTO rastreamentoDto = rastreamentoService.obterRastreamentoPorProduto(infosGeraisProdutoUsuarioDto, Boolean.TRUE);
//			VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, rastreamentoDto.getComprador());
//			
//			transferenciaDeValoresService.inserirProdutoVendidoNaTransferenciaDeValores(rastreamentoDto, contaBancariaGatewayDePagamentoDto, Boolean.TRUE);
//		}
//		
//		
//	}
//	
//	
////	@Secured({"ROLE_ADMIN","ROLE_USER"})
////	@RequestMapping(value = "/usuario/incluir_carrinho_transferencia_de_valores_desistencia")
////	public void incluirCarrinhoDesistencia(HttpServletRequest request, BigInteger idProduto, BigInteger idContaTransferencia) throws UsuarioException, DesistenciaException, RastreamentoException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException {
////		
////		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
////		infosGeraisProdutoUsuarioDto.setId(idProduto);
////		
////		ContaBancariaGatewayDePagamentoDTO contaBancariaGatewayDePagamentoDto = new ContaBancariaGatewayDePagamentoDTO();
////		contaBancariaGatewayDePagamentoDto.setId(idContaTransferencia);
////		
////		
////		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
////		
////		DesistenciaProdutoDTO desistenciaDeProdutoDto = desistenciaService.obterDesistenciaPorProduto(infosGeraisProdutoUsuarioDto);
////		
////		UsuarioDTO usuarioDesistenciaDto = desistenciaService.obterUsuarioDeDesistencias(desistenciaDeProdutoDto);
////		
////		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, usuarioDesistenciaDto);
////		
////		transferenciaDeValoresService.inserirProdutoVendidoNaTransferenciaDeValores(infosGeraisProdutoUsuarioDto, contaBancariaGatewayDePagamentoDto);
////	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/excluir_carrinho_transferencia_de_valores")
//	public void excluirCarrinho(HttpServletRequest request, BigInteger idProduto, Boolean vendedor) throws UsuarioException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException {
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		if(vendedor) {
//			VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//			transferenciaDeValoresService.atualizarProdutoVendidoNaTransferenciaDeValores(infosGeraisProdutoUsuarioDto, Boolean.FALSE);
//			transferenciaDeValoresService.excluirProdutoVendidoNaTransferenciaDeValores(infosGeraisProdutoUsuarioDto, Boolean.FALSE);
//		} else {
//			RastreamentoDTO rastreamentoDto = rastreamentoService.obterRastreamentoPorProduto(infosGeraisProdutoUsuarioDto, Boolean.TRUE);
//			VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, rastreamentoDto.getComprador());
//			transferenciaDeValoresService.atualizarProdutoVendidoNaTransferenciaDeValores(infosGeraisProdutoUsuarioDto, Boolean.TRUE);
//			transferenciaDeValoresService.excluirProdutoVendidoNaTransferenciaDeValores(infosGeraisProdutoUsuarioDto, Boolean.TRUE);
//		}
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/fechar_transferencia_de_valores")
//	public void fecharTransferenciaDeValores(HttpServletRequest request, Boolean desistencia) throws UsuarioException, TransferenciaValoresUsuarioException, IOException, GatewayDePagamentoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		transferenciaDeValoresService.fecharTransferenciaDeValores(usuarioDto, desistencia);
//	
//	}
//	
//	
////	@Secured({"ROLE_ADMIN","ROLE_USER"})
////	@RequestMapping(value = "/usuario/fechar_transferencia_de_valores_desistencia")
////	public void fecharTransferenciaDeValoresDesistencia(HttpServletRequest request, BigInteger idTransferencia, BigInteger idContaTransferencia) throws UsuarioException, TransferenciaValoresUsuarioException, GatewayDePagamentoException, CompraVendaException, IOException {
////		
////		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
////		
////		TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto = new TransferenciaValoresUsuarioDTO();
////		transferenciaValoresUsuarioDto.setId(idTransferencia);
////		transferenciaValoresUsuarioDto = transferenciaDeValoresService.obterPorTransferencia(transferenciaValoresUsuarioDto);
////		
////		ContaBancariaGatewayDePagamentoDTO contaBancariaGatewayDePagamentoDto = new ContaBancariaGatewayDePagamentoDTO();
////		contaBancariaGatewayDePagamentoDto.setId(idContaTransferencia);
////		
////		try {
////			contaBancariaGatewayDePagamentoDto = gatewayDePagamentoService.obterContaBancariaGatewayDePagamento(contaBancariaGatewayDePagamentoDto);
////		} catch (GatewayDePagamentoException e) {
////			contaBancariaGatewayDePagamentoDto = null;
////		}
////		
////		transferenciaValoresUsuarioDto.setContaUsadaNaTransferencia(contaBancariaGatewayDePagamentoDto);
////		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, transferenciaValoresUsuarioDto.getUsuario());
////		
////		transferenciaDeValoresService.fecharTransferenciaDeValoresDesistencia(transferenciaValoresUsuarioDto);
////		
////	}
//		
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/produtos_desistidos")
//	public ModelAndView produtosDesistidos(HttpServletRequest request ) throws UsuarioException  {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//				
//		
//		List<DesistenciaProdutoDTO> listaDeDesistenciasDto = desistenciaService.obterDesistenciasEmAbertoPorUsuario(usuarioDto);
//		
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		model.addObject("h3", "Produtos desistidos liberados para retorno");
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("listaDeDesistencias", transferenciaDeValoresService.obterDesistenciasSemListaDeTransferencias(listaDeDesistenciasDto));
//		model.addObject("pagina", "area_usuario/gateway_de_pagamento/produtos_desistidos");
//		model.addObject("transferenciAtiva", transferenciaDeValoresService.verificarTransferenciaAtiva(usuarioDto));
//		
//		return model;
//	}
	
}
