package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class CadastroDeProdutosParaVendaController {
	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private PesquisaProdutoService pesquisaProdutoService;
//	
//	@Autowired
//	private GatewayDePagamentoService gatewayDePagamentoService;
//	
//	@Autowired
//	private InteresseDeCompraService interesseDeCompraService;
//	
//	@Autowired
//	private PrecoPrazoCorreiosService precoPrazoCorreiosService;
//	
//	@Autowired
//	private RastreamentoService rastreamentoService;
//		
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/produtos_venda")
//	public ModelAndView produtosVenda(HttpServletRequest request, Model model) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		model.addAttribute("infosGeraisProdutoUsuarioDto", new ProdutoDTO());
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		modelAndView.addObject("titulo", "- Produtos para venda");
//		modelAndView.addObject("h3", "Produtos para venda");
//		modelAndView.addObject("listaDeProdutosParaVenda", pesquisaProdutoService.obterProdutosParaVenda(usuarioDto));
//		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		modelAndView.addObject("listaDeEnvioEnum", TipoEnvioEnum.values());
//		modelAndView.addObject("paises", PaisesDeFabricacaoEnum.values());
//		modelAndView.addObject("pagina", "area_usuario/produtos_para_venda/cadastro_de_produtos_para_venda");
//		
//		return modelAndView;
//	}
//	
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/cadastro_de_fotos", method = {RequestMethod.POST, RequestMethod.GET})
//	public ModelAndView cadastroAlteracaoEfetuada(HttpServletRequest request, HttpServletResponse response , @ModelAttribute("usuarioDto") ProdutoDTO infosGeraisProdutoUsuarioDto,  BindingResult result) throws RegraDeNegocioException, IllegalArgumentException, IllegalAccessException, JSONException, IOException, ProdutoException {
//		
//		response.addHeader("Pragma", "no-cache");
//		response.addHeader("Cache", "no-cache");
//		response.addHeader("Cache-Control", "no-cache, must-revalidate");
//		response.addHeader("Expires", Calendar.getInstance().getTime().toString());
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		/*
//		 * verificacao de usuarios
//		 */
//		UsuarioDTO usuarioProdutoJaCadastrado = produtoService.verificarUsuarioPorProduto(usuarioDto, infosGeraisProdutoUsuarioDto);
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, usuarioProdutoJaCadastrado);
//		
//		infosGeraisProdutoUsuarioDto.setUsuario(usuarioDto);
//		
//		CustoGatewayPagamentoDTO custoGatewayPagamentoDto = gatewayDePagamentoService.obterPorcentagemGatewayDePagamentoValorAVista();
//		
//		BigInteger idInfosGerais = produtoService.cadastrarProdutoParaVenda(infosGeraisProdutoUsuarioDto, custoGatewayPagamentoDto);
//		infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idInfosGerais);
//		
//		
//		List<InteressesDeCompraProdutoDTO> listaDeInteressadosDto = interesseDeCompraService.updateDeUsuariosInteressados(infosGeraisProdutoUsuarioDto);
//		
//		
//		
//		/*
//		 * metodo é usado para que no cadastro de um novo produto, as pessoas interessadas recebam e-mail
//		 */
//		listaDeInteressadosDto = precoPrazoCorreiosService.verificarDistanciaEntreUsuarios(listaDeInteressadosDto, usuarioDto, request.getMethod(), infosGeraisProdutoUsuarioDto);
//			
//		
//		/*
//		 * quando o metodo é get ele entra neste update nao pula o for
//		 */
//		interesseDeCompraService.updateDeInteresseDeCompraProduto(listaDeInteressadosDto);
//		
//		/*
//		 * as infos gerais retornam sem o usuario, então é setado o objeto novamente
//		 */
//		infosGeraisProdutoUsuarioDto.setUsuario(usuarioDto);
//		
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		model.addObject("titulo", "- Cadastro de Fotos");
//		model.addObject("h3", "Cadastro de fotos");
//		model.addObject("infosGeraisProdutoUsuarioDto", infosGeraisProdutoUsuarioDto);
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("listaDeApresentacao", produtoService.obterApresentacaoPorProduto(infosGeraisProdutoUsuarioDto));
//		
//		model.addObject("pagina", "area_usuario/produtos_para_venda/cadastro_de_fotos");
//		
//		return model;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/subida_de_arquivos")
//	public void subidaDeArquivos(HttpServletRequest request) throws FileUploadException, IOException, RegraDeNegocioException, ProdutoException {
//			
//		String idProdutoDtoString = request.getParameter("infosGeraisProdutoUsuarioDto");
//		BigInteger idProdutoDtoBigInteger = new BigInteger(idProdutoDtoString);
//		
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioVerificacaoDto = produtoService.obterProdutoPorId(idProdutoDtoBigInteger);
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioVerificacaoDto.getUsuario());
//		
//		
//		List<FileItem> itens = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
//		produtoService.cadastroDeApresentacaoProduto(idProdutoDtoBigInteger, itens);
//		
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/ajuste_de_imagens_guillotine/{infosGeraisProdutoUsuarioDtoId}")
//	@ResponseBody
//	public ModelAndView ajusteDeImagens(HttpServletRequest request, @PathVariable BigInteger infosGeraisProdutoUsuarioDtoId) throws UsuarioException, IOException, ProdutoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioVerificacaoDto = produtoService.obterProdutoPorId(infosGeraisProdutoUsuarioDtoId);
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioVerificacaoDto.getUsuario());
//		
//		List<ApresentacaoProdutoDTO> retornoApresentacao = produtoService.obterApresentacaoPorProduto(infosGeraisProdutoUsuarioVerificacaoDto);
//		
//		
//		ModelAndView model = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		model.addObject("titulo", "- Ajuste de Fotos");
//		model.addObject("h3", "Ajuste de fotos");
//		model.addObject("infosGeraisProdutoUsuarioDto", infosGeraisProdutoUsuarioVerificacaoDto);
//		
//		model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		model.addObject("pagina", "area_usuario/produtos_para_venda/ajuste_de_imagens");
//		
//		model.addObject("listaDeApresentacoesDto", produtoService.obterApresentacoesNaoAjustadas(retornoApresentacao));
//		
//		return model;
//	}
//	
//	
////	@Secured({"ROLE_ADMIN","ROLE_USER"})
////	@RequestMapping(value = "/usuario/obter_apresentacoes_apos_upload")
////	@ResponseBody
////	public List<ApresentacaoProdutoDTO> obterApresentacoesAposUpload(HttpServletRequest request, BigInteger infosGeraisProdutoUsuarioDtoId) throws UsuarioException, IOException, ProdutoException {
////		
////		
////		
////	}
//	
//		
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/inserir_apresentacao_video")
//	@ResponseBody
//	public List<ApresentacaoProdutoDTO> inserirApresentacaoVideo(HttpServletRequest request, BigInteger idProduto, String link) throws FileUploadException, IOException, RegraDeNegocioException, ProdutoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		
//		
//		produtoService.cadastroDeVideoProduto(idProduto, link);
//		
//		List<ApresentacaoProdutoDTO> retornoApresentacao = produtoService.obterApresentacaoPorProduto(infosGeraisProdutoUsuarioDto);
//		
//		return produtoService.obterApresentacoesNaoAjustadas(retornoApresentacao);
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/redimensionamento_de_arquivos")
//	public void redimensionamentoDeArquivos(HttpServletRequest request, BigInteger id, String endereco, String nomeDoArquivo, BigDecimal scale, Integer angle, BigInteger x, BigInteger y, Integer width, Integer height) throws FileUploadException, IOException, RegraDeNegocioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ApresentacaoProdutoDTO apresentacaoProdutoDto = new ApresentacaoProdutoDTO();
//
//		apresentacaoProdutoDto.setId(id);
//		apresentacaoProdutoDto.setEndereco(endereco);
//		apresentacaoProdutoDto.setNomeDoArquivo(nomeDoArquivo);
//		apresentacaoProdutoDto.setScale(scale);
//		apresentacaoProdutoDto.setAngle(BigInteger.valueOf(angle));
//		apresentacaoProdutoDto.setX(x);
//		apresentacaoProdutoDto.setY(y);
//		apresentacaoProdutoDto.setWidth(BigInteger.valueOf(width));
//		apresentacaoProdutoDto.setHeight(BigInteger.valueOf(height));
//		
//		ApresentacaoProdutoDTO apresentacaoVerificacaoUsuarioDto = produtoService.obterApresentacaoCompletaPorApresentacao(apresentacaoProdutoDto);
//		
//		
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, apresentacaoVerificacaoUsuarioDto.getProduto().getUsuario());
//		
//		produtoService.redimensionamentoDeFoto(apresentacaoProdutoDto);
//		apresentacaoProdutoDto = produtoService.obterApresentacaoCompletaPorApresentacao(apresentacaoProdutoDto);		
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/excluir_produto")
//	public void exclusaoDeProduto(HttpServletRequest request, String idProduto) throws FotosException, UsuarioException, ProdutoException, DesistenciaException, RastreamentoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//				
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(new BigInteger(idProduto));
//		
//		infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(infosGeraisProdutoUsuarioDto.getId());
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		infosGeraisProdutoUsuarioDto.setUsuario(usuarioDto);
//		
//		
//		infosGeraisProdutoUsuarioDto.setCompra(null);
//		
//		rastreamentoService.deletarRastreamentoPorProduto(infosGeraisProdutoUsuarioDto);
//		
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//				
//		produtoService.excluirProduto(infosGeraisProdutoUsuarioDto);
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/buscar_produto_cadastrado")
//	@ResponseBody
//	public ProdutoDTO buscarProdutoCadastrado(HttpServletRequest request, BigInteger idProduto) throws ProdutoException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		return infosGeraisProdutoUsuarioDto;
//				
//	}
//
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/excluir_apresentacao")
//	public void exclusaoDeApresentacao(HttpServletRequest request, BigInteger idApresentacao) throws FotosException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ApresentacaoProdutoDTO apresentacaoDoProduto = new ApresentacaoProdutoDTO();
//		
//		apresentacaoDoProduto.setId(idApresentacao);
//		apresentacaoDoProduto = produtoService.obterApresentacaoCompletaPorApresentacao(apresentacaoDoProduto);
//		
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, apresentacaoDoProduto.getProduto().getUsuario());
//		
//		produtoService.excluirApresentacao(apresentacaoDoProduto);
//				
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/redirecionamento_pagina_fotos/{idProduto}")
//	public ModelAndView redirecionamentoPaginaDeFotos(HttpServletRequest request, @PathVariable BigInteger idProduto, RedirectAttributes redirectAttributes) throws FotosException, ServletException, IOException, UsuarioException, ProdutoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		
//		redirectAttributes.addFlashAttribute("usuarioDto", infosGeraisProdutoUsuarioDto);
//		
//		return new ModelAndView("redirect:/usuario/cadastro_de_fotos");
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/redirecionamento_pagina_apresentacao/{idApresentacao}")
//	public ModelAndView redirecionamentoPaginaApresentacao(HttpServletRequest request, @PathVariable BigInteger idApresentacao, RedirectAttributes redirectAttributes) throws FotosException, ServletException, IOException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		
//		ApresentacaoProdutoDTO apresentacaoProdutoDto = new ApresentacaoProdutoDTO();
//		apresentacaoProdutoDto.setId(idApresentacao);
//		
//		apresentacaoProdutoDto = produtoService.obterApresentacaoCompletaPorApresentacao(apresentacaoProdutoDto);
//		
//		
//		/*
//		 * verificacao de usuarios
//		 */
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, apresentacaoProdutoDto.getProduto().getUsuario());
//		
//		redirectAttributes.addFlashAttribute("usuarioDto", apresentacaoProdutoDto.getProduto());
//		
//		return new ModelAndView("redirect:/usuario/cadastro_de_fotos");
//		
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/enviar_nova_categoria_de_produto")
//	public void enviarNovaCategoriaDeProduto(HttpServletRequest request, String nomeNovoProduto) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		SugestaoNovoProdutoDTO sugestaoNovoProdutoDto = new SugestaoNovoProdutoDTO();
//		sugestaoNovoProdutoDto.setSugestao(nomeNovoProduto);
//		sugestaoNovoProdutoDto.setUsuario(usuarioDto);
//		
//		produtoService.inserirSugestaoDeNovoProduto(sugestaoNovoProdutoDto);
//		
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/obter_paises")
//	@ResponseBody
//	public PaisesDeFabricacaoEnum[] obterPaises() throws UsuarioException {
//		
//		return PaisesDeFabricacaoEnum.values();
//		
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/obter_tipo_envio")
//	@ResponseBody
//	public TipoEnvioEnum[] obterTipoDeEnvio() throws UsuarioException {
//		
//		return TipoEnvioEnum.values();
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/verificar_vendedor_produto")
//	public void verificarVendedorProduto(HttpServletRequest request, BigInteger idProduto) throws UsuarioException, ProdutoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		if(!infosGeraisProdutoUsuarioDto.getAtivo()) {
//			throw new ProdutoException("Produto inativo");
//		}
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value = "/usuario/mudar_capa")
//	public void mudarCapa(HttpServletRequest request, BigInteger infosGeraisProdutoUsuarioId, BigInteger apresentacaoId) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(infosGeraisProdutoUsuarioId);
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		produtoService.mudarCapaApresentacao(infosGeraisProdutoUsuarioDto, apresentacaoId);
//	}
	
}
