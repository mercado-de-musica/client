package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class ProdutosCompradosController {
	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private CompraVendaService compraVendaService;
//	
//	@Autowired
//	private DesistenciaService desistenciaService;
//		
//	@Autowired
//	private RastreamentoService rastreamentoService;
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/produtos_comprados")
//	public ModelAndView produtosComprados(HttpServletRequest request) throws RastreamentoException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, UsuarioException, CorreiosException, DesistenciaException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		
//		List<CompraDTO> listaDeComprasFinalizadas = compraVendaService.obterListaDeComprasFinalizadasPorUsuarioComprador(usuarioDto);
//		/*
//		 * Se existir entrega em mão, será colocado dentro do atributo produtoComprado->rastreamentoProdutoDireto dos DTOS 
//		 */
//		
//		
//		rastreamentoService.obterRastreamentoPorListaDeCompras(listaDeComprasFinalizadas, Boolean.FALSE);
//		rastreamentoService.verificacaoDataDeCancelamentoDeCompra(listaDeComprasFinalizadas);
//		
//		produtoService.incluirInformacoesDoProdutoParaApresentacao(listaDeComprasFinalizadas);
//		desistenciaService.verificarSeExisteDesistencia(listaDeComprasFinalizadas);
//		
//		usuarioService.obterReputacaoVendedorProduto(listaDeComprasFinalizadas);
//		
//		modelAndView.addObject("pagina", "area_usuario/produtos/produtos_comprados");
//		modelAndView.addObject("h3", "Produtos comprados");
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("listaDeCompras", listaDeComprasFinalizadas);
//		
//		
//		return modelAndView;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value= "/usuario/verificar_codigo_vendedor_inserido")
//	@ResponseBody
//	public ProdutoDTO verificarCodigoVendedorInserido(HttpServletRequest request, BigInteger idProduto) throws UsuarioException, RastreamentoException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, ProdutoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraPorProduto(infosGeraisProdutoUsuarioDto);
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, compraDto.getComprador());
//		
//		List<CompraDTO> listaDeComprasFinalizadas = new ArrayList<CompraDTO>();
//		listaDeComprasFinalizadas.add(compraDto);
//		
//		rastreamentoService.obterRastreamentoPorListaDeCompras(listaDeComprasFinalizadas, Boolean.FALSE);
//		infosGeraisProdutoUsuarioDto = produtoService.escolherProdutoDaCompra(listaDeComprasFinalizadas.get(0), infosGeraisProdutoUsuarioDto);
//		
//		return rastreamentoService.verificacaoDeDatasDePostagensDeCodigos(infosGeraisProdutoUsuarioDto);
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value= "/usuario/cadastro_de_codigo_de_vendedor_direto")
//	public void cadastroDeCodigoDeVendedorDireto(HttpServletRequest request, BigInteger idProduto, String codigoVendedor) throws CodigoDeEntregaException, UsuarioException, CompraVendaException, RastreamentoException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getCompra().getComprador());
//		
//		RastreamentoDTO rastreamentoDto = rastreamentoService.obterRastreamentoPorProduto(infosGeraisProdutoUsuarioDto, Boolean.FALSE);
//		rastreamentoService.inserirCodigoVendedorPorComprador(rastreamentoDto, codigoVendedor);
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/incluir_reputacao_vendedor")
//	public void incluirReputacaoVendedor(HttpServletRequest request, Integer nota, String mensagem, BigInteger idProduto) throws CompraVendaException, ReputacaoVendedorException, UsuarioException, ProdutoException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getCompra().getComprador());
//		
//		ReputacaoVendedorDTO reputacaoVendedorDto = new ReputacaoVendedorDTO();
//		reputacaoVendedorDto.setIdProduto(idProduto);
//		reputacaoVendedorDto.setUsuarioComprador(usuarioDto);
//		reputacaoVendedorDto.setUsuarioVendedor(infosGeraisProdutoUsuarioDto.getUsuario());
//		reputacaoVendedorDto.setMensagem(mensagem);
//		reputacaoVendedorDto.setNota(nota);
//		
//		usuarioService.cadastrarReputacaoDoVendedor(reputacaoVendedorDto);
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/confirmar_desistencia")
//	@ResponseBody
//	public String confirmarDesistencia(HttpServletRequest request, BigInteger idProduto) throws UsuarioException, ProdutoException, RastreamentoException, NumberFormatException, GatewayDePagamentoException, IOException, DesistenciaException, JSONException, CodigoDeEntregaException, MessagingException, CorreiosException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraPorProduto(infosGeraisProdutoUsuarioDto);
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, compraDto.getComprador());
//		
//		List<CompraDTO> listaDeComprasFinalizadas = new ArrayList<CompraDTO>();
//		listaDeComprasFinalizadas.add(compraDto);
//		
//		rastreamentoService.obterRastreamentoPorListaDeCompras(listaDeComprasFinalizadas, Boolean.FALSE);
//		
//		infosGeraisProdutoUsuarioDto = produtoService.escolherProdutoDaCompra(listaDeComprasFinalizadas.get(0), infosGeraisProdutoUsuarioDto);
//		
//		
//		/*
//		 * verificacao se o produto já foi pago, não não tenha sido cancelamos o pagamento e finalizamos a desistencia
//		 */
//		
//		String statusPedido = compraVendaService.verificacaoGatewayDePagamentoMudancaStatus(compraDto);
//				
//		return desistenciaService.incluirDesistenciaDeCompra(infosGeraisProdutoUsuarioDto, statusPedido);
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/verificar_rastreamento_correios")
//	@ResponseBody
//	public ProdutoDTO verificarRastreamentoCorreios(HttpServletRequest request, BigInteger idProduto) throws UsuarioException, ProdutoException, NumberFormatException, GatewayDePagamentoException, IOException{
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		ProdutoDTO infosGeraisProdutoUsuarioDto = produtoService.obterProdutoPorId(idProduto);
//		
//		CompraDTO compraDto = compraVendaService.obterCompraPorProduto(infosGeraisProdutoUsuarioDto);
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, compraDto.getComprador());
//		
//		List<CompraDTO> listaDeComprasFinalizadas = new ArrayList<CompraDTO>();
//		listaDeComprasFinalizadas.add(compraDto);
//		
//		rastreamentoService.obterRastreamentoPorListaDeCompras(listaDeComprasFinalizadas, Boolean.FALSE);
//		rastreamentoService.verificacaoDataDeCancelamentoDeCompra(listaDeComprasFinalizadas);
//				
//		return produtoService.escolherProdutoDaCompra(listaDeComprasFinalizadas.get(0), infosGeraisProdutoUsuarioDto);	
//		
//	}
}
