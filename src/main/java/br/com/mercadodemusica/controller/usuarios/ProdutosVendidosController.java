package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;

@Controller
public class ProdutosVendidosController {

//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
//	@Autowired
//	private CompraVendaService compraVendaService;
//	
//	@Autowired
//	private PesquisaProdutoService pesquisaProdutoService;
//	
//	@Autowired
//	private DesistenciaService desistenciaService;
//	
//	@Autowired
//	private RastreamentoService rastreamentoService;
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/produtos_vendidos")
//	public ModelAndView produtosVendidos(HttpServletRequest request) throws RastreamentoException, UsuarioException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException, DesistenciaException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		/* 
//		 *	fazemos a pesquisa de produtos inativos para depois verificar qual deles estão com status autorizado e qual deles tem compra. 
//		 */
//		
//		List<ProdutoDTO> listaDeProdutosInativosDto = pesquisaProdutoService.pesquisaDeProdutosInativosPorUsuario(usuarioDto);
//		List<CompraDTO> listaDeComprasFinalizadasDto = compraVendaService.verificarProdutosComComprasFinalizadas(listaDeProdutosInativosDto);
//		
//		desistenciaService.verificarSeExisteDesistencia(listaDeComprasFinalizadasDto);
//		
//		rastreamentoService.obterRastreamentoPorListaDeCompras(listaDeComprasFinalizadasDto, Boolean.FALSE);
//		
//
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		produtoService.incluirInformacoesDoProdutoParaApresentacao(listaDeComprasFinalizadasDto);
//		
//		modelAndView.addObject("pagina", "area_usuario/produtos/produtos_vendidos");
//		modelAndView.addObject("h3", "Produtos vendidos");
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("listaDeCompras", listaDeComprasFinalizadasDto);
//		
//		return modelAndView;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value= "/usuario/cadastro_de_codigo_de_comprador_direto")
//	public void cadastroDeCodigoDeVendedorDireto(HttpServletRequest request, BigInteger idProduto, String codigoComprador) throws CodigoDeEntregaException, UsuarioException, RastreamentoException, AutenticacaoException, SigepClienteException, NumberFormatException, RemoteException, CorreiosException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		infosGeraisProdutoUsuarioDto = produtoService.obterProduto(infosGeraisProdutoUsuarioDto);
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, infosGeraisProdutoUsuarioDto.getUsuario());
//		
//		RastreamentoDTO rastreamentoDto = rastreamentoService.obterRastreamentoPorProduto(infosGeraisProdutoUsuarioDto, Boolean.FALSE);
//		
//		rastreamentoService.inserirCodigoCompradorPorVendedor(rastreamentoDto, codigoComprador);
//	}
}
