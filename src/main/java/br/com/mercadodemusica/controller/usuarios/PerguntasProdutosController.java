package br.com.mercadodemusica.controller.usuarios;

import org.springframework.stereotype.Controller;


/*
 * não tem como colocar verificacao de usuario pois os métodos são bi-direcionais, 
 * servem para o vendedor do produto e comprador
 * 
 * 
 */

@Controller
public class PerguntasProdutosController {
	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private PerguntasRespostasService perguntasRespostasService;
//	
//	@Autowired
//	private PesquisaProdutoService pesquisaProdutoService;
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/perguntas_usuarios")
//	public ModelAndView listaDePerguntasParaMeusProdutos(HttpServletRequest request) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		
//		List<ProdutoDTO> listaDeInfosGeraisRetorno = pesquisaProdutoService.pesquisarProdutoAVendaPorUsuario(usuarioDto);
//
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("usuarioDto", usuarioDto);
//		modelAndView.addObject("titulo", "Pergunta feitas para o seu produto");
//		modelAndView.addObject("h3", "Pergunta feitas para o seu produto");
//		modelAndView.addObject("pagina", "area_usuario/perguntas_respostas/lista_de_perguntas");
//		modelAndView.addObject("listaDePerguntasRepostas", perguntasRespostasService.obterPerguntasRespostasDeTodosProdutos(listaDeInfosGeraisRetorno));
//		
//		return modelAndView;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/envio_de_resposta")
//	public void envioDeResposta(HttpServletRequest request, BigInteger idPergunta, String resposta, Boolean encerraDiscussao) throws PerguntasRespostasException, UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//		PerguntasRespostasDTO perguntaRespostaDto = new PerguntasRespostasDTO();
//		
//		PerguntaCategoriaProdutoDTO perguntaDto = new PerguntaCategoriaProdutoDTO();
//		perguntaDto.setId(idPergunta);
//		perguntaDto.setIdPai(BigInteger.ZERO);
//		perguntaDto.setEncerrarDiscussao(encerraDiscussao);
//		
//		PerguntaCategoriaProdutoDTO respostaDto = new PerguntaCategoriaProdutoDTO();
//		respostaDto.setIdPai(idPergunta);
//		respostaDto.setPergunta(resposta);
//		respostaDto.setUsuarioInteressado(usuarioDto);
//		
//		ArrayList<PerguntaCategoriaProdutoDTO> listaDeRespostas = new ArrayList<PerguntaCategoriaProdutoDTO>();
//		listaDeRespostas.add(respostaDto);
//		
//		perguntaRespostaDto.setPerguntaFeita(perguntaDto);
//		perguntaRespostaDto.setRespostas(listaDeRespostas);
//		
//		perguntasRespostasService.inserirRespostaPelaPergunta(perguntaRespostaDto, usuarioDto);
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/minhas_perguntas")
//	public ModelAndView minhasPerguntas(HttpServletRequest request) throws UsuarioException {
//		
//		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuario(request.getRemoteUser());
//	
//		ModelAndView modelAndView = new ModelAndView(Pagina.AREA_USUARIO_VIEW);
//		
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		modelAndView.addObject("usuarioDto", usuarioDto);
//		modelAndView.addObject("titulo", "Minhas perguntas");
//		modelAndView.addObject("h3", "Minhas perguntas");
//		modelAndView.addObject("pagina", "area_usuario/perguntas_respostas/minhas_perguntas");
//		modelAndView.addObject("listaDeMinhasPerguntas", perguntasRespostasService.obterPerguntasFeitasPorUsuario(usuarioDto));
//		
//		return modelAndView;
//	}
//	
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping("/usuario/deixar_de_visualizar")
//	public void deixarDeVIsualizar(BigInteger idPergunta) throws PerguntasRespostasException {
//		
//		PerguntaCategoriaProdutoDTO perguntaCategoriaProdutoDto = new PerguntaCategoriaProdutoDTO();
//		perguntaCategoriaProdutoDto.setId(idPergunta);
//		
//		perguntasRespostasService.deixarDeVisualizar(perguntaCategoriaProdutoDto);
//		
//	}
}
