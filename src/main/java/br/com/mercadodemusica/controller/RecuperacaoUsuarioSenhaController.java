package br.com.mercadodemusica.controller;

import org.springframework.stereotype.Controller;

@Controller
public class RecuperacaoUsuarioSenhaController {
	
	
//	@Autowired
//	UsuarioFacade usuarioService;
//	
//	@Autowired
//	TokenService tokenService;
//	
//	@Autowired
//	TemplateEmails templateEmails;
//	
//	@Autowired
//	EnvioEmails envioEmails;
//	
//	@Autowired
//	ProdutoService produtoService;
//	
//	@RequestMapping("/recuperar_usuario_senha")
//	public ModelAndView recuperarUsuarioSenha(Model model) {
//		
//		UsuarioDTO usuarioDto = new UsuarioPessoaFisicaDTO();
//		
//		model.addAttribute("infosGeraisProdutoDto", new CategoriaProdutoDTO());
//		model.addAttribute("usuarioDto", usuarioDto);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		modelAndView.addObject("titulo", "- Recuperar senha");
//		modelAndView.addObject("h3", "Recuperar senha");
//		modelAndView.addObject("pagina", "usuario/recuperacao_usuario_senha");
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		return modelAndView;
//		
//	}
//	
//	@RequestMapping(value = "/envio_de_dados_senha", method=RequestMethod.POST)
//	public ModelAndView envioDeDados(String cpfCnpj) throws DocumentException, UsuarioException, MessagingException, DocumentosException {
//		
//		usuarioService.verificacaoCpfCnpj(cpfCnpj);
//		
//		UsuarioDTO infosUsuario = usuarioService.obterInfosUsuarioPorCpfCnpj(cpfCnpj);
//		
//		TokenRecuperacaoSenhaDTO tokenDto = tokenService.tokenRecuperacaoUsuario(infosUsuario);
//		
//		String retornoTemplate = templateEmails.recuperacaoUsuarioSenha(tokenDto);
//		
//		envioEmails.enviarRecuperacaoDeUsuarioSenha(infosUsuario, retornoTemplate);
//		
//		ModelAndView model = new ModelAndView(Pagina.PAGINA_VIEW);
//		model.addObject("titulo", "");
//		model.addObject("pagina", "usuario/recuperacao_usuario_senha_ok");
//		model.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		return model;
//		
//	}
//	
//	
//	@RequestMapping("/recuperar_usuario_senha/token/{token}")
//	public ModelAndView recuperarUsuarioSenhaTokenRecebido(@PathVariable String token, Model model) throws TokenException, UsuarioException {
//		
//		TokenRecuperacaoSenhaDTO tokenRecuperacaoDeSenha = tokenService.trazerDadosViaToken(token);
//	
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		modelAndView.addObject("titulo", "Cadastro de nova senha de " + NomePessoaJuridicaOuFisica.obterNomeDto(tokenRecuperacaoDeSenha.getUsuario()));
//		modelAndView.addObject("tokenRecuperacaoDeSenhaDto", tokenRecuperacaoDeSenha);
//		modelAndView.addObject("pagina", "usuario/cadastrar_nova_senha");
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		return modelAndView;
//		
//	}
//	
//	
//	@RequestMapping(value = "/alteracao_senha", method=RequestMethod.POST)
//	public ModelAndView alterarSenha(@ModelAttribute("tokenRecuperacaoDeSenhaModeloDto") TokenRecuperacaoSenhaDTO tokenRecuperacaoDeSenhaDto, Model model) throws Exception {
//		
//		model.addAttribute("infosGeraisProdutoDto", new CategoriaProdutoDTO());
//		
//		TokenRecuperacaoSenhaDTO tokenRecuperacaoDeSenhaDtoRecebido = tokenService.trazerDadosViaToken(tokenRecuperacaoDeSenhaDto.getToken());
//		tokenRecuperacaoDeSenhaDtoRecebido.getUsuario().setSenha(tokenRecuperacaoDeSenhaDto.getSenha());
//		
//		UsuarioDTO usuarioDto = usuarioService.setsToList(tokenRecuperacaoDeSenhaDtoRecebido.getUsuario());
//		
//		
//		tokenRecuperacaoDeSenhaDtoRecebido.setUsuario(usuarioDto);
//		
//		usuarioService.cadastrarUsuarioPessoaFisicaOuJuridica(usuarioDto);
//		
//		tokenService.cancelarToken(tokenRecuperacaoDeSenhaDtoRecebido);
//		
//		
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		modelAndView.addObject("pagina", "usuario/cadastrar_nova_senha_ok");
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		return modelAndView;
//		
//	}
}
