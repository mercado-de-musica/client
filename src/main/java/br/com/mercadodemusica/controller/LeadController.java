package br.com.mercadodemusica.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.dto.TokenLeadDTO;
import br.com.mercadodemusica.emails.EnvioEmails;
import br.com.mercadodemusica.facade.LeadFacade;
import br.com.mercadodemusica.facade.TokenLeadFacade;
import br.com.mercadodemusica.templates.TemplateEmails;

@Controller
public class LeadController {
	
	@Autowired
	private LeadFacade leadFacade;
	
	@Autowired
	private TokenLeadFacade tokenLeadFacade;
	
	@Autowired
	private TemplateEmails templateEmails;
	
	@Autowired
	private EnvioEmails envioEmails;
	
	@RequestMapping(value = "/insercaoLead", method = RequestMethod.POST)
	@ResponseBody
	public void insercaoLead(@RequestBody LeadDTO leadDto) throws MessagingException {
		leadFacade.inserirLead(leadDto);	
		leadDto = leadFacade.obterLeadPorEmail(leadDto);
		
		TokenLeadDTO tokeanLeadDto = tokenLeadFacade.criarTokenLead(leadDto);
		String template = templateEmails.tokenLeadEmail(tokeanLeadDto);
		envioEmails.enviarTokenLeadEmail(tokeanLeadDto, template);
	}
	
	
}
