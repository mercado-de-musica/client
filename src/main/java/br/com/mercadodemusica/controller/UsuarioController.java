package br.com.mercadodemusica.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UsuarioController {
	
	@RequestMapping(value = "/obterUsuarioLogado")
	@ResponseBody
	public Boolean obterUsuarioLogado(HttpServletRequest request) {
		
		if(request.getSession().getAttribute("usuarioDto") != null) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
}
