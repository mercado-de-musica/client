package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.FaleConoscoDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.emails.EnvioEmails;
import br.com.mercadodemusica.enums.ProdutosPorPaginaEnum;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.templates.TemplateEmails;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@Controller
public class FaleConoscoAjudeMelhorarController {
	
	@Autowired
	private UsuarioFacade usuarioFacade;
		
	@Autowired
	private TemplateEmails templateEmails;
	
	@Autowired
	private EnvioEmails envioEmails;
	
	@RequestMapping("/fale_conosco")
	public ModelAndView faleConosco(HttpServletRequest request) throws JSONException, ParseException, IOException, RegraDeNegocioException, URISyntaxException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
		
//		Verificacao de usuario pelo ip ou online
		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("pagina", "fale_conosco_ajude_melhorar/fale_conosco");	
		modelAndView.addObject("titulo", "- Fale conosco");
		modelAndView.addObject("h3", "Fale conosco");
		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
		return modelAndView;
	}
	
	@RequestMapping(value = "/fale_conosco_ok", method=RequestMethod.POST)
	public ModelAndView cadastroNovoUsuarioPf(HttpServletRequest request, @ModelAttribute("faleConoscoDto") FaleConoscoDTO faleConoscoDto, BindingResult result, Model model) throws Exception {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
		
//		Verificacao de usuario pelo ip ou online
		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		
		String templateFaleConosco = templateEmails.faleConosco(faleConoscoDto);

		envioEmails.faleConosco(faleConoscoDto, templateFaleConosco);
		
		modelAndView.addObject("titulo", "- Fale conosco");
		modelAndView.addObject("pagina", "fale_conosco_ajude_melhorar/fale_conosco_ok");
		modelAndView.addObject("h3", "Fale conosco");
		
		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
		
		return modelAndView;
	}
	
//	@RequestMapping("/ajude_a_melhorar")
//	public ModelAndView ajudeAMelhorar(HttpServletRequest request) throws JSONException, ParseException, IOException, RegraDeNegocioException, URISyntaxException {
//		
//		JSONObject jsonObject = new JSONObject();
//		jsonObject.put("remoteUser", request.getRemoteUser());
//		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
//		
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
//		
//		
//		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
//		modelAndView.addObject("pagina", "fale_conosco_ajude_melhorar/fale_conosco");	
//		modelAndView.addObject("titulo", "- Ajude a melhorar o Mercado de Música");
//		modelAndView.addObject("h3", "Ajude a melhorar o Mercado de Música");
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		return modelAndView;
//	}
}
