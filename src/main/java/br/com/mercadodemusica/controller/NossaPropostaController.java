package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@Controller
public class NossaPropostaController {

	@Autowired
	private UsuarioFacade usuarioFacade;
	
	@RequestMapping(value = "/nossa_proposta")
	public ModelAndView nossaProposta(HttpServletRequest request) throws JSONException, ParseException, IOException, RegraDeNegocioException, URISyntaxException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("titulo",  "Nossa proposta");
		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
		modelAndView.addObject("pagina", "footer/nossa_proposta");
		
		return modelAndView;
	}
}
