package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.FotoUsuarioDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.ApresentacaoFacade;
import br.com.mercadodemusica.facade.MapeamentoPesquisaFacade;
import br.com.mercadodemusica.facade.PrecoFacade;
import br.com.mercadodemusica.facade.ProdutoFacade;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;

@Controller
public class DetalhamentoProdutoController {
	
//	@Autowired
//	DetalhamentoProdutoService detalhamentoProdutoService;
	
	@Autowired
	private UsuarioFacade usuarioFacade;
	
	@Autowired
	private ProdutoFacade produtoFacade;
	
	@Autowired
	private ApresentacaoFacade apresentacaoFacade;
	
	@Autowired
	private PrecoFacade precoFacade;
	
	@Autowired
	private MapeamentoPesquisaFacade mapeamentoPesquisaFacade;
	
//	@Autowired
//	PerguntasRespostasService perguntasRespostasService;
	
//	@Autowired
//	CompraVendaService compraVendaService;
	
//	@Autowired
//	GatewayDePagamentoService gatewayDePagamentoService;
	
	
	@RequestMapping(value="/detalhamento_produto/{idProdutoCrypt}", method=RequestMethod.GET)
	public ModelAndView detalheProduto(HttpServletRequest request, @PathVariable String idProdutoCrypt) throws JSONException, ParseException, IOException, RegraDeNegocioException, URISyntaxException, IllegalArgumentException, IllegalAccessException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
//		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		ProdutoDTO produtoDto = produtoFacade.obterProdutoPorId(idProdutoCrypt);
		produtoDto.setIdCrypt(idProdutoCrypt);
		List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = apresentacaoFacade.obterlistaDeApresentacaoPorProduto(produtoDto);
		FotoUsuarioDTO fotoUsuarioDto = apresentacaoFacade.obterFotoUsuario(produtoDto.getUsuario());
		
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("pagina", "detalhamento_produto/detalhe");
		modelAndView.addObject("produtoDto", produtoDto);
		modelAndView.addObject("titulo", produtoDto.getNome());
		modelAndView.addObject("h3", produtoDto.getNome());
		modelAndView.addObject("listaDeApresentacoesDto", listaDeApresentacoesDto);
		modelAndView.addObject("fotoUsuario", fotoUsuarioDto);
		modelAndView.addObject("pesquisaDosUsuarios", mapeamentoPesquisaFacade.pesquisasFeitasPelosUsuariosPorProduto(idProdutoCrypt, request.getRemoteAddr()));
		modelAndView.addObject("compraDosUsuarios", mapeamentoPesquisaFacade.comprasEfetuadasPelosUsuariosPorProduto(idProdutoCrypt, request.getRemoteAddr()));
		
		
//		modelAndView.addObject("historicoDePrecos", precoFacade.obterHistoricoDePreco(idProdutoCrypt, request.getRemoteAddr()));
		
		
		return modelAndView;
		
	}
//	
//	
//	@RequestMapping(value="/verificacao_parcelamento_valores", method=RequestMethod.GET)
//	@ResponseBody
//	public List<BandeiraValoresParcelasGatewayDePagamentoDTO> verificacaoParcelamentoValores(BigInteger idProduto, BigInteger parcela) throws PesquisaDeProdutosException, NaoEncontradoException, FotosException, GatewayDePagamentoException, UsuarioException {
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		PesquisaDeProdutosDTO pesquisaDeProdutosDto = detalhamentoProdutoService.detalhamentoProdutoId(infosGeraisProdutoUsuarioDto);
//		
//		List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaBandeiraValoresParcelasGatewayDePagamento = gatewayDePagamentoService.obterValorDoParcelamento(Parcelamentos.retornoEnum(parcela), pesquisaDeProdutosDto.getListaApresentacaoProduto().get(0).getProduto(), TipoPagamentoEnum.CartaoCredito, null);
//		return listaBandeiraValoresParcelasGatewayDePagamento;
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/envio_de_pergunta")
//	public void envioDePerguntaSobreProduto(HttpServletRequest request, BigInteger idProduto, String perguntaAoVendedor) throws UnknownHostException, MalformedURLException, IOException, UsuarioException, ProdutoException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDtoPergunta = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		PerguntaCategoriaProdutoDTO perguntaDto = new PerguntaCategoriaProdutoDTO();
//	
//		perguntaDto.setProduto(produtoService.obterProdutoPorId(idProduto));
//		perguntaDto.setUsuarioInteressado(usuarioDtoPergunta);
//		perguntaDto.setPergunta(perguntaAoVendedor);
//		
//		perguntasRespostasService.inserirPerguntaAoProdutoDeInteresse(perguntaDto);
//		
//	}
//	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
//	@RequestMapping(value="/inserir_like")
//	public void inserirLike(HttpServletRequest request, BigInteger idProduto) throws UnknownHostException, MalformedURLException, IOException, UsuarioException {
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		ProdutoDTO infosGeraisProdutoUsuarioDto = new ProdutoDTO();
//		infosGeraisProdutoUsuarioDto.setId(idProduto);
//		
//		produtoService.inserirLikeParaProduto(infosGeraisProdutoUsuarioDto, usuarioDto);
//	}	
}
