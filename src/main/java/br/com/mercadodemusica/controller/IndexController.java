package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.PesquisaDeProdutosDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.enums.ProdutosPorPaginaEnum;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.PesquisaDeProdutoFacade;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@Controller
public class IndexController {
	
	@Autowired
	private UsuarioFacade usuarioFacade;
	
	@Autowired
	private PesquisaDeProdutoFacade pesquisaProdutoFacade;
	
	@RequestMapping(value = "/")
	public ModelAndView index(HttpServletRequest request, Model model) throws ParseException, IOException, RegraDeNegocioException, URISyntaxException, JSONException {		
	
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		model.addAttribute("produtoDto", new ProdutoDTO());
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("titulo", "");
		modelAndView.addObject("pagina", "index/index");
		
		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
		
		PesquisaDeProdutosDTO pesquisaProdutoDto = pesquisaProdutoFacade.pesquisaInicialDeProdutos(usuarioDto, BigInteger.ONE);
		
		modelAndView.addObject("pesquisaProdutoDto", pesquisaProdutoDto);
		
		return modelAndView;
		
	}
}
