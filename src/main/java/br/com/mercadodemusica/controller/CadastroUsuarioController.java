package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.AcessoUsuarioDTO;
import br.com.mercadodemusica.dto.CategoriaProdutoDTO;
import br.com.mercadodemusica.dto.TokenLiberacaoUsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaJuridicaDTO;
import br.com.mercadodemusica.emails.EnvioEmails;
import br.com.mercadodemusica.enums.EstadosEnum;
import br.com.mercadodemusica.enums.TipoDeTelefoneEnum;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.AcessoUsuarioFacade;
import br.com.mercadodemusica.facade.EnderecoFacade;
import br.com.mercadodemusica.facade.TokenLiberacaoUsuarioFacade;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.templates.TemplateEmails;

@Controller
public class CadastroUsuarioController {
	
	@Autowired
	private UsuarioFacade usuarioFacade;
	
	@Autowired
	private TemplateEmails templateEmails;
	
	@Autowired
	private EnvioEmails envioEmails;
	
	@Autowired
	private AcessoUsuarioFacade acessoUsuarioFacade;
	
	@Autowired
	private TokenLiberacaoUsuarioFacade tokenLiberacaoUsuarioFacade;
	
	
	@Autowired
	private EnderecoFacade enderecoFacade;
	
	
	@RequestMapping(value = "/cadastre_se")
	public ModelAndView novoUsuario(Model model) {
		
		model.addAttribute("usuarioDtoPF", new UsuarioPessoaFisicaDTO());
		model.addAttribute("usuarioDtoPJ", new UsuarioPessoaJuridicaDTO());
		model.addAttribute("infosGeraisProdutoDto", new CategoriaProdutoDTO());
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		
		
		
		
		modelAndView.addObject("titulo", "- Cadastre-se");
		modelAndView.addObject("h3", "Cadastre-se");
		modelAndView.addObject("pagina", "usuario/novo_usuario");
		modelAndView.addObject("tiposTelefone", TipoDeTelefoneEnum.values());
		modelAndView.addObject("listaDeEstados", EstadosEnum.values());
		
		return modelAndView;
	}
	
	
	@RequestMapping(value = "/cadastro_novo_usuario_pf", method=RequestMethod.POST)
	public ModelAndView cadastroNovoUsuarioPf(@ModelAttribute("usuarioDto") UsuarioPessoaFisicaDTO usuarioPessoaFisicaDto, BindingResult result, Model model) throws Exception {
		
		model.addAttribute("infosGeraisProdutoDto", new CategoriaProdutoDTO());
		
		usuarioFacade.cadastrarUsuarioPessoaFisica(usuarioPessoaFisicaDto);
		UsuarioDTO usuarioDto = usuarioFacade.obterInfosUsuarioPorCpfCnpj(usuarioPessoaFisicaDto.getCpf());
		AcessoUsuarioDTO acessoUsuarioDto = acessoUsuarioFacade.obterPorUsuarioId(usuarioDto.getId());
		
		/*
		 * 
		 */
		TokenLiberacaoUsuarioDTO tokenLiberacaoUsuarioDto = tokenLiberacaoUsuarioFacade.criarTokenDeLiberacao(usuarioDto.getId());
		String template = templateEmails.liberacaoDeUsuario(tokenLiberacaoUsuarioDto);
		envioEmails.enviarEmailLiberacaoUsuario(usuarioDto, acessoUsuarioDto, template); 
		/*
		 * 
		 */

		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		
		modelAndView.addObject("titulo", "- Cadastro de novo usuário");
		modelAndView.addObject("h3", "Cadastro de novo usuário");
		modelAndView.addObject("pagina", "usuario/cadastro_ok");
		modelAndView.addObject("cadastroAlteracaoTitulo", "Confirme o seu cadastro!");
		modelAndView.addObject("cadastroAlteracaoFrase", "Enviamos um e-mail para você. Clique no link deste e-mail e confirme seu cadastro!");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/cadastro_novo_usuario_pj", method=RequestMethod.POST)
	public ModelAndView cadastroNovoUsuarioPj(@ModelAttribute("usuarioDtoPJ") UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDto, Model model) throws Exception {
		
		model.addAttribute("infosGeraisProdutoDto", new CategoriaProdutoDTO());
		
		usuarioFacade.cadastrarUsuarioPessoaJuridica(usuarioPessoaJuridicaDto);
		UsuarioDTO usuarioDto = usuarioFacade.obterInfosUsuarioPorCpfCnpj(usuarioPessoaJuridicaDto.getCnpj());
		AcessoUsuarioDTO acessoUsuarioDto = acessoUsuarioFacade.obterPorUsuarioId(usuarioDto.getId());
		
		/*
		 * 
		 */
		TokenLiberacaoUsuarioDTO tokenLiberacaoUsuarioDto = tokenLiberacaoUsuarioFacade.criarTokenDeLiberacao(usuarioDto.getId());
		String template = templateEmails.liberacaoDeUsuario(tokenLiberacaoUsuarioDto);
		envioEmails.enviarEmailLiberacaoUsuario(usuarioDto, acessoUsuarioDto, template);
		/*
		 * 
		 */
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		
		modelAndView.addObject("titulo", "- Cadastro de novo usuário");
		modelAndView.addObject("h3", "Cadastro de novo usuário");
		modelAndView.addObject("pagina", "usuario/cadastro_ok");
		modelAndView.addObject("cadastroAlteracaoTitulo", "Confirme o seu cadastro!");
		modelAndView.addObject("cadastroAlteracaoFrase", "Enviamos um e-mail para você. Clique no link deste e-mail e confirme seu cadastro!");
		
		return modelAndView;
	}
	
	
	@RequestMapping(value = "/verificar_cpf_nao_existente")
	@ResponseBody
	public Boolean verificarCpfNaoExistente (UsuarioPessoaFisicaDTO usuarioDto) throws ParseException, IOException, RegraDeNegocioException, JSONException, URISyntaxException {
		
		return usuarioFacade.verificarCpfNaoExistente(usuarioDto);
	}
	
	@RequestMapping(value = "/verificar_cnpj_nao_existente")
	@ResponseBody
	public Boolean verificarCnpjNaoExistente (UsuarioPessoaJuridicaDTO usuarioDto) throws ParseException, IOException, RegraDeNegocioException, JSONException, URISyntaxException {
		
		return usuarioFacade.verificarCnpjNaoExistente(usuarioDto);
	}
	
	@RequestMapping(value = "/verificar_cpf_nao_existente_pessoa_juridica")
	@ResponseBody
	public Boolean verificarCpfNaoExistentePessoaJuridica(UsuarioPessoaJuridicaDTO usuarioDto) throws ParseException, IOException, RegraDeNegocioException, JSONException, URISyntaxException {
		
		return usuarioFacade.verificarCpfPessoaJuridicaNaoExistente(usuarioDto);
	}
	
	@RequestMapping(value = "/verificar_email")
	@ResponseBody
	public Boolean verificarEmail (String email) throws ParseException, IOException, RegraDeNegocioException, JSONException, URISyntaxException {
		
		
		AcessoUsuarioDTO acessoUsuarioDto = new AcessoUsuarioDTO();
		acessoUsuarioDto.setEmail(email);
		
		return acessoUsuarioFacade.verificarEmail(acessoUsuarioDto);
	}
	
	@RequestMapping(value = "/obter_endereco_por_cep")
	@ResponseBody
	public String obterEnderecoPorCep (String cep) throws MalformedURLException, IOException {
		
		return enderecoFacade.obterEnderecoPorCep(cep);
	}
	
	@RequestMapping(value = "/verificar_data_de_nascimento")
	@ResponseBody
	public Boolean verificarDataDeNacimento (String dataDeNascimentoString) throws ParseException, IOException, RegraDeNegocioException, JSONException, URISyntaxException {
		
		return usuarioFacade.verificarDataDeNascimento(dataDeNascimentoString);
	}
	
	
	@RequestMapping(value = "/obter_estados")
	@ResponseBody
	public EstadosEnum[] obterEstados () {
		
		return EstadosEnum.values();
	}
	
}
