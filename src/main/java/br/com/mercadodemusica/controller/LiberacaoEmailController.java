package br.com.mercadodemusica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.TokenLeadDTO;
import br.com.mercadodemusica.facade.TokenLeadFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@Controller
@RequestMapping("/liberacao_email")
public class LiberacaoEmailController {
	
	@Autowired
	private TokenLeadFacade tokenLeadFacade;
	
	@RequestMapping("/token/{token}")
	public ModelAndView liberacaoTokenLead(@PathVariable("token") String token) {
		
		tokenLeadFacade.confirmarLeadFacade(token);
		TokenLeadDTO tokenLeadDto = tokenLeadFacade.obterPorToken(token);
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("pagina", "lead/confirmacao_lead");	
		modelAndView.addObject("h3", "Confirmação de e-mail");
		modelAndView.addObject("nome", tokenLeadDto.getLead().getNome());
		
		return modelAndView;
	}
}
