package br.com.mercadodemusica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.TokenLiberacaoUsuarioDTO;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.facade.TokenLiberacaoUsuarioFacade;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@Controller
public class LiberacaoUsuarioController {
	
	@Autowired
	private UsuarioFacade usuarioFacade;
	
	@Autowired
	private TokenLiberacaoUsuarioFacade tokenLiberacaoUsuarioFacade;
	
	@RequestMapping("/liberacao_usuario/token/{token}")
	public ModelAndView liberacaoUsuario(@PathVariable String token) throws TokenException, UsuarioException {
		
		TokenLiberacaoUsuarioDTO tokenLiberacaoUsuarioDto = tokenLiberacaoUsuarioFacade.obterPorToken(token);
		usuarioFacade.liberarUsuarioPorToken(tokenLiberacaoUsuarioDto.getUsuario());
		
		tokenLiberacaoUsuarioFacade.inativarToken(tokenLiberacaoUsuarioDto);
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("titulo", "liberaçao de  " + NomePessoaJuridicaOuFisica.obterNomeDto(tokenLiberacaoUsuarioDto.getUsuario()));
		modelAndView.addObject("pagina", "usuario/usuario_liberado");
		modelAndView.addObject("cadastroAlteracaoTitulo", "Cadastro liberado com sucesso!");
		modelAndView.addObject("cadastroAlteracaoFrase", "Faça seu login e aproveite todos os produtos que temos para você!");
		
		return modelAndView;
	}
}
