package br.com.mercadodemusica.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.PorcentagemFacade;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@Controller
public class PoliticaController {
	
	@Autowired
	private UsuarioFacade usuarioFacade;
//	
//	@Autowired
//	private ProdutoService produtoService;
//	
	@Autowired
	private PorcentagemFacade porcentagemFacade;
//	
//	@RequestMapping("/politica_de_compra_e_venda")
//	public ModelAndView politicaCompraVenda(HttpServletRequest request) throws UnknownHostException, MalformedURLException, UsuarioException, IOException {
//		
//		
////		Verificacao de usuario pelo ip ou online
//		UsuarioDTO usuarioDto = usuarioService.obterUsuarioEnderecoPesquisaProdutos(request);
//		
//		ModelAndView modelAndView = new ModelAndView(Pagina.PAGINA_VIEW);
//		modelAndView.addObject("pagina", "footer/politica_de_compra_venda");	
//		modelAndView.addObject("h3", "Política de compra e venda");
//		modelAndView.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
//		modelAndView.addObject("listaInstrumentoAcessorioDto", produtoService.obterInstrumentoAcessorio());
//		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
//		return modelAndView;
//	}
//	
//	
	@RequestMapping("/politica_de_privacidade")
	public ModelAndView politicaDePrivacidade(HttpServletRequest request) throws JSONException, ParseException, IOException, RegraDeNegocioException, URISyntaxException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
		
//		Verificacao de usuario pelo ip ou online
		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("pagina", "footer/politica_de_privacidade");	
		modelAndView.addObject("h3", "Política de privacidade");
		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
		return modelAndView;
	}
	
	
	@RequestMapping("termos_de_uso")
	public ModelAndView termosDeUso(HttpServletRequest request) throws JSONException, ParseException, IOException, RegraDeNegocioException, URISyntaxException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("remoteUser", request.getRemoteUser());
		jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
		
//		Verificacao de usuario pelo ip ou online
		UsuarioDTO usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		
		
		
		ModelAndView modelAndView = new ModelAndView(PaginaClient.PAGINA_VIEW);
		modelAndView.addObject("pagina", "footer/termos_de_uso");	
		modelAndView.addObject("h3", "Termos de uso");
		modelAndView.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
		modelAndView.addObject("porcentagemAtivaDto", porcentagemFacade.obterPorcentagemAtiva());
		return modelAndView;
	}
}
