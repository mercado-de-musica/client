package br.com.mercadodemusica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	
	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;
	
	@Autowired
	private LoginSuccessHandler loginSuccessHandler;
	
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		
//		entrada inicial no sistema
		
//		 auth.inMemoryAuthentication().withUser("usuario").password("3edc4rfv").roles("USER");
//		 auth.inMemoryAuthentication().withUser("admin").password("3edc4rfv").roles("ADMIN");
    
	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter,CsrfFilter.class);
		
        
//		http.headers().disable();
////		http.headers().cacheControl();
//		http.headers().httpStrictTransportSecurity();
//		
//		http.authorizeRequests().antMatchers("/**").permitAll().and().requiresChannel().anyRequest().requiresSecure();
//		
////		http.authorizeRequests().antMatchers("/administracao/**").access("hasRole('ROLE_ADMIN')");
////		http.authorizeRequests().antMatchers("/usuario/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')");
//		http.authorizeRequests().and().formLogin().loginProcessingUrl("/j_spring_security_check").loginPage("/login").failureUrl("/login_error")
//		.successHandler(loginSuccessHandler)
//		.and().logout().logoutSuccessUrl("/logout").and().csrf();
//        
        
        
//        porta 8080
        
		http.headers().disable();
//		http.headers().cacheControl();
//		http.headers().httpStrictTransportSecurity();
		
		http.authorizeRequests().antMatchers("/**").permitAll();
		
//		http.authorizeRequests().antMatchers("/administracao/**").access("hasRole('ROLE_ADMIN')");
//		http.authorizeRequests().antMatchers("/usuario/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')");
		http.authorizeRequests().and().formLogin().loginProcessingUrl("/j_spring_security_check").loginPage("/login").failureUrl("/login_error")
		.successHandler(loginSuccessHandler)
		.and().logout().logoutSuccessUrl("/logout").and().csrf();
        
		
	}
	
	@Bean(name="authenticationManager")
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(customAuthenticationProvider);
	}
	
	
}
