package br.com.mercadodemusica.cron;

//@Component("EnvioDePerguntasRespostas")
public class EnvioDePerguntasRespostasCron {
		
//	@Autowired
//	private PerguntasRespostasService perguntasRespostasService;
//	
//	@Autowired
//	private UsuarioFacade usuarioService;
//	
//	@Autowired
//	private TemplateEmails templateEmails;
//	
//	@Autowired
//	private EnvioEmails envioEmails;
//	
//	private final static Logger logger = Logger.getLogger(EnvioDePerguntasRespostasCron.class);
//	
//	@Transactional(readOnly = true)
//	public void obterListaDePerguntas() {
//		
//		logger.info("obterListaDePerguntas()");
//		
//		List<PerguntaCategoriaProdutoDTO> listaDePerguntasDto = perguntasRespostasService.obterPerguntasNaoEnviadoEmailLimit(ConstantesDoSistema.LIMITE_EMAIL_POR_DUAS_HORAS);
//		
//		for(PerguntaCategoriaProdutoDTO perguntaDto : listaDePerguntasDto) {
//			
//			UsuarioDTO usuarioInteressadoRetornoDto = usuarioService.obterInfosUsuarioPorUsuario(perguntaDto.getUsuarioInteressado());
//			perguntaDto.setUsuarioInteressado(usuarioInteressadoRetornoDto);
//			this.enviarPerguntas(perguntaDto);
//		}		
//	}
//	
//	@Transactional(propagation=Propagation.REQUIRED)
//	private void enviarPerguntas(PerguntaCategoriaProdutoDTO perguntaDto) {
//		
//		logger.info("obterListaDePerguntas()");
//		
//		String retornoTemplate = templateEmails.perguntaAoVendedor(perguntaDto);
//		
//		try {
//			envioEmails.enviarAvisoDePerguntaAoVendedor(perguntaDto, retornoTemplate);
//			
////			finalizamos com o true no atributo de envio de email e data de envio
//			
//			perguntaDto.setEnviadoEmail(Boolean.TRUE);
//			perguntaDto.setDataEnvioEmail(new GregorianCalendar());
//			
//			perguntasRespostasService.updatePerguntaEResposta(perguntaDto);
//		} catch (MessagingException e) {
//			logger.error(e.getMessage());
//		}
//	}
//	
//	
//	@Transactional(readOnly = true)
//	public void obterListaDeRespostas(){
//		
//		List<PerguntaCategoriaProdutoDTO> listaDeRespostasDto = perguntasRespostasService.obterRespostasNaoEnviadoEmailLimit(ConstantesDoSistema.LIMITE_EMAIL_POR_DUAS_HORAS);
//		
//		for(PerguntaCategoriaProdutoDTO respostaDto : listaDeRespostasDto) {
//			
//			this.enviarRespostas(respostaDto);
//		}		
//	}
//	
//	@Transactional(propagation=Propagation.REQUIRED)
//	private void enviarRespostas(PerguntaCategoriaProdutoDTO respostaDto){
//		
//		PerguntaCategoriaProdutoDTO perguntaCategoriaProdutoDto = null;
//		
//		/*
//		 * verificacoes apenas para pegar a contrapartida na conversa
//		 */
//		if(respostaDto.getProduto().getUsuario().getId().equals(respostaDto.getUsuarioInteressado().getId())) {
//			
//			perguntaCategoriaProdutoDto = perguntasRespostasService.obterPerguntaPeloIdPai(respostaDto);
//			
//		} else {
//			perguntaCategoriaProdutoDto = (PerguntaCategoriaProdutoDTO) SerializationUtils.clone(respostaDto);
//			perguntaCategoriaProdutoDto.setUsuarioInteressado(respostaDto.getProduto().getUsuario());
//		}
//		
//		
//		String retornoTemplate = templateEmails.respostaAoInteressado(perguntaCategoriaProdutoDto, respostaDto);
//		
//		try {
//			envioEmails.enviarAvisoDeRespostaAoInteressado(perguntaCategoriaProdutoDto, retornoTemplate);
//			
////			finalizamos com o true no atributo de envio de email e data de envio
//			
//			respostaDto.setEnviadoEmail(Boolean.TRUE);
//			respostaDto.setDataEnvioEmail(new GregorianCalendar());
//			
//			perguntasRespostasService.updatePerguntaEResposta(respostaDto);
//		} catch (MessagingException e) {
//			logger.error(e.getMessage());
//		}
//
//	}
	
	
}
