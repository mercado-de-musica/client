package br.com.mercadodemusica.cron;

//@Component("Pagamento")
//@Controller
public class PagamentoCron {
	
//	@Autowired
//	private RastreamentoService rastreamentoService;
//	
//	@Autowired
//	private CompraVendaService compraVendaService;
//	
//	@Autowired
//	private EnvioEmails envioEmails;
//	
//	@Autowired
//	private TemplateEmails templateEmails;
//	
//	private final static Logger logger = Logger.getLogger(PagamentoCron.class);
//	
////	@RequestMapping("/pagamento")
//	public void verificacaoDePagamentosEmAnalise() {
//		
//		logger.info("verificacaoDePagamentosEmAnalise()");
//		
//		List<CompraDTO> listaDeComprasDto = compraVendaService.obterCompraEmAnalise();
//			
//		for(CompraDTO compraDto : listaDeComprasDto) {
//			
//			try {
//				
//				String statusPedido = compraVendaService.verificacaoGatewayDePagamentoMudancaStatus(compraDto);
//				
//				
////				pedido que finalizou
//		    	Boolean updateNaCompra = Boolean.FALSE;
//			    if(statusPedido.equals(StatusTransacaoEnum.APROVADO.getNumero())) {
//			   		
//			    	/*
//			    	 * apenas caso seja entregue em maos porque nao perdemos o codigo
//			    	 */
//					rastreamentoService.inserirCodigosEntrega(compraDto);
//			    	
//					for(HistoricoDatasDeCompraDTO dataDaCompraDto : compraDto.getDatasDaCompra()) {
//			    		dataDaCompraDto.setDataAtualizada(Boolean.FALSE);
//			    	}
//			    	
//				    Calendar dataDeHoje = Calendar.getInstance();
//				    
//				    
//			    	HistoricoDatasDeCompraDTO novoHistoricoDeCompraDto = new HistoricoDatasDeCompraDTO();
//			    	novoHistoricoDeCompraDto.setDataAtualizada(Boolean.TRUE);
//			    	novoHistoricoDeCompraDto.setDataDaCompra(dataDeHoje);
//			    	
//			    	compraDto.getDatasDaCompra().add(novoHistoricoDeCompraDto);
//			    	compraDto.setStatusTransacao(StatusTransacaoEnum.APROVADO);
//			    	
//			    	String templateEnviodeDadosVendedoresAoComprador = templateEmails.envioDeDadosDosVendedoresAoComprador(compraDto);
//			    	envioEmails.envioConfirmacaoDeCompraAoComprador(compraDto, templateEnviodeDadosVendedoresAoComprador);
//			    	
//			    	for(ProdutoDTO produtosComprado : compraDto.getProdutosComprados()) {
//			    		String templateEnviodeDadosCompradorAoVendedor = templateEmails.envioDeDadosDoCompradorAovendedor(compraDto, produtosComprado.getUsuario());
//				    	envioEmails.envioConfirmacaoDeCompraAoVendedor(compraDto, templateEnviodeDadosCompradorAoVendedor, produtosComprado.getUsuario());
//			    	}
//			    	
//			    	updateNaCompra = Boolean.TRUE;
//			    	
//			    } else if(statusPedido.equals(StatusTransacaoEnum.CANCELADO.getNumero()) || statusPedido.equals(StatusTransacaoEnum.REPROVADO.getNumero())) {
//			    	
////			    	Status a ser usado na transacao será aquele do gateway de pagamento
//			    	StatusTransacaoEnum statusCanceladoReprovado = null;
//			    	for (StatusTransacaoEnum statusFor : StatusTransacaoEnum.values()) {
//			    		if(statusFor.getNumero().equals(statusPedido)) {
//			    			statusCanceladoReprovado = statusFor;
//			    		}
//			    	}
//			    	
//			    	
//			    	/*
//					 * todas as datas anteriores passam para false
//					 * 
//					 */
//					for(HistoricoDatasDeCompraDTO dataDaCompraDto : compraDto.getDatasDaCompra()) {
//			    		dataDaCompraDto.setDataAtualizada(Boolean.FALSE);
//			    	}
//			    	
//				    Calendar dataDeHoje = Calendar.getInstance();
//				    
//				    
//			    	HistoricoDatasDeCompraDTO novoHistoricoDeCompraDto = new HistoricoDatasDeCompraDTO();
//			    	novoHistoricoDeCompraDto.setDataAtualizada(Boolean.TRUE);
//			    	
//			    	
//			    	
//			    	dataDeHoje.add(Calendar.DATE, -1);
//			    	
//			    	novoHistoricoDeCompraDto.setDataDaCompra(dataDeHoje);
//			    	novoHistoricoDeCompraDto.setCompra(compraDto);
//			    	
//			    	
//			    	compraDto.setStatusTransacao(statusCanceladoReprovado);
//			    	compraDto.setFinalizado(Boolean.FALSE);
//			    	
//			    	for(ProdutoDTO produtoComprado : compraDto.getProdutosComprados()) {
//			    		produtoComprado.setAtivo(Boolean.TRUE);
//			    	}
//			    	
//			    	
//			    	
//			    	CompraCartaoDeCreditoDTO compraCartaoDeCreditoDto = compraVendaService.obterCompraPorCartaoDeCredito(compraDto);
//			    	
//			    	if(compraCartaoDeCreditoDto != null) {
//			    		
//				    	
//				    	String templateCancelamentoDeCompraCartaoDeCredito = templateEmails.envioDeCancelamentoDeCompraCartaoDeCreditoAoComprador(compraCartaoDeCreditoDto);
//				    	
//				    	envioEmails.envioCancelamentoDeCompraCartaoDeCreditoAoComprador(compraCartaoDeCreditoDto, templateCancelamentoDeCompraCartaoDeCredito);
//				    	
//				    	updateNaCompra = Boolean.TRUE;
//			    	
//			    	} else {
////			    		compra por boleto
//			    		
//			    		CompraBoletoDTO compraBoletoBancarioDto = compraVendaService.obterCompraPorBoleto(compraDto);
//			    		
//				    	String templateCancelamentoBoletoBancario = templateEmails.envioCancelamentoDeCompraBoletoBancario(compraBoletoBancarioDto);
//				    	envioEmails.envioCancelamentoCompraBoletoBancario(compraBoletoBancarioDto, templateCancelamentoBoletoBancario);
//				    	updateNaCompra = Boolean.TRUE;
//				    
//			    	}
//			    	
//			    	compraDto.getDatasDaCompra().add(novoHistoricoDeCompraDto);
//			    	
//			    	
//			    	
//			    } 
//			    
//			    
//			    if(updateNaCompra) {
//			    	compraVendaService.updateNaCompra(compraDto);
//			    }
//			} catch (GatewayDePagamentoException | NumberFormatException | CodigoDeEntregaException | IOException | MessagingException e) {
//				
//				logger.error(e.getMessage());
//				
//			}
//
//		}
//		
//	}
}
