package br.com.mercadodemusica.cron;

//@Component("TransferenciaDeValores")
public class TransferenciaDeValoresCron {

//	@Autowired
//	TransferenciaDeValoresService transferenciaDeValoresService;
//	
//	@Autowired
//	private EnvioEmails envioEmails;
//	
//	@Autowired
//	private TemplateEmails templateEmails;
//	
//	private final static Logger logger = Logger.getLogger(TransferenciaDeValoresCron.class);
//	
//	@SuppressWarnings("resource")
//	public void verificarStatusTransferenciaDeValores() {
//		
//		logger.info("verificarStatusTransferenciaDeValores()");
//		
//		List<TransferenciaValoresUsuarioDTO> listaTransferenciaDeValoresUsuarioDto = transferenciaDeValoresService.obterTransferenciasRequisitadasGatewayDePagamento();
//		
//		try {
//			XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File("/arquivos/template_transferencia_de_valores.xlsx")));
//		
//			XSSFSheet sheet = workbook.getSheet("mercadodemusica");
//			
//			Integer linhaParaEscrita = 6;
//			for(TransferenciaValoresUsuarioDTO transferenciaDeValoresUsuarioDto : listaTransferenciaDeValoresUsuarioDto) {
//		
//				XSSFRow row = sheet.getRow(linhaParaEscrita);
//				if(row == null) {
//					row = sheet.createRow(linhaParaEscrita);
//				}
//				
//				
//				for (int i = 0; i <= 8; i++) {
//		
//					XSSFCell cell = row.createCell(i);
//					String info = this.obterInformacaoPorNumeral(transferenciaDeValoresUsuarioDto, i);
//					cell.setCellValue(info);
//				}
//				
//				linhaParaEscrita++;
//			}
//			
//			logger.info("criacao de arquivo para envio por e-mail");
//			
//			File fileSaida = File.createTempFile("transferencia_" + new DatasTransformacao().calendarToStringPadraoAmericano(Calendar.getInstance()), ".xlsx");
//			FileOutputStream fileOutputStream = new FileOutputStream(fileSaida);
//			workbook.write(fileOutputStream);
//			
//			String template = templateEmails.envioPlanilhaTransferenciaParaResponsavel();
//			envioEmails.envioTransferenciaParaResponsavel(fileSaida, template);
//			
//			for(TransferenciaValoresUsuarioDTO transferenciaDeValoresUsuarioDto : listaTransferenciaDeValoresUsuarioDto) {
//				for(StatusTransferenciaDeValorDTO statusTransferenciaValor : transferenciaDeValoresUsuarioDto.getListaStatusTransferenciaDeValor()) {
//					statusTransferenciaValor.setAtivo(Boolean.FALSE);
//				}
//				
//				StatusTransferenciaDeValorDTO statusTransferenciaDeValorDto = new StatusTransferenciaDeValorDTO();
//				statusTransferenciaDeValorDto.setAtivo(Boolean.TRUE);
//				statusTransferenciaDeValorDto.setDataTransferencia(Calendar.getInstance());
//				statusTransferenciaDeValorDto.setStatusTransferenciaDeValoresGatewayDePagamento(StatusTransferenciaDeValoresGatewayDePagamentoEnum.FILE_CREATED);
//				statusTransferenciaDeValorDto.setTransferenciaValoresUsuario(transferenciaDeValoresUsuarioDto);
//			
//				transferenciaDeValoresUsuarioDto.getListaStatusTransferenciaDeValor().add(statusTransferenciaDeValorDto);
//				
//				transferenciaDeValoresService.atualizarTransferenciaValoresUsuario(transferenciaDeValoresUsuarioDto);
//			}
//			
//		} catch (IOException | MessagingException e) {
//			logger.error(e.getMessage());
//		}	
//	}
//
//	private String obterInformacaoPorNumeral(TransferenciaValoresUsuarioDTO transferenciaDeValoresUsuarioDto, int i) {
//		
//		logger.info("obterInformacaoPorNumeral(TransferenciaValoresUsuarioDTO transferenciaDeValoresUsuarioDto, int i)");
//		
//		switch (i) {
//			case 0:
//				if(transferenciaDeValoresUsuarioDto.getUsuario() instanceof UsuarioPessoaJuridicaDTO) {
//					UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDto = (UsuarioPessoaJuridicaDTO) transferenciaDeValoresUsuarioDto.getUsuario();
//					return usuarioPessoaJuridicaDto.getRazaoSocial();
//				} else {
//					UsuarioPessoaFisicaDTO usuarioPessoaFisicaDto = (UsuarioPessoaFisicaDTO) transferenciaDeValoresUsuarioDto.getUsuario();
//					return usuarioPessoaFisicaDto.getNome() + " " + usuarioPessoaFisicaDto.getSobrenome();
//				}
//			case 1:
//				if(transferenciaDeValoresUsuarioDto.getUsuario() instanceof UsuarioPessoaJuridicaDTO) {
//					UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDto = (UsuarioPessoaJuridicaDTO) transferenciaDeValoresUsuarioDto.getUsuario();
//					try {
//						return CpfCnpj.inserirPontuacaoCnpj(usuarioPessoaJuridicaDto.getCnpj());
//					} catch (DocumentosException e) {
//						logger.error(e.getMessage());
//					}
//				} else {
//					UsuarioPessoaFisicaDTO usuarioPessoaFisicaDto = (UsuarioPessoaFisicaDTO) transferenciaDeValoresUsuarioDto.getUsuario();
//					try {
//						return CpfCnpj.inserirPontuacaoCpf(usuarioPessoaFisicaDto.getCpf());
//					} catch (DocumentosException e) {
//						logger.error(e.getMessage());
//					}
//				}
//			case 2:
//				return transferenciaDeValoresUsuarioDto.getContaUsadaNaTransferencia().getNumeroDoBanco().getNumero();	
//			
//			case 3:
//				return transferenciaDeValoresUsuarioDto.getContaUsadaNaTransferencia().getNumeroDaAgencia();
//			
//			case 4:
//				return transferenciaDeValoresUsuarioDto.getContaUsadaNaTransferencia().getNumeroDaConta() + "-" + transferenciaDeValoresUsuarioDto.getContaUsadaNaTransferencia().getDigitoDaConta();
//				
//			case 5:
//				return new DatasTransformacao().calendarToString(Calendar.getInstance());
//				
//			case 6:
//				return transferenciaDeValoresUsuarioDto.getDesistencia() ? "Sim" : "Não";
//				
//			case 7:
//				
//				String stringRetorno = "";
//				for (ProdutoDTO infosGeraisProdutoUsuarioDto : transferenciaDeValoresUsuarioDto.getListaProduto()) {
//					stringRetorno = stringRetorno + infosGeraisProdutoUsuarioDto.getCategoriaProduto().getInstrumentoAcessorio().getNome() + " " +
//						infosGeraisProdutoUsuarioDto.getCategoriaProduto().getMarca().getNome() + " " + 
//						infosGeraisProdutoUsuarioDto.getCategoriaProduto().getProduto().getNome() + " " +
//						infosGeraisProdutoUsuarioDto.getCategoriaProduto().getModelo().getNome() + " - " ;
//				}
//	
//				
//				return stringRetorno;
//				
//			case 8:
//				
//				String valorParaPagar = "";
//				BigDecimal precoFinal = BigDecimal.ZERO;
//				for (ProdutoDTO infosGeraisProdutoUsuarioDto : transferenciaDeValoresUsuarioDto.getListaProduto()) {
//					BigDecimal precoVezesPorcentagem = infosGeraisProdutoUsuarioDto.getPreco().multiply(transferenciaDeValoresUsuarioDto.getPorcentagem());
//					BigDecimal precoVezesPorcentagemMenosCem = precoVezesPorcentagem.divide(BigDecimal.valueOf(100));
//					BigDecimal precoFinalProduto = infosGeraisProdutoUsuarioDto.getPreco().subtract(precoVezesPorcentagemMenosCem);
//					precoFinal = precoFinal.add(precoFinalProduto).setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					
//					valorParaPagar = "R$ " + new Precos().padraoAmericanoParaBrasileiro(precoFinal);
//				}
//				return valorParaPagar;	
//		}
//		return null;
//	}
}
