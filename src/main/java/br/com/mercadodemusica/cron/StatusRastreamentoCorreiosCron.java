package br.com.mercadodemusica.cron;

//@Component("StatusRastreamentoCorreios")
public class StatusRastreamentoCorreiosCron {
	
//	@Autowired
//	private ContratoCorreiosService contratoCorreiosService;
//	
//	@Autowired
//	private RastreamentoService rastreamentoService;
//	
//	@Autowired
//	private CorreiosService correiosService;
//	
//	private final static Logger logger = Logger.getLogger(StatusRastreamentoCorreiosCron.class);
//	
//	public void atualizarServicos() {
//		
//		logger.info("atualizarServicos()");
//		
//		ContratoCorreiosDTO infosContratoCorreiosDto = null;
//		try {
//			infosContratoCorreiosDto = contratoCorreiosService.obterInfosContratoCorreios();
//		} catch (CorreiosException e1) {
//			logger.error(e1.getMessage());
//		}
//		
//		List<RastreamentoDTO> arrayRastreamentosDto = rastreamentoService.obterRastreamentosEmAberto(); 
//		
//		for(RastreamentoDTO rastreamentoDto : arrayRastreamentosDto) {
//			
//			try {
//				
//				if(rastreamentoDto != null && rastreamentoDto instanceof RastreamentoProdutoCorreiosDTO && infosContratoCorreiosDto != null) {
//					
//					RastreamentoProdutoCorreiosDTO rastreamentoProdutoCorreiosDto = (RastreamentoProdutoCorreiosDTO) rastreamentoDto;
//					
//					AtendeClienteProxy atendeClienteCorreios = new AtendeClienteProxy();
//					
//					String xmlDeRetorno = atendeClienteCorreios.solicitaXmlPlp(Long.parseLong(rastreamentoProdutoCorreiosDto.getCodigoPlpCorreio()), infosContratoCorreiosDto.getUsuario(), infosContratoCorreiosDto.getSenha());
//					
//					 
//					XStream xStream = new XStream(new Dom4JDriver());
//		            xStream.alias("correioslog", XmlCorreiosDTO.class);
//		           
//		            XmlCorreiosDTO xmlDeserializado = (XmlCorreiosDTO) xStream.fromXML(xmlDeRetorno);
//		            
//		           
//		            if(! xmlDeserializado.getObjeto_postal().getStatus_processamento().equals("0")) {
//						 StatusPostagemCorreiosEnum statusRecebidoCorreios = null;
//						 for (StatusPostagemCorreiosEnum statusPostagemCorreiosEnum : StatusPostagemCorreiosEnum.values()) {
//							 if(statusPostagemCorreiosEnum.getNumeroCorreio().equals(xmlDeserializado.getObjeto_postal().getStatus_processamento())) {
//								 statusRecebidoCorreios = statusPostagemCorreiosEnum;
//							 }
//						 }
//						 
//						 if(statusRecebidoCorreios != null) {
//							 for(StatusPostagemProdutoCorreioDTO statusPostagemProdutoCorreiosDto : rastreamentoProdutoCorreiosDto.getStatusPostagemProdutoCorreio()) {
//								 if(statusPostagemProdutoCorreiosDto.getAtivo()) {
//									 statusPostagemProdutoCorreiosDto.setAtivo(false);
//								 }
//							 }
//							 
//							 StatusPostagemProdutoCorreioDTO statusPostagemProdutoCorreioNovaDto = new StatusPostagemProdutoCorreioDTO();
//							 statusPostagemProdutoCorreioNovaDto.setDataDoProcessamento(Calendar.getInstance());
//							 statusPostagemProdutoCorreioNovaDto.setAtivo(true);
//							 statusPostagemProdutoCorreioNovaDto.setStatus(statusRecebidoCorreios);
//							 statusPostagemProdutoCorreioNovaDto.setRastreamentoProdutoCorreios(rastreamentoProdutoCorreiosDto);
//							 rastreamentoProdutoCorreiosDto.getStatusPostagemProdutoCorreio().add(statusPostagemProdutoCorreioNovaDto);
//							 
//							
//							 
//							 ArrayList<EventosCorreiosDTO> eventosCorreios = correiosService.obterEventosRastreamentoCorreiosPorProduto(rastreamentoDto.getProduto());
//							 
//							 rastreamentoService.finalizarRastreamentoEventoCorreios(rastreamentoProdutoCorreiosDto, eventosCorreios);
//						 }
//		            }
//				}
//			} catch (NumberFormatException | RemoteException | CorreiosException | UnsupportedEncodingException e) {
//				
//				logger.error(e.getMessage());
//			
//			} 
//		}
//	}
}
