package br.com.mercadodemusica.controllerExceptions;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.hibernate.TransientObjectException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO;
import br.com.mercadodemusica.enums.ProdutosPorPaginaEnum;
import br.com.mercadodemusica.exceptions.CarrinhoDeCompraException;
import br.com.mercadodemusica.exceptions.CodigoDeEntregaException;
import br.com.mercadodemusica.exceptions.CompraVendaException;
import br.com.mercadodemusica.exceptions.ContaBancariaGatewayDePagamentoException;
import br.com.mercadodemusica.exceptions.CorreiosException;
import br.com.mercadodemusica.exceptions.DesistenciaException;
import br.com.mercadodemusica.exceptions.DocumentosException;
import br.com.mercadodemusica.exceptions.EmailException;
import br.com.mercadodemusica.exceptions.EnderecoException;
import br.com.mercadodemusica.exceptions.FotosException;
import br.com.mercadodemusica.exceptions.GatewayDePagamentoException;
import br.com.mercadodemusica.exceptions.ParcelamentoDeValorException;
import br.com.mercadodemusica.exceptions.PerguntasRespostasException;
import br.com.mercadodemusica.exceptions.PesquisaDeProdutosException;
import br.com.mercadodemusica.exceptions.ProdutoException;
import br.com.mercadodemusica.exceptions.RastreamentoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.ReputacaoVendedorException;
import br.com.mercadodemusica.exceptions.TelefoneException;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.TransferenciaValoresUsuarioException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.facade.UsuarioFacade;
import br.com.mercadodemusica.resolvers.PaginaClient;
import br.com.mercadodemusica.transformacaoDeDados.NomePessoaJuridicaOuFisica;

@ControllerAdvice
public class Erro500 {
	

	@Autowired
	private UsuarioFacade usuarioFacade;
	
	private final static Logger logger = Logger.getLogger(Erro500.class);

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(MappingException.class)
	public ModelAndView handleMapping(HttpServletRequest request, MappingException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
	
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(InvalidDataAccessApiUsageException.class)
	public ModelAndView handleDataAccessApiUsage(HttpServletRequest request, InvalidDataAccessApiUsageException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
	
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(TransientObjectException.class)
	public ModelAndView handleConflictTransientObject(HttpServletRequest request, TransientObjectException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
	
	}
	
	
//	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
//	@ExceptionHandler(JSONException.class)
//	public ModelAndView handleConflictJson(HttpServletRequest request, JSONException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException {
//		
//		return this.ajusteDePagina(request, ex);
//	
//	}
	
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(MailSendException.class)
	public ModelAndView handleConflictMailSender(HttpServletRequest request, MailSendException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
	
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(NullPointerException.class)
	public ModelAndView handleConflictNullPointer(HttpServletRequest request, NullPointerException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
	
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(RegraDeNegocioException.class)
	public ModelAndView handleConflictDocumentos(HttpServletRequest request, RegraDeNegocioException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(DocumentosException.class)
	public ModelAndView handleConflictDocumentos(HttpServletRequest request, DocumentosException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(EmailException.class)
	public ModelAndView handleConflictEmail(HttpServletRequest request, EmailException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(EnderecoException.class)
	public ModelAndView handleConflictEndereco(HttpServletRequest request, EnderecoException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(TelefoneException.class)
	public ModelAndView handleConflictTelefone(HttpServletRequest request, TelefoneException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(UsuarioException.class)
	public ModelAndView handleConflictUsuario(HttpServletRequest request, UsuarioException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(TokenException.class)
	public ModelAndView handleConflictToken(HttpServletRequest request, TokenException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(ProdutoException.class)
	public ModelAndView handleConflictProduto(HttpServletRequest request, ProdutoException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(FotosException.class)
	public ModelAndView handleConflictFoto(HttpServletRequest request, FotosException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(PesquisaDeProdutosException.class)
	public ModelAndView handlePesquisaDeProdutos(HttpServletRequest request, PesquisaDeProdutosException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(UnknownHostException.class)
	public ModelAndView handleUnknownHost(HttpServletRequest request, UnknownHostException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(MalformedURLException.class)
	public ModelAndView handleMalformedURL(HttpServletRequest request, MalformedURLException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(IOException.class)
	public ModelAndView handleIO(HttpServletRequest request, IOException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(PerguntasRespostasException.class)
	public ModelAndView handlePerguntasRespostas(HttpServletRequest request, PerguntasRespostasException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(CompraVendaException.class)
	public ModelAndView handleCompraVenda(HttpServletRequest request, CompraVendaException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(GatewayDePagamentoException.class)
	public ModelAndView handleMoip(HttpServletRequest request, GatewayDePagamentoException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(ParcelamentoDeValorException.class)
	public ModelAndView handleParcelamentoDeValor(HttpServletRequest request, ParcelamentoDeValorException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(CarrinhoDeCompraException.class)
	public ModelAndView handleCarrinhoDeCompra(HttpServletRequest request, CarrinhoDeCompraException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(RastreamentoException.class)
	public ModelAndView handleRastreamento(HttpServletRequest request, RastreamentoException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(CodigoDeEntregaException.class)
	public ModelAndView handleCodigoDeEntrega(HttpServletRequest request, CodigoDeEntregaException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(CorreiosException.class)
	public ModelAndView handleCorreios(HttpServletRequest request, CorreiosException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(ReputacaoVendedorException.class)
	public ModelAndView handleReputacaoVendedor(HttpServletRequest request, ReputacaoVendedorException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(ContaBancariaGatewayDePagamentoException.class)
	public ModelAndView handleContaBancariaGatewayDePagamento(HttpServletRequest request, ContaBancariaGatewayDePagamentoException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(TransferenciaValoresUsuarioException.class)
	public ModelAndView handleTransferenciaValoresUsuario(HttpServletRequest request, TransferenciaValoresUsuarioException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
	@ExceptionHandler(DesistenciaException.class)
	public ModelAndView handleDesistencia(HttpServletRequest request, DesistenciaException ex) throws UsuarioException, UnknownHostException, MalformedURLException, IOException, ParseException, URISyntaxException, JSONException {
		
		return this.ajusteDePagina(request, ex);
		
	}

	/*
	 * metodo usado por todos os outros para acertar as views de exibicao 
	 */
	private ModelAndView ajusteDePagina(HttpServletRequest request, Exception ex) throws ParseException, URISyntaxException, JSONException {
		
		logger.error(ex.getMessage());
		
		ModelAndView model = null;
		if((request.getHeader("Referer") == null && request.getRequestURL().toString().contains("/usuario")) || (request.getHeader("Referer") != null && request.getHeader("Referer").contains("/usuario"))) {
			model = new ModelAndView(PaginaClient.AREA_USUARIO_VIEW);
			
			UsuarioDTO usuarioDto = null;
			try {
				usuarioDto = usuarioFacade.obterInfosUsuario(request.getRemoteUser());
			} catch (RegraDeNegocioException | IOException  e) {
				usuarioDto = new UsuarioPessoaFisicaDTO();
			}
				
			model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
			model.addObject("pagina", "exception/erro500_usuario_view");
		} else {
			model = new ModelAndView(PaginaClient.PAGINA_VIEW);
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("remoteUser", request.getRemoteUser());
			jsonObject.put("usuarioDto", request.getSession().getAttribute("usuarioDto") != null ? request.getSession().getAttribute("usuarioDto").toString() : null);
			
			UsuarioDTO usuarioDto = null;
			try {
				usuarioDto = usuarioFacade.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
			} catch (RegraDeNegocioException | IOException  e) {
				usuarioDto = new UsuarioPessoaFisicaDTO();
			}
			
			model.addObject("listaDeProdutosPorPagina", ProdutosPorPaginaEnum.values());
			model.addObject("nomeUsuario", NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDto));
			model.addObject("pagina", "exception/erro500");
		}
		
		
		if(ex instanceof InvalidDataAccessApiUsageException) {
			model.addObject("mensagem", "Ocorreu um erro ao inserir suas informações");
		} else {
			model.addObject("mensagem", ex.getMessage());
		}

		model.addObject("url", request.getHeader("Referer"));
		model.addObject("urlApresenta", request.getHeader("Referer") == null ? false : true);
		
		
		return model;
	}	
}
