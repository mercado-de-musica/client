package br.com.mercadodemusica;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		
		String urlCompraDeProduto = (String) request.getSession().getAttribute("urlCompraDeProduto");
		
		String stringUrlFinal = "";
		if(urlCompraDeProduto != null) {
			stringUrlFinal = urlCompraDeProduto;
			
			request.getSession().setAttribute("urlCompraDeProduto", null);
			
		} else {
			
			String requestURL = request.getRequestURL().toString();
			String[] requestCortada = requestURL.split("/");
			
			for(int i = 0; i < requestCortada.length - 1; i++) {
				if(i == 0) {
					stringUrlFinal = stringUrlFinal + requestCortada[i];
				} else {
					stringUrlFinal = stringUrlFinal + "/" + requestCortada[i];
				}
				
			}
			
			stringUrlFinal = stringUrlFinal + "/usuario";
		}
		
		response.sendRedirect(stringUrlFinal);
	}

}
