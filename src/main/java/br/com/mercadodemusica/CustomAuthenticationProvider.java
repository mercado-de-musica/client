package br.com.mercadodemusica;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.ParseException;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.facade.UserDetailsFacade;
import br.com.mercadodemusica.transformacaoDeDados.Senha;


@Component
public class CustomAuthenticationProvider  implements AuthenticationProvider{

	@Autowired
	UserDetailsFacade userDetailsFacade;
	
	private final static Logger logger = Logger.getLogger(CustomAuthenticationProvider.class);
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String login = authentication.getPrincipal().toString();
		
		if(login == null || login.isEmpty()) {
			throw new AuthenticationServiceException("O login não pode estar vazio.");
		}
		
		UserDetails userDetails = null;
		try {
			userDetails = userDetailsFacade.loadUserByUsername(authentication.getPrincipal().toString());
		} catch (IllegalArgumentException | IllegalAccessException | IOException | RegraDeNegocioException | JSONException | ParseException | URISyntaxException e) {
			logger.error(e.getMessage());
			throw new AuthenticationServiceException(e.getMessage());
		}
		
		Senha senha = new Senha();
		String senhaCriptografada = senha.criptografar(authentication.getCredentials().toString() + login);
		
		if(! senhaCriptografada.equals(userDetails.getPassword())) {
			throw new AuthenticationServiceException("A senha está incorreta.");
		}
		
		if(!userDetails.isEnabled()) {
			throw new AuthenticationServiceException("Este usuário não está ativo.");
		}
		
		
		return new UsernamePasswordAuthenticationToken((Object) userDetails.getUsername(), (Object) userDetails.getPassword(), userDetails.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}
}
