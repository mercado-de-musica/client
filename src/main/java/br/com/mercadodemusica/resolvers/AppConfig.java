package br.com.mercadodemusica.resolvers;

//@Configuration
//@ComponentScan("br.com.mercadodemusica")
//@EnableWebMvc
public class AppConfig { //extends WebMvcConfigurerAdapter {
	
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
//	}
//	
//	@Override
//	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//		configurer.enable();
//	}
//	
//	@Override
//	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//		
//		configurer.ignoreAcceptHeader(true).defaultContentType(MediaType.TEXT_HTML).mediaType("json", MediaType.APPLICATION_JSON);
//	
//	}
//	
//	@Bean
//    public JavaMailSender mailSender() {
//        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
//        
//        Properties props = new Properties();
////        props.put("mail.smtp.host", "smtp.gmail.comsmtp.gmail.com");
//        props.put("mail.smtp.host", "smtp.gmail.com");
//        props.put("mail.smtp.socketFactory.port", "587");
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.port", "587");
//        props.put("mail.smtp.starttls.enable", "true");
//        
//        javaMailSender.setJavaMailProperties(props);
//        
//        javaMailSender.setUsername("mercadodemusica@mercadodemusica.com.br");
//        javaMailSender.setPassword("gibson100");
//        
//
//        return javaMailSender;
//    }
//		
//	@Bean
//    public ViewResolver contentNegotiatingViewResolver(ContentNegotiationManager manager) {
//        ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
//        resolver.setContentNegotiationManager(manager);
// 
//        // Define all possible view resolvers
//        List<ViewResolver> resolvers = new ArrayList<ViewResolver>();
// 
////        resolvers.add(jaxb2MarshallingXmlViewResolver());
//        resolvers.add(jsonViewResolver());
//        resolvers.add(jspViewResolver());
////        resolvers.add(pdfViewResolver());
////        resolvers.add(excelViewResolver());
//         
//        resolver.setViewResolvers(resolvers);
//        return resolver;
//    }
//	
//	@Bean
//    public ViewResolver jsonViewResolver() {
//        return new JsonViewResolver();
//    }
//	
//	@Bean
//	public ViewResolver jspViewResolver() {
//		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//        viewResolver.setViewClass(JstlView.class);
//        viewResolver.setPrefix("/WEB-INF/views/");
//        viewResolver.setSuffix(".jsp");
//        viewResolver.setContentType("text/html;charset=utf-8");
//        return viewResolver;
//	}
//	
//	/*
//	 * dozer mapeamentos
//	 * 
//	 */
//	
//	@Bean 
//	public Mapper dozerBeanMapper(){
//		DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
//		ArrayList<String> listaDeXmls = new ArrayList<String>();
//		listaDeXmls.add("dozerMapping.xml");
//		
//		dozerBeanMapper.setMappingFiles(listaDeXmls);
//		
//		return dozerBeanMapper;
//	}
//	
//	/*
//	 * crontab
//	 * 
//	 */
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean cancelarToken() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("CancelarToken");
//		obj.setTargetMethod("cancelarToken");
//		return obj;
//	}
//	
//	
//	@Bean
//	public CronTriggerFactoryBean cronCancelarTokenTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(cancelarToken().getObject());
//		stFactory.setName("Cancelamento de token por hora");
//		stFactory.setGroup("token");
//		stFactory.setCronExpression("0 0/60 * * * ? *");
//		return stFactory;
//	} 
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean envioDePerguntas() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("EnvioDePerguntasRespostas");
//		obj.setTargetMethod("obterListaDePerguntas");
//		return obj;
//	}
//	
//	
//	@Bean
//	public CronTriggerFactoryBean cronEnvioDePerguntasTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(envioDePerguntas().getObject());
//		stFactory.setName("Perguntas");
//		stFactory.setGroup("enviosERecebimentos");
//		stFactory.setCronExpression("0 0/2 * * * ? *");
//		return stFactory;
//	}
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean envioDeRespostas() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("EnvioDePerguntasRespostas");
//		obj.setTargetMethod("obterListaDeRespostas");
//		return obj;
//	}
//	
//	
//	@Bean
//	public CronTriggerFactoryBean cronEnvioDeRespostasTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(envioDeRespostas().getObject());
//		stFactory.setName("Respostas");
//		stFactory.setGroup("enviosERecebimentos");
//		stFactory.setCronExpression("0 0/2 * * * ? *");
//		return stFactory;
//	}
//	
//	
//	/*
//	 * Produtos
//	 */
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean verificacaoDePagamentosEmAnalise() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("Pagamento");
//		obj.setTargetMethod("verificacaoDePagamentosEmAnalise");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronVerificacaoDePagamentosEmAnaliseTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(verificacaoDePagamentosEmAnalise().getObject());
//		stFactory.setName("Verificacao de compra em analise");
//		stFactory.setGroup("Tray checkout");
//		stFactory.setCronExpression("0 0/5 * * * ? *");
//		return stFactory;
//	}
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean emailProdutosInteresseDeCompra() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("ProdutosInteresseDeCompra");
//		obj.setTargetMethod("enviarEmailInteressadoDeProdutoNovo");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronProdutosInteresseDeCompraTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(emailProdutosInteresseDeCompra().getObject());
//		stFactory.setName("Envio de email de interesses de compra");
//		stFactory.setGroup("ComprasDeProdutos");
//		stFactory.setCronExpression("0 0 11,17 * * ? *");
//		return stFactory;
//	}
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean emailEnvioDeProdutoVendido() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("EnvioDeProduto");
//		obj.setTargetMethod("verificarSeProdutoFoiEnviado");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronEnvioDeProdutoVendidoTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(emailEnvioDeProdutoVendido().getObject());
//		stFactory.setName("Envio de produtos");
//		stFactory.setGroup("Envio de e-mail para vendedor");
//		stFactory.setCronExpression("0 15 10 * * ? *");
//		return stFactory;
//	}
//	
//	//correios
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean verificacaoDeContratoComOsCorreios() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("ContaCorreios");
//		obj.setTargetMethod("verificarContaCorreios");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronVerificacaoDeContratoComOsCorreiosTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(verificacaoDeContratoComOsCorreios().getObject());
//		stFactory.setName("Verificação contrato com os correios");
//		stFactory.setGroup("Correios");
//		stFactory.setCronExpression("0 0 2am,2pm * * ? *");
//		return stFactory;
//	}
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean atualizaCodigoEntregaEmMaos() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("EntregaEmMaos");
//		obj.setTargetMethod("criarParDeCodigos");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronAtualizaCodigoEntregaEmMaosTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(atualizaCodigoEntregaEmMaos().getObject());
//		stFactory.setName("Atualizacao de entrega em maos");
//		stFactory.setGroup("Entrega em maos");
//		stFactory.setCronExpression("0 45 4 ? * SUN *");
//		return stFactory;
//	}
//	
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean atualizaContaCorreios() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("ServicoCorreios");
//		obj.setTargetMethod("atualizarServicos");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronAtualizaServicosTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(atualizaContaCorreios().getObject());
//		stFactory.setName("Atualizacao de servicos do correios");
//		stFactory.setGroup("Correios");
//		stFactory.setCronExpression("0 30 3 ? * SUN *");
////		stFactory.setCronExpression("0 00 23 * * ? *");
//		return stFactory;
//	}
//	
//
//
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean atualizaRastreamentoCorreios() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("StatusRastreamentoCorreios");
//		obj.setTargetMethod("atualizarServicos");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronAtualizaRastreamentoCorreiosTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(atualizaRastreamentoCorreios().getObject());
//		stFactory.setName("Atualizacao de servicos do correios");
//		stFactory.setGroup("Correios");
//		stFactory.setCronExpression("0 0 3 * * ? *");
//		return stFactory;
//	}
//	
//	/*
//	 * transferencias de  valores
//	 * 
//	 */
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean atualizarStatusTransferenciaDeValores() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("TransferenciaDeValores");
//		obj.setTargetMethod("verificarStatusTransferenciaDeValores");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronAtualizarStatusTransferenciaDeValoresTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(atualizarStatusTransferenciaDeValores().getObject());
//		stFactory.setName("Atualizacao de transferencia de valores ao vendedor");
//		stFactory.setGroup("tray checkout");
//		stFactory.setCronExpression("0 0 4 * * ? *");
//		return stFactory;
//	}
//	
//	
//	/*
//	 *  sugestao de novos produtos
//	 */
//	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean sugestaoDeNovosProdutos() {
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		obj.setTargetBeanName("SugestaoNovoProduto");
//		obj.setTargetMethod("enviarEmailParaResponsavelDeSugestoes");
//		return obj;
//	}
//	
//	@Bean
//	public CronTriggerFactoryBean cronSugestaoDeNovosProdutosTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(sugestaoDeNovosProdutos().getObject());
//		stFactory.setName("Sugestao de novos produtos ao responsavel");
//		stFactory.setGroup("sugestoes");
//		stFactory.setCronExpression("0 0/43 * * * ? *");
//		return stFactory;
//	}
//	
//	
////	Triggers Juntas
//	
//	
//	@Bean
//	public SchedulerFactoryBean schedulerFactoryBean() {
//		
//		SchedulerFactoryBean quartzScheduler = new SchedulerFactoryBean();
//		quartzScheduler.setOverwriteExistingJobs(true);
//	    
//		Trigger cancelarToken = cronCancelarTokenTriggerFactoryBean().getObject();
//        Trigger envioPerguntas = cronEnvioDePerguntasTriggerFactoryBean().getObject();
//        Trigger envioRespostas = cronEnvioDeRespostasTriggerFactoryBean().getObject();
//        Trigger verificacaoDeCompraEmAnalise = cronVerificacaoDePagamentosEmAnaliseTriggerFactoryBean().getObject();
//        Trigger produtosInteresseDeCompra = cronProdutosInteresseDeCompraTriggerFactoryBean().getObject();
//        Trigger envioDeProdutosVendedor = cronEnvioDeProdutoVendidoTriggerFactoryBean().getObject();
//        Trigger verificacaoDeContratoComOsCorreios = cronVerificacaoDeContratoComOsCorreiosTriggerFactoryBean().getObject();
//        Trigger atualizacaoDeServicosCorreios = cronAtualizaServicosTriggerFactoryBean().getObject();
//        Trigger atualizacaoRastreamentoCorreios = cronAtualizaRastreamentoCorreiosTriggerFactoryBean().getObject();
//        Trigger atualizacaoCodigoEntregaEmMaos = cronAtualizaCodigoEntregaEmMaosTriggerFactoryBean().getObject();
//        Trigger atualizacaoStatusTransferenciaDeValores = cronAtualizarStatusTransferenciaDeValoresTriggerFactoryBean().getObject();
//        Trigger sugestaoDeNovosProdutos = cronSugestaoDeNovosProdutosTriggerFactoryBean().getObject();
//		
//        ArrayList<Trigger> triggersJuntasList = new ArrayList<Trigger>();
//        triggersJuntasList.add(cancelarToken);
//        triggersJuntasList.add(envioPerguntas);
//        triggersJuntasList.add(envioRespostas);
//        triggersJuntasList.add(produtosInteresseDeCompra);
//        triggersJuntasList.add(envioDeProdutosVendedor);
//        triggersJuntasList.add(verificacaoDeCompraEmAnalise);
//        triggersJuntasList.add(verificacaoDeContratoComOsCorreios);
//        triggersJuntasList.add(atualizacaoDeServicosCorreios);
//        triggersJuntasList.add(atualizacaoRastreamentoCorreios);
//        triggersJuntasList.add(atualizacaoCodigoEntregaEmMaos);
//        triggersJuntasList.add(atualizacaoStatusTransferenciaDeValores);
//        triggersJuntasList.add(sugestaoDeNovosProdutos);
//        
//        
//        Trigger[] triggerFinal = triggersJuntasList.toArray(new Trigger[triggersJuntasList.size()]);
//       
//        quartzScheduler.setTriggers(triggerFinal);
//        return quartzScheduler;
//	}
}
