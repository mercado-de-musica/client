package br.com.mercadodemusica.resolvers;

public final class PaginaClient {
	
	public static final String PAGINA_VIEW = "main/main";
	public static final String AREA_USUARIO_VIEW = "area_usuario/usuario_main";
}
