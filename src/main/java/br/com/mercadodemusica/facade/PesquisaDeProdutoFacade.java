package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dto.PesquisaDeProdutosDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.enums.TipoPesquisa;
import br.com.mercadodemusica.exceptions.Excecao;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class PesquisaDeProdutoFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PRODUTO);
	
	public PesquisaDeProdutosDTO pesquisaInicialDeProdutos(UsuarioDTO usuarioDto, BigInteger pagina) throws IOException, RegraDeNegocioException, ParseException, URISyntaxException {
		try {
			
			JSONObject jsonObject = new JSONObject();
			ObjectMapper objectMapper = new ObjectMapper();
			
			jsonObject.put("usuarioDto", new JSONObject(objectMapper.writeValueAsString(usuarioDto)));
			jsonObject.put("pagina", pagina);
						
//			PesquisaDeProdutosDTO pesquisaDeProdutosDto = (PesquisaDeProdutosDTO) GsonJsonString.stringParaObject((String) httpRequestResponse.requisicao(urlModuloProduto.getUrlModulo() + "pesquisaProduto/pesquisaInicialDeProdutos", TipoDeRequisicaoDoSiteEnum.POST, jsonObject, request), PesquisaDeProdutosDTO.class);
			String pesquisaDeProdutosString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "pesquisaProduto/pesquisaInicialDeProdutos", ("pesquisaProduto=" + jsonObject), request.getRemoteAddr());
			
			PesquisaDeProdutosDTO pesquisaDeProdutosDto = objectMapper.readValue(pesquisaDeProdutosString, PesquisaDeProdutosDTO.class);
			return pesquisaDeProdutosDto;
			
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JSONException e) {
			Excecao.chamarExcecao(e);
			return null;
		}
	}

	public PesquisaDeProdutosDTO pesquisaDeProdutos(UsuarioDTO usuarioDto, ProdutoDTO produtoDto, Integer produtosPorPagina, BigInteger pagina, TipoPesquisa tipoPesquisa, BigDecimal precoInicial, BigDecimal precoFinal, JSONArray categoriasJson) throws IOException, RegraDeNegocioException {
		try {
			
			JSONObject jsonObject = new JSONObject();
			ObjectMapper objectMapper = new ObjectMapper();
			
			jsonObject.put("usuarioDto", new JSONObject(objectMapper.writeValueAsString(usuarioDto)));
			
			jsonObject.put("produtoDto", new JSONObject(objectMapper.writeValueAsString(produtoDto)));
			jsonObject.put("pagina", pagina);
			jsonObject.put("produtosPorPagina", produtosPorPagina);
			jsonObject.put("tipoPesquisa", tipoPesquisa);
			jsonObject.put("precoInicial", precoInicial);
			jsonObject.put("precoFinal", precoFinal);
			jsonObject.put("categoriasJson", categoriasJson);
			
			String pesquisaDeProdutosString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "pesquisaProduto/pesquisaDeProdutos", ("pesquisaProduto=" + jsonObject),  request.getRemoteAddr());
			PesquisaDeProdutosDTO pesquisaDeProdutosDto = objectMapper.readValue(pesquisaDeProdutosString, PesquisaDeProdutosDTO.class);
			return pesquisaDeProdutosDto;
			
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JSONException | ParseException | URISyntaxException e) {
			Excecao.chamarExcecao(e);
			return null;
		}
		
	}
}
