package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.Excecao;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class ProdutoFacade {
	
	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PRODUTO);

	public ProdutoDTO obterProdutoPorId(String idProdutoCrypt) throws IOException, RegraDeNegocioException {
		try {
			String produtoString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "produto/obterProdutoPorId", ("idCrypt=" + idProdutoCrypt),  request.getRemoteAddr());
			ObjectMapper objectMapper = new ObjectMapper();
			ProdutoDTO pesquisaDeProdutosDto = objectMapper.readValue(produtoString, ProdutoDTO.class);
			
			return pesquisaDeProdutosDto;
		} catch (IllegalArgumentException | ParseException | IOException| RegraDeNegocioException | URISyntaxException e) {
			Excecao.chamarExcecao(e);
			return null;
		}
		
	}
}
