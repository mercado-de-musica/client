package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDetalheDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.Excecao;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class MapeamentoPesquisaFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PESQUISA);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public void salvarPesquisa(MapeamentoPesquisaDTO mapeamentoPesquisaDto) throws IOException, RegraDeNegocioException {

		try {
			
			ObjectMapper objectMapper = new ObjectMapper();
			String mapeamentoPesquisaString = objectMapper.writeValueAsString(mapeamentoPesquisaDto);
			
			
			HTTPRequestResponse.post(urlModuloProduto.getUrlModulo() + "mapeamentoPesquisa/salvarPesquisa", mapeamentoPesquisaString,  request.getRemoteAddr());
			
		} catch (URISyntaxException | IOException | RegraDeNegocioException | IllegalArgumentException | IllegalAccessException | ParseException | JSONException e) {
			Excecao.chamarExcecao(e);
		}
		
		
	}

	public void salvarPesquisaDetalhe(MapeamentoPesquisaDetalheDTO mapeamentoPesquisaDetalheDto) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(mapeamentoPesquisaDetalheDto, headers);
		
		restTemplate.exchange(
				urlModuloProduto.getUrlModulo() + "mapeamentoPesquisa/salvarPesquisaDetalhe", HttpMethod.POST, httpEntity,
				new ParameterizedTypeReference<Void>() {
				});
	}

	public List<ApresentacaoProdutoDTO> pesquisasFeitasPelosUsuariosPorProduto(String idProdutoCrypt, String ip) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<List<ApresentacaoProdutoDTO>> response = restTemplate.exchange(
				urlModuloProduto.getUrlModulo() + "mapeamentoPesquisa/obterPesquisasFeitasPelosUsuariosPorProduto/" + idProdutoCrypt + "/" + ip + "/", HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<List<ApresentacaoProdutoDTO>>() {
				});
		
		return response.getBody();
	}

	
	public List<ApresentacaoProdutoDTO> comprasEfetuadasPelosUsuariosPorProduto(String idProdutoCrypt, String ip) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<List<ApresentacaoProdutoDTO>> response = restTemplate.exchange(
				urlModuloProduto.getUrlModulo() + "mapeamentoPesquisa/comprasEfetuadasPelosUsuariosPorProduto/" + idProdutoCrypt + "/" + ip + "/", HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<List<ApresentacaoProdutoDTO>>() {
				});
		
		return response.getBody();
	}
	
}
