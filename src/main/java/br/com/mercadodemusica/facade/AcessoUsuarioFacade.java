package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.dto.AcessoUsuarioDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class AcessoUsuarioFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloUsuario = new UrlModulo(ModuloSistemaEnum.USUARIO);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	
	public AcessoUsuarioDTO obterPorUsuarioId(BigInteger id) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<AcessoUsuarioDTO> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "acessoUsuario/obterPorUsuarioId/" + id, HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<AcessoUsuarioDTO>() {
				});
		
		return response.getBody();
	}


	public Boolean verificarEmail(AcessoUsuarioDTO acessoUsuarioDto) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<Boolean> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "acessoUsuario/verificarEmail/" + acessoUsuarioDto.getEmail(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<Boolean>() {
				});
		
		return response.getBody();
	}
	
	
}
