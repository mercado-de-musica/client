package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonSyntaxException;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaJuridicaDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.Excecao;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class UsuarioFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloUsuario = new UrlModulo(ModuloSistemaEnum.USUARIO);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private final Logger logger = LogManager.getLogger(UsuarioFacade.class);
	
	public void cadastrarUsuarioPessoaFisica(UsuarioPessoaFisicaDTO usuarioPessoaFisicaDto) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(usuarioPessoaFisicaDto, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "usuario/cadastrarUsuarioPessoaFisica", HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
		}); 
	}
	
	public void cadastrarUsuarioPessoaJuridica(UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDto) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(usuarioPessoaJuridicaDto, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "usuario/cadastrarUsuarioPessoaJuridica", HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
		});
	}
	
	public UsuarioDTO obterInfosUsuarioPorCpfCnpj(String cpfCnpj) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String retornoString = (String) HTTPRequestResponse.get(urlModuloUsuario.getUrlModulo() + "usuario/obterInfosUsuarioPorCpfCnpj/" + cpfCnpj, null,  request.getRemoteAddr());
			UsuarioDTO usuarioDto = objectMapper.readValue(retornoString, UsuarioDTO.class);
			return usuarioDto;
			
//			return this.retornarTipoUsuarioDto(retornoString);
		
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JsonSyntaxException e) {
			logger.error(e.getMessage());
			Excecao.chamarExcecao(e);
			return null;
		}
	}
	
	public Boolean verificarCpfNaoExistente(UsuarioPessoaFisicaDTO usuarioDto) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<Boolean> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "usuario/verificarCpfNaoExistente/" + usuarioDto.getCpf(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<Boolean>() {
				});
		
		return response.getBody();
	}

	public Boolean verificarCnpjNaoExistente(UsuarioPessoaJuridicaDTO usuarioDto) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<Boolean> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "usuario/verificarCnpjNaoExistente/" + URLEncoder.encode(usuarioDto.getCnpj(), "UTF-8"), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<Boolean>() {
				});
		
		return response.getBody();
	}

	
			/*
			 * erik verificar
			 */
	public Boolean verificarCpfPessoaJuridicaNaoExistente(UsuarioPessoaJuridicaDTO usuarioDto) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<Boolean> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "usuario/verificarCpfPessoaJuridicaNaoExistente/" + usuarioDto.getCpfTitular(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<Boolean>() {
				});
		
		return response.getBody();
	}

	public Boolean verificarDataDeNascimento(String dataDeNascimentoString) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, JSONException, URISyntaxException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<Boolean> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "usuario/verificarDataDeNascimento/" + URLEncoder.encode(dataDeNascimentoString, "UTF-8"), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<Boolean>() {
				});
		
		return response.getBody();
	}

	public UsuarioDTO obterUsuarioEnderecoPesquisaProdutos(JSONObject jsonObject) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, URISyntaxException {
		
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String retornoString = (String) HTTPRequestResponse.get(urlModuloUsuario.getUrlModulo() + "usuario/obterUsuarioEnderecoPesquisaProdutos", ("jsonObject=" + jsonObject),  request.getRemoteAddr());
			UsuarioDTO usuarioDto = objectMapper.readValue(retornoString, UsuarioDTO.class);
			return usuarioDto;
			
//			return this.retornarTipoUsuarioDto(retornoString);
		
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JsonSyntaxException e) {
			logger.error(e.getMessage());
			Excecao.chamarExcecao(e);
			return null;
		}
	}

	public UsuarioDTO obterInfosUsuario(String remoteUser) throws IOException, RegraDeNegocioException, org.apache.http.ParseException, URISyntaxException {
		try {
			String retornoString = (String) HTTPRequestResponse.get(urlModuloUsuario.getUrlModulo() + "usuario/obterInfosUsuario", remoteUser,  request.getRemoteAddr());
			return null;
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JsonSyntaxException e) {
			Excecao.chamarExcecao(e);
			return null;
		}
	}

	public void liberarUsuarioPorToken(UsuarioDTO usuarioDto) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(usuarioDto, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "usuario/liberarUsuario", HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
		}); 
	}

	
//	private UsuarioDTO retornarTipoUsuarioDto(String retornoString) throws JSONException, JsonSyntaxException, ClassNotFoundException, ParseException, RegraDeNegocioException {
//		
//		JSONObject jsonObj = new JSONObject(retornoString);
//		String tipoDtoUsuario = jsonObj.getString("tipoDtoUsuario");
//		UsuarioDTO usuarioDto = null;
//		
//		for(TipoDtoUsuarioEnum tipoDtoUsuarioEnum : TipoDtoUsuarioEnum.values()) {
//			if(tipoDtoUsuario != null && tipoDtoUsuarioEnum.toString().equals(tipoDtoUsuario)) {
//				
//				JsonCoordinatesCalendar jsonCoordinatesCalendar = new JsonCoordinatesCalendar();
//				jsonObj = jsonCoordinatesCalendar.retiradaDeCoordinatesECalendar(jsonObj);
//				
//				String jsonObjString = jsonObj.toString();
//				usuarioDto = (UsuarioDTO) SerializerDeserializer.stringParaObject(jsonObjString, Class.forName(tipoDtoUsuarioEnum.getNomeClasse()));
//						
//				
//				jsonCoordinatesCalendar.setDataDeCadastramentoUsuarioDTO(usuarioDto);
//				jsonCoordinatesCalendar.setDataDeUpdateUsuarioDTO(usuarioDto);
//				jsonCoordinatesCalendar.setCoordinatesListUsuarioDTO(usuarioDto);
//			}
//		}
//		
//		if(usuarioDto != null) {
//			return usuarioDto;
//		} else {
//			throw new RegraDeNegocioException("Não foi possível transformar os dados desta requisição");
//		}
//	}
}
