package br.com.mercadodemusica.facade;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.dto.CategoriaDTO;
import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class LeadFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloUsuario = new UrlModulo(ModuloSistemaEnum.USUARIO);
	
	private final Logger logger = LogManager.getLogger(LeadFacade.class);
	
	RestTemplate restTemplate = new RestTemplate();
	
	public void inserirLead(LeadDTO leadDto) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(leadDto, headers);
		
		
		restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "lead/inserirLead", HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
		});
	}


	public LeadDTO obterLeadPorEmail(LeadDTO leadDto) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<LeadDTO> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "lead/obterLeadPorEmail/" + leadDto.getEmail(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<LeadDTO>() {
				});
		
		return response.getBody();
	}

}
