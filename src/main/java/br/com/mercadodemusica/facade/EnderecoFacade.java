package br.com.mercadodemusica.facade;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class EnderecoFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloUsuario = new UrlModulo(ModuloSistemaEnum.USUARIO);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public String obterEnderecoPorCep(String cep) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "endereco/obterEnderecoPorCep/" + cep, HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<String>() {
				});
		
		return response.getBody();
	}

	
	
}
