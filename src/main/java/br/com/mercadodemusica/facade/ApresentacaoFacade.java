package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.CategoriaDTO;
import br.com.mercadodemusica.dto.FotoUsuarioDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.Excecao;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class ApresentacaoFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PRODUTO);
	
	private RestTemplate restTemplate = new RestTemplate();

	public List<ApresentacaoProdutoDTO> obterlistaDeApresentacaoPorProduto(ProdutoDTO produtoDto) throws IOException, RegraDeNegocioException, ParseException, URISyntaxException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			JSONObject produtoJson = new JSONObject(objectMapper.writeValueAsString(produtoDto));
			
			produtoJson.remove("descricao");
			
			String listaDeApresentacoesString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "apresentacao/obterlistaDeApresentacaoPorProduto", ("produtoDto=" + produtoJson), request.getRemoteAddr());
			
			List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = objectMapper.readValue(listaDeApresentacoesString, new TypeReference<List<ApresentacaoProdutoDTO>>(){});
			
			return listaDeApresentacoesDto;
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JSONException e) {
			Excecao.chamarExcecao(e);
			return null;
		}
		
	}

	public FotoUsuarioDTO obterFotoUsuario(UsuarioDTO usuarioDto) throws IOException, RegraDeNegocioException {
		
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			JSONObject produtoJson = new JSONObject(objectMapper.writeValueAsString(usuarioDto));
		
			String fotoUsuarioString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "apresentacao/obterFotoUsuario", ("usuarioDto=" + produtoJson), request.getRemoteAddr());
			
			FotoUsuarioDTO fotoUsuarioDto = objectMapper.readValue(fotoUsuarioString, FotoUsuarioDTO.class);
			
			return fotoUsuarioDto;
			
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JSONException | URISyntaxException e) {
			Excecao.chamarExcecao(e);
			return null;
		}
	}
}
