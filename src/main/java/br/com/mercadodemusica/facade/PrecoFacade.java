package br.com.mercadodemusica.facade;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.entities.Preco;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class PrecoFacade {

private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PRODUTO);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public List<Preco> obterHistoricoDePreco(String idProdutoCrypt, String ip) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", ip);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<List<Preco>> response = restTemplate.exchange(
				urlModuloProduto.getUrlModulo() + "preco/obterHistoricoDePreco/" + idProdutoCrypt, HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<List<Preco>>() {
				});
		
		return response.getBody();
		
	}
	
}
