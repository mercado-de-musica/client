package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class UserDetailsFacade {
	
	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloUsuario = new UrlModulo(ModuloSistemaEnum.USUARIO);
	
	public UserDetails loadUserByUsername(String string) throws IllegalArgumentException, IllegalAccessException, IOException, RegraDeNegocioException, JSONException, ParseException, URISyntaxException {
		return (UserDetails) HTTPRequestResponse.get(urlModuloUsuario.getUrlModulo() + "userDetails/loadUserByUsername", string,  request.getRemoteAddr());
	}

}
