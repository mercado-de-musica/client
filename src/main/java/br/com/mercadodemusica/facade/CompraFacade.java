package br.com.mercadodemusica.facade;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dto.ProdutoCompraDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.Excecao;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class CompraFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PEDIDO);
	
	public void insercaoCompra(ProdutoCompraDTO produtoCompraDto) throws IOException, RegraDeNegocioException {
		
		try {
			
			ObjectMapper objectMapper = new ObjectMapper();
			String produtoString = objectMapper.writeValueAsString(produtoCompraDto);
			
			JSONObject produtoJson = new JSONObject(produtoString);
			
			HTTPRequestResponse.post(urlModuloProduto.getUrlModulo() + "compra/insercaoCompra", produtoJson,  request.getRemoteAddr());
			
			
		} catch (IllegalArgumentException | IOException | RegraDeNegocioException | JSONException | IllegalAccessException | ParseException | URISyntaxException e) {
			Excecao.chamarExcecao(e);
		}		
	}
	
}
