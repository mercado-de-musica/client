package br.com.mercadodemusica.facade;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.dto.SubCategoriaDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class CategoriaFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PRODUTO);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public List<SubCategoriaDTO> obterSubcategoriasPorCategoria(String idCategoria, String pesquisa) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<List<SubCategoriaDTO>> response = restTemplate.exchange(
		urlModuloProduto.getUrlModulo() + "subcategoria/buscarSubcategoriasPorIdCategoriaPesquisa/" + idCategoria + "/" + pesquisa, HttpMethod.GET, httpEntity,
			new ParameterizedTypeReference<List<SubCategoriaDTO>>() {
		});
		
		
		return response.getBody();
	}
}
