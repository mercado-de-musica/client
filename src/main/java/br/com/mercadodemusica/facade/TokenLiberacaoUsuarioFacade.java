package br.com.mercadodemusica.facade;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.dto.TokenLiberacaoUsuarioDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class TokenLiberacaoUsuarioFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloUsuario = new UrlModulo(ModuloSistemaEnum.USUARIO);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	
	public TokenLiberacaoUsuarioDTO criarTokenDeLiberacao(BigInteger idUsuario) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<TokenLiberacaoUsuarioDTO> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "tokenLiberacaoUsuario/criarTokenDeLiberacaoPorIdUsuario/" + idUsuario, HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<TokenLiberacaoUsuarioDTO>() {
				});
		
		return response.getBody();
		
		
	}


	public TokenLiberacaoUsuarioDTO obterPorToken(String token) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		ResponseEntity<TokenLiberacaoUsuarioDTO> response = restTemplate.exchange(
				urlModuloUsuario.getUrlModulo() + "tokenLiberacaoUsuario/obterPorToken/" + token, HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<TokenLiberacaoUsuarioDTO>() {
				});
		
		return response.getBody();
	}


	public void inativarToken(TokenLiberacaoUsuarioDTO tokenLiberacaoUsuarioDto) {
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(tokenLiberacaoUsuarioDto, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "tokenLiberacaoUsuario/inativarToken", HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
		}); 
		
	}
}
