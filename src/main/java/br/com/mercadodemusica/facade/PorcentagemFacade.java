package br.com.mercadodemusica.facade;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.dto.PorcentagemMercadoDeMusicaDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class PorcentagemFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PEDIDO);
	
	public PorcentagemMercadoDeMusicaDTO obterPorcentagemAtiva() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<PorcentagemMercadoDeMusicaDTO> pAppproResponse = restTemplate.exchange(urlModuloProduto.getUrlModulo() + "porcentagem/obterPorcentagemAtiva", HttpMethod.GET, httpEntity, 
				new ParameterizedTypeReference<PorcentagemMercadoDeMusicaDTO>() {
		}); 
		
		return pAppproResponse.getBody();
	}
	
	
}
