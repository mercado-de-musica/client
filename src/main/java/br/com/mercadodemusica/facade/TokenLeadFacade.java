package br.com.mercadodemusica.facade;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.dto.TokenLeadDTO;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.properties.UrlModulo;

@Service
public class TokenLeadFacade {

	@Autowired
	private HttpServletRequest request;
	
	private static UrlModulo urlModuloUsuario = new UrlModulo(ModuloSistemaEnum.USUARIO);
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public TokenLeadDTO criarTokenLead(LeadDTO leadDto) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(leadDto, headers);
		ResponseEntity<TokenLeadDTO> response = restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "tokenLead/criarTokenLead", HttpMethod.POST, httpEntity, new ParameterizedTypeReference<TokenLeadDTO>() {
		}); 
		
		
		return response.getBody();
	}

	public void confirmarLeadFacade(String token) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(token, headers);
		restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "tokenLead/confirmarLeadFacade", HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
		}); 
		
	}

	public TokenLeadDTO obterPorToken(String token) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		ResponseEntity<TokenLeadDTO> response = restTemplate.exchange(urlModuloUsuario.getUrlModulo() + "tokenLead/obterPorToken/" + token, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<TokenLeadDTO>() {
		}); 
		
		
		return response.getBody();
	}

}
