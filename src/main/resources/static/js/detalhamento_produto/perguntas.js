	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}



$('#enviar_pergunta').click(function(){
	var perguntaAoVendedor = $('#pergunta_ao_vendedor').val();
	var idProduto = $('#id_produto_pergunta').val();
	
	
	$.ajax({
		url: urlFinal + "envio_de_pergunta.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto: idProduto, perguntaAoVendedor : perguntaAoVendedor, _csrf: $('#token').val()}
	}).success(function(){
		
		$("#pergunta_ok_modal").modal("show");
		
	}).fail(function(erro){
		if(erro.responseJSON !== undefined && erro.responseJSON != null && erro.responseJSON.mensagem !== undefined && erro.responseJSON.mensagem != null) {
			$("#erro_modal").find("#texto-erro-generico").each(function(){
				$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
			});
		} else {
			$("#erro_modal").modal("show");
		}
	});
	
});

