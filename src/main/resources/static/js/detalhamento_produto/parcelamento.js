	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}


$('#parcelamento_quantidade').change(function(){
		
	urlCortada = url.split('/');
	
	$.ajax({
		url: urlFinal + "verificacao_parcelamento_valores.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto: urlCortada[urlCortada.length - 1] , parcela: $("#parcelamento_quantidade option:selected").val()}
	}).success(function(dados){
		
		$('.valores-prestacao').remove();
		
//		bandeira
		for(var i = 0; i < dados.length; i++) {
//			Haja for!!!!!
			for(var j = 0; j < dados[i].listaDeParcelasValoresGatewayDePagamento.length; j++) {
				for (k in dados[i].listaDeParcelasValoresGatewayDePagamento[j].mapVezesValor) {
					$('#valores-prestacao-id').append('<p class="text-right valores-prestacao"><b><span>R$ ' + dados[i].listaDeParcelasValoresGatewayDePagamento[j].mapVezesValorString[parseInt(k)] + 
							'</span></b> no total de <b>R$ ' + dados[i].listaDeParcelasValoresGatewayDePagamento[j].totalParcelamentoString + '</b> na bandeira <b>' + dados[i].instituicao + '</b></p>');
				}
				
			}
		}
		
		
//		<c:forEach items="${listaDeParcelamentoDto}" var="parcelamentoDto">
//		<c:forEach items="${parcelamentoDto.listaDeParcelasValoresGatewayDePagamento}" var="parcelasValoresGatewayDePagamento">
//			<c:forEach items="${parcelasValoresGatewayDePagamento.mapVezesValor}" var="map">
//				<p class="text-right valores-prestacao"><b><span>R$ ${map.value}</span></b> no total de <b>R$ ${parcelasValoresGatewayDePagamento.totalParcelamento}</b> na bandeira <b>${parcelamentoDto.instituicao.descricao}</b></p>
//			</c:forEach>
//		</c:forEach>	
//	</c:forEach>
		
//		<p class="text-right valores-prestacao"><b><span>R$ ${map.value}</span></b> no total de <b>R$ ${parcelasValoresGatewayDePagamento.totalParcelamento}</b> na bandeira <b>${parcelamentoDto.instituicao.descricao}</b></p>
	}).fail(function(erro){
		$("#erro_modal_parcelamento").modal("show");
	});
	
});