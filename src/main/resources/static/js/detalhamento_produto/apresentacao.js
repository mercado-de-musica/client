
$('.thumbs_detalhamento').mouseover(function(){
	$('.thumbs_detalhamento').css('background-color', '#fff');
	$(this).css('background-color', '#00a4cc');
});


$('.thumbs_detalhamento').mouseout(function() {
	$(this).css('background-color', '#fff');
});



function mudarFotoPrincipal(dom) {
	
	var endereco = $(dom).attr('src');
	var enderecoArray = endereco.split('_');
	
//	var enderecoFinal = ''
//	for(var i = 0; i < endereceoArray.length - 1; i++) {
//		
//		if(endereceoArray[i].indexOf('mercado') > -1 || endereceoArray[i] == 'de' || endereceoArray[i].indexOf('musica') > -1) {
//			enderecoFinal = enderecoFinal + endereceoArray[i] + "_";
//		} else {
//			enderecoFinal = enderecoFinal + endereceoArray[i];
//		}
//		
//	}
	
	$('#apresentacao_principal audio').remove();
	$('#apresentacao_principal video').remove();
	$('#apresentacao_principal img').remove();
	
	
	if(enderecoArray.length == 1) {
		$('#apresentacao_principal').html('<img src="' + enderecoArray + '" class="img-responsive img-thumbnail" />');
	} else {
		var finalizacaoArray = enderecoArray[enderecoArray.length - 1].split('\.');
		$('#apresentacao_principal').html('<img src="' + enderecoArray[0] + '.' + finalizacaoArray[1] + '" class="img-responsive img-thumbnail" />');
	}
		
}


function mudarAudioPrincipal(endereco, tipoArquivo) {
	
	$('#apresentacao_principal audio').remove();
	$('#apresentacao_principal video').remove();
	$('#apresentacao_principal img').remove();
	
	$('#apresentacao_principal').html('<audio controls id="audio_apresentacao_principal"><source src="' + endereco + '" type="audio/'+tipoArquivo+'"></audio>');
	$("audio").trigger('load');
	$('audio').trigger('play');

	
}

function mudarVideoPrincipal(endereco, width, height) {
		
	$('#apresentacao_principal audio').remove();
	$('#apresentacao_principal video').remove();
	$('#apresentacao_principal img').remove();
	
	$('#apresentacao_principal').html('<div class="embed-responsive embed-responsive-16by9 img-responsive img-thumbnail"><iframe width="'+(width * 100 / 17)+'" height="'+(height * 100 / 17)+'" src="'+endereco+'?autoplay=1" allowfullscreen></iframe></div>');	
}

