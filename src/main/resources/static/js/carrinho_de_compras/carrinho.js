	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}


$('#botao_adicionar_carrinho_de_compras').click(function(){
	var url = window.location.href.toString(); 
	urlArray = url.split('/');
	var numeroDoProduto = urlArray[urlArray.length - 1];
	
	
	$.ajax({
		url: urlFinal + "inserir_carrinho_de_compras.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : numeroDoProduto}
	}).success(function(){
		$('#botao_adicionar_carrinho_de_compras').attr('disabled', true);
		$('#insercao_carrinho_de_compras_ok').modal("show");
	}).fail(function(erro){
		if(erro.responseJSON !== undefined && erro.responseJSON.mensagem !== undefined && erro.responseJSON.mensagem == 'offline') {
			window.location = urlFinal + "login";
		} else {
			$('#insercao_carrinho_de_compras_erro').modal("show");
		}
	});
});


function verificarRemocaoProduto(idProduto) {
	
	$('#confirma_exclusao_produto_carrinho').attr("onclick", "confirmacaoExclusao("+idProduto+")");
	$('#verificacao_remocao_carrinho_de_compras').modal("show");
}

function confirmacaoExclusao(idProduto) {
	
	$.ajax({
		url: urlFinal + "remover_produto_do_carrinho_de_compras.json",
	    dataType: "text",
	    contentType: "application/json",
		data: { idProduto : idProduto}
	}).success(function(retorno){		
		$('#produto_' + idProduto).remove();
		$('#hr_' + idProduto).remove();
		if(retorno == null || retorno == "") {
			$('#div_fechar_pedido').remove();
			$('#div_valor_total').remove();
			
			$('#carrinho_de_compras_vazio').modal("show");
		} else {
			$('#valor_total').html("Valor total: <b>R$ " + retorno + "</b>");
			$('#remocao_carrinho_de_compras_ok').modal("show");
		}
		
	}).fail(function(erro){	
		$('#remocao_carrinho_de_compras_erro').modal("show");
	});
	
}










//produtos com aviso de distancia

$.ajax({
	url: urlFinal + "produtos_com_aviso_de_entrega.json",
    dataType: "json",
    contentType: "application/json",
}).success(function(retornoLista){
	if(retornoLista != null && retornoLista.length != 0) {				
		var pComTexto = "";
		for(var i = 0; i < retornoLista.length; i++) {
		
			var descricao = '';
			if(retornoLista[i].tipoEnvio == 'EM_MAOS') {
				descricao = 'A combinar entre as partes';
			} else if(retornoLista[i].tipoEnvio == 'CORREIOS') {
				descricao = 'Correios com frete pago pelo comprador';
			}
			
			pComTexto = pComTexto + '<div><p>'+retornoLista[i].infosGeraisProduto.instrumentoAcessorio.nome+' '+
			retornoLista[i].infosGeraisProduto.marca.nome+' '+retornoLista[i].infosGeraisProduto.produto.nome+' '+retornoLista[i].infosGeraisProduto.modelo.nome+'</p><p>Valor: <b>R$ ' + retornoLista[i].precoVisualizacaoString + '</b></p>'; 
			
			if(retornoLista[i].usuario.razaoSocial !== undefined && retornoLista[i].usuario.razaoSocial != null) {
				if(retornoLista[i].usuario.nomeFantasia !== undefined && retornoLista[i].usuario.nomeFantasia != null) {
					pComTexto = pComTexto = '<p>Vendedor: <b>' + retornoLista[i].usuario.nomeFantasia + '</b></p>';
				} else {
					pComTexto = pComTexto = '<p>Vendedor: <b>' + retornoLista[i].usuario.razaoSocial + '</b></p>';
				}
			} else {
				pComTexto = pComTexto + '<p>Vendedor: <b>' + retornoLista[i].usuario.nome + ' ' + retornoLista[i].usuario.sobrenome + '</b></p>';
			}
			
			pComTexto = pComTexto + '<p>Tipo de entrega: <b>' + descricao + '</b></p></div><hr />';
		}
		
		$('.escrita_de_produtos_div').html(pComTexto);
		$('#produto_com_aviso_de_entrega').modal("show");
	}
});

//


function estouCienteDaDistancia() {
	$.ajax({
		url: urlFinal + "estou_ciente_da_distancia.json",
	    dataType: "json",
	    contentType: "application/json",
	}).success(function(){
	}).fail(function(erro){	
		$('#ciente_erro').modal("show");
	});
}




// confirmacao de fechamento de pedido

$('#fechar_pedido').click(function(){
	$('#fechar_pedido_formulario').submit();
});




$('#modal_enviar_cartao_de_credito_novo').click(function(){
	var mostrarModalPergunta = true;
	
	if($("select[name='expireMonth'] option:selected").val() === undefined || $("select[name='expireMonth'] option:selected").val() == "") {
		$('#favor_escolher_mes_de_warning').modal("show");
		mostrarModalPergunta = false;
	}
	
	if($("input[name='number']").val() === undefined || $("input[name='number']").val() == "") {
		$('#favor_colocar_numero_do_cartao').modal("show");
		mostrarModalPergunta = false;
	}
	
	if($("select[name='expireYear'] option:selected").val() === undefined || $("select[name='expireYear'] option:selected").val() == "") {
		$('#favor_escolher_ano_de_warning').modal("show");
		mostrarModalPergunta = false;
	}
	
	if($("input[name='cvv2']").val() === undefined || $("input[name='cvv2']").val() == "") {
		$('#favor_colocar_codigo_seguranca').modal("show");
		mostrarModalPergunta = false;
	}
	
	if($("input[name='nomeCartao']").val() === undefined || $("input[name='nomeCartao']").val() == "") {
		$('#favor_colocar_nome').modal("show");
		mostrarModalPergunta = false;
	}
	
	if(mostrarModalPergunta) {
		aceitePoliticaDeCompraModal('cartaoDeCredito');
	}
});


function aceitePoliticaDeCompraModal(tipoDeCompra) {
	
	if(tipoDeCompra == 'cartaoDeCredito') {
		$('#tipo_de_ok_politica_de_compra').attr('onclick', 'aceiteDePoliticaDeCompraCartaoCredito()');
	
	} else {
		$('#tipo_de_ok_politica_de_compra').attr('onclick', 'aceiteDePoliticaDeCompraBoletoBancario()');
	} 
	
	
	$('#aceite_politica_de_compra').modal("show");
}

function aceiteDePoliticaDeCompraBoletoBancario() {
	
	waitingDialog.show('Aguarde');
	window.location = urlFinal + "compra_boleto";	
}

function aceiteDePoliticaDeCompraCartaoCredito() {
	
	waitingDialog.show('Aguarde');
	
	$('.img-cartoes-de-credito').each(function( index ) {
		if($(this).hasClass('cartao-escolhido')) {
			$("input[name='instituicao']").val($(this).attr('id').toUpperCase());
		}
	});
	
	$("input[name='armazenamento']").val(false);
	$('#form_enviar_cartao_de_credito_novo').submit();
	

}


$('#parcelamento_quantidade_total').change(function(){
	
	valoresParcelamentoCartoesDeCredito();
	
});


$('.escolha_de_frete_class').change(function(){
	var dom = $(this );
	$.ajax({
		url: urlFinal + "mudanca_frete.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idFrete: $("option:selected", dom).val()}
	}).success(function(dados){
		$('#valor_total').html('Valor total: <b>R$ ' + dados.valorTotal + '</b>');
		
		for(var i = 0; i < dados.listaApresentacoesDoDia.length; i++ ) {
			if(dados.listaApresentacoesDoDia[i].infosGeraisProdutoUsuario.tipoEnvio.sigla == 'CORREIOS') {
				
				for(var j = 0; j < dados.listaApresentacoesDoDia[i].infosGeraisProdutoUsuario.calculoFrete.length; j++) {
					if(dados.listaApresentacoesDoDia[i].infosGeraisProdutoUsuario.calculoFrete[j].ativoParaCompra === true) {
						$('#preco_do_produto_frete_' + dados.listaApresentacoesDoDia[i].infosGeraisProdutoUsuario.id).html(
							'Valor: <b>R$ '+ dados.listaApresentacoesDoDia[i].infosGeraisProdutoUsuario.precoString + '</b> + ' + 
							'<b>'+dados.listaApresentacoesDoDia[i].infosGeraisProdutoUsuario.calculoFrete[j].valorString+'</b> de frete');
					}
				}
				
//				<c:if test="${frete.ativoParaCompra == true}">
				
				
			}	
		}
		
	}).fail(function(erro){
		$("#erro_modal_generico").modal("show");
	});
});



$('#number').keyup(function(){
	
	$('.img-cartoes-de-credito').removeClass('cartao-escolhido').css('background-color', '#FFF');
	
	
	if($(this).val().length >= 15) {
		var numeros = $(this).val();
		var retornoMarca = moip.creditCard.cardType(numeros); 
		if(retornoMarca != null) {
			
			switch (retornoMarca.brand) {
				case "MASTERCARD":
					$('#mastercard').addClass('cartao-escolhido');
					$('#mastercard').css('background-color', '#e8e8e8');
					break;
				case "VISA":
					$('#visa').addClass('cartao-escolhido');
					$('#visa').css('background-color', '#e8e8e8');
					break;	
				case "AMEX":
					$('#amex').addClass('cartao-escolhido');
					$('#amex').css('background-color', '#e8e8e8');
					break;
				case "HIPERCARD":
					$('#hipercard').addClass('cartao-escolhido');
					$('#hipercard').css('background-color', '#e8e8e8');
					break;
				case "HIPER":
					$('hiper').addClass('cartao-escolhido');
					$('hiper').css('background-color', '#e8e8e8');
					break;
				case "ELO":
					$('#elo').addClass('cartao-escolhido');
					$('#elo').css('background-color', '#e8e8e8');
					break;
			}
			
			valoresParcelamentoCartoesDeCredito();
			
		} else if(retornoMarca == null && numeros.length == 16) {
			$('#erro_cartao_de_credito').modal('show')
		}
		
		
	}
});

$('#cvv2').change(function(){
	var codigoDeSeguranca = $(this);
	var numeroDoCartao = $('#number').val();
	
	if($(codigoDeSeguranca).val().length > 0) {
		var cartaoValido = moip.creditCard.isSecurityCodeValid(numeroDoCartao, $(codigoDeSeguranca).val());
		if(!cartaoValido) {
			$('#cartao-invalido-erro').modal('show')
		}
	}
});


function focusCodigoDeSeguranca(){
	$('#cvv2').focus();
}

function valoresParcelamentoCartoesDeCredito() {
	var bandeiraCartao = null;
	$('.img-cartoes-de-credito').each(function( index ) {
		if($(this).hasClass('cartao-escolhido')) {
			bandeiraCartao = $(this).attr('id');
		}
		
	});
	
	$.ajax({
		url: urlFinal + "verificacao_parcelamento_total.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { parcela: $("#parcelamento_quantidade_total option:selected").val(), bandeiraCartao : bandeiraCartao}
	}).success(function(dados){
		
		$('.valores-parcelas-insercao-dados').remove();
		
//		bandeira
		for(var i = 0; i < dados.length; i++) {
//			Haja for!!!!!
			for(var j = 0; j < dados[i].listaDeParcelasValoresGatewayDePagamento.length; j++) {
				for (k in dados[i].listaDeParcelasValoresGatewayDePagamento[j].mapVezesValor) {
					$('#valores-parcelas-insercao-dados-id').append('<p class="valores-parcelas-insercao-dados"><b>R$ <span>' + dados[i].listaDeParcelasValoresGatewayDePagamento[j].mapVezesValorString[parseInt(k)] + 
							'</span></b> no total de <b><span>R$ ' + dados[i].listaDeParcelasValoresGatewayDePagamento[j].totalParcelamentoString + '</span></b> na bandeira <b>' + dados[i].instituicao + '</b></p>');
				}
				
			}
		}
		
	}).fail(function(erro){
		$("#erro_modal_generico").modal("show");
	});
}


function irParaLoja(idCrypt) {
	
	$.ajax({
		url: urlFinal + "insercao_compra_ir_para_loja.json",
	    dataType: "text",
	    contentType: "application/json",
		data: { idCrypt: idCrypt }
	}).success(function(url){
		window.open(url, '_blank');
//		nofollow external
		
		
		
	}).fail(function(erro){
		$("#erro_modal_generico").modal("show");
	});
}


function cadastrarLead() {
	$("#lead-preenchido").html('');
	
	if($('#nome-lead').val() == undefined || $('#nome-lead').val() == '') {
		$("#lead-preenchido").html('<p class="text-center">Seu nome precisa ser preenchido</p>');
		return;
	}
	
	if($('#email-lead').val() == undefined || $('#email-lead').val() == '') {
		$("#lead-preenchido").html('<p class="text-center">Seu e-mail precisa ser preenchido</p>');
		return;
	}
	
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test($('#email-lead').val())) {
    	$("#lead-preenchido").html('<p class="text-center">Este não é um e-mail válido</p>');
    	return
    }
    
	var valoresVoceToca = $("select[name='voce-toca[]']").select2("val");
	
	if(valoresVoceToca === undefined || valoresVoceToca == null || valoresVoceToca.length == 0) {
		$("#lead-preenchido").html('<p class="text-center">Você precisa preencher seus interesses</p>');
		return;
	}

	var objeto = {};
	objeto.nome = $('#nome-lead').val();
	objeto.email = $('#email-lead').val();
	objeto.interesses = [];
	
	for(var i = 0; i < valoresVoceToca.length; i++) {
		var objetoInteresse = {};
		objetoInteresse.nome = valoresVoceToca[i];
		objeto.interesses.push(objetoInteresse);
	}
	
	var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
	
    waitingDialog.show('Aguarde');
    
	$.ajax({
		url:"/insercaoLead.json",
        contentType: "application/json",
	    beforeSend: function(xhr){
	    	xhr.setRequestHeader(header, token);
	    },
	    type: "POST",
		data: JSON.stringify(objeto)
	}).success(function(){
		waitingDialog.hide();
		$("#modal-lead").css("display", 'none');
		$("#aviso-de-envio-email").modal("show");
	}).fail(function(erro){
		$("#erro_modal_generico").modal("show");
		waitingDialog.hide();
	});
}
