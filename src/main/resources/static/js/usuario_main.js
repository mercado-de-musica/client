
$('.datepicker').datepicker({
	format: 'dd/mm/yyyy',                
	language: 'pt-BR',
});

var headerClasseAntiga = [];
/*
 * hook pra fazer aparecer o loading nas requisicoes ajax
 */
$(document).bind("ajaxSend", function(){
	var url = window.location.href.toString();
	if(url.indexOf("lista_de_likes") == -1) {
		waitingDialog.show('Aguarde');
	}
}).bind("ajaxComplete", function(){
	waitingDialog.hide();
});


$("#menu-toggle").click(function() {
//    e.preventDefault();
    
	var larguraTela = $(document).width();
	
	if(larguraTela < 992) {
			
		if(larguraTela <= 767) {
			var larguradesejada = larguraTela * 60 / 100;
		
		} else if(larguraTela > 767 && larguraTela <= 991) {
			var larguradesejada = larguraTela * 30 / 100;
		}
		
		
		
		if(headerClasseAntiga.length == 0 || !$("#wrapper").hasClass("toggled")) {
	    	
			headerClasseAntiga = [];
	    	$('.menu-usuario-header').each(function(index, value){
	    		 var classe = $(value).attr('class');
	    		 var id = $(value).attr('id');
	    		 var objeto = {};
	    		 
	    		 objeto.id = id;
	    		 objeto.classe = classe;
	    		 
	    		 var objetoInsercao = $.extend(true, {}, objeto);
	    		 headerClasseAntiga.push(objetoInsercao);
	    		 
	    		 $(value).attr('class', 'col-xs-12 col-sm-12 menu-usuario-header');
	    		 
	    		 $(value).find('p').each(function(indexP, valueP){
	    			 $(valueP).attr('class', 'text-header-mercado-de-musica');
	    		 });
	    		 
	    		 $("#wrapper").addClass("toggled");
	    		 
	    		$('.toggled #sidebar-wrapper').css('width', larguradesejada);
	    		$('.toggled').css('padding-left', larguradesejada);
	    	 });
	    } else if(headerClasseAntiga.length > 0){
	    	$('.menu-usuario-header').each(function(index, value){
	    		for(var i = 0; i < headerClasseAntiga.length; i++ ) {
	    			if($(value).attr('id') == headerClasseAntiga[i].id) {
	    				$(value).attr('class', headerClasseAntiga[i].classe);
	    			}
	    		}
	    		
	    		$(value).find('p').each(function(index, p){
	    			$(p).attr('class', 'text-center text-header-mercado-de-musica');
		   		});
	    	});
	    	
	    	$('.toggled #sidebar-wrapper').removeAttr('style');
			$('.toggled').removeAttr('style');
	    	
	    	headerClasseAntiga = [];
	    	$("#wrapper").removeClass("toggled");
	    }
	}
});	