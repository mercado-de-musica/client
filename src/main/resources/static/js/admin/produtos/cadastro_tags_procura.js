var obterInstrumentoAcessorio = function(request, response) {
	$.ajax({
		url: "obter_instrumento_acessorio.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { instrumentoAcessorio : request.term }
	}).success(function(dados){
		var instrumentoAcessorio = new Array();
	
		for(var i = 0; i < dados.length; i++) {
			instrumentoAcessorio.push(dados[i].nome);	
		}
		
		response(instrumentoAcessorio);
	
	}).fail(function(erro){
		$('#produtos_gerais_modal').modal("show");
	});
}


var obterMarca = function(request, response) {
	var instrumentoAcessorio = $("input[name='instrumentoAcessorio.nome']").val();
	
	$.ajax({
		url: "obter_marca.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { instrumentoAcessorio : instrumentoAcessorio, marca: request.term }
	}).success(function(dados){
		var marca = new Array();

		for(var i = 0; i < dados.length; i++) {
			marca.push(dados[i].nome);	
		}
		
		response(marca);

	}).fail(function(erro){
		$('#produtos_gerais_modal').modal("show");
	});
}


var obterProduto = function(request, response) {
	var instrumentoAcessorio = $("input[name='instrumentoAcessorio.nome']").val();
	var marca = $("input[name='marca.nome']").val();
	
	$.ajax({
		url: "obter_produto.json",
        dataType: "json",
        contentType: "application/json",
		data: { instrumentoAcessorio : instrumentoAcessorio, marca: marca, produto: request.term }
	}).success(function(dados){
		var produto = new Array();

		for(var i = 0; i < dados.length; i++) {
			produto.push(dados[i].nome);	
		}
		
		response(produto);
		
	}).fail(function(erro){
		$('#produtos_gerais_modal').modal("show");
	});
}


var obterModelo = function(request, response) {
	
	var instrumentoAcessorio = $("input[name='instrumentoAcessorio.nome']").val();
	var marca = $("input[name='marca.nome']").val();
	var produto = $("input[name='produto.nome']").val();
	
	$.ajax({
		url: "obter_modelo.json",
        dataType: "json",
        contentType: "application/json",
		data: { instrumentoAcessorio : instrumentoAcessorio, marca: marca, produto: produto, modelo: request.term }
	}).success(function(dados){
		var modelo = new Array();

		for(var i = 0; i < dados.length; i++) {
			modelo.push(dados[i].nome);	
		}
		
		response(modelo);
		
	}).fail(function(erro){
		$('#produtos_gerais_modal').modal("show");
	});	
}


$("input[name='instrumentoAcessorio.nome']").autocomplete({
	minLength: 3,
	source: obterInstrumentoAcessorio
});


$("input[name='marca.nome']").autocomplete({
	minLength: 3,
	source: obterMarca
});	


$("input[name='produto.nome']").autocomplete({
	minLength: 3,
	source: obterProduto
});


$("input[name='modelo.nome']").autocomplete({
	minLength: 3,
	source: obterModelo 
});




function buscarMarca(idInstrumentoAcessorioExclusao) {
	$.ajax({
		url: "obter_marca_por_instrumento_acessorio.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idInstrumentoAcessorio : idInstrumentoAcessorioExclusao }
	}).success(function(dados){
		
		$('#instrAcessorio_' + idInstrumentoAcessorioExclusao).find('ul').remove();
		
		$('#instrAcessorio_' + idInstrumentoAcessorioExclusao).append('<ul></ul>');
		for(var i = 0; i < dados.length; i++) {
			$('#instrAcessorio_' + idInstrumentoAcessorioExclusao).find('ul').each(function(){
				
				$(this).append('<li class="list-group-item" id="marca_'+idInstrumentoAcessorioExclusao+'_'+dados[i].id+'">'+
						'<button type="button" class="btn btn-default badge" onclick="buscarProduto('+idInstrumentoAcessorioExclusao+', ' + dados[i].id + ')">Buscar</button>'+dados[i].nome+'</li>');
			
			});
			
		}

	}).fail(function(erro){
		$('#produtos_gerais_modal').modal("show");
	});
}

function buscarProduto(idInstrumentoAcessorioExclusao, idMarcaExclusao) {
	$.ajax({
		url: "obter_produto_por_instrumento_acessorio_marca.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idInstrumentoAcessorio : idInstrumentoAcessorioExclusao, idMarca: idMarcaExclusao }
	}).success(function(dados){
		
		$('#marca_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao).find('ul').remove();
		
		$('#marca_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao).append('<ul></ul>');
		for(var i = 0; i < dados.length; i++) {
			$('#marca_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao).find('ul').each(function(){
				
				$(this).append('<li class="list-group-item" id="produto_'+idInstrumentoAcessorioExclusao+'_'+idMarcaExclusao+'_'+dados[i].id+'">'+
						'<button type="button" class="btn btn-default badge" onclick="buscarModelo('+idInstrumentoAcessorioExclusao+', '+idMarcaExclusao+', ' + dados[i].id + ')">Buscar</button>'+dados[i].nome+'</li>');
			});
			
		}

	}).fail(function(erro){
		$('#produtos_gerais_modal').modal("show");
	});
}

function buscarModelo(idInstrumentoAcessorioExclusao, idMarcaExclusao, idProdutoExclusao) {
	$.ajax({
		url: "obter_modelo_por_instrumento_acessorio_marca_produto.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idInstrumentoAcessorio : idInstrumentoAcessorioExclusao, idMarca: idMarcaExclusao, idProduto :  idProdutoExclusao}
	}).success(function(dados){
		
		$('#produto_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao + '_' + idProdutoExclusao).find('ul').remove();
		
		$('#produto_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao + '_' + idProdutoExclusao).append('<ul></ul>');
		for(var i = 0; i < dados.length; i++) {
			$('#produto_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao + '_' + idProdutoExclusao).find('ul').each(function(){
				
				$(this).append('<li class="list-group-item" id="modelo_'+idInstrumentoAcessorioExclusao+'_'+idMarcaExclusao+'_'+'_'+idProdutoExclusao+'_'+dados[i].id+'">'+
						'<button type="button" class="btn btn-default badge" onclick="excluirInfosGerais('+idInstrumentoAcessorioExclusao+', '+idMarcaExclusao+', '+idProdutoExclusao+', ' + dados[i].id + ')">Excluir</button>'+dados[i].nome+'</li>');
			});
			
		}

	}).fail(function(erro){
		$('#produtos_gerais_modal').modal("show");
	});
}


function excluirInfosGerais(idInstrumentoAcessorioExclusao, idMarcaExclusao, idProdutoExclusao, idModeloExclusao) {
	$.ajax({
		url: "excluir_infos_gerais.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idInstrumentoAcessorio : idInstrumentoAcessorioExclusao, idMarca: idMarcaExclusao, idProduto :  idProdutoExclusao, idModelo : idModeloExclusao}
	}).success(function(dados){
		
		$('#produto_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao + '_' + idProdutoExclusao).find('ul').remove();
		
		$('#produto_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao + '_' + idProdutoExclusao).append('<ul></ul>');
		for(var i = 0; i < dados.length; i++) {
			$('#produto_' + idInstrumentoAcessorioExclusao +'_' + idMarcaExclusao + '_' + idProdutoExclusao).find('ul').each(function(){
				
				$(this).append('<li class="list-group-item" id="modelo_'+idInstrumentoAcessorioExclusao+'_'+idMarcaExclusao+'_'+'_'+idProdutoExclusao+'_'+dados[i].id+'">'+
						'<button type="button" class="btn btn-default badge" onclick="excluirInfosGerais('+idInstrumentoAcessorioExclusao+', '+idMarcaExclusao+', '+idProdutoExclusao+', ' + dados[i].id + ')">Excluir</button>'+dados[i].nome+'</li>');
			});
			
		}

	}).fail(function(erro){
		console.log(erro);
		
		$('#produtos_gerais_modal').modal("show");
	});
}