	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}
	
	
	
	
	
	
	var obterInstrumentoAcessorio = function(request, response) {
		
		$.ajax({
			url: "obter_instrumento_acessorio.json",
		    dataType: "json",
		    contentType: "application/json",
			data: { instrumentoAcessorio : request.term }
		}).success(function(dados){
			var instrumentoAcessorio = new Array();
		
			for(var i = 0; i < dados.length; i++) {
				instrumentoAcessorio.push(dados[i].nome);	
			}
			
			response(instrumentoAcessorio);
		
		}).fail(function(erro){
			$('#produtos_gerais_modal').modal("show");
		});
	}


	var obterMarca = function(request, response) {
		var idInput = $(this).get(0).element.get(0).id;
		var idInputArray = idInput.split('_');
		
		var instrumentoAcessorio = $("#instrumentoAcessorio_" +idInputArray[1]).val();
		
		$.ajax({
			url: "obter_marca.json",
		    dataType: "json",
		    contentType: "application/json",
			data: { instrumentoAcessorio : instrumentoAcessorio, marca: request.term }
		}).success(function(dados){
			var marca = new Array();

			for(var i = 0; i < dados.length; i++) {
				marca.push(dados[i].nome);	
			}
			
			response(marca);

		}).fail(function(erro){
			$('#produtos_gerais_modal').modal("show");
		});
	}


	var obterProduto = function(request, response) {
		var idInput = $(this).get(0).element.get(0).id;
		var idInputArray = idInput.split('_');
		
		var instrumentoAcessorio = $("#instrumentoAcessorio_" + idInputArray[1]).val();
		var marca = $("#marca_" + idInputArray[1]).val();
		
		$.ajax({
			url: "obter_produto.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { instrumentoAcessorio : instrumentoAcessorio, marca: marca, produto: request.term }
		}).success(function(dados){
			var produto = new Array();

			for(var i = 0; i < dados.length; i++) {
				produto.push(dados[i].nome);	
			}
			
			response(produto);
			
		}).fail(function(erro){
			$('#produtos_gerais_modal').modal("show");
		});
	}


	var obterModelo = function(request, response) {
		
		var idInput = $(this).get(0).element.get(0).id;
		var idInputArray = idInput.split('_');
		
		var instrumentoAcessorio = $("#instrumentoAcessorio_" + idInputArray[1]).val();
		var marca = $("#marca_" + idInputArray[1]).val();
		var produto = $("#produto_" + idInputArray[1]).val();
		
		$.ajax({
			url: "obter_modelo.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { instrumentoAcessorio : instrumentoAcessorio, marca: marca, produto: produto, modelo: request.term }
		}).success(function(dados){
			var modelo = new Array();

			for(var i = 0; i < dados.length; i++) {
				modelo.push(dados[i].nome);	
			}
			
			response(modelo);
			
		}).fail(function(erro){
			$('#produtos_gerais_modal').modal("show");
		});	
	}	
	
	
	
	
	/*
	 * procura de tags
	 */
	
	$("input[name='instrumentoAcessorio']").autocomplete({
		minLength: 3,
		source: obterInstrumentoAcessorio,
	});


	$("input[name='marca']").autocomplete({
		minLength: 3,
		source: obterMarca,
	});	


	$("input[name='produto']").autocomplete({
		minLength: 3,
		source: obterProduto,
	});


	$("input[name='modelo']").autocomplete({
		minLength: 3,
		source: obterModelo ,
	});
	
	
	
	
	
	
	
	
function modalExclusaoSugestaoNovosProdutos(idSugestao) {
	$('#exclusao_sim').attr('onclick', 'confirmaExclusao(' + idSugestao + ')');
	$("#exclusao_modal").modal("show");
}	


function confirmaExclusao(idSugestao) {
	$.ajax({
		url: urlFinal + "administracao/excluir_sugestao_novo_produto.json",
        dataType: "json",
        contentType: "application/json",
		data: { idSugestao: idSugestao }
	}).success(function(){
		
		$("#exclusao_ok_modal").modal("show");
		location.reload();
		
	}).fail(function(erro){
		
		$("#exclusao_erro").modal("show");
	});
}