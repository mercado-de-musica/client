	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}

$("input[name='porcentagemString']").maskMoney({
	thousands:'.', 
	decimal:',',
	affixesStay: false,
	allowZero:false
});



$("#cadastro_porcentagem").validate({
    rules: {
    	porcentagemString : {
            required: true
        },
    },
    submitHandler: function(form) {
    	
    	form.submit(); 
    },
    invalidHandler: function(event, validator) {    	
    	
    	var textoFinal = "";
    	Object.getOwnPropertyNames(validator.invalid).forEach(function(val, idx, array) {
    		valSemString = val.replace('String', '');  
    		textoFinal += '<p>' + valSemString + ' - ' + validator.invalid[val] + '</p>';
    	});
    	    	
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html(textoFinal);
		});
		
    	$("#erro_modal").modal("show");
    },
    highlight: function(element) {
        $(element).closest('#cadastro_porcentagem').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('#cadastro_porcentagem').removeClass('has-error');
    }
});


function modalPorcentagem(idPorcentagem) {
	$('#confirmacao_exclusao').attr('onclick', 'confirmaExclusao(' + idPorcentagem + ')');
	$("#certeza_exclusao").modal("show");
}

function confirmaExclusao(idPorcentagem) {
	$.ajax({
		url: urlFinal + "administracao/excluir_porcentagem.json",
        dataType: "json",
        contentType: "application/json",
		data: { idPorcentagem: idPorcentagem }
	}).success(function(){
		
		$("#exclusao_ok_modal").modal("show");
		
	}).fail(function(erro){
		
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
		});
		
		$("#erro_modal").modal("show");
	});
}