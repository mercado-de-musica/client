$("#fale-conosco").validate({
    rules: {
        nome : {
        	minlength: 3,
            maxlength: 50,
            required: true
        },
        sobrenome : {
        	minlength: 3,
            maxlength: 50,
            required: true
        },
        email : {
        	maxlength: 80,
        	required: true,
            email: true, 
        },
        mensagem : {
        	required: true
        }  
    },
    submitHandler: function(form) {
    	form.submit();  	
    },
    invalidHandler: function(event, validator) {
    	console.log(event);
    },
    highlight: function(element) {
        $(element).closest('.fale-conosco').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.fale-conosco').removeClass('has-error');
    }
});