//$(document).ready(function(){
	
	$("#login_form").validate({
	    rules: {
		    username: {	
		    	maxlength: 80,
	        	required: true,
	            email: true
		    },
		    password : {
		    	minlength: 3,
		        maxlength: 40,
		        required: true
		    } 
    },
    submitHandler: function(form) {
    	
    	var senha = $("#login_form input[name='password']").val();
    	senha = $.md5(senha);
    	$("#login_form input[name='password']").val(senha);
    	
    	form.submit();  	
    },
    invalidHandler: function(event, validator) {
    	console.log(event);
    },
    highlight: function(element) {
        $(element).closest('#login_form').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('#login_form').removeClass('has-error');
    }
	});
//});