	/* ck editor */

	try {
		initSample();
	} catch (e) {
		console.log(e);
	}


	



	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}
	
jQuery.validator.addMethod("defaultInvalid", function(value, element) {
    return !(element.value == "0" || element.value == 0);
});


$("input[name='precoString']").maskMoney({
	prefix:'R$ ',
	thousands:'.', 
	decimal:',',
	affixesStay: false,
	allowZero:false
});


$("input[name='alturaString']").maskMoney({
	thousands:'.',
	precision: 0,
	affixesStay: false,
	allowZero:false
});

$("input[name='larguraString']").maskMoney({
	thousands:'.',
	precision: 0,
	affixesStay: false,
	allowZero:false
});

$("input[name='comprimentoString']").maskMoney({
	thousands:'.',
	precision: 0,
	affixesStay: false,
	allowZero:false
});

$("input[name='pesoString']").maskMoney({
	thousands:'.', 
	decimal:',',
	affixesStay: false,
	allowZero:false
});



$("#cadastro_alteracao_produtos_venda").validate({
    rules: {
    	"infosGeraisProduto.instrumentoAcessorio.id" : {
    		message: {
                defaultInvalid: "Instrumento "
            }
    	},
    	"infosGeraisProduto.marca.id" : {
    		message: {
                defaultInvalid: "Marca "
            }
    	},
    	"infosGeraisProduto.produto.id" : {
    		message: {
                defaultInvalid: "Produto "
            }
    	},	
    	"infosGeraisProduto.modelo.id" : {
    		message: {
                defaultInvalid: "Modelo "
            }
    	},
    	precoString : {
            required: true
        },
        alturaString : {
            required: true
        },
        larguraString : {
        	required: true
        },
        comprimentoString : {
        	required: true
        },
        pesoString : {
        	required: true
        },
        tipoEnvio : {
        	required: true
        }
    },
    submitHandler: function(form) {
    	
    		    
    	if($("#instrumento_acessorio_escolha").val() == false || $("#instrumento_acessorio_escolha").val() == "false") {
    		
    		$("#erro_selects_modal").modal("show");
    		
    	} else if($("#instrumento_acessorio_escolha").val() == 0 || $("#instrumento_acessorio_escolha").val() == "0" || 
    		$("#instrumento_acessorio_escolha").val() == -1 || $("#instrumento_acessorio_escolha").val() == "-1" || 
    		$("#instrumento_acessorio_escolha").val() == false) {
    		
    		
    		$("#erro_selects_modal").modal("show");
    	
    	} else if($("#marca_escolha").val() == 0 || $("#marca_escolha").val() == "0" || 
    		$("#marca_escolha").val() == -1 || $("#marca_escolha").val() == "-1" ||
    		$("#marca_escolha").val() == false) {
    	
    		$("#erro_selects_modal").modal("show");
    	
    	} else if($("#produto_escolha").val() == 0 || $("#produto_escolha").val() == "0" ||
    		$("#produto_escolha").val() == -1 || $("#produto_escolha").val() == "-1" ||
    		$("#produto_escolha").val() == false) {
    	
    		$("#erro_selects_modal").modal("show");
    	
    	} else if($("#modelo_escolha").val() == 0 || $("#modelo_escolha").val() == "0" || 
    		$("#modelo_escolha").val() == -1 || $("#modelo_escolha").val() == "-1" ||
    		$("#modelo_escolha").val() == false) {
    	
    		$("#erro_selects_modal").modal("show");
    	
    	} else {
    		
    		form.submit(); 
    	
    	} 
        	
    	 	

    },
    invalidHandler: function(event, validator) {    	
    	
    	var textoFinal = "";
    	Object.getOwnPropertyNames(validator.invalid).forEach(function(val, idx, array) {
    		valSemString = val.replace('String', '');  
    		textoFinal += '<p>' + valSemString + ' - ' + validator.invalid[val] + '</p>';
    	});
    	    	
    	$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html(textoFinal);
		});
		
    	$("#erro_modal").modal("show");
    },
    highlight: function(element) {
        $(element).closest('#cadastro_alteracao_produtos_venda').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('#cadastro_alteracao_produtos_venda').removeClass('has-error');
    }
});

$("input[name='alturaString']").bind('change', function(){
	
	
	valores = new Object();
	valores.altura = $(this).val();
	valores.peso = $("input[name='pesoString']").val();
	
	if( $("input[name='termosDePesoAltura']").val() == 'false' || $("input[name='termosDePesoAltura']").val() == false) {
		$("#validacao_tamanhos_modal").modal("show");
	}
	
	retiradaEmMaos(valores);
});


$("input[name='pesoString']").bind('change', function(){
	
	valores = new Object();
	valores.peso = $(this).val();
	valores.altura = $("input[name='alturaString']").val();
	
	retiradaEmMaos(valores);
});


$("#estou_ciente").click(function(){
	$("input[name='termosDePesoAltura']").val(true);
});



function retiradaEmMaos(valores) {
	
	$('#tipo_de_envio').select2({
		allowClear: true
	});
	
	
	/*
	 * 
	 * tipo envio
	 */
	$("select[name='tipoEnvio']").find('option').remove().end();
	$.ajax({
		url: urlFinal + "usuario/obter_tipo_envio.json",
        dataType: "json",
        contentType: "application/json",
	}).success(function(tipoEnvios){
		
		for (var i = 0; i < tipoEnvios.length; i++) {
			
			if((valores.altura != "" && parseInt(valores.altura.replace(/\./g, '')) > 105) || (valores.peso != "" && parseFloat(valores.peso.replace(/\./g, '').replace(/,/g, '.')) > 30)) {
				
				if(tipoEnvios[i].sigla == "CORREIOS") {
					$("select[name='tipoEnvio']").append('<option value="'+tipoEnvios[i].sigla+'" disabled>'+tipoEnvios[i].descricao+'</option>');
				} else {
					$("select[name='tipoEnvio']").append('<option value="'+tipoEnvios[i].sigla+'" selected>'+tipoEnvios[i].descricao+'</option>');
					$("input[name='termosDeEnvio']").val(true);
				}
				
			} else {
				$("select[name='tipoEnvio']").append('<option value="'+tipoEnvios[i].sigla+'">'+tipoEnvios[i].descricao+'</option>');
			}
		
//			if($("#retiradaem_maos_modal").attr('selected')) {
//				
//				$("#retiradaem_maos_modal").modal("show");
//				$("#retiradaem_maos_modal").attr('disabled', true);
//				
//				$("input[name='termosDeEnvio']").val(true);
//			}	
			
		}		
		$("select[name='tipoEnvio'] option").trigger('change.select2');
	
	}).fail(function(erro){
		waitingDialog.hide();
		$('#erro_requisicao').modal('show');
	});
	
	
	
	$('#tipo_de_envio').trigger("change.select2");
}

var csrfName = $('#csrf').attr('name');
var csrfValue = $('#csrf').val();
var infosGeraisProdutoUsuarioDtoValue = $('#infosGeraisProdutoUsuarioDto').val();


$('#file_upload').uploadifive({
	'buttonImage' : 'http://mercadodemusica.com.br/resources/image/escolher-arquivos.jpg',
	'uploadScript' : urlFinal + 'usuario/subida_de_arquivos.json?'+ csrfName + '=' + csrfValue + '&infosGeraisProdutoUsuarioDto=' + infosGeraisProdutoUsuarioDtoValue,
    'fileTypeExts' : '*.gif; *.jpg; *.png; *.mp3; *.wma; *.mpeg; *.mp4;',
    'uploadLimit': 5,
    'onUploadStart'   : function(instance) {
    	$('.btn').attr('disabled', true);
     },
     'onSelect' : function(file) {
    	 waitingDialog.show('Aguarde'); 
     },
    'onQueueComplete' : function(data) {
    	window.location = urlFinal + "usuario/ajuste_de_imagens_guillotine/" + infosGeraisProdutoUsuarioDtoValue;
    },
    'onError' : function(errorType) {
    	if(errorType == 'UPLOAD_LIMIT_EXCEEDED') {
    		window.location = urlFinal + "usuario/ajuste_de_imagens_guillotine/" + infosGeraisProdutoUsuarioDtoValue;
    	} else {
    		printErro(errorType);
    	}
    	
    }
});


$('#subida_redimensionamento').click(function(){
	$('.fotografia').each(function() {	
		 $.when( redimensionamento($(this)) ).done(function() {
		 });
	});
});


function redimensionamento(div) {
	var divPai = $(div).parent().parent();
	if($(divPai).attr('id') != 'row_excluir') {
		var data = $(div).guillotine('getData');
	     	
		var infosGeraisProdutoUsuarioDTO = new Object();
		 
		var src = $(div).attr('src');
 		var id = $(div).attr('id').replace('_', '');
		var corteSrcFoto = src.indexOf('mercado-de-musica-arquivos');
     	var novoSrcComPasta = src.substring(corteSrcFoto, src.length);
     	var corteSegundo = novoSrcComPasta.indexOf('/');
     	var pastaNomeDoArquivo = novoSrcComPasta.substring(corteSegundo + 1, novoSrcComPasta.length)
     	var pastaArray = pastaNomeDoArquivo.split("/");
     	var endereco = pastaArray[0] + "/" + pastaArray[1] + "/";
		var nomeDoArquivo = pastaArray[2];
     	
     	var scale = null; 
     	var angle = null;
     	var x = null;
     	var y = null;
     	var width = null;
     	var height = null;
     	
		for(var key in data) { 
			
			switch (key) {
				case 'scale':
					scale = data[key];
					break;
				case 'angle':
					angle = data[key];
					break;
				case 'x':
					x = data[key];
					break;	
				case 'y':
					y = data[key];
					break;
				case 'w':
					width = data[key];
					break;
				case 'h':
					height = data[key];
					break;			
			} 	
	    }
		
		var dados = new Object();
		
		dados.id = id;
		dados.endereco = endereco;
		dados.nomeDoArquivo = nomeDoArquivo;
		dados.scale = scale;
		dados.angle = angle;
		dados.x = x;
		dados.y = y;
		dados.width = width;
		dados.height = height;

		redimensionamentoDeArquivo(dados);
	}
	
	
}

function redimensionamentoDeArquivo(dados) {
	$.ajax({
		url: urlFinal + "usuario/redimensionamento_de_arquivos.json",
        dataType: "json",
        contentType: "application/json",
		data: { id: dados.id, endereco : dados.endereco, nomeDoArquivo: dados.nomeDoArquivo, scale: dados.scale, angle: dados.angle, x: dados.x, y: dados.y, width: dados.width, height: dados.height}
	}).success(function(){
		$('#ok_modal_pagina_inicial').removeProp("onclick");
		$('#ok_modal_pagina_inicial').click(function(){ paginaApresentacoes(dados.id)});
		$("#ok_modal").modal("show");
		
		
	}).fail(function(erro){
		
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
		});
		
		
		$("#erro_modal").modal("show");
	});
}


function fit(elemento) {
	
	$(elemento).parent().parent().find('.fotografia').guillotine('fit');

}

function zoomIn(elemento) {
	
	$(elemento).parent().parent().find('.fotografia').guillotine('zoomIn');
	 
}

function zoomOut(elemento) {
	
	$(elemento).parent().parent().find('.fotografia').guillotine('zoomOut');
	
}



/*
 * redirects
 */

function paginaDeFotos(idProduto) {	
	window.location = urlFinal + "usuario/redirecionamento_pagina_fotos/" + idProduto;
}


function paginaApresentacoes(idApresentacao) {
	
	window.location = urlFinal + "usuario/redirecionamento_pagina_apresentacao/"+idApresentacao;
}

/*
 * redirects
 */


function modalExclusao(idProduto) {
	
	$("#certeza_exclusao").find("#confirmacao_exclusao").each(function(){
		$(this).removeProp("onclick");
		$(this).click(function(){ excluirProduto(idProduto) });
	});
	
	$("#certeza_exclusao").modal("show");

}

function excluirProduto(idProduto) {
	if(idProduto == undefined || idProduto == null) {
		
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>Não existe ID do produto</p>');
		});
		
		$("#erro_modal").modal("show");
		
	} else {
		
		$.ajax({
			url: urlFinal + "usuario/excluir_produto.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { idProduto : idProduto}
		}).success(function(){
		
			$("#exclusao_ok_modal").modal("show");
			$("#linha_"+idProduto).remove();
			
		}).fail(function(erro){
			
			$("#erro_modal").find("#texto_selecionado_erro").each(function(){
				$(this).hide();
			});
			
			$("#erro_modal").find("#texto_ser_escrito").each(function(){
				$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
			});
			
			
			$("#erro_modal").modal("show");
		});
		
	}
}


function reloadPage() {
	location.reload();
}

function editarProduto(idProduto) {
	
	$('linha_' + idProduto).css('background-color', '#e0b36f');	
	
	$.ajax({
		url: urlFinal + "usuario/buscar_produto_cadastrado.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : idProduto}
	}).success(function(dadosRetorno){
		$("select[name='infosGeraisProduto.instrumentoAcessorio.id']").select2({
			allowClear: true
		});
		
		$("select[name='infosGeraisProduto.marca.id']").select2({
			allowClear: true
		});
		
		$("select[name='infosGeraisProduto.produto.id']").select2({
			allowClear: true
		});
		
		$("select[name='infosGeraisProduto.modelo.id']").select2({
			allowClear: true
		});
		
		
		
		/*
		 * 
		 */
		var arrayInstrumentosGeraisProdutoAcessorio = [];
		$("select[name='infosGeraisProduto.instrumentoAcessorio.id'] option").filter(function () { 
			var objeto = {};
			objeto.descricao =  $(this).html();
			objeto.valor = $(this).val();
			arrayInstrumentosGeraisProdutoAcessorio.push(objeto);
		});
			
		$("select[name='infosGeraisProduto.instrumentoAcessorio.id']").find('option').remove().end();
		
		
		
		
		
//		$("select[name='infosGeraisProduto.marca.id']").append('<option value="' + dadosRetorno.infosGeraisProduto.marca.id + '">'+ dadosRetorno.infosGeraisProduto.marca.nome +'</option>');
		for(var i = 0; i < arrayInstrumentosGeraisProdutoAcessorio.length; i++) {
			if (arrayInstrumentosGeraisProdutoAcessorio[i].valor == dadosRetorno.infosGeraisProduto.instrumentoAcessorio.id) {
				$("select[name='infosGeraisProduto.instrumentoAcessorio.id']").append('<option value="'+arrayInstrumentosGeraisProdutoAcessorio[i].valor+'" selected>'+arrayInstrumentosGeraisProdutoAcessorio[i].descricao+'</option>');
			} else {
				$("select[name='infosGeraisProduto.instrumentoAcessorio.id']").append('<option value="'+arrayInstrumentosGeraisProdutoAcessorio[i].valor+'">'+arrayInstrumentosGeraisProdutoAcessorio[i].descricao+'</option>');
			}
		}
		$("select[name='infosGeraisProduto.instrumentoAcessorio.id']").trigger("change.select2");
		
		
		$("select[name='infosGeraisProduto.marca.id']").append('<option value="' + dadosRetorno.infosGeraisProduto.marca.id + '">'+ dadosRetorno.infosGeraisProduto.marca.nome +'</option>');
		$("select[name='infosGeraisProduto.marca.id'] option").each(function(key, value){
			if ($(value).val() == dadosRetorno.infosGeraisProduto.marca.id) {
				$(value).attr('selected', true);
			} else {
				$(value).remove();
			}
		});
		$("select[name='infosGeraisProduto.marca.id']").trigger("change.select2");
		
		
		$("select[name='infosGeraisProduto.produto.id']").append('<option value="' + dadosRetorno.infosGeraisProduto.produto.id + '">'+ dadosRetorno.infosGeraisProduto.produto.nome +'</option>');
		$("select[name='infosGeraisProduto.produto.id'] option").each(function(key, value){
			if ($(value).val() == dadosRetorno.infosGeraisProduto.produto.id) {
				$(value).attr('selected', true);
			} else {
				$(value).remove();
			}
		});
		$("select[name='infosGeraisProduto.produto.id']").trigger("change.select2");
		
		
		$("select[name='infosGeraisProduto.modelo.id']").append('<option value="' + dadosRetorno.infosGeraisProduto.modelo.id + '">'+ dadosRetorno.infosGeraisProduto.modelo.nome +'</option>');
		$("select[name='infosGeraisProduto.modelo.id'] option").each(function(key, value){
			if ($(value).val() == dadosRetorno.infosGeraisProduto.modelo.id) {
				$(value).attr('selected', true);
			} else {
				$(value).remove();
			}
		});
		$("select[name='infosGeraisProduto.modelo.id']").trigger("change.select2");
		
		
		$("input[name='id']").val(dadosRetorno.id);
		$("input[name='dataInsercaoAnuncioString']").val(dadosRetorno.dataInsercaoAnuncioString);
		$("input[name='termosDePesoAltura']").val(dadosRetorno.termosDePesoAltura);
		$("input[name='termosDeEnvio']").val(dadosRetorno.termosDeEnvio);
		
		$("input[name='precoString']").val(dadosRetorno.precoString);
		
		CKEDITOR.instances.descricao.setData(dadosRetorno.descricao);

//		$("textarea[name='descricao']").val(dadosRetorno.descricao);
		$("input[name='alturaString']").val(dadosRetorno.alturaString);
		$("input[name='larguraString']").val(dadosRetorno.larguraString);
		$("input[name='comprimentoString']").val(dadosRetorno.comprimentoString);
		$("input[name='pesoString']").val(dadosRetorno.pesoString);
		$("input[name='anoDeFabricacao']").val(dadosRetorno.anoDeFabricacao);
		$("input[name='numeroDeSerie']").val(dadosRetorno.numeroDeSerie);
		
		
		
		/*
		 * 
		 * tipo envio
		 */
		$("select[name='tipoEnvio']").find('option').remove().end();
		$.ajax({
			url: urlFinal + "usuario/obter_tipo_envio.json",
	        dataType: "json",
	        contentType: "application/json",
		}).success(function(tipoEnvios){
			
			for (var i = 0; i < tipoEnvios.length; i++) {
				if(tipoEnvios[i].sigla == dadosRetorno.tipoEnvio.sigla) {
					$("select[name='tipoEnvio']").append('<option value="'+tipoEnvios[i].sigla+'" selected>'+tipoEnvios[i].descricao+'</option>');
				} else {
					if(dadosRetorno.termosDeEnvio == true && tipoEnvios[i].sigla == "CORREIOS") {
						$("select[name='tipoEnvio']").append('<option value="'+tipoEnvios[i].sigla+'" disabled>'+tipoEnvios[i].descricao+'</option>');
					} else {
						$("select[name='tipoEnvio']").append('<option value="'+tipoEnvios[i].sigla+'">'+tipoEnvios[i].descricao+'</option>');
					}
					
					
				}
			}
			
			$("select[name='tipoEnvio'] option").trigger('change.select2');
		
		}).fail(function(erro){
			waitingDialog.hide();
			$('#erro_requisicao').modal('show');
		});
	
		
		/*
		 * 
		 * paises
		 */
		
		$("select[name='paisDeFabricacao']").find('option').remove().end();
		
		$.ajax({
			url: urlFinal + "usuario/obter_paises.json",
	        dataType: "json",
	        contentType: "application/json",
		}).success(function(paises){
			
			$("select[name='paisDeFabricacao']").append('<option value="">Escolha o país de fabricação</option>');
			for (var i = 0; i < paises.length; i++) {
				if(dadosRetorno.paisDeFabricacao != null && dadosRetorno.paisDeFabricacao.sigla != null && paises[i].sigla == dadosRetorno.paisDeFabricacao.sigla) {
					$("select[name='paisDeFabricacao']").append('<option value="'+paises[i].sigla+'" selected>'+paises[i].descricao+'</option>');
				} else {
					$("select[name='paisDeFabricacao']").append('<option value="'+paises[i].sigla+'">'+paises[i].descricao+'</option>');
				}
			}
			
			$("select[name='paisDeFabricacao'] option").trigger('change.select2');
		
		}).fail(function(erro){
			waitingDialog.hide();
			$('#erro_requisicao').modal('show');
		});
		
		
		
		
		$('#cadastro_edicao').text('Edição do produto: ' + dadosRetorno.infosGeraisProduto.instrumentoAcessorio.nome + " " +
				dadosRetorno.infosGeraisProduto.marca.nome +" "+ dadosRetorno.infosGeraisProduto.produto.nome + " " +
				dadosRetorno.infosGeraisProduto.modelo.nome + " - R$ " + dadosRetorno.precoString);
		
		$('.form-group').addClass('bg-info');
		
	}).fail(function(erro){
		
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>Não foi possível buscar os dados para edição</p>');
		});
		
		
		$("#erro_modal").modal("show");
	});
	
}

function modalExclusaoFotos(idApresentacao) {
	
	$('#certeza_exclusao').find('#confirmacao_exclusao_apresentacao').each(function(){
		
		$(this).removeProp("onclick");
		$(this).click(function(){ confirmaExclusaoApresentacao(idApresentacao) });
	});	
	$('#certeza_exclusao').modal('show');
}


function confirmaExclusaoApresentacao(idApresentacao) {
	
	$.ajax({
		url: urlFinal + "usuario/excluir_apresentacao.json",
        dataType: "json",
        contentType: "application/json",
		data: { idApresentacao : idApresentacao}
	}).success(function(){
		$('#apresentacao_excluida').modal('show');

	}).fail(function(erro){
		$('#exclusao_apresentacao_erro').find('#texto_de_erro').each(function(){
			$(this).text(erro.responseJSON.mensagem);
		});
		$('#exclusao_apresentacao_erro').modal('show');
		
	});
	
	
}


function subidaDeVideoLink() {
	
	var infosGeraisProdutoUsuarioDtoValue = $("input[name='infosGeraisProdutoUsuarioDto']").val();
	var linkSite = $("input[name='link_de_video_upload']").val();
	
	if(linkSite == null || linkSite === undefined || linkSite == "") {
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>O campo do link não pode estar vazio.</p>');
		});
    	
		$("#erro_modal").modal("show");
	}
	
	$('.fotos').append('<div id="watch7-content" class="watch-main-col " itemscope itemid="" itemtype="http://schema.org/VideoObject"><link itemprop="url" href="'+linkSite+'"></div>');
	
	$.ajax({
		url: urlFinal + "usuario/inserir_apresentacao_video.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : infosGeraisProdutoUsuarioDtoValue, link: linkSite}
	}).success(function(){
		
		$('#ok_modal_pagina_inicial').click(function(){ paginaDeFotos(infosGeraisProdutoUsuarioDtoValue) });
		$("#ok_modal").modal("show");
		
	}).fail(function(erro){
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
		});
    	
		$("#erro_modal").modal("show");
		
	});
	
}

/*
 * verificacao se é retorno do cadastro de fotos
 * 
 */
if(url.indexOf("?idProduto") > -1) {
	var urlArray = url.split('=');
	
	$.ajax({
		url: urlFinal + "usuario/verificar_vendedor_produto.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : urlArray[1]}
	}).success(function(){

		editarProduto(urlArray[1]);
		
	}).fail(function(erro){
		$("#erro_modal").modal("show");	
	});
}

/*
 * 
 * 
 * 
 * 
 */

function retornarDadosDoProdutoCadastro(idProduto) {
	
	$.ajax({
		url: urlFinal + "usuario/verificar_vendedor_produto.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : idProduto}
	}).success(function(){

		window.location = urlFinal + 'usuario/produtos_venda?idProduto=' + idProduto;
		
	}).fail(function(erro){
		$("#erro_modal").modal("show");	
	});
}



function enviarNovaCategoriaInput() {
	var sugestao = $('#nova-categoria-input').val();
	
	if(sugestao.length > 100) {
		$("#erro_modal").modal("show");
	} else {
		
		$.ajax({
			url: urlFinal + "usuario/enviar_nova_categoria_de_produto.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { nomeNovoProduto : sugestao}
		}).success(function(){

			$("#novo-produto-envio-ok").modal("show");
			
		}).fail(function(erro){
			$("#erro_modal").modal("show");	
		});
	}
}



function printErro(erro) {
	$('.btn').attr('disabled', false);
	
	$("#erro_modal").find("#texto_selecionado_erro").each(function(){
		$(this).hide();
	});
	
	$("#erro_modal").find("#texto_ser_escrito").each(function(){
		$(this).html('<p>' + errorType + '</p>');
	});
	
	$("#erro_modal").modal("show");
}

if(window.location.href.toString().indexOf("ajuste_de_imagens_guillotine") > -1) {
	
	waitingDialog.show('Aguarde');
	
	var contagemDivs = $('.fotografia').length;
	setTimeout(function guillotineF() {
		$('.fotografia').each(function() {
			$(this).guillotine({width: 400, height: 300});
		});	
		
		waitingDialog.hide();
	}, contagemDivs * 1000);
}


$("input[name='capa']").click(function(){
	var radio = $(this).val();	
	$("input[name='capa']").attr('checked', false);
	
	var infosGeraisProdutoUsuarioDto = $("input[name='infosGeraisProdutoUsuarioDto']").val();
	
	$.ajax({
		url: urlFinal + "usuario/mudar_capa.json",
        dataType: "json",
        contentType: "application/json",
		data: { infosGeraisProdutoUsuarioId : infosGeraisProdutoUsuarioDto, apresentacaoId: radio}
	}).success(function(){

		$("input[name='capa'][value=" + radio + "]").attr('checked', 'checked');
		$("input[name='capa'][value=" + radio + "]").attr('checked', true);
		$("input[name='capa'][value=" + radio + "]").prop('checked', 'checked');
		$("input[name='capa'][value=" + radio + "]").prop('checked', true);
		
	}).fail(function(erro){
		$("#erro_modal").modal("show");	
	});
});