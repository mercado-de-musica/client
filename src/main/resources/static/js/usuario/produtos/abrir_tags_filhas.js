	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}



$('#instrumento_acessorio_escolha').change(function(){
	
	$('#marca_escolha').select2({
		allowClear: true
	});
	
	$('#produto_escolha').select2({
		allowClear: true
	});
	
	$('#modelo_escolha').select2({
		allowClear: true
	});
	
	var valorIdEscolhido = $(this).val();
	if(valorIdEscolhido != -1 ){
		$.ajax({
			url: urlFinal + "obter_marca_por_instrumento_acessorio.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { idInstrumentoAcessorio : valorIdEscolhido}
		}).success(function(dados){
			
			$('#marca_escolha').find('option').remove();
			$('#marca_escolha').append('<option value="-1">Escolha aqui a marca</option>');
			
			for(var i = 0; i < dados.length; i++) {
				$('#marca_escolha').append('<option value="' + dados[i].id + '">'+ dados[i].nome +'</option>');
			}
			
			$('#produto_escolha').find('option').remove();
			$('#produto_escolha').append('<option value="-1">Escolha aqui o produto</option>');
			
			$('#modelo_escolha').find('option').remove();
			$('#modelo_escolha').append('<option value="-1">Escolha aqui o modelo</option>');
			
			$('#marca_escolha').trigger("change.select2");
			$('#produto_escolha').trigger("change.select2");
			$('#modelo_escolha').trigger("change.select2");
			
		}).fail(function(erro){
			console.log(erro);
		});	
		
	} else {
		$('#marca_escolha').find('option').each(function(){
			if($(this).val() == -1) {
				$(this).attr('selected', true);
			} else {
				$(this).remove();
			}
		});
		
		$('#produto_escolha').find('option').each(function(){
			if($(this).val() == -1) {
				$(this).attr('selected', true);
			} else {
				$(this).remove();
			}
		});
		
		$('#modelo_escolha').find('option').each(function(){
			if($(this).val() == -1) {
				$(this).attr('selected', true);
			} else {
				$(this).remove();
			}
		});
		
		$('#marca_escolha').trigger("change.select2");
		$('#produto_escolha').trigger("change.select2");
		$('#modelo_escolha').trigger("change.select2");
	}
	
});

$('#marca_escolha').change(function(){
	var idInstrumentoAcessorio = $('#instrumento_acessorio_escolha').val();
	var idMarca = $(this).val();
	
	if(idInstrumentoAcessorio != -1 && idMarca != -1){
		
		$('#produto_escolha').select2({
			allowClear: true
		});
		
		$('#modelo_escolha').select2({
			allowClear: true
		});
		
		
		$.ajax({
			url: urlFinal + "obter_produto_por_instrumento_acessorio_marca.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { idInstrumentoAcessorio : idInstrumentoAcessorio, idMarca: idMarca}
		}).success(function(dados){
			
			$('#produto_escolha').find('option').remove();
			$('#produto_escolha').append('<option value="-1">Escolha aqui o produto</option>');
			
			for(var i = 0; i < dados.length; i++) {
				$('#produto_escolha').append('<option value="' + dados[i].id + '">'+ dados[i].nome +'</option>');
			}
		
			$('#modelo_escolha').find('option').remove();
			$('#modelo_escolha').append('<option value="-1">Escolha aqui o modelo</option>');
			
			$('#produto_escolha').trigger("change.select2");
			$('#modelo_escolha').trigger("change.select2");
			
		}).fail(function(erro){
			console.log(erro);
		});	
		
	} else {
		$('#produto_escolha').find('option').each(function(){
			if($(this).val() == -1) {
				$(this).attr('selected', true);
			} else {
				$(this).remove();
			}
		});
		
		$('#modelo_escolha').find('option').each(function(){
			if($(this).val() == -1) {
				$(this).attr('selected', true);
			} else {
				$(this).remove();
			}
		});
	
		$('#produto_escolha').trigger("change.select2");
		$('#modelo_escolha').trigger("change.select2");
	}
});

$('#produto_escolha').change(function(){
	var idInstrumentoAcessorio = $('#instrumento_acessorio_escolha').val();
	var idMarca = $('#marca_escolha').val();
	var idProduto = $(this).val();
	
	if(idInstrumentoAcessorio != -1 && idMarca != -1 && idProduto != -1){
		
		$('#modelo_escolha').select2({
			allowClear: true
		});
		
		$.ajax({
			url: urlFinal + "obter_modelo_por_instrumento_acessorio_marca_produto.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { idInstrumentoAcessorio : idInstrumentoAcessorio, idMarca: idMarca, idProduto : idProduto}
		}).success(function(dados){
			$('#modelo_escolha').select2();

			$('#modelo_escolha').find('option').remove();
			$('#modelo_escolha').append('<option value="-1">Escolha aqui o modelo</option>');
			
			for(var i = 0; i < dados.length; i++) {
				$('#modelo_escolha').append('<option value="' + dados[i].id + '">'+ dados[i].nome +'</option>');
			}
			
			$('#modelo_escolha').trigger("change.select2");
			
		}).fail(function(erro){
			console.log(erro);
		});	
		
	} else {
		$('#modelo_escolha').find('option').each(function(){
			if($(this).val() == -1) {
				$(this).attr('selected', true);
			} else {
				$(this).remove();
			}
		});
		
		
		$('#modelo_escolha').trigger("change.select2");
	}
});