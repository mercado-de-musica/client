function buscarSubcategoriaPorCategoria(idCategoria, dom) {

	var pesquisa = $('input[name="pesquisa"]').val();
	
	if(pesquisa === undefined || pesquisa == null || pesquisa == '') {
		pesquisa = 'null';
	}
	
	var li = $(dom).parent().parent().parent();
	
	if(!$(dom).is(':checked') && $(li).find('li').length > 0) {
		$(li).find('li').remove();
	
	} else {
		
		$.ajax({
			url:"/obter_subcategorias_por_categoria.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { idCategoria : idCategoria, pesquisa : pesquisa}
		}).success(function(dados){
			
			
			var ulLi = '<ul class="list-group">';
			for(var i = 0; i < dados.length; i++) {
				ulLi = ulLi + '<li class="list-group-item" style="border: 0px; padding: 0px 15px"><div class="checkbox">' +
						'<label><input type="checkbox" name="subcategoriaEscolhida[]" value="' + dados[i].idCrypt + '">' + dados[i].nome + '</label></div></li>';
			}
			ulLi = ulLi + '</ul>';
			$(li).append(ulLi);
		}).fail(function(erro){
			console.log(erro);
		});	
		
	}
}