function excluirInteresseDeCompra(idInteresseDeCompra) {
	$('#exclusao_modal').find('#exclusao_sim').attr('onclick', 'exclusaoFinalizada('+idInteresseDeCompra+')')
	
	$('#exclusao_modal').modal("show");
}


function exclusaoFinalizada(idInteresseDeCompra) {
	$.ajax({
		url: "exclusao_de_interesse_de_compra.json",
        contentType: "application/json",
		data: { interesseDeCompra : idInteresseDeCompra}
	}).success(function(){
		
		$('#li_'+idInteresseDeCompra).remove();
		$('#exclusao_ok').modal("show");
		
	}).fail(function(erro){
		$('#exclusao_erro').modal("show");
	});
	
}