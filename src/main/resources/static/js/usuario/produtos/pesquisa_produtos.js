function pesquisar() {
	$('#tipodePesquisa').trigger('change');
	
	var categoriasJson = obterCategorias();
	var precoInicial = obterPrecoInicial();
	var precoFinal = obterPrecoFinal();
	var stringPesquisa = verificarStringPesquisa(categoriasJson);
	
	if(stringPesquisa == null && categoriasJson.length == 0) {
		chamaModalDeErro();
	} else {
		enviarRequisicao($("input[name='pagina']").val(), stringPesquisa, 
				$("input[name='tipoPesquisa']").val(), $("input[name='produtosPorPagina']").val(), precoInicial, precoFinal, categoriasJson);
	}
	
	
}


function escolhaTipoPesquisa(dom) {
	$("input[name='tipoPesquisa']").val($(dom).val());
	$("input[name='pagina']").val(1);
}

function buscaResultadosPorPagina(pagina) {
	
	$("input[name='pagina']").val(pagina);
	
	var categoriasJson = obterCategorias();
	var precoInicial = obterPrecoInicial();
	var precoFinal = obterPrecoFinal();
	var stringPesquisa = verificarStringPesquisa(categoriasJson);
	
	if(stringPesquisa == null && categoriasJson.length == 0) {
		
		var pesquisaStringUrl = pegarPesquisaStringUrl();
		var categoriasStringUrl = pegarCategoriasUrl();
		
		if(pesquisaStringUrl != null && categoriasStringUrl == null) {
			
			enviarRequisicao($("input[name='pagina']").val(), pesquisaStringUrl, 
					$("input[name='tipoPesquisa']").val(), $("input[name='produtosPorPagina']").val(), precoInicial, precoFinal, []);
			
		} else if(categoriasStringUrl != null && pesquisaStringUrl == null) {
			
			enviarRequisicao($("input[name='pagina']").val(), null, 
					$("input[name='tipoPesquisa']").val(), $("input[name='produtosPorPagina']").val(), precoInicial, precoFinal, categoriasStringUrl);
			
		} else {
			chamaModalDeErro();
		}
		
	} else {
		enviarRequisicao($("input[name='pagina']").val(), stringPesquisa, 
				$("input[name='tipoPesquisa']").val(), $("input[name='produtosPorPagina']").val(), precoInicial, precoFinal, categoriasJson);
	}
	
}


function enviarRequisicao(pagina, pesquisa, tipoPesquisa, produtosPorPagina, precoInicial, precoFinal, categoriasJson) {
	
	
	
	$(location).attr('href', "/pesquisa_produtos/" + pagina + "/" + pesquisa + "/" 
			+ tipoPesquisa + "/" + produtosPorPagina + "/" + precoInicial + "/" + precoFinal + "/" + JSON.stringify(categoriasJson));
}


function mudarpagina() {
	$("input[name='pagina']").val(1);
}


function obterCategorias() {
	var categoriasJson = [];
	$("input[name='categoriaEscolhida[]']:checked").each(function() {
		var categoria = $(this);
		
		var categoriaJson = {};
		categoriaJson.categoria = $(categoria).val();
		categoriaJson.subcategoria = [];
		
		var li = $(this).parent().parent().parent();
		
		$(li).find('ul').each(function() {
			var liSegundo = $(this).find('li');
			var input = $(liSegundo).children('div').children('label').children('input:checked');
			
			$(input).each(function() {
				categoriaJson.subcategoria.push($(this).val());
			});
		});
		
		categoriasJson.push(categoriaJson);
	});
	
	return categoriasJson;
}



function obterPrecoInicial() {
	var valorPrecos = $("input[name='pesquisa-preco']").val();
	if(valorPrecos !== undefined && valorPrecos != null) {
		var precosInicialeFinal = valorPrecos.split(',');
		return precosInicialeFinal[0];
	} else {
		return 0;
	}
}



function obterPrecoFinal() {
	var valorPrecos = $("input[name='pesquisa-preco']").val();
	if(valorPrecos !== undefined && valorPrecos != null) {
		var precosInicialeFinal = valorPrecos.split(',');
		return precosInicialeFinal[1];
	} else {
		return 0;
	}
}


function verificarStringPesquisa(categoriasJson) {
	if(categoriasJson.length > 0) {
		return null;
	} else if($("input[name='pesquisa']").val() === undefined || $("input[name='pesquisa']").val() == null || $("input[name='pesquisa']").val() == ''){
		return null;
	} else {
		return $("input[name='pesquisa']").val();
	}
	
}

function pegarPesquisaStringUrl() {
	var urlAtual = window.location.href;
	var urlSplit = urlAtual.split("/pesquisa_produtos/");

	var parametersSplit = urlSplit[1].split('/')
	var pesquisa = parametersSplit[1];
	
	if(pesquisa === undefined || pesquisa == null || pesquisa == '' || pesquisa == 'null') {
		return null
	} else {
		return pesquisa;
	}
}

function pegarCategoriasUrl() {
	var urlAtual = window.location.href;
	var urlSplit = urlAtual.split("/pesquisa_produtos/");
	
	var parametersSplit = urlSplit[1].split('/')
	
	var categorias = parametersSplit[6];
	categorias = categorias.replace(/%7B/g, '{');
	categorias = categorias.replace(/%7D/g, '}');
	categorias = categorias.replace(/%22/g, '"');
	
	var json = $.parseJSON(categorias);
	
	
	if(json === undefined || json == null || json.length == 0) {
		return null;
	} else {
		return json;
	}
}

function chamaModalDeErro() {
	$('#modalDeErroPesquisa').modal("show");
}

function inserirPesquisaDetalhe(idCrypt) {
	var pesquisa = $("input[name='pesquisa']").val();
	
	var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
	
	
	$.ajax({
		url:"/inserir_pesquisa_detalhe.json",
        dataType: "json",
        contentType: "application/json",
	    beforeSend: function(xhr){
	    	xhr.setRequestHeader(header, token);
	    },
	    type: "POST",
		data: JSON.stringify({ "idProdutoCrypt" : idCrypt, "pesquisa" : pesquisa})
	}).success(function(){
		
		console.log("foi");
		
	}).fail(function(erro){
		console.log(erro);
	});	
	
}