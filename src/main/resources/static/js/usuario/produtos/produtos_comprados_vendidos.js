var url = window.location.href.toString();
var urlFinal = '';
//	Dev
if(url.indexOf("Client") != -1) {
	
	var urlCortada = url.split('/');
	urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
	
} else {
//	Prod	
	
	var urlCortada = url.split('/');
	urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
}


function enviarCodigoVendedor(idProduto) {
	var codigoVendedor = $("input[name='codigoVendedor_" + idProduto + "']").val();
	
	if(verificacaoCodigo(codigoVendedor)){
		$.ajax({
			url: urlFinal + "usuario/cadastro_de_codigo_de_vendedor_direto.json",
	        contentType: "application/json",
			data: { idProduto : idProduto, codigoVendedor: codigoVendedor}
		}).success(function(){
			$('#enviar_cod_vendedor').remove();
			$("input[name='codigoVendedor_" + idProduto + "']").val('Código já incluído: '+ codigoVendedor.toUpperCase());
			$("input[name='codigoVendedor_" + idProduto + "']").attr('disabled', true);
			$("input[name='codigoVendedor_" + idProduto + "']").removeAttr('name');
			
			$("#enviar_cod_vendedor_" + idProduto).remove();
			
			$('#envio_sucesso').modal("show");
		}).fail(function(erro){
			if(erro.responseJSON.mensagem !== undefined) {
				$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
			} else {
				$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
			}
			$('#erro_generico').modal("show");
		});
	}
}


function enviarCodigoComprador(idProduto) {
	var codigoComprador = $("input[name='codigoComprador_" + idProduto + "']").val();
	
	if(verificacaoCodigo(codigoComprador)){
		$.ajax({
			url: urlFinal + "usuario/cadastro_de_codigo_de_comprador_direto.json",
	        contentType: "application/json",
			data: { idProduto : idProduto, codigoComprador: codigoComprador}
		}).success(function(){
			
			$("input[name='codigoComprador_" + idProduto + "']").val('Código já incluído: '+ codigoComprador.toUpperCase());
			$("input[name='codigoComprador_" + idProduto + "']").attr('disabled', true);
			$("input[name='codigoComprador_" + idProduto + "']").removeAttr('name');
			
			$('#enviar_cod_comprador_' + idProduto).remove();
			
			
			$('#envio_sucesso').modal("show");
		}).fail(function(erro){
			if(erro.responseJSON.mensagem !== undefined) {
				$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
			} else {
				$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
			}
			$('#erro_generico').modal("show");
		});
	}
}


function verificacaoCodigo(codigo) {
	var regexCaracteresEspeciais = /[^0-9A-Za-z]$/i;
	
	if(codigo === undefined || codigo == null || codigo == '') {
		$('#erro_a_ser_escrito').text('O código não pode estar vazio');
		$('#erro_codigo').modal("show");
		return false;
	
	} else if(regexCaracteresEspeciais.test(codigo)) {
		$('#erro_a_ser_escrito').text('Existem caracteres inválidos no código');
		$('#erro_codigo').modal("show");
		return false;
		
	} else {
		return true;
	
	}
}


function mostrarAceiteReputacaoVendedor(idProdutoComprado) {
	
	if($("input[name='nota_" + idProdutoComprado + "']:checked").val() === undefined) {
		$("#erro_nota_vazia").modal("show");
		return;
		
	} else {
		$("#com_certeza_quero_enviar_as_informacoes").attr('onclick', 'enviarReputacaoVendedor('+idProdutoComprado+')');
		$('#aceite_informacoes').modal("show");
	}
}


function enviarReputacaoVendedor(idProdutoComprado) {
	var mensagem = null;	
	var nota = $("input[name='nota_" + idProdutoComprado + "']:checked").val();

	if($("textarea[name='mensagem_" + idProdutoComprado + "']").val() !== undefined) {
		mensagem = $("textarea[name='mensagem_" + idProdutoComprado + "']").val();
	}

	
	$.ajax({
		url: urlFinal + "usuario/incluir_reputacao_vendedor.json",
        contentType: "application/json",
		data: { nota : nota, mensagem: mensagem, idProduto : idProdutoComprado}
	}).success(function(){
		$("textarea[name='mensagem_" + idProdutoComprado + "']").attr('disabled', true);
		$("input[name='nota_" + idProdutoComprado + "']").attr('disabled', true);
		$("#enviar_reputacao_vendedor_" + idProdutoComprado).remove();
		
		$('#envio_sucesso_reputacao').modal("show");
	}).fail(function(erro){
		if(erro.responseJSON.mensagem !== undefined) {
			$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
		} else {
			$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
		}
		$('#erro_generico').modal("show");
	});
	
	
}


function desistenciaDeCompra(idProduto) {
	
	$.ajax({
		url: urlFinal + "usuario/verificar_codigo_vendedor_inserido.json",
        contentType: "application/json",
		data: { idProduto : idProduto}
	}).success(function(retorno){
		
		if(retorno.rastreamentoProdutoDireto != null && retorno.desistenciaDeCompra == false) {
			$('#desistencia_de_compra_cancelada').modal("show");
		
		} else if(retorno.rastreamentoProdutoDireto != null && retorno.desistenciaDeCompra == true && 
				retorno.rastreamentoProdutoDireto.codigo.codigoInseridoVendedor == null && retorno.rastreamentoProdutoDireto.codigo.codigoInseridoComprador != null) {
		
			$('#desistencia_insercao_codigo_de_venda').modal("show");
		
		} else {
			
			$('#desistencia_de_compra_aceite').attr('onclick', 'confirmarDesistenciaDeCompra(' + idProduto + ')');
			$('#desistencia_de_compra_modal').modal("show");
			
		}
	}).fail(function(erro){
		if(erro.responseJSON.mensagem !== undefined) {
			$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
		} else {
			$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
		}
		$('#erro_generico').modal("show");
	});
}




function desistenciaDeCompraCorreios(idProduto) {
	
	$.ajax({
		url: urlFinal + "usuario/verificar_rastreamento_correios.json",
        contentType: "application/json",
		data: { idProduto : idProduto}
	}).success(function(retorno){
		
		if(retorno.rastreamentoProdutoDireto != null && retorno.desistenciaDeCompra == false) {
			$('#desistencia_de_compra_cancelada').modal("show");
		
		} else if(retorno.rastreamentoProdutoDireto != null && retorno.desistenciaDeCompra == true && 
				retorno.rastreamentoProdutoDireto.codigo.codigoInseridoVendedor == null && retorno.rastreamentoProdutoDireto.codigo.codigoInseridoComprador != null) {
		
			$('#desistencia_insercao_codigo_de_venda').modal("show");
		
		} else {
			
			$('#desistencia_de_compra_aceite').attr('onclick', 'confirmarDesistenciaDeCompra(' + idProduto + ')');
			$('#desistencia_de_compra_modal').modal("show");
			
		}
	}).fail(function(erro){
		if(erro.responseJSON.mensagem !== undefined) {
			$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
		} else {
			$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
		}
		$('#erro_generico').modal("show");
	});
}


function confirmarDesistenciaDeCompra(idProduto) {
	
	$.ajax({
		url: urlFinal + "usuario/confirmar_desistencia.json",
        contentType: "application/json",
		data: { idProduto : idProduto}
	}).success(function(retorno){
		$('#protocolo_sucesso_desistencia').text("Seu protocolo de desistencia: " + retorno);
		$('#desistencia_sucesso').modal("show");
		
	}).fail(function(erro){
		if(erro.responseJSON.mensagem !== undefined) {
			$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
		} else {
			$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
		}
		$('#erro_generico').modal("show");
	});
}

function enviarCodigoVendedorDesistencia(idProduto) {
	
	var codigoVendedor = $("input[name='codigoVendedorDesistencia_" + idProduto + "']").val();
	
	if(verificacaoCodigo(codigoVendedor)){
		$.ajax({
			url: urlFinal + "usuario/cadastro_de_codigo_de_vendedor_direto_desistencia.json",
	        contentType: "application/json",
			data: { idProduto : idProduto, codigoVendedor: codigoVendedor}
		}).success(function(){
			$('#enviar_cod_vendedor_desistencia_' + idProduto).remove();
			$("input[name='codigoVendedorDesistencia_" + idProduto + "']").val('Código já incluído: '+ codigoVendedor.toUpperCase());
			$("input[name='codigoVendedorDesistencia_" + idProduto + "']").attr('disabled', true);
			$("input[name='codigoVendedorDesistencia_" + idProduto + "']").removeAttr('name');
			
			$("#enviar_cod_vendedor_desistencia_" + idProduto).remove();
			
			$('#envio_sucesso').modal("show");
		}).fail(function(erro){
			if(erro.responseJSON.mensagem !== undefined) {
				$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
			} else {
				$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
			}
			$('#erro_generico').modal("show");
		});
	}
}

function enviarCodigoCompradorDesistencia(idProduto) {
	
	var codigoComprador = $("input[name='codigoCompradorDesistencia_" + idProduto + "']").val();
	
	if(verificacaoCodigo(codigoComprador)){
		$.ajax({
			url: urlFinal + "usuario/cadastro_de_codigo_de_comprador_direto_desistencia.json",
	        contentType: "application/json",
			data: { idProduto : idProduto, codigoComprador: codigoComprador}
		}).success(function(){
			$('#enviar_cod_comprador_desistencia_' + idProduto).remove();
			$("input[name='codigoCompradorDesistencia_" + idProduto + "']").val('Código já incluído: '+ codigoComprador.toUpperCase());
			$("input[name='codigoCompradorDesistencia_" + idProduto + "']").attr('disabled', true);
			$("input[name='codigoCompradorDesistencia_" + idProduto + "']").removeAttr('name');
			
			$("#enviar_cod_comprador_desistencia_" + idProduto).remove();
			
			$('#envio_sucesso').modal("show");
		}).fail(function(erro){
			if(erro.responseJSON.mensagem !== undefined) {
				$('#informacao_a_ser_escrita').text(erro.responseJSON.mensagem);
			} else {
				$('#informacao_a_ser_escrita').text("Ocorreu um erro com o envio da informação, por favor entre em contato conosco.");
			}
			$('#erro_generico').modal("show");
		});
	}
}

function gerarPDFDeCodigo(codigo, idProduto, tipoDeComercianteDoProduto) {
	
	$.ajax({
		url: urlFinal + "usuario/gerar_codigo_em_PDF.json",
        contentType: "application/json",
        data: { codigo : codigo, idProduto : idProduto, tipoDeComercianteDoProduto : tipoDeComercianteDoProduto},
        dataType: 'text',
	}).success(function(dados){
//		height="'+(height * 100 / 17)+'" 
//		width="'+(width * 100 / 17)+'"
		$('#geracao_de_pdf_visualizacao').html('<div class="embed-responsive embed-responsive-16by9 img-responsive img-thumbnail"><iframe src="'+dados+'" allowfullscreen></iframe></div>');
		$('#ok_exclusao_de_arquivo_pdf').attr('onclick', 'excluirPdf("'+dados+'")');
		
		$('#geracao_de_PDF_modal').modal('show');
		
	}).fail(function(erro){
		$('#erro_generico').modal("show");
	});
}


function excluirPdf(path) {
	$.ajax({
		url: urlFinal + "usuario/excluir_PDF.json",
        contentType: "application/json",
        data: { path : path}
	}).success(function(){		
	});
}


function imprmirChancelaCorreios(idProduto, desistencia) {
	$.ajax({
		url: urlFinal + "usuario/imprimir_chancela_correios.json",
        contentType: "application/json",
        data: { idProduto : idProduto, desistencia : desistencia},
        dataType: 'text',
	}).success(function(dados){
//		height="'+(height * 100 / 17)+'" 
//		width="'+(width * 100 / 17)+'"
		$('#geracao_de_pdf_visualizacao_chancela_correios').html('<div class="embed-responsive embed-responsive-16by9 img-responsive img-thumbnail"><iframe src="'+dados+'" allowfullscreen></iframe></div>');
		$('#ok_exclusao_de_arquivo_pdf_chancela_correios').attr('onclick', 'excluirPdfChancelaCorreios("'+dados+'")');
		
		$('#geracao_de_PDF_chancela_correios').modal('show');
		
	}).fail(function(erro){
		$('#erro_generico').modal("show");
	});
}


function excluirPdfChancelaCorreios(path) {
	
	$.ajax({
		url: urlFinal + "usuario/excluir_chancela_correios.json",
        contentType: "application/json",
        data: { path : path}
	}).success(function(){		
	});
	
}

function rastreamentoDoProduto(idProduto) {
	if(idProduto !== undefined && idProduto != null && idProduto != '') {
		$.ajax({
			url: urlFinal + "usuario/rastreamento_correios_produto.json",
	        contentType: "application/json",
	        data: { idProduto : idProduto}
		}).success(function(dados){
			
			$('#correios_rastreamento_eventos_escritas').html('');
		
			for (var i = 0; i < dados.length; i++) {
				
				$('#correios_rastreamento_eventos_escritas').append('<p class="text-muted"> Data: <b>'+dados[i].data+' as '+dados[i].hora+' </b> - UF: <b>'+dados[i].uf+'</b> - Cidade: <b>'+ 
						dados[i].cidade+'</b> Local: <b>'+dados[i].local+'</b> - '+dados[i].descricao+'</p>');
			}
			
			
			$('#correios_rastreamento_eventos').modal("show");
			
		}).fail(function(erro){
			var jsonErro = JSON.parse(erro.responseText);
			
			if(jsonErro.mensagem !== undefined && jsonErro.mensagem != null && jsonErro.mensagem != '') {
				$('#texto-erro-rastreamento-correios').text(jsonErro.mensagem);
				$('#erro_correios_rastreamento').modal("show");
			} else {
				$('#erro_generico').modal("show");
			}
			
		});
	}
}