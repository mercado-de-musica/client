	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}


$(".enviarResposta").click(function(){
	var name = $(this).attr('name');
	var idPerguntaArray = name.split('_');
	
	var resposta = $("textarea[name='reposta_" + idPerguntaArray[1] + "']").val();
	var encerraDiscussao = $("input[name='encerraDiscussao_" + idPerguntaArray[1] + "']").is(":checked");
	var idPergunta = $("input[name='idPergunta_" + idPerguntaArray[1] + "']").val();
	
	if(idPergunta != idPerguntaArray[1] || (resposta == "" && encerraDiscussao == false)) {
	
		$('#erro_modal').modal("show");
	} else {
		
		$.ajax({
			url: urlFinal + "usuario/envio_de_resposta.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { idPergunta: idPergunta, resposta : resposta, encerraDiscussao: encerraDiscussao }
		}).success(function(){
			$("textarea[name='reposta_" + idPerguntaArray[1] + "']").val("");
			$("input[name='encerraDiscussao_" + idPerguntaArray[1] + "']").attr("checked", false);
			$("#ok_modal").modal("show");
			
		}).fail(function(erro){
			$('#erro_modal').modal("show");
		});
	} 
	
});



function deixarDeVisualizar(idPergunta) {
	
	$.ajax({
		url: urlFinal + "usuario/deixar_de_visualizar.json",
        dataType: "json",
        contentType: "application/json",
		data: { idPergunta: idPergunta}
	}).success(function(){
		$("#ok_modal").modal("show");
		
	}).fail(function(erro){
		$('#erro_modal').modal("show");
	});
	
}