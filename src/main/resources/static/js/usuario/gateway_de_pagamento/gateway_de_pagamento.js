var url = window.location.href.toString();
var urlFinal = '';
//	Dev
if(url.indexOf("Client") != -1) {
	
	var urlCortada = url.split('/');
	urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
	
} else {
//	Prod	
	
	var urlCortada = url.split('/');
	urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
}

function alertaParaCadastraraContaNoGatewayDePagamento() {
	
	$('#gateway_de_pagamento_modal').modal("show");
}




$("input[name='cadastro_de_conta_gateway_de_pagamento']").click(function(){
	$.ajax({
		url: urlFinal + "usuario/cadastrar_conta_de_gateway_de_pagamento.json",
        dataType: "json",
        contentType: "application/json",
	}).success(function(){
		$('#cadastramento_de_conta_gateway_de_pagamento_ok').modal("show");
		
	}).fail(function(erro){
		
		if(erro.responseJSON !== null && erro.responseJSON !== undefined) {
			$('#erro_ocorrido').html(erro.responseJSON.mensagem);
		} else {
			$('#erro_ocorrido').html("Erro: 1001");
		}
		
		$('#cadastramento_de_conta_gateway_de_pagamento_erro').modal("show");
		
	});	
});