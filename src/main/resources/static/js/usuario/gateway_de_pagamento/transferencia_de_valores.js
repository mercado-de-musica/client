var contaTransferencia = null;




	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}

	
function incluirCarrinhoDeTransferencia(idProduto, vendedor) {
	
	$.ajax({
		url: urlFinal + "usuario/verificar_conta_bancaria_de_insercao.json",
        dataType: "json",
        contentType: "application/json",
	}).success(function(dados){
		
		if(dados.length == 1) {
			
			if(contaTransferencia == null || contaTransferencia === undefined) {
				contaTransferencia = dados[0].id;
			}
			
			
			
			incluirTransferencia(idProduto, vendedor);
			
		
		} else if(dados.length > 1) {
			var cabecalho = $('#cabecalho');
			
			$('#inserir_informacoes_de_conta').html("");
			
			var bodyTabela = "";
			for(var i = 0; i < dados.length; i++) {
				bodyTabela = bodyTabela + "<tr>";
				bodyTabela = bodyTabela + "<td>" + dados[i].nomeDaConta + "</td>";
				bodyTabela = bodyTabela + "<td>" + dados[i].numeroDoBanco.replace("_", "") + "</td>";
				bodyTabela = bodyTabela + "<td>" + dados[i].numeroDaAgencia + "</td>";
				bodyTabela = bodyTabela + "<td>" + dados[i].numeroDaConta + "</td>";
				bodyTabela = bodyTabela + "<td>" + dados[i].digitoDaConta + "</td>";
				bodyTabela = bodyTabela + '<td><button type="button" class="btn btn-default btn-default" onclick="setContaTransferencia(' + idProduto + ', ' + dados[i].id + ')"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button></td>';
				bodyTabela = bodyTabela + "</tr>";
			}
			
			$('#inserir_informacoes_de_conta').append(cabecalho);
			$('#inserir_informacoes_de_conta').append(bodyTabela);
			
			$('#modal_escolha_de_conta').modal('show');
		
		} else {
			$('#erro_modal').modal('show');
		}		
		
	}).fail(function(erro){
		
		if(erro.responseJSON.mensagem != 'Não existe conta bancária cadastrada') {
			$('#erro_modal').modal('show');
		} else {
			$('#erro_nao_existe_conta').modal('show');
		}
		
	});
}


function incluirTransferencia(idProduto, vendedor) {
	$.ajax({
		url: urlFinal + "usuario/incluir_carrinho_transferencia_de_valores.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : idProduto, idContaTransferencia : contaTransferencia, vendedor : vendedor}
	}).success(function(){
		
		$('#inclusao_ok').modal('show');
		
	}).fail(function(erro){
		
		$('#erro_modal').modal('show');
		
	});
}

function setContaTransferencia(idProduto, contaTransferencia) {
	this.contaTransferencia = contaTransferencia;
	
	incluirTransferencia(idProduto);
}

function setContaTransferenciaDesistencia(idTransferencia, contaTransferencia) {
	this.contaTransferencia = contaTransferencia;
	
	fecharTransferenciaDesistencia(idTransferencia);
}




function excluirCarrinhoDeTransferencia(idProduto, vendedor) {
	$.ajax({
		url: urlFinal + "usuario/excluir_carrinho_transferencia_de_valores.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : idProduto, vendedor : vendedor}
	}).success(function(){
		
		$('#exclusao_ok').modal('show');
		
	}).fail(function(erro){
		
		$('#erro_modal').modal('show');
		
	});
}


$('#fechar_transferencia').click(function(){
	$('#fechar_transferencia_modal').modal('show');
});

function fecharTransferencia(desistencia) {
	$.ajax({
		url: urlFinal + "usuario/fechar_transferencia_de_valores.json",
        contentType: "application/json",
        data: { desistencia : desistencia}
	}).success(function(){
		
		$('#fechamento_de_transferencia_ok').modal('show');
		
	}).fail(function(erro){
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
		});
		
		$('#erro_modal').modal('show');
	});
}