	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}


function modalExclusaoConta(idContaBancaria) {
	
	$("#certeza_exclusao").find("#confirmacao_exclusao").each(function(){
		$(this).removeAttr("onclick");
		$(this).attr("onclick", "excluirContaBancaria("+idContaBancaria+")");
	});
	
	$("#certeza_exclusao").modal("show");

}


function excluirContaBancaria(idContaBancaria) {
	if(idContaBancaria == undefined || idContaBancaria == null) {
		
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>Não existe ID do produto</p>');
		});
		
		$("#erro_modal").modal("show");
		
	} else {
		
		$.ajax({
			url: urlFinal + "usuario/excluir_conta_bancaria.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { idContaBancaria : idContaBancaria}
		}).success(function(){
		
			$("#exclusao_ok_modal").modal("show");
			
		}).fail(function(erro){
			
			$("#erro_modal").find("#texto_selecionado_erro").each(function(){
				$(this).hide();
			});
			
			$("#erro_modal").find("#texto_ser_escrito").each(function(){
				$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
			});
			
			
			$("#erro_modal").modal("show");
		});
		
	}
}


function editarConta(idContaBancaria) {	
	$("#numero_do_banco_id").select2();
	
	
	
	$.ajax({
		url: urlFinal + "usuario/buscar_conta_bancaria.json",
        dataType: "json",
        contentType: "application/json",
		data: { idContaBancaria : idContaBancaria}
	}).success(function(dados){
		$("input[name='id']").val(dados.id);
		$("input[name='nomeDaConta']").val(dados.nomeDaConta);
		
		$("#numero_do_banco_id").find('option').each(function(){
			if($(this).val() == dados.numeroDoBanco) {
				$(this).attr('selected', true);
			}
		});	
		$("#numero_do_banco_id").trigger("chosen:updated");
		/*
		 * gato para o chosen funcionar
		 */
		$("#numero_do_banco_id_chosen").css("width", "100%"); 
		
		$("input[name='numeroDaAgencia']").val(dados.numeroDaAgencia);
		$("input[name='digitoDaAgencia']").val(dados.digitoDaAgencia);
		$("input[name='numeroDaConta']").val(dados.numeroDaConta);
		$("input[name='digitoDaConta']").val(dados.digitoDaConta);
		
		$('#update_conta_bancaria').modal("show");
	}).fail(function(erro){
		
		$("#erro_modal").find("#texto_selecionado_erro").each(function(){
			$(this).hide();
		});
		
		$("#erro_modal").find("#texto_ser_escrito").each(function(){
			$(this).html('<p>' + erro.responseJSON.mensagem + '</p>');
		});
		
		
		$("#erro_modal").modal("show");
	});
	
}	