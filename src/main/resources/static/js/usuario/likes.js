	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}

$('#botao_like').click(function(){
	
	var url = window.location.href.toString(); 
	urlArray = url.split('/');
	var numeroDoProduto = urlArray[urlArray.length - 1];
	
	$.ajax({
		url: urlFinal + "inserir_like.json",
        dataType: "json",
        contentType: "application/json",
		data: { idProduto : numeroDoProduto}
	}).success(function(){
		$('#botao_like').attr('disabled', true);
		$('#insercao_like_ok').modal("show");
	}).fail(function(erro){
		$('#insercao_like_erro').modal("show");
	});
});



function verproduto(idProduto) {
	$(location).attr('href', urlFinal + 'detalhamento_produto/' + idProduto);
}



function excluirLike(idLike) {
	
	$.ajax({
		url: urlFinal + "usuario/deletar_like.json",
        dataType: "json",
//        type: 'DELETE',
        contentType: "application/json",
		data: { idLike : idLike}
	}).success(function(){
		$('#linha_'+idLike).remove();
		$('#hr_'+idLike).remove();
		$('#exclusao_like_ok').modal("show");
	}).fail(function(erro){
		$('#exclusao_like_erro').modal("show");
	});
}


$('.ver_produto').mouseover(function(){
	var dom = $(this);
	var idCompleto = $(dom).attr('id');
	var idArray = idCompleto.split('_');
	
	
	$.ajax({
		url: urlFinal + "usuario/buscar_apresentacoes.json",
        dataType: "json",
        contentType: "application/json",
		data: { idInfosGeraisUsuario : idArray[2]}
	})
	.success(function(dados){
		var html = '';
		for(var i = 0; i < dados.length; i++) {
			if(dados[i].tipoApresentacao == 'FOTO') {
				var nomeDoArquivoArray = dados[i].nomeDoArquivo.split('\.');
				
				html = html + '<span><img src="' + dados[i].endereco + nomeDoArquivoArray[0] + '_thumb.' + nomeDoArquivoArray[1] + '" /></span>';
			}
		}
		
		if(html != '') {
			$(dom).tooltipster({
	            content: $(html)
	        });
		} 
	}).fail(function(erro){
		$('#erro_generico').modal('show');
		
	});
});

