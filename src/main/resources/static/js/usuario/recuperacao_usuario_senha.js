$('.tipo-de-pessoa-recuperacao_senha').click(function(){
	
	var tipoDePessoa = $(this).val();
	
	$('#cpf_cnpj').removeAttr('disabled');
	$('#enviar_recuperacao').removeAttr('disabled');
	
	if(tipoDePessoa == 'pf') {
		$('#cpf_cnpj').mask("999.999.999-99");
//		$('#cpf_cnpj').attr('name', 'cpf');
		
		$('#cpf_cnpj').rules("add", {
			maxlength: 14,
		    required: true
		});
	
	} else if(tipoDePessoa == 'pj') {
		$('#cpf_cnpj').mask("99.999.999/9999-99");
//		$('#cpf_cnpj').attr('name', 'cnpj');
		
		$('#cpf_cnpj').rules("add", {
			maxlength: 18,
			required: true
		});
	} else {
		$('#cpf_cnpj').attr('disable', 'disabled');
		$('#enviar_recuperacao').attr('disable', 'disabled');
	}
});

$('#recupera_usuario_senha_form').validate({
	 submitHandler: function(form) {
		 var cpfCnpj = $('#cpf_cnpj').val();
		 
		 cpfCnpj = cpfCnpj.replace(/\./g, '');
		 cpfCnpj = cpfCnpj.replace(/-/g, '');
		 cpfCnpj = cpfCnpj.replace(/\//g, '');
		 
		 $('#cpf_cnpj').val(cpfCnpj);
		 
		 form.submit();
	 }
});


$('#alterar_senha_form').validate({
	rules: {
	    senha : {
	        minlength: 3,
	        maxlength: 20,
	        required: true
	    },
	    confirmacao_de_senha: {
	        minlength: 3,
	        maxlength: 20,
	        required: true,
	        equalTo: "#alteracao_senha"
	    }, 
	
	},
	submitHandler: function(form) {
		
		var senha = $("#alterar_senha_form input[name='senha']").val();
    	senha = $.md5(senha);
    	$("#alterar_senha_form input[name='senha']").val(senha);
		 
		
		form.submit();
	}
});
