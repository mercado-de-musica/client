function mudarCorBotao(input) {
	
	var ariaExpandedvalue = $(input).attr("aria-expanded");
	if(ariaExpandedvalue == true || ariaExpandedvalue == 'true') {
		$(input).addClass('active');
	} else {
		$(input).removeClass('active');
	}
}


$(document).ready(function(){
	
	var ano = new Date().getFullYear() - 18;
	var anoRetorno = new Date();
	anoRetorno.setYear(ano);
	
	$("input[name='cpf']").mask("999.999.999-99");	
	
	$("input[name='dataDeNascimentoString']").datepicker("setDate", anoRetorno);
	
	$("input[name='telefonesList[0].telefone']").mask("(99) 9999-9999?9",{placeholder:"(xx) xxxx-xxxxx"});
	$("input[name='enderecosList[0].cep']").mask("99999-999",{placeholder:"xxxxx-xxx"});
	
	$("input[name='cnpj']").mask("99.999.999/9999-99");
	$("input[name='cpfTitular']").mask("999.999.999-99");
	
	$("input[name='dataDeNascimentoTitularString']").datepicker("setDate", anoRetorno);
	
	$("#cadastro_novo_usuario_pf input[name='enderecosList[0].cep']").focusout(function(){
		
		waitingDialog.show('Aguarde');
		
		var campo = $(this);
		var campoInteiro = campo.val().replace(/-/g, '');
	
		$.ajax({
			url: "obter_endereco_por_cep.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { cep : campoInteiro}
		}).success(function(dados){
			
			$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome']").select2({
			  allowClear: true
			});
			
			if(dados.resultado == 1) {
				
				$("#cadastro_novo_usuario_pf").remove('#enderecosList[0].bairro-error');
				
				$("#cadastro_novo_usuario_pf #enderecosList[0].bairro-error").remove();
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].bairro']").val(dados.bairro).attr('readonly', true).parent().removeClass("has-error");
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].bairro']").val(dados.bairro).prop('readonly', true).parent().removeClass("has-error");
				
				$("#cadastro_novo_usuario_pf").remove('#enderecosList[0].cidade.nome-error');
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].cidade.nome']").val(dados.cidade).attr('readonly', true).parent().removeClass("has-error");
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].cidade.nome']").val(dados.cidade).prop('readonly', true).parent().removeClass("has-error");
				
				$("#cadastro_novo_usuario_pf").remove('#enderecosList[0].logradouro-error');
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].logradouro']").val(dados.tipo_logradouro + " " + dados.logradouro).attr('readonly', true).parent().removeClass("has-error");
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].logradouro']").val(dados.tipo_logradouro + " " + dados.logradouro).prop('readonly', true).parent().removeClass("has-error");
				
				$("#cadastro_novo_usuario_pf").remove('#enderecosList[0].cidade.estado.nome-error');
				
				$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome']").find('option').remove().end();
			    
				$.ajax({
					url: "obter_estados.json",
			        dataType: "json",
			        contentType: "application/json",
				}).success(function(estados){
					
					for (var i = 0; i < estados.length; i++) {
						if(estados[i].sigla == dados.uf) {
							$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome']").append('<option value="'+estados[i].sigla+'" selected readonly>'+estados[i].descricao+'</option>');
						}
					}
				
				}).fail(function(erro){
					waitingDialog.hide();
					$('#erro_requisicao').modal('show');
				});
				
				$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome'] option").trigger('change.select2');
//				$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome']").select2();
			} else {
				
				$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome']").find('option').remove().end();
				
				$.ajax({
					url: "obter_estados.json",
			        dataType: "json",
			        contentType: "application/json",
				}).success(function(estados){
					
					for (var i = 0; i < estados.length; i++) {
						$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome']").append('<option value="'+estados[i].sigla+'">'+estados[i].descricao+'</option>');
					}
				
				}).fail(function(erro){
					waitingDialog.hide();
					$('#erro_requisicao').modal('show');
				});
				
				
//				$("#cadastro_novo_usuario_pf select option").each(function(index, value){
//					$(value).attr('disabled', false);
//					$(value).prop('disabled', false);
//				});
				
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].bairro']").attr('readonly', false);
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].cidade.nome']").attr('readonly', false);
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].logradouro']").attr('readonly', false);
				
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].bairro']").prop('readonly', false);
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].cidade.nome']").prop('readonly', false);
				$("#cadastro_novo_usuario_pf input[name='enderecosList[0].logradouro']").prop('readonly', false);
								
				$("#cadastro_novo_usuario_pf").remove('#enderecosList[0].cidade.estado.nome-error');
				$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome'] option").trigger('change.select2');
				$("#cadastro_novo_usuario_pf select[name='enderecosList[0].cidade.estado.nome']").select2();
				$('#usuario_modal').modal("show");
				
			}
			
			waitingDialog.hide();
			
		}).fail(function(erro){
			waitingDialog.hide();
			$('#erro_requisicao').modal('show');
		});	
		
		
	});
	
	
	
	$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cep']").focusout(function(){
		
		waitingDialog.show('Aguarde');
		
		var campo = $(this);
		var campoInteiro = campo.val().replace(/-/g, '');
		
		
		$.ajax({
			url: "obter_endereco_por_cep.json",
	        dataType: "json",
	        contentType: "application/json",
			data: { cep : campoInteiro}
		}).success(function(dados){
			
			$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome']").select2({
				allowClear: true, 	
			});
			
			if(dados.resultado == 1) {
				
				$("#cadastro_novo_usuario_pj").remove('#enderecosList[0].bairro-error');
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].bairro']").val(dados.bairro).attr('readonly', true).parent().removeClass("has-error");
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].bairro']").val(dados.bairro).prop('readonly', true).parent().removeClass("has-error");
				
				$("#cadastro_novo_usuario_pj").remove('#enderecosList[0].cidade.nome-error');
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cidade.nome']").val(dados.cidade).attr('readonly', true).parent().removeClass("has-error");
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cidade.nome']").val(dados.cidade).prop('readonly', true).parent().removeClass("has-error");
				
				$("#cadastro_novo_usuario_pj").remove('#enderecosList[0].logradouro-error');
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].logradouro']").val(dados.tipo_logradouro + " " + dados.logradouro).attr('readonly', true).parent().removeClass("has-error");
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].logradouro']").val(dados.tipo_logradouro + " " + dados.logradouro).prop('readonly', true).parent().removeClass("has-error");
				
				
				$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome']").find('option').remove().end();
				
				$.ajax({
					url: "obter_estados.json",
			        dataType: "json",
			        contentType: "application/json",
				}).success(function(estados){
					
					for (var i = 0; i < estados.length; i++) {
						if(estados[i].sigla == dados.uf) {
							$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome']").append('<option value="'+estados[i].sigla+'" selected readonly>'+estados[i].descricao+'</option>');
						}
					}
				
				}).fail(function(erro){
					waitingDialog.hide();
					$('#erro_requisicao').modal('show');
				});
					
				$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome'] option").trigger('change.select2');
//				$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome']").select2();
				
			} else {
				
				$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome']").find('option').remove().end();
				
				$.ajax({
					url: "obter_estados.json",
			        dataType: "json",
			        contentType: "application/json",
				}).success(function(estados){
					
					for (var i = 0; i < estados.length; i++) {
						$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome']").append('<option value="'+estados[i].sigla+'">'+estados[i].descricao+'</option>');
					}
				
				}).fail(function(erro){
					waitingDialog.hide();
					$('#erro_requisicao').modal('show');
				});
				
				
				
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].bairro']").attr('readonly', false);
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cidade.nome']").attr('readonly', false);
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].logradouro']").attr('readonly', false);
				
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].bairro']").prop('readonly', false);
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cidade.nome']").prop('readonly', false);
				$("#cadastro_novo_usuario_pj input[name='enderecosList[0].logradouro']").prop('readonly', false);
				
				$('#usuario_modal').modal("show");
				
				$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome'] option").trigger('change.select2');
//				$("#cadastro_novo_usuario_pj select[name='enderecosList[0].cidade.estado.nome']").select2();
			}
		
			waitingDialog.hide();
			
		}).fail(function(erro){
			waitingDialog.hide();
			$('#erro_requisicao').modal('show');
		});
	});
	
	$("#cadastro_novo_usuario_pf").validate({
	    rules: {
	        senha : {
	        	lettersAndNumbersPassword: true,
	        	minlength: 3,
	            maxlength: 20,
	            required: true
	        },
	        confirmacao_de_senha: {
	            minlength: 3,
	            maxlength: 20,
	            required: true,
	            equalTo: "#senha_pf"
	        },
	        nome : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        sobrenome : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        rg : {
	        	minlength: 3,
	            maxlength: 20,
	            required: true
	        },
	        cpf : {
	            maxlength: 14,
	            required: true,
	            remote: {
	            	url: "verificar_cpf_nao_existente.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        		cpf: function() {
			        			return $("input[name='cpf']").val();
			        		}
			          }
	            } 
	        },
	        dataDeNascimentoString : {
	            required: true, 
//	            remote: {
//	            	url: "verificar_data_de_nascimento.json",
//			        dataType: "json",
//			        contentType: "application/json",
//			        data: {
//			        	dataDeNascimentoString: function() {
//			        			return $("input[name='dataDeNascimentoString']").val();
//			        		}
//			          }
//	            } 
	        },
	        "telefonesList[0].telefone" : {
	            required: true
	        },
	        email : {
	        	lowercases : true,
	        	maxlength: 80,
	        	required: true,
	            email: true, 
	            remote: {
	            	url: "verificar_email.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        	email : function() {
			        			return $("#email_pf").val();
			        	}
			        }
	            }
	        },
	        sexo : {
	        	required: true
	        },
	        "enderecosList[0].cep" : {
	        	required : true,
	        	minlength: 9,
	        	maxlength: 9
	        },
	        "enderecosList[0].logradouro" : {
	        	required : true,
	        	maxlength: 70
	        },
	        "enderecosList[0].numero" : {
	        	required : true,
	        	digits: true
	        },
	        "enderecosList[0].bairro" : {
	        	required : true
	        },
	        "enderecosList[0].cidade.estado.nome" : {
	        	required : true,
	        	estadoVazio: "default"
	        },
	        "enderecosList[0].cidade.nome" : {
	        	required : true,
	        	maxlength: 40
	        }  
	    },
	    submitHandler: function(form) {
	    	
	    	$('#enviar_cadastro_usuario_pf').attr('disabled', true);
	    	
	    	waitingDialog.show('Aguarde');
	    	
	    	var senha = $("#cadastro_novo_usuario_pf input[name='senha']").val();
	    	senha = $.md5(senha);
	    	$("#cadastro_novo_usuario_pf input[name='senha']").val(senha);
	    	
	    	
	    	var cpf = $("#cadastro_novo_usuario_pf input[name='cpf']").val();
	    	cpf = cpf.replace(/\./g, '');
	    	cpf = cpf.replace(/-/g, '');
	    	$("#cadastro_novo_usuario_pf input[name='cpf']").val(cpf);
	    	
	    	var cep = $("#cadastro_novo_usuario_pf input[name='enderecosList[0].cep']").val();
	    	cep = cep.replace(/-/g, '');
	    	$("#cadastro_novo_usuario_pf input[name='enderecosList[0].cep']").val(cep);
	    	
	    	var telefone = $("#telefone_pf").val();
	    	telefone = telefone.replace(/-/g, '');
	    	telefone = telefone.replace(/ /g, '');
	    	
	    	var dddTelefone = telefone.split(')');
	    	
	    	dddTelefone[0] = dddTelefone[0].replace('(', '');
	    	
	    	
	    	$("#ddd_pf").val(dddTelefone[0]);
	    	$("#telefone_pf").val(dddTelefone[1]);
	    	
	    	form.submit();  	
	    },
	    invalidHandler: function(event, validator) {
	    	console.log(event);
	    },
	    highlight: function(element) {
	        $(element).closest('.formulario-usuario-pf').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.formulario-usuario-pf').removeClass('has-error');
	    }
	});
	
	$("#cadastro_novo_usuario_pj").validate({
	    rules: {
	        senha : {
	        	lettersAndNumbersPassword: true,
	        	minlength: 3,
	            maxlength: 20,
	            required: true
	        },
	        confirmacao_de_senha: {
	            minlength: 3,
	            maxlength: 20,
	            required: true,
	            equalTo: "#senha_pj"
	        },
	        nomeFantasia : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        razaoSocial : {
	        	minlength: 3,
	            maxlength: 100,
	            required: true
	        },
	        cnpj : {
	            maxlength: 18,
	            required: true,
	            remote: {
	            	url: "verificar_cnpj_nao_existente.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        		cnpj: function() {
			        			return $("input[name='cnpj']").val();
			        		}
			          }
	            } 
	        },
	        nomeTitular : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        sobrenomeTitular : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        rgTitular : {
	        	minlength: 3,
	            maxlength: 20,
	            required: true
	        },
	        cpfTitular : {
	            maxlength: 14,
	            required: true,
	            remote: {
	            	url: "verificar_cpf_nao_existente_pessoa_juridica.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        		cpf: function() {
			        			return $("input[name='cpfTitular']").val();
			        		}
			          }
	            } 
	        },
	        dataDeNascimentoTitularString : {
	            required: true,
//	            remote: {
//	            	url: "verificar_data_de_nascimento.json",
//			        dataType: "json",
//			        contentType: "application/json",
//			        data: {
//			        	dataDeNascimentoString: function() {
//			        			return $("input[name='dataDeNascimentoTitularString']").val();
//			        		}
//			          }
//	            }
	        },
	        "telefonesList[0].telefone" : {
	            required: true
	        },
	        "email" : {
	        	lowercases : true,
	        	maxlength: 80,
	        	required: true,
	            email: true,
	            remote: {
	            	url: "verificar_email.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        	email : function() {
			        			return $("#email_pj").val();
			        	}
			        }
	            }
	        },
	        "enderecosList[0].cep" : {
	        	required : true,
	        	minlength: 9,
	        	maxlength: 9
	        },
	        "enderecosList[0].logradouro" : {
	        	required : true,
	        	maxlength: 70
	        },
	        "enderecosList[0].numero" : {
	        	required : true,
	        	digits: true
	        },
	        "enderecosList[0].bairro" : {
	        	required : true
	        },
	        "enderecosList[0].cidade.estado.nome" : {
	        	required : true, 
	        	estadoVazio: "default"
	        },
	        "enderecosList[0].cidade.nome" : {
	        	required : true,
	        	maxlength: 40
	        }  
	    },
	    submitHandler: function(form) {
	    	
	    	$('#enviar_cadastro_usuario_pj').attr('disabled', true);
	    	$('#enviar_cadastro_usuario_pj').prop('disabled', true);
	    	
	    	waitingDialog.show('Aguarde');
	    	
	    	var senha = $("#cadastro_novo_usuario_pj input[name='senha']").val();
	    	senha = $.md5(senha);
	    	$("#cadastro_novo_usuario_pj input[name='senha']").val(senha);
	    	
	    	
	    	var cnpj = $("#cadastro_novo_usuario_pj input[name='cnpj']").val();
	    	cnpj = cnpj.replace(/\./g, '');
	    	cnpj = cnpj.replace(/-/g, '');
	    	cnpj = cnpj.replace(/\//g, '');
	    	$("#cadastro_novo_usuario_pj input[name='cnpj']").val(cnpj);
	    	
	    	
	    	var cpfTitular = $("#cadastro_novo_usuario_pj input[name='cpfTitular']").val();
	    	cpfTitular = cpfTitular.replace(/\./g, '');
	    	cpfTitular = cpfTitular.replace(/-/g, '');
	    	cpfTitular = cpfTitular.replace(/\//g, '');
	    	$("#cadastro_novo_usuario_pj input[name='cpfTitular']").val(cpfTitular);
	    	
	    	
	    	var cep = $("#cadastro_novo_usuario_pj input[name='enderecosList[0].cep']").val();
	    	cep = cep.replace(/-/g, '');
	    	$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cep']").val(cep);
	    	
	    	var telefone = $("#telefone_pj").val();
	    	telefone = telefone.replace(/-/g, '');
	    	telefone = telefone.replace(/ /g, '');
	    	
	    	var dddTelefone = telefone.split(')');
	    	
	    	dddTelefone[0] = dddTelefone[0].replace('(', '');
	    	
	    	
	    	$("#ddd_pj").val(dddTelefone[0]);
	    	$("#telefone_pj").val(dddTelefone[1]);
	    	
	    	form.submit();  	
	    },
	    invalidHandler: function(event, validator) {
	    	console.log(event);
	    },
	    highlight: function(element) {
	        $(element).closest('.formulario-usuario-pj').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.formulario-usuario-pj').removeClass('has-error');
	    }
	});
	
	
	$.validator.addMethod("estadoVazio", function(value, element, arg){
		return arg != value;
	}, "Escolha um estado");
	
	
	$("input[name='dataDeNascimentoString']").rules("add", {
		messages: {
			remote : "É necessário ser maior de 18 anos"
		}
	});
	
	$("input[name='dataDeNascimentoTitularString']").rules("add", {
		messages: {
			remote : "É necessário ser maior de 18 anos"
		}
	});
	
	
	jQuery.validator.addMethod("lettersAndNumbers", function(value, element) 
	{
		return this.optional(element) || /^[a-z0-9]+$/g.test(value);
	}, "Apenas letras minúsculas e números são permitidos"); 
	
	jQuery.validator.addMethod("lettersAndNumbersPassword", function(value, element) 
	{
		return this.optional(element) || /^[a-zA-Z0-9]+$/g.test(value);
	}, "Apenas letras e números são permitidos");
	
	$.validator.addMethod('lowercases', function(value) {
	    return value.match(/^[^A-Z]+$/);
	}, 'Apenas letras minúsculas são permitidos');
});