	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}

function enviarParaCarrinhoDeCompras() {
	window.location = urlFinal + "mostrar_carrinho_de_compras";
}

function verificarCompraNaoFinalizada (idCompra) {
	
	$.ajax({
		url: urlFinal + "usuario/verificar_compra_no_dia.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idCompra : idCompra}
	}).success(function(retorno){		
		
		$('#confirma_finalizacao_de_compra_nao_finalizada').attr("onclick", "finalizarCompraNaoFinalizada("+idCompra+")");
		
		if(retorno === true || retorno == true) {
			$('#texto_finalizacao_de_compra').text('Já existe uma compra iniciada no dia de hoje, você deseja juntar os produtos dessa compra com a compra do dia de hoje?');
			
		} else {
			$('#texto_finalizacao_de_compra').text('Você tem certeza que deseja finalizar esta compra?')
		}
		
		$('#verificacao_finalizacao_de_compra').modal("show");
		
	}).fail(function(erro){	
		$('#erro_generico_compra_nao_finalizada').modal("show");
	});
}


function finalizarCompraNaoFinalizada(idCompra) {
	$.ajax({
		url: urlFinal + "usuario/finalizar_compra_nao_finalizada.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idCompra : idCompra}
	}).success(function(){		
		$('#finalizacao_sucesso_carrinho_de_compra').modal("show");
	}).fail(function(erro){	
		$('#finalizacao_compras_nao_finalizadas_erro').modal("show");
	});
}


function excluirCompraNaoFinalizada(idCompra) {
	$.ajax({
		url: urlFinal + "usuario/remover_compra_nao_finalizada.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idCompra : idCompra}
	}).success(function(retorno){		
		$('#compraDeId_' + idCompra).remove();
		
		if(retorno.length == 0) {	
			$('#carrinho_de_compras_vazio').modal("show");
		} else {
			$('#remocao_carrinho_de_compras_ok').modal("show");	
		}
		
	}).fail(function(erro){	
		$('#remocao_carrinho_de_compras_erro').modal("show");
	});
	
	
}

function verificarRemocaoCompra(idCompra) {
	
	$('#confirma_exclusao_compra').attr("onclick", "excluirCompraNaoFinalizada("+idCompra+")");
	
	$('#verificacao_remocao_compra').modal("show");
}

function verificarRemocaoProduto(idProduto) {
	
	$('#confirma_exclusao_produto_carrinho').attr("onclick", "confirmacaoExclusao("+idProduto+")");
	$('#cancela_exclusao_produto_carrinho').attr("onclick", "retirarClickBotao("+idProduto+")");
	
	$('#verificacao_remocao_carrinho_de_compras').modal("show");
}

function retirarClickBotao(idProduto) {
	$("#cancelar_remocao_" + idProduto).attr("checked", false);
}

function confirmacaoExclusao(idProduto) {
	
	$.ajax({
		url: urlFinal + "usuario/remover_produto_de_compras_nao_finalizadas.json",
	    dataType: "json",
	    contentType: "application/json",
		data: { idProduto : idProduto}
	}).success(function(retorno){		
		$('#produto_' + idProduto).remove();
		
		if(retorno.length == 0) {	
			$('#carrinho_de_compras_vazio').modal("show");
			$('#valor_total_' + id).text('Valor total: R$ 0,00');
		} else {
			$('#remocao_carrinho_de_compras_ok').modal("show");
			
			var retiradaHtml = [];
			$('.comprasComIds').each(function(){
				var idCompleto = $(this).attr('id');
				var idArray = idCompleto.split('_');
				var id = parseInt(idArray[1]);
				var existeCompra = false;
				for(var i = 0; i < retorno.length; i++) {
					
					if(id === retorno[i].compraDto.id) {
						existeCompra = true;
						var correios = false;
						for(var j = 0; j < retorno[i].listaApresentacoes.length; j++) {
							if(retorno[i].listaApresentacoes[j].infosGeraisProdutoUsuario.tipoEnvio == "CORREIOS"){
								correios = true;
							} 
						}
						
						if(correios) {	
							$('#valor_total_' + id).text('Valor total: R$ ' + retorno[i].valorTotal + ' Sem cálculo de frete');
						} else if(!correios) {
							$('#valor_total_' + id).text('Valor total: R$ ' + retorno[i].valorTotal);
						} 
					}
					
				}
				
				/*
				 * retirada da div que não existe mais
				 */
				if(!existeCompra){
					retiradaHtml.push(id);
				}	
			});
			
			for(var i = 0; i < retiradaHtml.length; i++) {
				$('#compraDeId_' + retiradaHtml[i]).remove();
			}
			
			
		}
		
	}).fail(function(erro){	
		$('#remocao_carrinho_de_compras_erro').modal("show");
	});
	
}