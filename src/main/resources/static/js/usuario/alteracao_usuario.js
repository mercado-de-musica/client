	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}


//	excluir telefone
	
function excluirTelefone(numeroDoTelefone){
	if($(".telefone").size() > 1) {
		$("#telefone_" + numeroDoTelefone).remove();
	}
}

function excluirEndereco(numeroDoEndereco){
	if($(".endereco").size() > 1) {
		$("#endereco_" + numeroDoEndereco).remove();
	}
}

$(document).ready(function(){
	
//	acrescentar telefones
	
	$("#acrescentar_telefone").click(function(){
		
		var elementoClonado = null;
		var ultimoElementoContagem = 0;
		
		$(".telefone").each(function(){
			ultimoElementoContagem++;
			$(this).find('.select2').each(function(){
				$(this).remove();
			});
			elementoClonado = $(this).clone();
		});	
		
		$(elementoClonado).attr("id", "telefone_"+(ultimoElementoContagem));
		
		$(elementoClonado).find("input, select").each(function(index){
			$(this).val("");
			
			var splitNames = $(this).attr("name").split("[");
			var splitNumero = splitNames[1].split("]");
			var numeroNovo = parseInt(splitNumero[0]) + 1;
			
			$(this).attr("name", splitNames[0] +"["+ numeroNovo + "]" + splitNumero[1]);
			
			if($(this).hasClass("select-tipo-telefone")) {
				$(this).trigger('change.select2');
			}
		});
		
		
		$(elementoClonado).find("button").each(function(index){
			
			var splitOnClick = $(this).attr("onclick").split("(");
			var splitNumero = splitOnClick[1].split(")");
			var numeroNovo = parseInt(splitNumero[0]) + 1;
			
			$(this).attr("onclick", splitOnClick[0] +"("+ numeroNovo + ")" + splitNumero[1]);
		});
			
			
		$("#telefones").append(elementoClonado);
		
		
		
		$("input[name='telefonesList["+ultimoElementoContagem+"].telefone']").mask("(99) 9999-9999?9");
		
		$("input[name='telefonesList["+ultimoElementoContagem+"].telefone']").rules("add", {
	        required: true
		});
		
		
		$('.select-tipo-telefone').select2({
			allowClear: true
		});
		
	});
	
	
	$("#acrescentar_endereco").click(function(){
		
		var elementoClonado = $(".endereco").clone();
		var inputs = $(elementoClonado).children('input');
		var select = $(elementoClonado).children('select');
		
		var ultimoElementoContagem = $(elementoClonado).length;
		var ultimoElemento = $(elementoClonado).get(ultimoElementoContagem - 1);
		
		$(elementoClonado).attr("id", "endereco_"+(ultimoElementoContagem));
		
		$(ultimoElemento).find("input, select").each(function(index){
			$(this).val("");
			
			if($(this).attr("type") == "radio") {
				$(this).attr("checked", false);
				$(this).val(ultimoElementoContagem);
			} else {
				var splitNames = $(this).attr("name").split("[");
				var splitNumero = splitNames[1].split("]");
				var numeroNovo = parseInt(splitNumero[0]) + 1;
				
				$(this).attr("name", splitNames[0] +"["+ numeroNovo + "]" + splitNumero[1]);
			}
			
			
		});
		
		$(ultimoElemento).find("button").each(function(index){
			
			var splitOnClick = $(this).attr("onclick").split("(");
			var splitNumero = splitOnClick[1].split(")");
			var numeroNovo = parseInt(splitNumero[0]) + 1;
			
			$(this).attr("onclick", splitOnClick[0] +"("+ numeroNovo + ")" + splitNumero[1]);
		});
			
			
		$("#enderecos").append(elementoClonado);
		
		var tipoDoForm = $('form').attr('id');
		
		
		$("#" + tipoDoForm + " input[name='enderecosList["+ultimoElementoContagem+"].cep']").focusout(function(){
			var infosEndereco = new Object();
			infosEndereco.tipoDeFormulario = tipoDoForm;
			infosEndereco.index = ultimoElementoContagem;
			infosEndereco.cep = $(this);

			obterEnderecoPorCep(infosEndereco);	
		});
		
		
		$("input[name='enderecosList["+ultimoElementoContagem+"].cep']").mask("99999-999",{placeholder:"xxxxx-xxx"});
		
		
		$("input[name='enderecosList["+ultimoElementoContagem+"].cep']").rules("add", {
			required : true,
        	minlength: 9,
        	maxlength: 9
		});
		
		$("input[name='enderecosList["+ultimoElementoContagem+"].logradouro']").rules("add", {
			required : true,
	    	maxlength: 70
		});
		
		$("input[name='enderecosList["+ultimoElementoContagem+"].numero']").rules("add", {
			required : true,
	    	digits: true
		});
		
		$("input[name='enderecosList["+ultimoElementoContagem+"].bairro']").rules("add", {
			required : true
		});
		
		$("input[name='enderecosList["+ultimoElementoContagem+"].cidade.estado.nome']").rules("add", {
			required : true
		});
		
		$("input[name='enderecosList["+ultimoElementoContagem+"].cidade.nome']").rules("add", {
			required : true,
			maxlength: 40
		});		
	});
	
	
	$("input[name='cpf']").mask("999.999.999-99");
	$(".telefone-input").mask("(99) 9999-9999?9");
	$(".cep-input").mask("99999-999");
	
	$("input[name='cnpj']").mask("99.999.999/9999-99");
	$("input[name='cpfTitular']").mask("999.999.999-99");
	
	$("#cadastro_novo_usuario_pf input[name='enderecosList[0].cep']").focusout(function(){
		var infosEndereco = new Object();
		infosEndereco.tipoDeFormulario = "cadastro_novo_usuario_pf";
		infosEndereco.index = "0";
		infosEndereco.cep = $(this);

		obterEnderecoPorCep(infosEndereco);	
	});
	
	
	
	$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cep']").focusout(function(){
		var infosEndereco = new Object();
		infosEndereco.tipoDeFormulario = "cadastro_novo_usuario_pj";
		infosEndereco.index = "0";
		infosEndereco.cep = $(this);

		obterEnderecoPorCep(infosEndereco);	
	});
	
	$("#cadastro_novo_usuario_pf").validate({
	    rules: {
	        nome : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        sobrenome : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        rg : {
	        	minlength: 3,
	            maxlength: 20,
	            required: true
	        },
	        cpf : {
	            maxlength: 14,
	            required: true, 
	        },
	        dataDeNascimentoString : {
	            required: true,
	            remote: {
	            	url: urlFinal + "verificar_data_de_nascimento.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        	dataDeNascimentoString: function() {
			        		return $("input[name='dataDeNascimentoString']").val();
			        	}
			        }
	            } 
	        },
	        "telefonesList[0].ddd" : {
	        	required: true
	        },
	        "telefonesList[0].telefone" : {
	            required: true
	        },
	        "email" : {
	        	maxlength: 80,
	        	required: true,
	            email: true, 
	            remote: {
	            	url: urlFinal + "verificar_email.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        	email : function() {
			        			return $("#email_pf").val();
			        	}
			        }
	            }
	        },
	        sexo : {
	        	required: true
	        },
	        "enderecosList[0].cep" : {
	        	required : true,
	        	minlength: 9,
	        	maxlength: 9
	        },
	        "enderecosList[0].logradouro" : {
	        	required : true,
	        	maxlength: 70
	        },
	        "enderecosList[0].numero" : {
	        	required : true,
	        	digits: true
	        },
	        "enderecosList[0].bairro" : {
	        	required : true
	        },
	        "enderecosList[0].cidade.estado.nome" : {
	        	required : true
	        },
	        "enderecosList[0].cidade.nome" : {
	        	required : true,
	        	maxlength: 40
	        }  
	    },
	    submitHandler: function(form) {
	    	
	    	var cpf = $("#cadastro_novo_usuario_pf input[name='cpf']").val();
	    	cpf = cpf.replace(/\./g, '');
	    	cpf = cpf.replace(/-/g, '');
	    	$("#cadastro_novo_usuario_pf input[name='cpf']").val(cpf);
	    	
	    	
	    	
	    	$(".cep-input").each(function() {
	    		var cep = $(this).val();
	    		cep = cep.replace(/-/g, '');
	    		$(this).val(cep);
	    	});
	    	
	    	
	    	$(".telefone-input").each(function(index, value) {
		    	
	    		var telefone = $(value).val();
		    	telefone = telefone.replace(/-/g, '');
		    	telefone = telefone.replace(/ /g, '');
		    	
		    	var dddTelefone = telefone.split(')');
		    	
		    	dddTelefone[0] = dddTelefone[0].replace('(', '');
		    	
		    
		    	$("#cadastro_novo_usuario_pf input[name='telefonesList[" + index + "].ddd']").val(dddTelefone[0]);
		    	$("#cadastro_novo_usuario_pf input[name='telefonesList[" + index + "].telefone']").val(dddTelefone[1]);

	    	});
	    	
	    	
	    	form.submit();  	
	    },
	    invalidHandler: function(event, validator) {
	    	console.log(event);
	    },
	    highlight: function(element) {
	        $(element).closest('.formulario-usuario-pf').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.formulario-usuario-pf').removeClass('has-error');
	    }
	});
	
	
	$("#cadastro_novo_usuario_pj").validate({
	    rules: {
	        nomeFantasia : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        razaoSocial : {
	        	minlength: 3,
	            maxlength: 100,
	            required: true
	        },
	        cnpj : {
	            maxlength: 18,
	            required: true
	        },
	        nomeTitular : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        sobrenomeTitular : {
	        	minlength: 3,
	            maxlength: 50,
	            required: true
	        },
	        rgTitular : {
	        	minlength: 3,
	            maxlength: 20,
	            required: true
	        },
	        cpfTitular : {
	            maxlength: 14,
	            required: true,
	            remote: {
	            	url: urlFinal + "verificar_cpf_nao_existente_pessoa_juridica.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        	id: function() {
			        		return $("input[name='id']").val();
			        	},
			        	cpf: function() {
			        		return $("input[name='cpfTitular']").val();
			        	}
			        }
	            } 
	        },
	        dataDeNascimentoTitularString : {
	            required: true, 
	            remote: {
	            	url: urlFinal + "verificar_data_de_nascimento.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        	dataDeNascimentoString: function() {
			        		return $("input[name='dataDeNascimentoTitularString']").val();
			        	}
			        }
	            } 
	        },
	        "email" : {
	        	maxlength: 80,
	        	required: true,
	            email: true,
	            remote: {
	            	url: urlFinal + "verificar_email.json",
			        dataType: "json",
			        contentType: "application/json",
			        data: {
			        	email : function() {
			        			return $("#email_pj").val();
			        	}
			        }
	            }
	        }
	    },
	    submitHandler: function(form) {
	    	
	    	
	    	var cnpj = $("#cadastro_novo_usuario_pj input[name='cnpj']").val();
	    	cnpj = cnpj.replace(/\./g, '');
	    	cnpj = cnpj.replace(/-/g, '');
	    	cnpj = cnpj.replace(/\//g, '');
	    	$("#cadastro_novo_usuario_pj input[name='cnpj']").val(cnpj);
	    	
	    	var cpf = $("#cadastro_novo_usuario_pj input[name='cpfTitular']").val();
	    	cpf = cpf.replace(/\./g, '');
	    	cpf = cpf.replace(/-/g, '');
	    	cpf = cpf.replace(/\//g, '');
	    	$("#cadastro_novo_usuario_pj input[name='cpfTitular']").val(cpf);
	    	
	    	
	    	var cep = $("#cadastro_novo_usuario_pj input[name='enderecosList[0].cep']").val();
	    	cep = cep.replace(/-/g, '');
	    	$("#cadastro_novo_usuario_pj input[name='enderecosList[0].cep']").val(cep);
	    	 	
	    	
	    	$(".telefone-input").each(function(index, value) {
		    	
	    		var telefone = $(value).val();
		    	telefone = telefone.replace(/-/g, '');
		    	telefone = telefone.replace(/ /g, '');
		    	
		    	var dddTelefone = telefone.split(')');
		    	
		    	dddTelefone[0] = dddTelefone[0].replace('(', '');
		    	
		    
		    	$("#cadastro_novo_usuario_pj input[name='telefonesList[" + index + "].ddd']").val(dddTelefone[0]);
		    	$("#cadastro_novo_usuario_pj input[name='telefonesList[" + index + "].telefone']").val(dddTelefone[1]);

	    	});
	    	
	    	
	    	form.submit();  	
	    },
	    invalidHandler: function(event, validator) {
	    	console.log(event);
	    },
	    highlight: function(element) {
	        $(element).closest('.formulario-usuario-pj').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.formulario-usuario-pj').removeClass('has-error');
	    }
	});
});