$('#cadastrar_nova_conta_bancaria').validate({
	rules: {
		numeroDaAgencia : {
			minlength: 4,
			maxlength: 4,
			number: true,
			required : true
		},
		digitoDaAgencia : {
			maxlength: 2,
			required : true
		},
		numeroDaConta : {
			maxlength: 20,
			number: true,
			required : true
		},
		digitoDaConta : {
			maxlength: 2,
			required : true
		},
		nomeDaConta : {
			maxlength: 30
		}
	},
	submitHandler: function(form) {
		
		form.submit();  
	},
	invalidHandler: function(event, validator) {
    	console.log(event);
    },
    highlight: function(element) {
        $(element).closest('#cadastrar_nova_conta_bancaria').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('#cadastrar_nova_conta_bancaria').removeClass('has-error');
    }
});