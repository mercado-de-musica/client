	var url = window.location.href.toString();
	var urlFinal = '';
//	Dev
	if(url.indexOf("Client") != -1) {
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2]  + '/' + urlCortada[3] + '/';
		
	} else {
//	Prod	
		
		var urlCortada = url.split('/');
		urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	}
	

function obterEnderecoPorCep(infosEndereco) {
	
	var campo = $(infosEndereco.cep);
	var campoInteiro = campo.val().replace(/-/g, '');
		
	$.ajax({
		url: urlFinal + "obter_endereco_por_cep.json",
        dataType: "json",
        contentType: "application/json",
		data: { cep : campoInteiro}
	}).success(function(dados){
		
		$("#"+infosEndereco.tipoDeFormulario+" select[name='enderecosList["+infosEndereco.index+"].cidade.estado.nome']").select2({
		  allowClear: true
		});
		
		if(dados.resultado == 1) {
			
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].bairro']").val(dados.bairro).attr('readonly', true).parent().removeClass("has-error");
			
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].cidade.nome']").val(dados.cidade).attr('readonly', true).parent().removeClass("has-error");
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].logradouro']").val(dados.tipo_logradouro + " " + dados.logradouro).attr('readonly', true).parent().removeClass("has-error");
			
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].numero']").val("");
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].complemento']").val("");
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].proximidade']").val("");
			
			
			$("#"+infosEndereco.tipoDeFormulario+" select[name='enderecosList["+infosEndereco.index+"].cidade.estado.nome']").find('option').remove().end();
			
			$.ajax({
				url: urlFinal + "obter_estados.json",
		        dataType: "json",
		        contentType: "application/json",
			}).success(function(estados){
				
				for (var i = 0; i < estados.length; i++) {
					if(estados[i].sigla == dados.uf) {
						$("#"+infosEndereco.tipoDeFormulario+" select[name='enderecosList["+infosEndereco.index+"].cidade.estado.nome']").append('<option value="'+estados[i].sigla+'" selected readonly>'+estados[i].descricao+'</option>');
					}
				}
			
			}).fail(function(erro){
				waitingDialog.hide();
				$('#erro_requisicao').modal('show');
			});
			
			
			$("#"+infosEndereco.tipoDeFormulario+" select[name='enderecosList["+infosEndereco.index+"].cidade.estado.nome'] option").trigger('change.select2');
			
		} else {
			
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].bairro']").attr('readonly', false);
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].cidade.nome']").attr('readonly', false);
			$("#"+infosEndereco.tipoDeFormulario+" input[name='enderecosList["+infosEndereco.index+"].logradouro']").attr('readonly', false);
			
			
			
			$("#"+infosEndereco.tipoDeFormulario+" select[name='enderecosList["+infosEndereco.index+"].cidade.estado.nome']").find('option').remove().end();
			
			$.ajax({
				url: "obter_estados.json",
		        dataType: "json",
		        contentType: "application/json",
			}).success(function(estados){
				
				for (var i = 0; i < estados.length; i++) {
					$("#"+infosEndereco.tipoDeFormulario+" select[name='enderecosList[0].cidade.estado.nome']").append('<option value="'+estados[i].sigla+'">'+estados[i].descricao+'</option>');
				}
			
			}).fail(function(erro){
				waitingDialog.hide();
				$('#erro_requisicao').modal('show');
			});
			
			
			
			
			$("#"+infosEndereco.tipoDeFormulario+" select[name='enderecosList["+infosEndereco.index+"].cidade.estado.nome'] option").trigger('change.select2');
			
			
			
			$('#usuario_modal').modal("show");
		}	
	}).fail(function(erro){
		console.log(erro);
	});
}