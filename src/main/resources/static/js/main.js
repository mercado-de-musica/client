$(document).ready(function () {		 
	$( window ).load(function() {
		if($('.apresentacao-inicial')[0]) {
			var height = 0;
			$( '.apresentacao-inicial' ).each(function() {
				if($(this).height() > height) {
					height = $(this).height();
				}
			});
			$('.apresentacao-inicial').height(height);
		}
	});
	
	if($('.datepicker')[0]) {
		$('.datepicker').datepicker({
			format: 'dd/mm/yyyy',                
			language: 'pt-BR',
		});
	}
	
	if($('.lojas-online-carousel')[0]) {
		$('.lojas-online-carousel').slick({
			  dots: true,
			  infinite: false,
			  speed: 300,
			  slidesToShow: 3,
			  slidesToScroll: 3,
			  responsive: [
			    {
			      breakpoint: 768,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
			});
	}
	
	
	var url = window.location.href.toString();
	var urlFinal = '';
	var urlCortada = url.split('/');
	urlFinal = urlCortada[0] + '/' + urlCortada[1] + '/' + urlCortada[2] + '/';
	
	
	$("select[name='loja-virtual']").change(function(){
		if($("select[name='loja-virtual'] option:selected").val() != undefined && $("select[name='loja-virtual'] option:selected").val() != -1) {
			
			window.location = urlFinal + "loja/" + $("select[name='loja-virtual'] option:selected").val();
		} else {
			$('#ir-loja-virtual').modal("show");
		}
	});
	
//	function formatState (state) {
//		  if (!state.id) { return state.text; }
//		  var $state = $(
//		    '<span><img src="vendor/images/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
//		  );
//		  return $state;
//		};
//
//		$(".js-example-templating").select2({
//		  templateResult: formatState
//		});
	
	$("select[name='loja-virtual']").select2({
		language: "pt-BR",
		placeholder: {
			id: '-1',
			text: 'Escolha aqui a loja virtual'
		},
		allowClear: true,
		 ajax: {
		    url: urlFinal + "bucar_loja_virtual.json",
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      return {
		        q: params.term, // search term
		        page: params.page
		      };
		    },
		    processResults: function (data, params) {
		    	
		    	var arrayResults = [];
		    	for(var i = 0; i < data.length; i++) {
		    		if(data[i].razaoSocial !== undefined && data[i].razaoSocial != null && data[i].nomeFantasia !== undefined && data[i].nomeFantasia != null) {
		    			arrayResults.push({"id": data[i].id, "text": data[i].nomeFantasia});
		    			
		    		} else if (data[i].razaoSocial !== undefined && data[i].razaoSocial != null && (data[i].nomeFantasia === undefined || data[i].nomeFantasia == null)) {
		    			arrayResults.push({"id": data[i].id, "text": data[i].razaoSocial});
		    			
		    		} else { 		
		    			arrayResults.push({"id": data[i].id, "text": data[i].nome + ' ' + data[i].sobrenome});
		    	
		    		}	
		    	}
		    	
		    	return {
		    		results: arrayResults
		        };
		    },
		    cache: true
		  },
		  escapeMarkup: function (markup) { return markup; },
		  minimumInputLength: 3
	});
	
	
	if($('#modal-lead').length > 0) {
		$.ajax({
			url: "/obterUsuarioLogado.json",
	        dataType: "json",
	        contentType: "application/json",
		}).success(function(dados){
			
			if(dados == false) {
				$('#modal-lead').modal('show');
			}
		}).fail(function(erro){
			$('#erro_requisicao').modal('show');
		});	
	}
	
	
});