<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-2percent margin-bottom-2percent">
			<c:choose>
				<c:when test="${compraBoletoDto != null}">
					<!-- boleto -->
					
					<c:if test="${compraDto.statusTransacao == 'EM_MONITORAMENTO' || compraDto.statusTransacao == 'AGUARDANDO_PAGAMENTO' || compraDto.statusTransacao == 'EM_RECUPERACAO'}">
						<p>Sua compra está <b>em análise</b>. Assim que houver a constatação de pagamento lhe enviaremos um e-mail com a autorização da compra.</p>
					</c:if>
					
					<p class="text-center"><a href="${compraBoletoDto.urlBoleto}" target="_blank">Clique aqui para abrir o seu boleto</a></p>
				</c:when>
				<c:otherwise>
					<!-- cartao de credito -->
					
					<c:if test="${compraDto.statusTransacao == 'EM_MONITORAMENTO' || compraDto.statusTransacao == 'AGUARDANDO_PAGAMENTO' || compraDto.statusTransacao == 'EM_RECUPERACAO'}">
						<p>Sua compra foi confirmada com sucesso e seu cartão de crédito está <b>em análise</b>. Assim que houver a liberação por parte do Gateway de pagamento lhe enviaremos um e-mail com a autorização da compra.</p>
					</c:if>
					
					<c:if test="${compraDto.statusTransacao != 'AGUARDANDO_PAGAMENTO' && compraDto.statusTransacao != 'EM_MONITORAMENTO' && compraDto.statusTransacao != 'EM_RECUPERACAO'}">
						<p>Sua compra foi confirmada com sucesso. Enviamos um e-mail com as instruções de envio dos vendedores dos produtos.</p>
					</c:if>
				</c:otherwise>
			</c:choose>	
		</div>
		
	</div>
</div>