<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-2percent margin-bottom-2percent">
			<c:forEach var="apresentacaoProduto" items="${listaApresentacoesDoDia}">
				<div id="produto_${apresentacaoProduto.infosGeraisProdutoUsuario.id}" class="row margin-top-2percent margin-bottom-2percent">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<img src="${apresentacaoProduto.endereco}${apresentacaoProduto.nomeDoArquivo}" class="img-responsive img-thumbnail" alt="${apresentacaoProduto.nomeDoArquivo}" />
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<h4> 
							${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.instrumentoAcessorio.nome} 
						   	${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.marca.nome} 
						   	${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.produto.nome} 
						   	${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.modelo.nome}
						</h4>
						
						<c:choose>
							<c:when test="${apresentacaoProduto.infosGeraisProdutoUsuario.tipoEnvio == 'CORREIOS'}">
								<c:forEach var="frete" items="${apresentacaoProduto.infosGeraisProdutoUsuario.calculoFrete}">
									<c:if test="${frete.ativoParaCompra == true}">
										<p id="preco_do_produto_frete_${apresentacaoProduto.infosGeraisProdutoUsuario.id}">Valor: <b>R$ ${apresentacaoProduto.infosGeraisProdutoUsuario.precoVisualizacaoString}</b> + <b>${frete.valorString}</b> de frete</p>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<p id="preco_do_produto_entrega_em_maos_${apresentacaoProduto.infosGeraisProdutoUsuario.id}">valor: <b>R$ ${apresentacaoProduto.infosGeraisProdutoUsuario.precoVisualizacaoString}</b></p>
							</c:otherwise>
						</c:choose>		
						
						<c:choose>
							<c:when test="${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
								<c:choose>
									<c:when test="${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.nomeFantasia != null}">
										Nome do vendedor: <b>${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.nomeFantasia}</b>		
									</c:when>
									<c:otherwise>
										Nome do vendedor: <b>${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.razaoSocial}</b>
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								Nome do vendedor: <b>${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.nome} ${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.sobrenome}</b>
							</c:otherwise>
						</c:choose>
						
						<c:forEach var="endereco" items="${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.enderecos}">
							<c:if test="${endereco.enderecoDefault}">
								<p><b>${endereco.cidade.nome} - ${endereco.cidade.estado.nome}</b></p>	
							</c:if>
						</c:forEach>
						
						<c:choose>
							<c:when test="${apresentacaoProduto.infosGeraisProdutoUsuario.tipoEnvio == 'CORREIOS'}">
								<select class="form-control escolha_de_frete_class" id="escolha_de_frete_${apresentacaoProduto.infosGeraisProdutoUsuario.id}">
			 			    		<c:forEach var="frete" items="${apresentacaoProduto.infosGeraisProdutoUsuario.calculoFrete}">
										<c:choose>
											<c:when test="${frete.ativoParaCompra == true}">
												<option value="${frete.id}" selected>R$ ${frete.valorString} - ${frete.descricaoCorreio}</option>
											</c:when>
											<c:otherwise>
												<option value="${frete.id}">R$ ${frete.valorString} - ${frete.descricaoCorreio}</option>
											</c:otherwise>
										</c:choose>	
									</c:forEach>
			 			    	</select>
			 			    	
							</c:when>
							<c:otherwise>
								<p>Tipo de entrega: <b>${apresentacaoProduto.infosGeraisProdutoUsuario.tipoEnvio.descricao}</b></p>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-lg-offset-3 margin-top-3percent">
						<div class="form-group" id="remover_carrinho_de_compras_${apresentacaoProduto.infosGeraisProdutoUsuario.id}">
	 				    	<button type="button" class="btn btn-default" id="cancelar_remocao_${apresentacaoProduto.infosGeraisProdutoUsuario.id}" onclick="verificarRemocaoProduto(${apresentacaoProduto.infosGeraisProdutoUsuario.id})">
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Remover do carrinho
							</button>
	 				    	
	 				  	</div>
					</div>
				</div>
				
				<hr class="hr-mercado-de-musica" id="hr_${apresentacaoProduto.infosGeraisProdutoUsuario.id}" />
			</c:forEach>
		</div>
		
		<div id="div_valor_total" class="col-xs-12 col-sm-12 col-md-8 col-md-offset-4 col-lg-9 col-lg-offset-3 margin-top-2percent margin-bottom-2percent">
			<p class="text-right" id="valor_total">Valor total: <b>R$ ${valorTotal}</b></p>
		</div>
		
		
		<form:form id="fechar_pedido_formulario" action="${pageContext.request.contextPath}/insercao_de_dados_para_pagamento" method="post" modelAttribute="compraDtoModel">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<input type="hidden" name="id" value="${compraDto.id}">
			<div id="div_fechar_pedido" class="col-xs-12 col-sm-12 col-md-8 col-md-offset-4 col-lg-9 col-lg-offset-3 margin-top-2percent margin-bottom-2percent">
				<p class="text-right">
					<button type="button" class="btn btn-default" id="fechar_pedido">
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Fechar Pedido
					</button>
				</p>
			</div>
		</form:form>
	
	
	</div>
</div>


<div id="gateway_depagamentos_modal_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro com o acesso do sistema e não será possível finalizar a compra agora. Por favor entre em contato conosco para maiores informações.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="gateway_depagamentos_modal_redirect_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Aviso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning" id="texto_selecionado_erro">
				Para finalizar a compra, você deve ter uma conta no MOIP cadastrada. <a href="${pageContext.request.contextPath}/usuario/cadastro_gateway_de_pagamento">Clique aqui para ir à área de cadastramento de conta.</a>
			</p>
		</div>
    	<div class="modal-footer">
			<a href="${pageContext.request.contextPath}/usuario/cadastro_gateway_de_pagamento">Ok</a>
		</div>
    </div>
  </div>
</div>


<div id="remocao_carrinho_de_compras_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro ao remover produto do carrinho de compras</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Não foi possível remover o produto do seu carrinho de compras. Por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="remocao_carrinho_de_compras_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Produto removido com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-muted">
				Seu produto foi removido do carrinho de compras com sucesso!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="carrinho_de_compras_vazio" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Carrinho vazio</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-muted">
				Seu produto foi removido do carrinho de compras com sucesso e agora seu carrinho está vazio
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="verificacao_remocao_carrinho_de_compras" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Remoção do produto</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning">
				Você tem certeza que deseja remover este produto?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirma_exclusao_produto_carrinho" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" id="cancela_exclusao_produto_carrinho" data-dismiss="modal">Cancelar</button>
		</div>
    </div>
  </div>
</div>

<div id="produto_com_aviso_de_entrega" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Aviso de distância</h3>
		</div>
    	<div class="modal-body">
			<div>
				<p class="text-center text-warning">
					Os seguintes produtos serão entregues em mãos e vocês se encontram a mais de 65km de distância. 
				</p>
			</div>
			<div class="escrita_de_produtos_div">
			</div>
			<div>
				<p class="text-center text-warning">
					Caso você não deseje finalizar a compra com esses produtos a mais de 65km de distância e com entrega em mãos, clique no botão remover ao lado do produto. 
				</p>
			</div>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" onclick="estouCienteDaDistancia()">Estou ciente dos riscos sobre a distância</button>
		</div>
    </div>
  </div>
</div>

<div id="ciente_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">		
			<p class="text-center text-danger">
				O sistema não reconheceu que você está ciente sobre as distâncias maiores que 65 km. Por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>