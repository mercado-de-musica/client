<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-2percent margin-bottom-2percent">
			<div class="table-responsive">
				<table class="table table-hover">
					<c:forEach var="produtosParaVenda" items="${compraDoDiaDto.produtosComprados}">
						<tr class="info">
		        			<td>${produtosParaVenda.infosGeraisProduto.instrumentoAcessorio.nome} ${produtosParaVenda.infosGeraisProduto.marca.nome} ${produtosParaVenda.infosGeraisProduto.produto.nome} ${produtosParaVenda.infosGeraisProduto.modelo.nome}</td>
		        			<td class="text-right"><b>R$ ${produtosParaVenda.precoString}
		        			
		        			<c:forEach items="${produtosParaVenda.calculoFrete}" var="frete">
		        				<c:if test="${frete.ativoParaCompra}">
		        					+ R$ ${frete.getValorString()} de frete
		        				</c:if>
		        			</c:forEach>
		        			
		        			</b></td>
		   				</tr>
					</c:forEach>
				</table>
			</div>
		</div>

		
		
		<!--  cartao de credito -->
				
		<section class="col-xs-12 col-sm-6 col-md-8 col-lg-5 margin-top-2percent margin-bottom-2percent">
			
			<h4>Cartão de crédito</h4>
			
			<form:form id="form_enviar_cartao_de_credito_novo" action="${pageContext.request.contextPath}/compra_cartao_de_credito" method="post" modelAttribute="pagamentoCartoesDto">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="hidden" name="armazenamento" value="" />
				<input type="hidden" name="instituicao" value="" />
				
				<div class="row margin-top-2percent margin-bottom-2percent" id="cartao_de_credito">
					<div class="col-xs-4 col-sm-4 col-md-2">
						<img class="img-responsive img-thumbnail img-cartoes-de-credito" id="mastercard" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/mastercard.jpg" alt="Mastercard" />
					</div>
					
					<div class="col-xs-4 col-sm-4 col-md-2 margin-bottom-2percent">
						<img class="img-responsive img-thumbnail img-cartoes-de-credito" id="visa" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/visa.jpg" alt="Visa" />
					</div>
					
					<div class="col-xs-4 col-sm-4 col-md-2 margin-bottom-2percent">
						<img class="img-responsive img-thumbnail img-cartoes-de-credito" id="amex" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/amex.jpg" alt="American Express" />
					</div>				
					
					<div class="col-xs-4 col-sm-4 col-md-2 margin-bottom-2percent">				
						<img class="img-responsive img-thumbnail img-cartoes-de-credito" id="elo" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/elo.jpg" alt="Elo" />
					</div>
										
					<div class="col-xs-4 col-sm-4 col-md-2 margin-bottom-2percent">
						<img class="img-responsive img-thumbnail img-cartoes-de-credito" id="hipercard" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/hipercard.jpg" alt="Hipercard" />
					</div>
					
					<div class="col-xs-4 col-sm-4 col-md-2 margin-bottom-2percent">
						<img class="img-responsive img-thumbnail img-cartoes-de-credito" id="hiper" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/hiper.jpg" alt="Hiper" />
					</div>
				</div>
			
			
				<div class="row margin-top-2percent margin-bottom-2percent" id="cartao_de_credito_inserts">
					<div class="form-group">
				    	<label for="number">Número do cartão:</label>
						<input type="text" name="number" class="form-control" id="number">
					</div>
					<div class="form-group">
				    	<label for="expireMonth">Mês de vencimento:</label>
						<select name="expireMonth" class="form-control" id="expireMonth">
							<option value="1">01</option>
						    <option value="2">02</option>
						    <option value="3">03</option>
						    <option value="4">04</option>
						    <option value="5">05</option>
						    <option value="6">06</option>
						    <option value="7">07</option>
						    <option value="8">08</option>
						    <option value="9">09</option>
						    <option value="10">10</option>
						    <option value="11">11</option>
						    <option value="12">12</option>		    
						</select>
						
						<label for="expireYear">Ano de vencimento:</label>
						<select name="expireYear" class="form-control" id="expireYear">
							<c:forEach var="anosDeVencimento" items="${listaDeAnosDeVencimentoCartaoDeCredito}">
								<option value="${anosDeVencimento}">${anosDeVencimento}</option>
							</c:forEach>
						</select>
						<div class="form-group">
					    	<label for="cvv2">Código de segurança:</label>
							<input type="text" name="cvv2" class="form-control" id="cvv2">
						</div>
						<div class="form-group">
					    	<label for="nomeCartao">Nome inserido no cartão:</label>
							<input type="text" name="nomeCartao" class="form-control" id="nomeCartao">
						</div>
						<div class="form-group" id="valores-parcelas-insercao-dados-id">
					    	<label for="nomeCartao">Parcelado em:</label>
							<select name="parcelas" class="form-control" id="parcelamento_quantidade_total">
								
								<c:if test="${valorTotalBigDecimal >= 10 && valorTotalBigDecimal < 20}">
									<option value="1" selected="selected">01x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 20 && valorTotalBigDecimal < 30}">
									<option value="1">01x</option>
									<option value="2" selected="selected">02x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 30 && valorTotalBigDecimal < 40}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3" selected="selected">03x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 40 && valorTotalBigDecimal < 50}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4" selected="selected">04x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 50 && valorTotalBigDecimal < 60}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5" selected="selected">05x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 60 && valorTotalBigDecimal < 70}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5">05x</option>
									<option value="6" selected="selected">06x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 70 && valorTotalBigDecimal < 80}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5">05x</option>
									<option value="6">06x</option>
									<option value="7" selected="selected">07x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 80 && valorTotalBigDecimal < 90}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5">05x</option>
									<option value="6">06x</option>
									<option value="7">07x</option>
									<option value="8" selected="selected">08x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 90 && valorTotalBigDecimal < 100}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5">05x</option>
									<option value="6">06x</option>
									<option value="7">07x</option>
									<option value="8">08x</option>
									<option value="9" selected="selected">09x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 100 && valorTotalBigDecimal < 110}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5">05x</option>
									<option value="6">06x</option>
									<option value="7">07x</option>
									<option value="8">08x</option>
									<option value="9">09x</option>
									<option value="10" selected="selected">10x</option>
								</c:if>
								<c:if test="${valorTotalBigDecimal >= 110 && valorTotalBigDecimal < 120}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5">05x</option>
									<option value="6">06x</option>
									<option value="7">07x</option>
									<option value="8">08x</option>
									<option value="9">09x</option>
									<option value="10">10x</option>
									<option value="11" selected="selected">11x</option>
								</c:if>
								
								
								<c:if test="${valorTotalBigDecimal >= 120}">
									<option value="1">01x</option>
									<option value="2">02x</option>
									<option value="3">03x</option>
									<option value="4">04x</option>
									<option value="5">05x</option>
									<option value="6">06x</option>
									<option value="7">07x</option>
									<option value="8">08x</option>
									<option value="9">09x</option>
									<option value="10">10x</option>
									<option value="11">11x</option>
									<option value="12" selected="selected">12x</option>
								</c:if>
						   
							</select>
							<p>Valor da parcela: </p>
							
							<c:forEach items="${parcelamentoTotalDaCompraDto}" var="parcelamentoDto">
								<c:forEach items="${parcelamentoDto.listaDeParcelasValoresGatewayDePagamento}" var="parcelasValoresGatewayDePagamento">
									<c:forEach items="${parcelasValoresGatewayDePagamento.mapVezesValorString}" var="map">
										<p class="valores-parcelas-insercao-dados"><b>R$ <span id="valor_parcela_total_compra_escolhido">${map.value}</span></b> no total de <b><span id="total_compra_parcelamento_escolhido">R$ ${parcelasValoresGatewayDePagamento.totalParcelamentoString}</span></b> na bandeira <b>${parcelamentoDto.instituicao}</b></p>
									</c:forEach>
								</c:forEach>	
							</c:forEach>
							
						</div>
						<div class="form-group">
							<button type="button" id="modal_enviar_cartao_de_credito_novo" class="btn btn-default"><span class="glyphicon glyphicon-send"></span> Enviar</button>	
						</div>
						
					</div>
				</div>
			</form:form>
		</section>	
		
		
		<!-- boleto -->
		<section class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-8 col-md-offset-4 col-lg-3 col-lg-offset-1 margin-top-2percent margin-bottom-2percent">
			
			<h4>Boleto bancário</h4>
		
			<div class="row margin-top-2percent margin-bottom-2percent" id="boleto_bancario">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="#" onclick="aceitePoliticaDeCompraModal('boleto')"><img class="img-responsive img-thumbnail" src="${pageContext.servletContext.contextPath}/resources/image/boleto_bancario.jpg"></a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12">
					<p class="text-center">No valor total de: <b>${valorTotalString}</b></p>
				</div>
				
			</div>
		</section>
		
		
		<div class="col-xs-12 col-sm-3 col-sm-offset-9 col-md-3 col-md-offset-9 col-lg-3 col-lg-offset-9 margin-top-2percent margin-bottom-2percent">
			<p>Powered by: <img alt="tray checkout" class="img-responsive" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/tray-checkout-locaweb.jpg"></p>
		</div>
	
	</div>	
</div>	




<div id="cartao-invalido-erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Cartão inválido</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">O número do cartão de crédito não coincide com o código de segurança</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" onclick="focusCodigoDeSeguranca()">Ok</button>
		</div>
    </div>
  </div>
</div>


<div id="aceite_politica_de_compra" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Aceite de política de compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">Para finalizar esta compra, você precisa estar ciente de nossa política de compra e venda. 
			<a href="${pageContext.request.contextPath}/politica_de_compra_e_venda" target="_blank">Para acessá-la, clique aqui.</a></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="tipo_de_ok_politica_de_compra" data-dismiss="modal">Aceito</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Não aceito</button>
		</div>
    </div>
  </div>
</div>


<div id="erro_cartao_de_credito" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-danger">Erro no cartão de crédito!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-danger">
				O cartão de crédito é inválido. Por favor escolha outro
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="favor_escolher_cartao_de_credito_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Cartão de crédito.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">Você precisa escolher uma bandeira de cartão de crédito.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="favor_escolher_mes_de_warning" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Cartão de crédito.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">Você precisa escolher o mês de vencimento do seu cartão.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="favor_escolher_ano_de_warning" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Cartão de crédito.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">Você precisa escolher o ano de vencimento do seu cartão.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="favor_colocar_numero_do_cartao" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Cartão de crédito.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">O campo de número do cartão de crédito não pode estar vazio.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="favor_colocar_codigo_seguranca" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Cartão de crédito.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">O campo do código de segurança não pode estar vazio.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="favor_colocar_nome" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Cartão de crédito.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">O campo do nome do cartão de crédito não pode estar vazio.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>


<div id="erro_modal_generico" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Erro.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">Ocorreu um erro no sistema. Por favor entre em contato conosco.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>	