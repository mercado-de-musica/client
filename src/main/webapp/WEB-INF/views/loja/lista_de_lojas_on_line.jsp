<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		<c:choose>
			<c:when test="${listaUsuarioDto != null && listaUsuarioDto.lista != null && !empty listaUsuarioDto.lista}">
				
				<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
					<div class="row">
						<div class="form-group col-sm-6 col-md-6 col-lg-6">
							<h4 class="loja-online mercadodemusica-radius">Lojas virtuais de: </h4>
					  	</div>
					  	
						<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6 margin-top-1percent">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<label class="procura">Lojas virtuais por página</label>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<select class="form-control" name="paginacao_lojas_online" style="width:100%">
									<c:forEach var="produtosPorPagina" items="${listaDeProdutosPorPagina}">
										<c:choose>
											<c:when test="${produtosPorPagina.descricao == listaUsuarioDto.lojasPorPagina}">
												<option value="${produtosPorPagina}" selected>${produtosPorPagina.descricao}</option>
											</c:when>
											<c:otherwise>
												<option value="${produtosPorPagina}">${produtosPorPagina.descricao}</option>
											</c:otherwise>
										</c:choose>
										
										
									</c:forEach>
								</select>
							</div>
					  	</div>
					</div>
					<div class="row">
						<c:forEach var="usuarioDto" items="${listaUsuarioDto.lista}" varStatus="contagem">	
							<c:choose>
								<c:when test="${contagem.index % 2 == 0}">
									<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-5 col-md-offset-0 col-lg-3 col-lg-offset-1">		
								</c:when>
								<c:otherwise>
									<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-2 col-md-5 col-md-offset-2 col-lg-3 col-lg-offset-1">
								</c:otherwise>
							</c:choose>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-5percent lojas-online-laranja mercadodemusica-radius apresentacao-inicial tamanho-minimo">
											<p class="p-lojas-online">
												<c:choose>
													<c:when test="${usuarioDto.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
														<c:choose>
															<c:when test="${usuarioDto.nomeFantasia != null && !empty usuarioDto.nomeFantasia}">
																<a class="p-lojas-online-a " href="${pageContext.servletContext.contextPath}/loja/${usuarioDto.id}">${usuarioDto.nomeFantasia}</a>
															</c:when>
															<c:otherwise>
																<a class="p-lojas-online-a" href="${pageContext.servletContext.contextPath}/loja/${usuarioDto.id}">${usuarioDto.razaoSocial}</a>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:otherwise>
														<a class="p-lojas-online-a" href="${pageContext.servletContext.contextPath}/loja/${usuarioDto.id}">${usuarioDto.nome} ${usuarioDto.sobrenome}</a>
													</c:otherwise>
												</c:choose>
												
											</p>
										</div>	
						</div>	
						</c:forEach>	
					</div>
					
					<div class="col-xs-12 margin-top-2percent margin-bottom-2percent text-center">
						<nav class="paginacao">
						  <ul class="pagination pagination-sm">
						    <c:forEach var="i" begin="1" end="${listaUsuarioDto.paginas}">
								<c:choose>
									<c:when test="${i == listaUsuarioDto.paginaAtiva}">
										<li class="active"><a href="${pageContext.servletContext.contextPath}/lojas_online/${listaUsuarioDto.produtosPorPagina}/${i}">${i}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${pageContext.servletContext.contextPath}/lojas_online/${listaUsuarioDto.produtosPorPagina}/${i}">${i}</a></li>
									</c:otherwise>
								</c:choose>
								
							</c:forEach> 
						  </ul>
						</nav>
						<p class="text-center">Do total de ${listaUsuarioDto.contagem} lojas virtuais</p>
					</div>
					
				</section>
					
			</c:when>
			<c:otherwise>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
					<h3 class="text-center">Não há loja virtual</h3>
				</div>
			</c:otherwise>
		</c:choose>
		
	</div>		
</div>



<div id="ir-loja-virtual" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro na paginação</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro na paginação das lojas. Entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>