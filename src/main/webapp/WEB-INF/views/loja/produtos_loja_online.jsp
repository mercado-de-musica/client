<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		<c:choose>
			<c:when test="${listaInstrumentoAcessorioLojaOnlineDto != null && !empty listaInstrumentoAcessorioLojaOnlineDto}">
				
				<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
					<c:forEach var="instrumentoAcessorioLojaOnlineDto" items="${listaInstrumentoAcessorioLojaOnlineDto}">
						<h4 class="loja-online mercadodemusica-radius">${instrumentoAcessorioLojaOnlineDto.instrumentoAcessorioDto.nome}</h4>
						<div class="row">
							<c:forEach var="infosGeraisProdutoUsuarioDto" items="${instrumentoAcessorioLojaOnlineDto.listaInfosGeraisProdutoUsuarioDto}" varStatus="contagem">
								<c:forEach var="apresentacaoProdutoDto" items="${infosGeraisProdutoUsuarioDto.listaApresentacaoProdutoDto}">
										
										<c:choose>
											<c:when test="${contagem.index % 2 == 0}">
												<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-5 col-md-offset-0 col-lg-3 col-lg-offset-1 apresentacao-inicial">		
											</c:when>
											<c:otherwise>
												<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-2 col-md-5 col-md-offset-2 col-lg-3 col-lg-offset-1 apresentacao-inicial">
											</c:otherwise>
										</c:choose>
										
																			
										<div class="row">
											<a href="${pageContext.servletContext.contextPath}/detalhamento_produto/${infosGeraisProdutoUsuarioDto.id}">
												<img src="${apresentacaoProdutoDto.endereco}${apresentacaoProdutoDto.nomeDoArquivo}" class="img-responsive img-thumbnail" alt="${apresentacaoProdutoDto.nomeDoArquivo}" />
											</a>
										</div>
										<div class="row">
											<h4> 
												<a class="nome-apresentacao-produto" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${infosGeraisProdutoUsuarioDto.id}">
													${infosGeraisProdutoUsuarioDto.infosGeraisProduto.instrumentoAcessorio.nome} 
											   		${infosGeraisProdutoUsuarioDto.infosGeraisProduto.marca.nome} 
											   		${infosGeraisProdutoUsuarioDto.infosGeraisProduto.produto.nome} 
											   		${infosGeraisProdutoUsuarioDto.infosGeraisProduto.modelo.nome}
												</a>
											</h4>
											<p><a class="preco-apresentacao-produto" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${infosGeraisProdutoUsuarioDto.id}"><b>R$ ${infosGeraisProdutoUsuarioDto.precoString}</b></a></p>
										</div>
									</div>	
								</c:forEach>
							</c:forEach>	
						</div>
					</c:forEach>	
				</section>
				
<!-- 				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-4 col-lg-9 col-lg-offset-3 margin-top-2percent margin-bottom-2percent text-center"> -->
<!-- 					<nav class="paginacao"> -->
<!-- 					  <ul class="pagination pagination-sm"> -->
<%-- 					    <c:forEach var="i" begin="1" end="${listaDePesquisaDeProdutos.paginas}"> --%>
<%-- 							<c:choose> --%>
<%-- 								<c:when test="${i == listaDePesquisaDeProdutos.paginaAtiva}"> --%>
<%-- 									<li class="active"><a href="${pageContext.servletContext.contextPath}/pesquisa_produtos?p=${i}&i=${listaDePesquisaDeProdutos.instrumentoAcessorioIdProcura}&m=${listaDePesquisaDeProdutos.marcaIdProcura}&pd=${listaDePesquisaDeProdutos.produtoIdProcura}&md=${listaDePesquisaDeProdutos.modeloIdProcura}&pp=_${listaDePesquisaDeProdutos.produtosPorPagina}">${i}</a></li> --%>
<%-- 								</c:when> --%>
<%-- 								<c:otherwise> --%>
<%-- 									<li><a href="${pageContext.servletContext.contextPath}/pesquisa_produtos?p=${i}&i=${listaDePesquisaDeProdutos.instrumentoAcessorioIdProcura}&m=${listaDePesquisaDeProdutos.marcaIdProcura}&pd=${listaDePesquisaDeProdutos.produtoIdProcura}&md=${listaDePesquisaDeProdutos.modeloIdProcura}&pp=_${listaDePesquisaDeProdutos.produtosPorPagina}">${i}</a></li> --%>
<%-- 								</c:otherwise> --%>
<%-- 							</c:choose> --%>
							
<%-- 						</c:forEach>  --%>
<!-- 					  </ul> -->
<!-- 					</nav> -->
<%-- 					<p class="text-center">Do total de ${listaDePesquisaDeProdutos.contagem} produtos</p> --%>
<!-- 				</div> -->
				
					
			</c:when>
			<c:otherwise>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
					<h3 class="text-center">Não existe nenhum produto nesta loja</h3>
				</div>
			</c:otherwise>
		</c:choose>
		
	</div>		
</div>