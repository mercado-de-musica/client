<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<h3 class="text-center">${mensagem}</h3>
	<c:if test="urlApresenta">
		<p class="text-center"><a href="${url}">Clique aqui para retornar à pagina anterior</a></p>
	</c:if>
</div>