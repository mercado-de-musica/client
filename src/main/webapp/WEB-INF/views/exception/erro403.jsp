<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
			<h3 class="text-center">Você não tem permissão para entrar nesta área</h3>
			<p class="text-center"><a href="${url}">Clique aqui para retornar à pagina anterior</a></p>
		</div>
	</div>
</div>