<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>		
		
		<meta name="_csrf" content="${_csrf.token}"/>
		<meta name="_csrf_header" content="${_csrf.headerName}"/>

		
<%-- 		<c:if test="${listaLojasOnline != null && !empty listaLojasOnline}"> --%>
<!-- 			<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent"> -->
<!-- 				<h4 class="h4-lojas-virtuais">Veja nossas lojas virtuais</h4> -->
<!-- 				<div class="lojas-online-carousel lojas-online-laranja mercadodemusica-radius"> -->
<%-- 				  <c:forEach items="${listaLojasOnline}" var="lojaOnline"> --%>
<%-- 				  	<div><p class="text-center p-lojas-online"><a class="p-lojas-online-a" href="${pageContext.servletContext.contextPath}/loja/${lojaOnline.idLoja}">${lojaOnline.nomeDaLoja}</a></p></div> --%>
<%-- 				  </c:forEach> --%>
<!-- 			  </div> -->
<!-- 			</section> -->
<%-- 		</c:if> --%>
		
		
		<c:choose>
			<c:when test="${pesquisaProdutoDto.listaApresentacaoProduto != null && !empty pesquisaProdutoDto.listaApresentacaoProduto}">
				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-1percent">
					<c:forEach var="apresentacaoProduto" items="${pesquisaProdutoDto.listaApresentacaoProduto}" varStatus="contagem">
						<c:choose>
							<c:when test="${contagem.index % 2 == 0}">
								<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-5 col-md-offset-0 col-lg-2 col-lg-offset-1 apresentacao-inicial margin-4percent">		
							</c:when>
							<c:otherwise>
								<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-2 col-md-5 col-md-offset-2 col-lg-2 col-lg-offset-1 apresentacao-inicial margin-4percent">
							</c:otherwise>
						</c:choose>
					
							<div class="row">
								<a href="${pageContext.servletContext.contextPath}/detalhamento_produto/${apresentacaoProduto.produto.idCrypt}">
									<img src="${apresentacaoProduto.endereco}" class="img-responsive img-thumbnail" alt="${apresentacaoProduto.endereco}" />
								</a>
							</div>
							<div class="row">
								<h4> 
									<a class="nome-apresentacao-produto" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${apresentacaoProduto.produto.idCrypt}">
										${apresentacaoProduto.produto.nome} 							
								   	</a>
								</h4>
							</div>
								<p><a class="preco-apresentacao-produto" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${apresentacaoProduto.produto.idCrypt}"><b>R$ ${apresentacaoProduto.produto.precoString}</b></a></p>
<%-- 							<c:forEach var="endereco" items="${apresentacaoProduto.produto.usuario.enderecos}"> --%>
<%-- 								<c:if test="${endereco.enderecoDefault}"> --%>
<!-- 									<div class="row margin-bottom-10percent"> -->
<%-- 										<p class="margin-top-3percent nome-apresentacao-produto"><b>${endereco.cidade.nome} - ${endereco.cidade.estado.nome}</b></p>							 --%>
<!-- 									</div> -->
<%-- 								</c:if> --%>
<%-- 							</c:forEach> --%>
						</div>
					</c:forEach>	
				</section>
				
				
<!-- 				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-4 col-lg-9 col-lg-offset-3 margin-top-2percent margin-bottom-2percent text-center"> -->
<!-- 					<nav class="paginacao"> -->
<!-- 					  <ul class="pagination pagination-sm"> -->
<%-- 					    <c:forEach var="i" begin="1" end="${pesquisaProdutoDto.paginas}"> --%>
<%-- 							<c:choose> --%>
<%-- 								<c:when test="${i == pesquisaProdutoDto.paginaAtiva}"> --%>
<%-- 									<li class="active"><a href="${pageContext.servletContext.contextPath}/q/${i}">${i}</a></li> --%>
<%-- 								</c:when> --%>
<%-- 								<c:otherwise> --%>
<%-- 									<li><a href="${pageContext.servletContext.contextPath}/q/${i}">${i}</a></li> --%>
<%-- 								</c:otherwise> --%>
<%-- 							</c:choose> --%>
							
<%-- 						</c:forEach>  --%>
<!-- 					  </ul> -->
<!-- 					</nav> -->
<%-- 					<p class="text-center">Do total de ${pesquisaProdutoDto.contagem} produtos</p> --%>
<!-- 				</div> -->
			</c:when>
			<c:otherwise>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-1percent">
					<h3 class="text-center">Não existe nenhum produto nesta área do site</h3>
				</div>
			</c:otherwise>
		</c:choose>
		
	</div>
</div>	