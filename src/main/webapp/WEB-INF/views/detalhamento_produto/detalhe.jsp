<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

		<meta name="_csrf" content="${_csrf.token}"/>
		<meta name="_csrf_header" content="${_csrf.headerName}"/>
		
		
		<div id="apresentacao_principal" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-offset-1 margin-top-2percent margin-bottom-2percent">
			<img src="${listaDeApresentacoesDto[0].endereco}" class="img-responsive img-thumbnail" alt="${listaDeApresentacoesDto[0].endereco}" />
		</div>
		
		<section class="col-xs-12 col-sm-6 col-md-6 col-lg-5 margin-top-2percent margin-bottom-2percent">
			<div class="col-xs-12 col-sm-12 margin-bottom-2percent">
				<c:if test="${pesquisaDeProdutosDto.apresentacaoProdutoCapa != null}">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 margin-top-2percent">
						<img class="img-responsive img-thumbnail thumbs_detalhamento" src="${listaDeApresentacoesDto[0].endereco}" alt="${listaDeApresentacoesDto[0].endereco}" onclick="mudarFotoPrincipal(this)">
					</div>
				</c:if>
				
				
				<c:forEach var="apresentacaoProduto" items="${listaDeApresentacoesDto}">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 margin-top-2percent">
						<c:choose>
	    					<c:when test="${apresentacaoProduto.tipoApresentacao == 'VIDEO'}">
	       						<img class="img-responsive img-thumbnail thumbs_detalhamento" src="${apresentacaoProduto.nomeDoThumb}" alt="${apresentacaoProduto.nomeDoArquivo}" onclick="mudarVideoPrincipal('${apresentacaoProduto.endereco}', ${apresentacaoProduto.width}, ${apresentacaoProduto.height})">
	    					</c:when>
	    					<c:when test="${apresentacaoProduto.tipoApresentacao == 'AUDIO'}">
	       						<img class="img-responsive img-thumbnail thumbs_detalhamento" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/glyphicon-music.jpg" alt="${apresentacaoProduto.nomeDoArquivo}" onclick="mudarAudioPrincipal('${apresentacaoProduto.endereco}${apresentacaoProduto.nomeDoArquivo}', '${apresentacaoProduto.tipoArquivo}')">
	    					</c:when>
	    					<c:otherwise>
	    						<img class="img-responsive img-thumbnail thumbs_detalhamento" src="${apresentacaoProduto.endereco}" alt="${apresentacaoProduto.endereco}" onclick="mudarFotoPrincipal(this)">	    						
	    					</c:otherwise>
						</c:choose>
					</div>
				</c:forEach>
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-6percent">
				<h2 class="text-h3 text-right">
					R$ ${produtoDto.precoString}
				</h2>
			</div>
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-6percent">
				<p class="text-h3 text-right">
					<c:if test="${produtoDto.usuario.class.simpleName == 'UsuarioCrawlerDTO'}">
						Nome do vendedor: <b>${produtoDto.usuario.nome}</b>
					</c:if>
					
					<c:if test="${produtoDto.usuario.class.simpleName == 'UsuarioPessoaFisicaDTO'}">
						Nome do vendedor: <b>${produtoDto.usuario.nome} ${produtoDto.usuario.sobrenome}</b>
					</c:if>
					
					<c:if test="${produtoDto.usuario.class.simpleName == 'UsuarioPessoaJuridicaDTO' && produtoDto.usuario.nomeFantasia != null}">
							Nome do vendedor: <b>${produtoDto.usuario.nomeFantasia}</b>
					</c:if>
					<c:if test="${produtoDto.usuario.class.simpleName == 'UsuarioPessoaJuridicaDTO' && produtoDto.usuario.nomeFantasia == null}">
							Nome do vendedor: <b>${produtoDto.usuario.razaoSocial}</b>
					</c:if>
				</p>
				<c:if test="${fotoUsuario != null && fotoUsuario.fundoBranco}">
					<p class="text-right">
						<img alt="${fotoUsuario.urlFoto}" src="${fotoUsuario.urlFoto}" />
					</p>	
				</c:if>
				<c:if test="${fotoUsuario != null && !fotoUsuario.fundoBranco}">
					<p class="text-right fundo-escuro padding-4percent">
						<img alt="${fotoUsuario.urlFoto}" src="${fotoUsuario.urlFoto}" />
					</p>	
				</c:if>
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-6percent">
				
				<c:if test="${produtoDto.usuario.class.simpleName == 'UsuarioCrawlerDTO'}">
					<p class="text-right">
						<button type="button" class="btn btn-primary" onclick="irParaLoja('${produtoDto.idCrypt}')">
							<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
							<span class="texto-branco">Ir para a loja</span>
						</button>
					</p>	
				</c:if>
				
				<c:if test="${produtoDto.usuario.class.simpleName != 'UsuarioCrawlerDTO'}">
					<button type="button" class="btn btn-primary" id="botao_adicionar_carrinho_de_compras">
						<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
						<span class="texto-branco">Comprar</span>
					</button>
				</c:if>
			
			</div>
			
		</section>
		
		
		<c:if test="${pesquisaDosUsuarios != null && !empty pesquisaDosUsuarios}">
			<section class="row">
		    	<h3 class="text-h3 text-center">
		    		Quem pesquisou este produto também pesquisou:
		    	</h3>
		    	<div class="owl-carousel">
				  <c:forEach var="pesquisa" items="${pesquisaDosUsuarios}">
					
					<div class="row">
		          			<div class="img-responsive img-thumbnail">
		          				<img src="${pesquisa.endereco}">
		           		</div>
						<div class="col-xs-12">
							<h4><a onclick="inserirPesquisaDetalhe('${pesquisa.produto.idCrypt}')" class="preco-apresentacao-produto margin-1percent" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${pesquisa.produto.idCrypt}"><b>R$ ${pesquisa.produto.precoString}</b></a></h4>
						</div>
		                   	<p class="col-xs-12"> 
								<a onclick="inserirPesquisaDetalhe('${pesquisa.produto.idCrypt}')" class="nome-apresentacao-produto" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${pesquisa.produto.idCrypt}">
									${pesquisa.produto.nome} 
								</a>
							</p>
		              	</div>
					
					
				  </c:forEach>
				</div>
		    </section>
		</c:if>
		
		
		
		<c:if test="${compraDosUsuarios != null && !empty compraDosUsuarios}">
			<section class="row">
		    	<h3 class="text-h3 text-center">
		    		Quem comprou este produto também comprou:
		    	</h3>
		    	<div class="owl-carousel">
				  <c:forEach var="pesquisa" items="${compraDosUsuarios}">
					
					<div class="row">
		          			<div class="img-responsive img-thumbnail">
		          				<img src="${pesquisa.endereco}">
		           		</div>
						<div class="col-xs-12">
							<h4><a onclick="inserirPesquisaDetalhe('${pesquisa.produto.idCrypt}')" class="preco-apresentacao-produto margin-1percent" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${pesquisa.produto.idCrypt}"><b>R$ ${pesquisa.produto.precoString}</b></a></h4>
						</div>
		                   	<p class="col-xs-12"> 
								<a onclick="inserirPesquisaDetalhe('${pesquisa.produto.idCrypt}')" class="nome-apresentacao-produto" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${pesquisa.produto.idCrypt}">
									${pesquisa.produto.nome} 
								</a>
							</p>
		              	</div>
					
					
				  </c:forEach>
				</div>
		    </section>
		</c:if>
		
		
		
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
			  <ul class="nav nav-tabs" role="tablist">
			    <c:if test="${produtoDto.usuario.class.simpleName == 'UsuarioCrawlerDTO'}">
			    	<li role="presentation" class="active"><a href="#informacoes" aria-controls="home" role="tab" data-toggle="tab">Informações</a></li>
			    </c:if>
			    <c:if test="${produtoDto.usuario.class.simpleName != 'UsuarioCrawlerDTO'}">
			    	<li role="presentation" class="active"><a href="#especificacoes" aria-controls="home" role="tab" data-toggle="tab">Especificações</a></li>
			    	<li role="presentation"><a href="#perguntas" aria-controls="profile" role="tab" data-toggle="tab">Perguntas</a></li>
			  	</c:if>
			  </ul>
			
			  <div class="tab-content">
			    <c:if test="${produtoDto.usuario.class.simpleName == 'UsuarioCrawlerDTO'}">
			    	<section role="tabpanel" class="tab-pane active" id="informacoes">
			    		<p class="text-h3">${produtoDto.descricao}</p>
			    	</section>
			    </c:if>
			    
			    <c:if test="${produtoDto.usuario.class.simpleName != 'UsuarioCrawlerDTO'}">
			    	
			    	<section role="tabpanel" class="tab-pane active" id="especificacoes">
					<p>Número de série: <b>${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.numeroDeSerie}</b></p>
					
					<p>Altura: <b>${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.alturaString} centímetros</b></p>
					<p>Largura: <b>${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.larguraString} centímetros</b></p>
					<p>Comprimento: <b>${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.comprimentoString} centímetros</b></p>
					<p>Peso: <b>${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.pesoString} kilos</b></p>
					<p>Ano de fabricação: <b>${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.anoDeFabricacao}</b></p>
					<p>País fabricação: <b>${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.paisDeFabricacao.descricao}</b></p>
				</section>
			    
			    
			    <section role="tabpanel" class="tab-pane" id="perguntas">
			    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">	
						<c:forEach var="perguntaListaResposta" items="${listaDePerguntasRespostas}">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<p>${perguntaListaResposta.perguntaFeita.pergunta}</p>
									<c:choose>
										<c:when test="${perguntaListaResposta.perguntaFeita.usuarioInteressado.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
											<c:choose>
												<c:when test="${perguntaListaResposta.perguntaFeita.usuarioInteressado.nomeFantasia != null}">
													<p class="text-right">Perguntado por: <b>${perguntaListaResposta.perguntaFeita.usuarioInteressado.nomeFantasia} em ${perguntaListaResposta.perguntaFeita.dataPerguntaString}</b></p>	
												</c:when>
												<c:otherwise>
													<p class="text-right">Perguntado por: <b>${perguntaListaResposta.perguntaFeita.usuarioInteressado.razaoSocial} em ${perguntaListaResposta.perguntaFeita.dataPerguntaString}</b></p>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											<p class="text-right">Perguntado por: <b>${perguntaListaResposta.perguntaFeita.usuarioInteressado.nome} ${perguntaListaResposta.perguntaFeita.usuarioInteressado.sobrenome} em ${perguntaListaResposta.perguntaFeita.dataPerguntaString}</b></p>
										</c:otherwise>
									</c:choose>
								
									<hr />
								
								</div>
								
								<c:forEach var="resposta" items="${perguntaListaResposta.respostas}">
									<div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
										<img id="resposta-img" class="img-responsive" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/enter.jpg" alt="Resposta à pergunta" />
									</div>
									
									<div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
										<p>${resposta.pergunta}</p>
									
										<c:choose>
											<c:when test="${resposta.infosGeraisProdutoUsuario.usuario.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
												<c:choose>
													<c:when test="${resposta.infosGeraisProdutoUsuario.usuario.nomeFantasia != null}">
														<p class="text-right">Respondido por: <b>${resposta.infosGeraisProdutoUsuario.usuario.nomeFantasia} em ${resposta.dataPerguntaString}</b></p>														
													</c:when>
													<c:otherwise>
														<p class="text-right">Respondido por: <b>${resposta.infosGeraisProdutoUsuario.usuario.razaoSocial} em ${resposta.dataPerguntaString}</b></p>														
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<p class="text-right">Respondido por: <b>${resposta.infosGeraisProdutoUsuario.usuario.nome} ${resposta.infosGeraisProdutoUsuario.usuario.sobrenome} em ${resposta.dataPerguntaString}</b></p>
											</c:otherwise>
										</c:choose>										
										
										<hr />
										
									</div>
									
								</c:forEach>
							</div>	
						</c:forEach>		
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">	
<%-- 						<security:authorize ifAnyGranted="ROLE_USER,ROLE_ADMIN"> --%>
				       		
<%-- 				       		<input type="hidden" value="${pesquisaDeProdutosDto.listaApresentacaoProduto[0].infosGeraisProdutoUsuario.id}" id="id_produto_pergunta" /> --%>
<%-- 				       		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" id="token" /> --%>
<!-- 				       		<div class="form-group"> -->
<!-- 							   <label>Faça aqui sua pergunta</label> -->
<!-- 							   <textarea class="form-control" rows="3" id="pergunta_ao_vendedor"></textarea> -->
<!-- 							</div> -->
<!-- 							<div class="form-group"> -->
<!-- 							   <button id="enviar_pergunta" type="button" class="btn btn-default">Enviar pergunta</button> -->
<!-- 							</div> -->
<%-- 				        </security:authorize>		 --%>
					</div>
			    </section>
			    	
			    
			    </c:if>
			    
			  </div>
			</div>
		</div>
		
		
		
		
	</div>	
</div>


<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro.</h3>
		</div>
    	<div class="modal-body">
			<p id="texto-erro-generico text--danger" class="text-center text-muted">Ocorreu um erro. Por favor entre em contato conosco.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>


<div id="erro_modal_parcelamento" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">Ocorreu um erro no parcelamento de seu valor. Por favor entre em contato conosco.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="pergunta_ok_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Pergunta enviada!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-muted">Sua pergunta foi enviada, dentro de instantes o vendedor irá recebê-la.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>


<div id="modal-lead" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Olá visitante. Cadastre aqui o seu e-mail e receba nossas ofertas semanais!</h3>
		</div>
    	<div class="modal-body">
			<div class="row" id="lead-preenchido">
				
			</div>
			
			
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 margin-top-2percent">
					<input type="text" name="nome-lead" class="form-control" placeholder="Nome" id="nome-lead" />
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 margin-top-2percent">	
					<input name="email-lead" type="text" class="form-control" placeholder="E-mail" id="email-lead" />
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 margin-top-2percent">
					<label>Você tem interesse em:</label>
				    <select name="voce-toca[]" multiple="multiple" class="form-control" style="width:100%;">
				    	<option value="violao">Violão</option>
				    	<option value="guitarra">Guitarra</option>
				    	<option value="baixo">Baixo</option>
				    	<option value="cavaco">Cavaco</option>
				    	<option value="banjo">Banjo</option>
				    	<option value="bateria">Bateria</option>
				    	<option value="percursao">Percursão</option>
				    	<option value="sopro">Instrumentos de sopro</option>
				    	<option value="canto">Canto</option>
				    </select>
				</div>
			</div>
		</div>
    	<div class="modal-footer">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
					<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Agora não</button>
				</div>
				<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
					<button type="button" class="btn btn-primary" onclick="cadastrarLead()"><span class="glyphicon glyphicon-usd"></span> <span class="texto-branco">Quero ofertas!</span></button>
				</div>
				
				
			</div>
			
		</div>
    </div>
  </div>
</div>


<div id="aviso-de-envio-email" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Enviamos um e-mail para sua caixa postal</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-muted">Acesse sua caixa postal e valide seu e-mail. E receba nossas ofertas!</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> <span class="texto-branco">Ok</span></button>
		</div>
    </div>
  </div>
</div>