<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
			<h4 class="text-center">${cadastroAlteracaoTitulo}</h4>
			<p class="text-center"><a href="${htmlPayPal}" target="_blank">Finalize seu cadastro no Pay Pal clicando aqui</a></p>
			
			<p class="text-center margin-top-1percent">Caso seu e-mail já esteja cadastrado no Pay Pal, será necessário sua confirmação clicando no link do e-mail que enviamos para você.</p>
		</div>
	</div>
</div>