<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>



		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-2percent margin-bottom-2percent">
			<form:form action="${pageContext.request.contextPath}/alteracao_senha" method="post" modelAttribute="tokenRecuperacaoDeSenhaModeloDto" id="alterar_senha_form">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="hidden" name="token" value="${tokenRecuperacaoDeSenhaDto.token}" />
				
				<input type="hidden" name="usuarioPessoaFisica.ativo" value="${tokenRecuperacaoDeSenhaDto.usuario.ativo}" />
				
				<section class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-top-2percent formulario-usuario-pf">
						<label>Usuario:</label>
						<input type="text" name="email" value="${tokenRecuperacaoDeSenhaDto.usuario.email}" readonly class="form-control" />
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-top-2percent formulario-usuario-pf">
						<label>Senha:</label>
						<input type="password" name="senha" class="form-control" placeholder="*****" id="alteracao_senha" />
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-top-2percent formulario-usuario-pf">
						<label>Confirmação de senha:</label>
						<input type="password" name="confirmacao_de_senha" class="form-control" placeholder="*****" />
					</div>	
				</section>
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
						<button id="enviar_alterar_senha" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Alterar senha</button>	
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>