<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
			<h4 class="text-center">Sua senha foi alterada!</h4>
			<p class="text-center">Utilize a nova senha para entrar na áreas reservadas.</p>
			<p class="text-center"><a href="${pageContext.request.contextPath}/login">Para fazer o login, clique aqui</a></p>
		</div>
	</div>	
</div>