<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-1 col-lg-5 col-lg-offset-2">
		  <form id="login_form" name="loginForm" action="<c:url value="j_spring_security_check" />" method="POST">
			  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			  <div class="form-group">
				<input type="text" class="form-control" name="username" placeholder="seu login">
			  </div>
			  <div class="form-group">
				<input type="password" class="form-control" name="password" placeholder="sua senha">
			  </div>
			  <div class="form-group">
			  	<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-log-in"></span> Entrar</button>
			  </div>
			  <div class="form-group">
			  	<a href="recuperar_usuario_senha">Esqueceu seu usuário ou senha?</a>
			  </div>
		  </form>
		</div>
	</div>
</div>	