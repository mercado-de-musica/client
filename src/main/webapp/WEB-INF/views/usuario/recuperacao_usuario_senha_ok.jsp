<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
			<h4 class="text-center">E-mail de recuperação de senha enviado!</h4>
			<p class="text-center">Verifique no seu e-mail cadastrado junto ao site para continuar o precesso de recadastramento de senha. O processo de recuperação senha é válido apenas por 1 hora e após este tempo será necessário refazer o processo novamente.</p>
		</div>
	</div>
</div>
