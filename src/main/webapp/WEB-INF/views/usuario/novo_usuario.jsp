<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>



		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-mercado-de-musica">
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
			          Pessoa física
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			        
			     	<form:form action="${pageContext.request.contextPath}/cadastro_novo_usuario_pf" method="post" modelAttribute="usuarioDtoPF" id="cadastro_novo_usuario_pf">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						<section class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-top-2percent formulario-usuario-pf">
									<label>E-mail:</label>
								    <input name="email" type="email" class="form-control" placeholder="seu email" id="email_pf" />
								</div>
							
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-top-2percent formulario-usuario-pf">
									<label>Senha:</label>
									<input type="password" name="senha" class="form-control" placeholder="*****" id="senha_pf" />
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-top-2percent formulario-usuario-pf">
									<label>Confirmação de senha:</label>
									<input type="password" name="confirmacao_de_senha" class="form-control" placeholder="*****" />
								</div>
							</div>	
						</section>
						
						<hr />
						
						<section class="row">
							<h4 class="text-center">Dados pessoais</h4>
							<div class="col-xs-12 col-sm-12 margin-top-2percent">	
									
							  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pf">
							    <label>Nome:</label>
							    <input type="text" name="nome" class="form-control" placeholder="Seu nome" />
							  </div>
							  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pf">
							    <label>Sobrenome:</label>
							    <input type="text" name="sobrenome" class="form-control" placeholder="Seu sobrenome" />
							  </div>
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
							    <label>RG:</label>
							    <input type="text" name="rg" class="form-control" />
							  </div>
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
							    <label>CPF:</label>
							    <input type="text" name="cpf" class="form-control" />
							  </div> 
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
							    <label>Data de nascimento:</label>
							    <input name="dataDeNascimentoString" type="text" class="datepicker form-control" />
							  </div>
							   
							  <input name="telefonesList[0].ddd" type="hidden" class="form-control" id="ddd_pf" /> 
							   
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
							    <label>Telefone:</label>
							    <input name="telefonesList[0].telefone" type="text" class="form-control" id="telefone_pf" />
							    
							  </div>
							  
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
							    <label>Tipo de telefone:</label>
							    <select name="telefonesList[0].tipoDeTelefone" class="form-control" style="width:100%;">
							    	<c:forEach items="${tiposTelefone}" var="tipoTelefone">
							    		<option value="${tipoTelefone}">${tipoTelefone.descricao}</option>
							    	</c:forEach>
							    </select>
							  </div>
							  	   
							  <div class="radio col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
								  <p class="text-left">
								    Sexo:
								  </p>
								  <label>
								    <input type="radio" name="sexo" value="M">
								    Masculino
								  </label>
								  <label>
								    <input type="radio" name="sexo" value="F">
								    Feminino
								  </label>
							  </div>
							</div>
						</section>
					
						<hr />
					
						<section class="row">
							<h4 class="text-center">Endereço</h4>
							<div class="col-xs-12 col-sm-12 margin-top-2percent">
							
								  <div class="col-xs-12 col-sm-3 margin-top-2percent formulario-usuario-pf">
										<label>CEP:</label>
									    <input type="text" name="enderecosList[0].cep" class="form-control" placeholder="seu CEP" />
								  </div>
								  
								  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pf">
									    <label>Logradouro:</label>
									    <input name="enderecosList[0].logradouro" type="text" class="form-control" placeholder="Sua rua, avenida, etc" />
								  </div>
								  <div class="col-xs-12 col-sm-3 margin-top-2percent formulario-usuario-pf">
									    <label>Numero:</label>
									    <input name="enderecosList[0].numero" type="text" class="form-control" placeholder="Seu numero" />
								  </div>
								  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
									    <label>Bairro:</label>
									    <input name="enderecosList[0].bairro" type="text" class="form-control" placeholder="Seu bairro" />
								  </div>
								  
								  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
									    <label>Complemento:</label>
									    <input name="enderecosList[0].complemento" type="text" class="form-control" placeholder="Complemento" />
								  </div>
								  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pf">
									    <label>Estado:</label>
									    <select name="enderecosList[0].cidade.estado.nome" class="form-control" style="width:100%;">
									   		<option value="">Selecione um estado</option>
									   		<c:forEach var="estado" items="${listaDeEstados}">
									   			<option value="${estado}">${estado.descricao}</option>
									   		</c:forEach>
									   </select>
								  </div>
								  
								  <div class="col-xs-12 col-sm-5 margin-top-2percent formulario-usuario-pf">
									    <label>Cidade:</label>
									    <input name="enderecosList[0].cidade.nome" type="text" class="form-control" placeholder="Sua cidade" />
								  </div>
								
								  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pf">
									    <label>Proximidade:</label>
									    <input type="text" name="enderecosList[0].proximidade" class="form-control" />
								  </div>
							</div>
						</section>
							
						<div class="col-xs-12 col-sm-12 margin-top-2percent">
							<label style="width:100%">&nbsp;</label>
							<button id="enviar_cadastro_usuario_pf" type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span> <span class="texto-branco">Enviar cadastro pessoa física</span></button>	
						</div>
					</form:form>
			     	
			      </div>
			    </div>
			  </div>
			  
			  <div class="panel panel-mercado-de-musica">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Pessoa Jurídica
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			        
			        <form:form action="${pageContext.request.contextPath}/cadastro_novo_usuario_pj" method="post" modelAttribute="usuarioDtoPJ" id="cadastro_novo_usuario_pj">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						<section class="row">
							<div class="col-xs-12 col-sm-12 margin-top-2percent">
								<div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
								    <label>E-mail:</label>
								    <input name="email" type="email" class="form-control" placeholder="Seu e-mail" id="email_pj" />
								</div>
								
								<div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
									<label>Senha:</label>
									<input type="password" name="senha" class="form-control" placeholder="*****" id="senha_pj" />
								</div>
								
								<div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
									<label>Confirmação de senha:</label>
									<input type="password" name="confirmacao_de_senha" class="form-control" placeholder="*****" />
								</div>
							</div>	
						</section>
						
						<hr />
								
						<section class="row">
							<h4 class="text-center">Dados empresariais</h4>
							<div class="col-xs-12 col-sm-12 margin-top-2percent">	
							  
							  
							  		
							  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pj">
							    <label>Nome fantasia:</label>
							    <input type="text" name="nomeFantasia" class="form-control" placeholder="Nome fantasia de sua empresa" />
							  </div>
							  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pj">
							    <label>Razão Social:</label>
							    <input type="text" name="razaoSocial" class="form-control" placeholder="Razão social de sua empresa" />
							  </div>
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
							    <label>CNPJ:</label>
							    <input type="text" name="cnpj" class="form-control" />
							  </div> 
								
							  <input name="telefonesList[0].ddd" type="hidden" class="form-control" id="ddd_pj" />
							  
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
							    <label>Telefone:</label>
							    <input name="telefonesList[0].telefone" type="text" class="form-control" id="telefone_pj" />
							    
							  </div>
							  
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
							    <label>Tipo de telefone:</label>
							    <select name="telefonesList[0].tipoDeTelefone" class="form-control" style="width:100%;">
							    	<c:forEach items="${tiposTelefone}" var="tipoTelefone">
							    		<option value="${tipoTelefone}">${tipoTelefone.descricao}</option>
							    	</c:forEach>
							    </select>
							  </div>

							</div>
						</section>
					
						<hr />
						
						<section class="row">
							<h4 class="text-center">Dados do titular da conta</h4>
							<div class="col-xs-12 col-sm-12 margin-top-2percent">	
									
							  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pj">
							    <label>Nome:</label>
							    <input type="text" name="nomeTitular" class="form-control" placeholder="Nome titular da conta" />
							  </div>
							  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pj">
							    <label>Sobrenome:</label>
							    <input type="text" name="sobrenomeTitular" class="form-control" placeholder="Sobrenome titular da conta" />
							  </div>
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
							    <label>RG:</label>
							    <input type="text" name="rgTitular" class="form-control" />
							  </div>
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
							    <label>CPF:</label>
							    <input type="text" name="cpfTitular" class="form-control" />
							  </div> 
							  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
							    <label>Data de nascimento:</label>
							    <input name="dataDeNascimentoTitularString" type="text" class="datepicker form-control" />
							  </div>
							</div>
						</section>
					
						<hr />
						
						<section class="row">
							<h4 class="text-center">Endereço</h4>
							<div class="col-xs-12 col-sm-12 margin-top-2percent">
								  
								  <div class="col-xs-12 col-sm-3 margin-top-2percent formulario-usuario-pj">
									<label>CEP:</label>
								    <input type="text" name="enderecosList[0].cep" class="form-control" placeholder="Seu CEP" />
								  </div>
								  
								  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pj">
								    <label>Logradouro:</label>
								    <input name="enderecosList[0].logradouro" type="text" class="form-control" placeholder="Sua rua, avenida, etc" />
								  </div>
								  <div class="col-xs-12 col-sm-3 margin-top-2percent formulario-usuario-pj">
								    <label>Numero:</label>
								    <input name="enderecosList[0].numero" type="text" class="form-control" placeholder="Seu número" />
								  </div>
								  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
								    <label>Bairro:</label>
								    <input name="enderecosList[0].bairro" type="text" class="form-control" placeholder="Seu B=bairro" />
								  </div>
								  
								  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
								    <label>Complemento:</label>
								    <input name="usuario.enderecosList[0].complemento" type="text" class="form-control" placeholder="Complemento" />
								  </div>
								  <div class="col-xs-12 col-sm-4 margin-top-2percent formulario-usuario-pj">
									    <label>Estado:</label>
									    
									   	<select name="enderecosList[0].cidade.estado.nome" class="form-control" style="width:100%;">
									   		<option value="">Selecione um estado</option>
									   		<c:forEach var="estado" items="${listaDeEstados}">
									   			<option value="${estado}">${estado.descricao}</option>
									   		</c:forEach>
									   </select>
								  </div>
								  
								  <div class="col-xs-12 col-sm-5 margin-top-2percent formulario-usuario-pj">
									    <label>Cidade:</label>
									    <input name="enderecosList[0].cidade.nome" type="text" class="form-control" placeholder="Sua cidade" />
								  </div>
								
								  <div class="col-xs-12 col-sm-6 margin-top-2percent formulario-usuario-pj">
									    <label>Proximidade:</label>
									    <input type="text" name="usuario.enderecosList[0].proximidade" class="form-control" />
								  </div>
							</div>
						</section>
						
						<div class="col-xs-12 margin-top-2percent">
							<label style="width:100%">&nbsp;</label>
							<button id="enviar_cadastro_usuario_pj" type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span> <span class="texto-branco">Enviar cadastro pessoa jurídica</span></button>	
						</div>
									
					</form:form>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		
		
	</div>		
</div>




<div id="usuario_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
				<h3 class="text-center">Endereço não encontrado</h3>
			</div>
	    	<div class="modal-body">
				<p class="text-center">O endereço não foi encontrado pelo CEP</p>
			</div>
	    	<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
	    </div>
	  </div>
	</div>
	
	<div id="erro_requisicao" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
				<h3 class="text-center text-mercado-de-musica">Erro!</h3>
			</div>
	    	<div class="modal-body">
				<p class="text-center text-danger" id="texto_selecionado_erro">
					Ocorreu um erro com o acesso do sistema. Por favor entre em contato conosco para maiores informações.
				</p>
			</div>
	    	<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
			</div>
	    </div>
	  </div>
	</div>	