<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

	
		<section class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-1 col-lg-5 col-lg-offset-2 margin-top-1percent">
		  <form:form action="${pageContext.request.contextPath}/envio_de_dados_senha" method="post" modelAttribute="usuarioDto" id="recupera_usuario_senha_form">
			  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			  <div class="form-group">
				  <label>
				    <input type="radio" name="tipo_de_pessoa" value="pf" class="tipo-de-pessoa-recuperacao_senha" />
				    Pessoa Física
				  </label>
			  </div>	
			  <div class="form-group">  
			  	<label>
				    <input type="radio" name="tipo_de_pessoa" value="pj" class="tipo-de-pessoa-recuperacao_senha" />
				    Pessoa Jurídica
				  </label>
			  </div>
			  <div class="form-group">
				<input type="text" class="form-control" name="cpfCnpj" id="cpf_cnpj" disabled />
			  </div>
			  
			  <div class="form-group">
			  	<button type="submit" id="enviar_recuperacao" class="btn btn-default" disabled><span class="glyphicon glyphicon-ok"></span> Enviar</button>
			  </div>
		  </form:form>
		</section>
	</div>
</div>	
