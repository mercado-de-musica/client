<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
			<h4 class="text-center">Você está desconectado</h4>
			<p class="text-center"><a href="usuario">Entre novamente na área de usuário</a></p>
		</div>
	</div>
</div>