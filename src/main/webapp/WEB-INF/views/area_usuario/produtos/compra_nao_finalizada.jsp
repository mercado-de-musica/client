<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:choose>
	<c:when test="${listaDeComprasNaoFinalizadas != null && !empty listaDeComprasNaoFinalizadas}">
		
		<c:forEach items="${listaDeComprasNaoFinalizadas}" var="compraNaoFinalizada">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent comprasComIds" id="compraDeId_${compraNaoFinalizada.compraDto.id}">
				<h4>CompraPretendida de ID ${compraNaoFinalizada.compraDto.id} em 
					<fmt:formatDate value="${compraNaoFinalizada.compraDto.criacao.time}" pattern="dd/MM/yyyy" />
					
		<%-- 			<c:forEach items="${compraNaoFinalizada.compraDto.datasDaCompra}" var="dataDaCompra"> --%>
		<%-- 				<c:if test="${dataDaCompra.dataAtualizada == true}"> --%>
		<%-- 					<fmt:formatDate value="${dataDaCompra.dataDaCompra.time}" pattern="dd/MM/yyyy" />		 --%>
		<%-- 				</c:if> --%>
		<%-- 			</c:forEach> --%>
				</h4>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
						<c:forEach var="apresentacaoProduto" items="${compraNaoFinalizada.listaApresentacoes}">
							<div id="produto_${apresentacaoProduto.infosGeraisProdutoUsuario.id}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<img src="${apresentacaoProduto.endereco}${apresentacaoProduto.nomeDoArquivo}" class="img-responsive" alt="${apresentacaoProduto.nomeDoArquivo}" />
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<p><b class="b-laranja"> 
										${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.instrumentoAcessorio.nome} 
									   	${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.marca.nome} 
									   	${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.produto.nome} 
									   	${apresentacaoProduto.infosGeraisProdutoUsuario.infosGeraisProduto.modelo.nome}
									</b></p>
									
									<p><b>Valor:</b> R$ ${apresentacaoProduto.infosGeraisProdutoUsuario.precoString}</p>
									
										
									<c:choose>
										<c:when test="${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
											<c:choose>
												<c:when test="${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.nomeFantasia != null}">
													<p><b>Vendedor:</b> ${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.nomeFantasia}</p>		
												</c:when>
												<c:otherwise>
													<p><b>Vendedor:</b> ${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.razaoSocial}</p>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											<p><b>Vendedor:</b> ${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.nome} ${apresentacaoProduto.infosGeraisProdutoUsuario.usuario.sobrenome}</p>
										</c:otherwise>
									</c:choose>
									
									<c:choose>
										<c:when test="${apresentacaoProduto.infosGeraisProdutoUsuario.tipoEnvio == 'CORREIOS'}">
											<p><b>Tipo de entrega:</b> ${apresentacaoProduto.infosGeraisProdutoUsuario.tipoEnvio.descricao}</p>
										</c:when>
										<c:otherwise>
											<p><b>Tipo de entrega:</b> ${apresentacaoProduto.infosGeraisProdutoUsuario.tipoEnvio.descricao}</p>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div class="form-group" id="remover_carrinho_de_compras_${apresentacaoProduto.infosGeraisProdutoUsuario.id}">
				 				    	<label>Remover</label>
				 				    	<input class="input-control botaoRadio" type="checkbox" id="cancelar_remocao_${apresentacaoProduto.infosGeraisProdutoUsuario.id}" onclick="verificarRemocaoProduto(${apresentacaoProduto.infosGeraisProdutoUsuario.id})" />
				 				  	</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
				
				<hr />
				
				<div class="row div_valor_total">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
						<div class="col-xs-12 col-sm-6 col-sm-offset-6 col-md-6 col-md-offset-6 col-lg-4 col-lg-offset-8 margin-top-2percent margin-bottom-2percent">
							<p id="valor_total_${compraNaoFinalizada.compraDto.id}">
								<b>Valor total:</b> R$ ${compraNaoFinalizada.valorTotal}
								<c:forEach var="apresentacaoProduto" items="${compraNaoFinalizada.listaApresentacoes}">
									<c:if test="${apresentacaoProduto.infosGeraisProdutoUsuario.tipoEnvio != 'CORREIOS'}">
										Sem cálculo de frete 
									</c:if>
								</c:forEach>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
						<div class="col-xs-6 col-sm-3 col-sm-offset-6 col-md-3 col-md-offset-6 col-lg-2 col-lg-offset-8 margin-top-2percent margin-bottom-2percent">
							<button type="button" class="btn btn-default btn-sm" onclick="verificarCompraNaoFinalizada(${compraNaoFinalizada.compraDto.id})" >
							  <span class="glyphicon glyphicon-ok"></span> Finalizar compra
							</button>
						</div>
						<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 margin-top-2percent margin-bottom-2percent">					
							<button type="button" class="btn btn-default btn-sm" onclick="verificarRemocaoCompra(${compraNaoFinalizada.compraDto.id})">
							  <span class="glyphicon glyphicon-remove"></span> Excluir compra
							</button>
						</div>
					</div>
				</div>
				
				
				<hr />
			</div>
			
		</c:forEach>
	</c:when>
	<c:otherwise>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent comprasComIds" id="compraDeId_${compraNaoFinalizada.compraDto.id}">
			<h4 class="text-center">Não existe compras não finalizadas</h4>
		</div>
	</c:otherwise>
</c:choose>


<div id="verificacao_remocao_carrinho_de_compras" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Remoção do produto</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success">
				Você tem certeza que deseja remover este produto?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirma_exclusao_produto_carrinho" onclick="confirmacaoExclusao(-1)" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" id="cancela_exclusao_produto_carrinho" data-dismiss="modal" onclick="retirarClickBotao(-1)">cancelar</button>
		</div>
    </div>
  </div>
</div>

<div id="verificacao_remocao_compra" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Remoção de compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success">
				Você tem certeza que deseja esta compra?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirma_exclusao_compra" onclick="excluirCompraNaoFinalizada(-1)" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">cancelar</button>
		</div>
    </div>
  </div>
</div>


<div id="verificacao_finalizacao_de_compra" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Finalizacao de compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success" id="texto_finalizacao_de_compra">
				Você tem certeza que deseja finalizar esta compra?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirma_finalizacao_de_compra_nao_finalizada" onclick="finalizarCompraNaoFinalizada(-1)" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">cancelar</button>
		</div>
    </div>
  </div>
</div>

<div id="remocao_carrinho_de_compras_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Remoção com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success">
				Sua remoção foi um sucesso!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="finalizacao_sucesso_carrinho_de_compra" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sua compra agora pode ser completada!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success">
				Clique no botão de ok e vá para o carrinho de compras.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="submit" class="btn btn-default" onclick="enviarParaCarrinhoDeCompras()" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="carrinho_de_compras_vazio" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Compras não finalizadas vazia</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success">
				Suas compras não finalizadas está vazia.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="remocao_carrinho_de_compras_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro ao remover produto do carrinho de compras</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Não foi possível remover o produto de suas compras não finalizadas. Por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="finalizacao_compras_nao_finalizadas_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro ao finalizar sua compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Não foi possível finalizar sua compra. Por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_generico_compra_nao_finalizada" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Ocorreu um erro com o acesso aos nossos servidores. Por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>
		