<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 margin-top-2percent margin-bottom-2percent">
		<c:choose>
			<c:when test="${desistenciaDeProdutoDto.rastreamento.class.simpleName == 'RastreamentoProdutoCorreiosDTO'}">
				<h4>Desistência de produto com envio pelos correios feita em <fmt:formatDate value="${desistenciaDeProdutoDto.dataDoProcessamento.time}" pattern="dd/MM/yyyy" /></h4>
			</c:when>
			<c:otherwise>
				<h4>Desistência de produto com entrega em mãos feita em <fmt:formatDate value="${desistenciaDeProdutoDto.dataDoProcessamento.time}" pattern="dd/MM/yyyy" /></h4>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 margin-top-2percent margin-bottom-2percent">
		<c:choose>
			<c:when test="${desistenciaDeProdutoDto.rastreamento.class.simpleName == 'RastreamentoProdutoCorreiosDTO'}">
				<p><b class="b-laranja">Rastreamento do produto:</b></p>
				<c:choose>
					<c:when test="${desistenciaDeProdutoDto.rastreamento.finalizado}">
						<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<p>Este produto não foi postado pelo vendedor. Dentro de 3 dias apartir da data de desistência você poderá transferí-lo na conta corrente do usuário.</p>
						</div>
					</c:when>
					<c:otherwise>
						<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<c:forEach items="${desistenciaDeProdutoDto.rastreamento.statusPostagemProdutoCorreio}" var="statusPostagemProdutoCorreio">
								<c:choose>
									<c:when test="${statusPostagemProdutoCorreio.status == 'EmBranco'}">
										<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Status do rastreamento:</b> Você não enviou o produto</p>
											<p><a href="#" onclick="imprmirChancelaCorreios('${desistenciaDeProdutoDto.rastreamento.infosGeraisProdutoUsuario.id}', true)"><span class="glyphicons glyphicons-print"></span> Gerar chancela dos Correios para impressão</a></p>
										</div>	
									</c:when>
									<c:otherwise>
										<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Status do rastreamento:</b> Produto ${statusPostagemProdutoCorreio.status} em ${statusPostagemProdutoCorreio.dataDoProcessamentoString}</p>
											<p><a href="#" onclick="rastreamentoDoProduto('${desistenciaDeProdutoDto.rastreamento.infosGeraisProdutoUsuario.id}')"><span class="glyphicon glyphicon-plane"></span> Rastreamento do produto</a></p>
										</div>	
									</c:otherwise>
								</c:choose>
								
							</c:forEach>
						</div>
					</c:otherwise>
				</c:choose>
			</c:when>
			
			
			<c:otherwise>
				
				<c:if test="${desistenciaDeProdutoDto.rastreamento != null && !desistenciaDeProdutoDto.rastreamento.finalizado}">
	
					<p><b class="b-laranja">Seu código de compra de produto (entrega em mãos):</b></p>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<p><b>Código de compra:</b> ${desistenciaDeProdutoDto.rastreamento.codigoComprador}</p>
							<p><a href="#" onclick="gerarPDFDeCodigo('${desistenciaDeProdutoDto.rastreamento.codigoComprador}', ${desistenciaDeProdutoDto.rastreamento.infosGeraisProdutoUsuario.id}, 'comprador')"><span class="glyphicons glyphicons-print"></span> Geração de pdf do código para a entrega ao vendedor</a></p>
						</div>
					</div>
			
					<hr />
			
					<p><b class="b-laranja">Código de venda dado pelo vendedor (entrega em mãos):</b></p>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<div class="form-group">
			 					<label>Código do vendedor:</label>
			 				    <c:choose>
			 				    	<c:when test="${desistenciaDeProdutoDto.rastreamento.codigoInseridoVendedor == null}">
			 				    		<input type="text" name="codigoVendedorDesistencia_${desistenciaDeProdutoDto.rastreamento.infosGeraisProdutoUsuario.id}" class="form-control" />
			 				    		<button class="btn btn-default" id="enviar_cod_vendedor_desistencia_${desistenciaDeProdutoDto.rastreamento.infosGeraisProdutoUsuario.id}" onclick="enviarCodigoVendedorDesistencia(${desistenciaDeProdutoDto.rastreamento.infosGeraisProdutoUsuario.id})" type="button">
			 				    			<span class="glyphicon glyphicon-send"></span> Enviar código do vendedor
			 				    		</button>
			 				    	</c:when>
			 				    	<c:otherwise>
			 				    		<input type="text" class="form-control" value="Código já incluído: ${desistenciaDeProdutoDto.rastreamento.codigoInseridoVendedor}" disabled />
			 				    	</c:otherwise>
			 				    </c:choose>
	 				    
			 				</div>
						</div>
					</div>
			
					<hr />
				</c:if>	
				<c:if test="${desistenciaDeProdutoDto.rastreamento != null && desistenciaDeProdutoDto.rastreamento.finalizado}">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<p>A transação foi finalizada. Dentro de alguns dias apartir da data de desistência você poderá transferí-lo na conta corrente do usuário.</p>
						</div>
					</div>
			
					<hr />
				</c:if>
				
			</c:otherwise>
		</c:choose>
	</div>
</div>	




<div id="envio_sucesso" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Você cadastrou o código de vendedor com sucesso! Com isso podemos finalizar os trâmites de pagamento, o Mercado de Música agradedece.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_generico" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="informacao_a_ser_escrita">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="geracao_de_PDF_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
			<p class="text-right"> <button type="button" class="btn btn-default" data-dismiss="modal" id="ok_exclusao_de_arquivo_pdf">OK</button></p>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success" id="geracao_de_pdf_visualizacao">
				Aguarde a geração de PDF
			</p>
		</div>
    	<div class="modal-footer">
		</div>
    </div>
  </div>
</div>

<div id="geracao_de_PDF_chancela_correios" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
			<p class="text-right"> <button type="button" class="btn btn-default" data-dismiss="modal" id="ok_exclusao_de_arquivo_pdf_chancela_correios">OK</button></p>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success" id="geracao_de_pdf_visualizacao_chancela_correios">
				Aguarde a geração de PDF
			</p>
		</div>
    	<div class="modal-footer">
		</div>
    </div>
  </div>
</div>


<div id="correios_rastreamento_eventos" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ratreamento do Correios</h3>
		</div>
    	<div class="modal-body" id="correios_rastreamento_eventos_escritas">
			
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_correios_rastreamento" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ratreamento do Correios</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto-erro-rastreamento-correios">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>