<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:choose>
	<c:when test="${listaDeCompras != null && !empty listaDeCompras}">
		<c:forEach items="${listaDeCompras}" var="compra">
			<c:forEach items="${compra.produtosComprados}" var="produtoComprado">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent">
						<div class="accordion" id="accordion_pai">
						  <div class="accordion-group">
						    <div class="accordion-heading">
						      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_pai" href="#collapse${produtoComprado.id}">
						        ${produtoComprado.infosGeraisProduto.instrumentoAcessorio.nome} ${produtoComprado.infosGeraisProduto.marca.nome} ${produtoComprado.infosGeraisProduto.produto.nome} ${produtoComprado.infosGeraisProduto.modelo.nome} - R$ ${produtoComprado.precoVisualizacaoString}
						      </a>
						    </div>
						    <div id="collapse${produtoComprado.id}" class="accordion-body collapse">
						      <div class="accordion-inner">
						        <p><b class="b-laranja">Informações do Comprador:</b></p>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
										<c:choose>
											<c:when test="${compra.comprador.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
												<c:choose>
													<c:when test="${compra.comprador.nomeFantasia != null}">
														<p><b>Nome:</b> ${compra.comprador.nomeFantasia}</p>
													</c:when>
													<c:otherwise>
														<p><b>Nome:</b> ${compra.comprador.razaoSocial}</p>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<p><b>Nome:</b> ${compra.comprador.nome} ${compra.comprador.sobrenome}</p>
											</c:otherwise>
										</c:choose>
										
										<p><b>E-mail:</b> ${compra.comprador.email}</p>
										<c:forEach items="${compra.comprador.telefonesList}" var="telefone">
											<p><b>Telefone:</b> (${telefone.ddd}) ${telefone.telefone} - ${telefone.tipoDeTelefone}</p>
										</c:forEach>	
									</div>
								</div>
								
								<hr />
								
								
								
								<p><b class="b-laranja">Informações do produto:</b></p>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
										
										<p><b>Número de série:</b> ${produtoComprado.numeroDeSerie}</p>
				
										<p><b>Altura:</b> ${produtoComprado.alturaString} centímetros</p>
										<p><b>Largura:</b> ${produtoComprado.larguraString} centímetros</p>
										<p><b>Comprimento:</b> ${produtoComprado.comprimentoString} centímetros</p>
										<p><b>Peso:</b> ${produtoComprado.pesoString} kilos</p>
										<p><b>Ano de fabricação:</b> ${produtoComprado.anoDeFabricacao}</p>
										<p><b>País fabricação:</b> ${produtoComprado.paisDeFabricacao.descricao}</p>
										
									</div>
								</div>
								
								<hr />
								
								
								<p><b class="b-laranja">Informações de pagamento:</b></p>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">		
										<c:forEach items="${compra.datasDaCompra}" var="dataDaCompra">
											<c:if test="${dataDaCompra.dataAtualizada == true}">
												<p><b>Data do pedido:</b> <fmt:formatDate value="${dataDaCompra.dataDaCompra.time}" pattern="dd/MM/yyyy" /></p>
												<p><b>Número do pedido:</b> ${compra.id}</p>		
											</c:if>
										</c:forEach>	
									</div>
								</div>
								
								
								<hr />
								
								<c:choose>
									<c:when test="${produtoComprado.tipoEnvio == 'CORREIOS'}">
										<p><b class="b-laranja">Rastreamento do produto:</b></p>
										<c:choose>
											<c:when test="${produtoComprado.rastreamento != null && produtoComprado.rastreamento.statusPostagemProdutoCorreio != null && not empty produtoComprado.rastreamento && not empty produtoComprado.rastreamento.statusPostagemProdutoCorreio}">
												<c:forEach items="${produtoComprado.rastreamento.statusPostagemProdutoCorreio}" var="statusPostagemProdutoCorreio">
													<c:if test="${statusPostagemProdutoCorreio.ativo && statusPostagemProdutoCorreio.status != 'EmBranco'}">
														<div class="row">
															<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
																<p><b>Status do rastreamento:</b> Produto ${statusPostagemProdutoCorreio.status} em ${statusPostagemProdutoCorreio.dataDoProcessamentoString}</p>
																<p><a href="#" onclick="rastreamentoDoProduto('${produtoComprado.id}')"><span class="glyphicon glyphicon-plane"></span> Rastreamento do produto</a></p>
															</div>
														</div>	
													</c:if>
													<c:if test="${statusPostagemProdutoCorreio.ativo && statusPostagemProdutoCorreio.status == 'EmBranco'}">
														<div class="row">
															<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
																<p><b>Status do rastreamento:</b> Produto ainda não foi postado</p>
																<p><a href="#" onclick="imprmirChancelaCorreios('${produtoComprado.id}', false)"><span class="glyphicons glyphicons-print"></span> Gerar chancela dos Correios para impressão</a></p>
															</div>
														</div>		
													</c:if>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
														<p><b>Status do rastreamento:</b> Produto ainda não foi postado</p>
														<p><a href="#" onclick="imprmirChancelaCorreios('${produtoComprado.id}', false)"><span class="glyphicons glyphicons-print"></span> Gerar chancela dos Correios para impressão</a></p>
													</div>
												</div>
											</c:otherwise>
										</c:choose>					
									</c:when>
									<c:otherwise>
										<p><b class="b-laranja">Seu código de venda de produto (entrega em mãos):</b></p>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
												<p><b>Código de venda:</b> ${produtoComprado.rastreamento.codigoVendedor}</p>
												<p><a href="#" onclick="gerarPDFDeCodigo('${produtoComprado.rastreamento.codigoVendedor}', ${produtoComprado.id}, 'vendedor')"><span class="glyphicons glyphicons-print"></span> Geração de pdf do código para a entrega ao comprador</a></p>
											</div>
										</div>
										
										<hr />
										
										<p><b class="b-laranja">Código de compra dado pelo comprador (entrega em mãos):</b></p>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
												<label>Código de compra:</label>
								 				    <c:choose>
								 				    	<c:when test="${produtoComprado.rastreamento.codigoInseridoComprador == null}">
								 				    		<input type="text" name="codigoComprador_${produtoComprado.id}" class="form-control" />	
								 				    		<button class="btn btn-default" id="enviar_cod_comprador_${produtoComprado.id}" onclick="enviarCodigoComprador(${produtoComprado.id})">
								 				    			<span class="glyphicon glyphicon-send"></span> Enviar código do comprador
								 				    		</button>
								 				    	</c:when>
								 				    	<c:otherwise>
								 				    		<input type="text" class="form-control" value="Código já incluído: ${produtoComprado.rastreamento.codigoInseridoComprador}" disabled />
								 				    	</c:otherwise>
								 				    </c:choose>
											</div>
										</div>
										
										<hr />
										
									</c:otherwise>
								</c:choose>								
								
								
								
								
								<c:if test="${produtoComprado.desistenciaDeCompra == true && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == true}">
									<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Você já tem uma desistência em andamento.</b> <a href="${pageContext.servletContext.contextPath}/usuario/desistencia_de_produto_vendedor/${produtoComprado.id}"><span class="glyphicon glyphicon-eye-open"></span> Verifique o andamento dela clicando aqui</a></p>
										</div>
									</div>
									<hr />
								</c:if>
								
								<c:if test="${produtoComprado.desistenciaDeCompra == false && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == true}">
									<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Você já tem uma desistência em andamento.</b> <a href="${pageContext.servletContext.contextPath}/usuario/desistencia_de_produto_vendedor/${produtoComprado.id}"><span class="glyphicon glyphicon-eye-open"></span> Verifique o andamento dela clicando aqui</a></p>
										</div>
									</div>
									<hr />
								</c:if>
								
								<!-- aviso para pagina de desistencia -->
								
								<c:if test="${produtoComprado.desistenciaDeCompra == true && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == false}">
		 				    		<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Sua desistência deste produto já foi finalizada.</b></p>
										</div>
									</div>
									<hr />
		 				    	</c:if>
		 				    	
		 				    	<c:if test="${produtoComprado.desistenciaDeCompra == false && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == false}">
		 				    		<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Sua desistência deste produto já foi finalizada.</b></p>
										</div>
									</div>
									<hr />
		 				    	</c:if>
						      </div>
						    </div>
						  </div>
						</div>	
						<!-- fim accordion -->
						
					</div>
				</div>	
				</c:forEach>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
				<h4 class="text-center">Não existem produtos vendidos</h4>
			</div>
		</div>
	</c:otherwise>
</c:choose>






<div id="geracao_de_PDF_chancela_correios" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
			<p class="text-right"> <button type="button" class="btn btn-default" data-dismiss="modal" id="ok_exclusao_de_arquivo_pdf_chancela_correios">OK</button></p>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success" id="geracao_de_pdf_visualizacao_chancela_correios">
				Aguarde a geração de PDF
			</p>
		</div>
    	<div class="modal-footer">
		</div>
    </div>
  </div>
</div>



<div id="geracao_de_PDF_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
			<p class="text-right"> <button type="button" class="btn btn-default" data-dismiss="modal" id="ok_exclusao_de_arquivo_pdf">OK</button></p>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success" id="geracao_de_pdf_visualizacao">
				Aguarde a geração de PDF
			</p>
		</div>
    	<div class="modal-footer">
		</div>
    </div>
  </div>
</div>


<div id="erro_codigo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro com o código do Comprador!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="erro_a_ser_escrito">
				
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="correios_rastreamento_eventos" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ratreamento do Correios</h3>
		</div>
    	<div class="modal-body" id="correios_rastreamento_eventos_escritas">
			
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_correios_rastreamento" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ratreamento do Correios</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto-erro-rastreamento-correios">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="erro_generico" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_direto" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="informacao_a_ser_escrita">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="envio_sucesso" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Você cadastrou o código de comprador com sucesso! Com isso podemos finalizar os trâmites de pagamento, o Mercado de Música agradedece.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>