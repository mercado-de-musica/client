<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:choose>
	<c:when test="${listaDeCompras != null && !empty listaDeCompras}">
		<c:forEach items="${listaDeCompras}" var="compra">
				
			<c:forEach items="${compra.produtosComprados}" var="produtoComprado">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
						<div class="accordion" id="accordion_pai">
						  <div class="accordion-group">
						    <div class="accordion-heading">
						      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_pai" href="#collapse${produtoComprado.id}">
						        ${produtoComprado.infosGeraisProduto.instrumentoAcessorio.nome} ${produtoComprado.infosGeraisProduto.marca.nome} ${produtoComprado.infosGeraisProduto.produto.nome} ${produtoComprado.infosGeraisProduto.modelo.nome} - R$ ${produtoComprado.precoVisualizacaoString}
						      </a>
						    </div>
						    <div id="collapse${produtoComprado.id}" class="accordion-body collapse">
						      <div class="accordion-inner">
						        <p><b class="b-laranja">Pagamento:</b></p>
						        <div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
										<p><b>Status deste pagamento:</b> ${compra.statusTransacao.descricaoPortugues}</p>
									</div>
								</div>	
						        
						        <p><b class="b-laranja">Informações do vendedor:</b></p>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
										<c:choose>
											<c:when test="${produtoComprado.usuario.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
												<c:choose>
													<c:when test="${produtoComprado.usuario.nomeFantasia != null}">
														<p><b>Nome:</b> ${produtoComprado.usuario.nomeFantasia}</p>
													</c:when>
													<c:otherwise>
														<p><b>Nome:</b> ${produtoComprado.usuario.razaoSocial}</p>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<p><b>Nome:</b> ${produtoComprado.usuario.nome} ${produtoComprado.usuario.sobrenome}</p>
											</c:otherwise>
										</c:choose>
										
										<p><b>E-mail:</b> ${produtoComprado.usuario.email}</p>
										<c:forEach items="${produtoComprado.usuario.telefonesList}" var="telefone">
											<p><b>Telefone:</b> (${telefone.ddd}) ${telefone.telefone} - ${telefone.tipoDeTelefone}</p>
										</c:forEach>	
									</div>
								</div>
								
								<hr />
								
								
								
								<p><b class="b-laranja">Informações do produto:</b></p>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
										
										<p><b>Número de série:</b> ${produtoComprado.numeroDeSerie}</p>
				
										<p><b>Altura:</b> ${produtoComprado.alturaString} centímetros</p>
										<p><b>Largura:</b> ${produtoComprado.larguraString} centímetros</p>
										<p><b>Comprimento:</b> ${produtoComprado.comprimentoString} centímetros</p>
										<p><b>Peso:</b> ${produtoComprado.pesoString} kilos</p>
										<p><b>Ano de fabricação:</b> ${produtoComprado.anoDeFabricacao}</p>
										<p><b>País fabricação:</b> ${produtoComprado.paisDeFabricacao.descricao}</p>
										
									</div>
								</div>
								
								<hr />
								
								
								
								
								<p><b class="b-laranja">Informações de pagamento:</b></p>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
										<p><b>Tipo de pagamento:</b> ${compra.tipoPagamento.descricao}</p>	
										<c:if test="${compra.compraBoleto.urlBoleto != null}">
											<p><b>Url Boleto:</b> <a href="${compra.compraBoleto.urlBoleto}" target="_blank">${compra.compraBoleto.urlBoleto}</a></p>
										</c:if>
										
										
										<c:forEach items="${compra.datasDaCompra}" var="dataDaCompra">
											<c:if test="${dataDaCompra.dataAtualizada == true}">
												<p><b>Data do pedido:</b> <fmt:formatDate value="${dataDaCompra.dataDaCompra.time}" pattern="dd/MM/yyyy" /></p>
												<p><b>Número do pedido:</b> ${compra.id}</p>		
											</c:if>
										</c:forEach>	
									</div>
								</div>
								
								
								<hr />
								
								<c:choose>
									<c:when test="${produtoComprado.rastreamento != null && not empty produtoComprado.rastreamento}">
										<c:choose>
											<c:when test="${produtoComprado.tipoEnvio == 'CORREIOS'}">
												
												<p><b class="b-laranja">Rastreamento do produto:</b></p>
												
												<c:forEach items="${produtoComprado.rastreamento.statusPostagemProdutoCorreio}" var="statusPostagemProdutoCorreio">
													<c:if test="${statusPostagemProdutoCorreio.ativo && statusPostagemProdutoCorreio.status != 'EmBranco'}">
														<div class="row">
															<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
																<p><b>Status do rastreamento:</b> Produto ${statusPostagemProdutoCorreio.status} em ${statusPostagemProdutoCorreio.dataDoProcessamentoString}</p>
																<p><a href="#" onclick="rastreamentoDoProduto('${produtoComprado.id}')"><span class="glyphicon glyphicon-plane"></span> Rastreamento do produto</a></p>
															</div>
														</div>
													</c:if>
													<c:if test="${statusPostagemProdutoCorreio.ativo && statusPostagemProdutoCorreio.status == 'EmBranco'}">
														<div class="row">
															<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
																<p><b>Status do rastreamento:</b> Produto ainda não foi postado</p>
															</div>
														</div>	
													</c:if>
												</c:forEach>
												
												<hr />
												
											</c:when>
											<c:otherwise>
												<p><b class="b-laranja">Seu código de compra de produto (entrega em mãos):</b></p>
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
														<p><b>Código de compra:</b> ${produtoComprado.rastreamento.codigoComprador}</p>
														<p><a href="#" onclick="gerarPDFDeCodigo('${produtoComprado.rastreamento.codigoComprador}', ${produtoComprado.id}, 'comprador')"><span class="glyphicons glyphicons-print"></span> Geração de pdf do código para a entrega ao vendedor</a></p>
													</div>
												</div>
												
												<hr />
												
												<p><b class="b-laranja">Código de venda dado pelo vendedor (entrega em mãos):</b></p>
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
														<div class="form-group">
										 					<label>Código de venda:</label>
										 				    <c:choose>
										 				    	<c:when test="${produtoComprado.rastreamento.codigoInseridoVendedor == null}">
										 				    		<input type="text" name="codigoVendedor_${produtoComprado.id}" class="form-control" />
										 				    		<button class="btn btn-default" id="enviar_cod_vendedor_${produtoComprado.id}" onclick="enviarCodigoVendedor(${produtoComprado.id})" type="button">
										 				    			<span class="glyphicon glyphicon-send"></span> Enviar código do vendedor
										 				    		</button>
										 				    	</c:when>
										 				    	<c:otherwise>
										 				    		<input type="text" class="form-control" value="Código já incluído: ${produtoComprado.rastreamento.codigoInseridoVendedor}" disabled />
										 				    	</c:otherwise>
										 				    </c:choose>
										 				    
										 				</div>
													</div>
												</div>
												
												<hr />
												
											</c:otherwise>
											
										</c:choose>		
										
									</c:when>
									<c:otherwise>
										<p><b class="b-laranja">Rastreamento do produto:</b></p>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
												<p><b>Status do rastreamento:</b> Ainda não foi gerado o rastreamento deste produto</p>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
								
								<c:if test="${produtoComprado.rastreamento != null && produtoComprado.rastreamento.finalizado == true}">
									<p><b class="b-laranja">Envio de reputação do vendedor: </b></p>
									
									<c:choose>
										<c:when test="${produtoComprado.usuario.reputacaoVendedor != null && !empty produtoComprado.usuario.reputacaoVendedor}">
											
											<c:forEach items="${produtoComprado.usuario.reputacaoVendedor}" var="reputacaoVendedor">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
														<div class="form-group">
										 					<label>Nota:</label>
															<c:forEach var="i" begin="1" end="10">
																<c:choose>
																	<c:when test="${reputacaoVendedor.nota == i}">
																		<label class="radio-inline"><input type="radio" value="${i}" name="nota_${produtoComprado.id}" checked disabled>${i}</label>															
																	</c:when>
																	<c:otherwise>
																		<label class="radio-inline"><input type="radio" value="${i}" name="nota_${produtoComprado.id}" disabled>${i}</label>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
										 				</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
														<div class="form-group">
										 					<label for="comment">Mensagem (não obrigatório):</label>
			  												<textarea class="form-control" rows="5" name="mensagem_${produtoComprado.id}" disabled>${reputacaoVendedor.mensagem}</textarea>
										 				</div>
													</div>
												</div>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
												<div class="form-group">
								 					<label>Nota:</label>
								 				   	
								 				   	<label class="radio-inline"><input type="radio" value="1" name="nota_${produtoComprado.id}">1</label>
													<label class="radio-inline"><input type="radio" value="2" name="nota_${produtoComprado.id}">2</label>
													<label class="radio-inline"><input type="radio" value="3" name="nota_${produtoComprado.id}">3</label>
													<label class="radio-inline"><input type="radio" value="4" name="nota_${produtoComprado.id}">4</label>
													<label class="radio-inline"><input type="radio" value="5" name="nota_${produtoComprado.id}">5</label>
													<label class="radio-inline"><input type="radio" value="6" name="nota_${produtoComprado.id}">6</label>
													<label class="radio-inline"><input type="radio" value="7" name="nota_${produtoComprado.id}">7</label>
													<label class="radio-inline"><input type="radio" value="8" name="nota_${produtoComprado.id}">8</label>
													<label class="radio-inline"><input type="radio" value="9" name="nota_${produtoComprado.id}">9</label>
													<label class="radio-inline"><input type="radio" value="10" name="nota_${produtoComprado.id}">10</label>
								 				</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
												<div class="form-group">
								 					<label for="comment">Mensagem (não obrigatório):</label>
	  												<textarea class="form-control" rows="5" name="mensagem_${produtoComprado.id}"></textarea>
								 				</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
												<div class="form-group">
								 					<button class="btn btn-default" id="enviar_reputacao_vendedor_${produtoComprado.id}" onclick="mostrarAceiteReputacaoVendedor(${produtoComprado.id})" type="button">
								 						<span class="glyphicon glyphicon-send"></span> Enviar reputação do vendedor
								 					</button>
								 				</div>
											</div>
										</div>
										</c:otherwise>
									</c:choose>
									
									
									<hr />
								</c:if>
								
								
								<c:if test="${produtoComprado.desistenciaDeCompra == true && produtoComprado.desistenciaEmAberto == null && compra.statusTransacao != 'AGUARDANDO_PAGAMENTO' && compra.statusTransacao != 'EM_PROCESSAMENTO' && compra.statusTransacao != 'EM_MONITORAMENTO' && compra.statusTransacao != 'EM_RECUPERACAO'}">
									<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<div class="form-group">
							 					<label>Clique aqui para desistir da compra:</label>
							 				   	<c:choose>
							 				   		<c:when test="${produtoComprado.tipoEnvio == 'CORREIOS'}">
							 				   			<button class="btn btn-default" id="desistencia_de_compra_${produtoComprado.id}" onclick="desistenciaDeCompraCorreios(${produtoComprado.id})" type="button">
									 				   		<span class="glyphicon glyphicon-trash"></span> Desistir
									 				   	</button>
							 				   		</c:when>
							 				   		<c:otherwise>
								 				   		<button class="btn btn-default" id="desistencia_de_compra_${produtoComprado.id}" onclick="desistenciaDeCompra(${produtoComprado.id})" type="button">
									 				   		<span class="glyphicon glyphicon-trash"></span> Desistir
									 				   	</button>
							 				   		</c:otherwise>
							 				   	</c:choose>	
							 				</div>
										</div>
									</div>
									<hr />
								</c:if>
								
								
								<c:if test="${produtoComprado.desistenciaDeCompra == true && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == true && compra.statusTransacao != 'AGUARDANDO_PAGAMENTO' && compra.statusTransacao != 'EM_PROCESSAMENTO' && compra.statusTransacao != 'EM_MONITORAMENTO' && compra.statusTransacao != 'EM_RECUPERACAO'}">
									<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Você já tem uma desistência em andamento.</b> <a href="${pageContext.servletContext.contextPath}/usuario/desistencia_de_produto/${produtoComprado.id}"><span class="glyphicon glyphicon-eye-open"></span> Verifique o andamento dela clicando aqui</a></p>
										</div>
									</div>
									<hr />
								</c:if>
								
								<c:if test="${produtoComprado.desistenciaDeCompra == false && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == true && compra.statusTransacao != 'AGUARDANDO_PAGAMENTO' && compra.statusTransacao != 'EM_PROCESSAMENTO' && compra.statusTransacao != 'EM_MONITORAMENTO' && compra.statusTransacao != 'EM_RECUPERACAO'}">
									<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Você já tem uma desistência em andamento.</b> <a href="${pageContext.servletContext.contextPath}/usuario/desistencia_de_produto/${produtoComprado.id}"><span class="glyphicon glyphicon-eye-open"></span> Verifique o andamento dela clicando aqui</a></p>
										</div>
									</div>
									<hr />
								</c:if>
								
<!-- 								aviso para pagina de desistencia -->
								
								<c:if test="${produtoComprado.desistenciaDeCompra == true && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == false && compra.statusTransacao != 'AGUARDANDO_PAGAMENTO' && compra.statusTransacao != 'EM_PROCESSAMENTO' && compra.statusTransacao != 'EM_MONITORAMENTO' && compra.statusTransacao != 'EM_RECUPERACAO'}">
		 				    		<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Sua desistência deste produto já foi finalizada.</b></p>
										</div>
									</div>
									<hr />
		 				    	</c:if>
		 				    	
		 				    	<c:if test="${produtoComprado.desistenciaDeCompra == false && produtoComprado.desistenciaEmAberto != null && produtoComprado.desistenciaEmAberto == false && compra.statusTransacao != 'AGUARDANDO_PAGAMENTO' && compra.statusTransacao != 'EM_PROCESSAMENTO' && compra.statusTransacao != 'EM_MONITORAMENTO' && compra.statusTransacao != 'EM_RECUPERACAO'}">
		 				    		<p><b class="b-laranja">Desistencia da compra: </b></p>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
											<p><b>Sua desistência deste produto já foi finalizada.</b></p>
										</div>
									</div>
									<hr />
		 				    	</c:if>
			 				    
						      </div>
						    </div>
						  </div>
						</div>	
						<!-- fim accordion -->
						
					</div>
				</div>	
			</c:forEach>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
				<h4 class="text-center">Não existem produtos comprados</h4>
			</div>
		</div>
	</c:otherwise>
</c:choose>	





<div id="geracao_de_PDF_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
			<p class="text-right"> <button type="button" class="btn btn-default" data-dismiss="modal" id="ok_exclusao_de_arquivo_pdf">OK</button></p>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success" id="geracao_de_pdf_visualizacao">
				
			</p>
		</div>
    	<div class="modal-footer">
		</div>
    </div>
  </div>
</div>

<div id="aceite_informacoes" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Você ter certeza que deseja enviar estas informações?</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning">
				As informações sobre o vendedor que você está prestes a enviar não poderá ser mudada futuramente. Você pretende enviá-las mesmo assim?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" id="com_certeza_quero_enviar_as_informacoes" class="btn btn-default" data-dismiss="modal">Sim</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
		</div>
    </div>
  </div>
</div>



<div id="erro_codigo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro com o código do vendedor!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="erro_a_ser_escrito">
				
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="erro_generico" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="informacao_a_ser_escrita">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_nota_vazia" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="informacao_a_ser_escrita">
				É necessário escolher uma nota para o vendedor do produto
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="envio_sucesso" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Você cadastrou o código de vendedor com sucesso! Com isso podemos finalizar os trâmites de pagamento, o Mercado de Música agradedece.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="envio_sucesso_reputacao" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Sua nota do vendedor foi enviada com sucesso! O Mercado de Música agradedece.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="desistencia_de_compra_cancelada" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Desistência de compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Você não pode mais desistir da compra a negociação já está concluída a mais de 10 dias. 
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="desistencia_insercao_codigo_de_venda" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Desistência de compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning">
				Insira o código de venda entregue pelo vendedor do produto. Logo após daremos continuidade ao processo de desistência do produto. 
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="desistencia_de_compra_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Desistência de compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="frase_escrita_desistencia">
				Você tem certeza que deseja desistir da compra? 
				Essa ação pode acarretar problemas  em relação ao retorno do dinheiro para sua conta de Gateway De Pagamento
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" id="desistencia_de_compra_aceite" class="btn btn-default" data-dismiss="modal">Sim</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
		</div>
    </div>
  </div>
</div>

<div id="desistencia_sucesso" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Desistencia realizada com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success" id="protocolo_sucesso_desistencia">
			</p>
			
			<p class="text-center text-success">
				Você acabou de desistir do seu produto, informaremos o vendedor do mesmo e dar continuidade no processo. Para acessar a página de desistência acesse: 
				Área de usuário -> Produtos comprados -> Seu produto -> Desistência da compra.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="correios_rastreamento_eventos" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ratreamento do Correios</h3>
		</div>
    	<div class="modal-body" id="correios_rastreamento_eventos_escritas">
			
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_correios_rastreamento" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ratreamento do Correios</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto-erro-rastreamento-correios">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>
