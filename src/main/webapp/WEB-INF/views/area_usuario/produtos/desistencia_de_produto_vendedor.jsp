<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 margin-top-2percent margin-bottom-2percent">
		<c:choose>
			<c:when test="${desistenciaCorreiosDto != null}">
				<h4>Desistência de produto com envio pelos correios feita em <fmt:formatDate value="${desistenciaCorreiosDto.dataDoProcessamento.time}" pattern="dd/MM/yyyy" /></h4>
			</c:when>
			<c:otherwise>
				<h4>Desistência de produto com entrega em mãos feita em <fmt:formatDate value="${desistenciaProdutoDiretoDto.dataDesistencia.time}" pattern="dd/MM/yyyy" /></h4>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 margin-top-2percent margin-bottom-2percent">
		<c:choose>
			<c:when test="${desistenciaCorreiosDto != null}">
				<p><b class="b-laranja">Rastreamento do produto:</b></p>
				<c:choose>
					<c:when test="${eventosCorreios != null && not empty eventosCorreios}">
						<c:forEach items="${eventosCorreios}" var="evento">
							<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
								<p> Data: <b>${evento.data} as ${evento.hora} </b> - UF: <b>${evento.uf}</b> - Cidade: <b>
								${evento.cidade}</b> Local: <b>${evento.local}</b> - ${evento.descricao}</p>
							</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<div class="col-xs-12 col-sm-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<p><b>Status do rastreamento:</b> Produto ainda não foi postado</p>
						</div>
					</c:otherwise>
				</c:choose>
			</c:when>
			
			<c:otherwise>
				
				<c:if test="${desistenciaProdutoDiretoDto != null && desistenciaProdutoDiretoDto.codigoCompradorNoSistema == true}">
			
					<p><b class="b-laranja">Seu código de compra de produto (entrega em mãos):</b></p>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<p><b>Código de venda:</b> ${desistenciaProdutoDiretoDto.rastreamentoProdutoDireto.codigo.codigoVendedor}</p>
						</div>
					</div>
					<hr />
					
					<p><b class="b-laranja">Código de compra dado pelo comprador (entrega em mãos):</b></p>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<div class="form-group">
			 					<label>Código do comprador:</label>
			 				    <c:choose>
			 				    	<c:when test="${desistenciaProdutoDiretoDto.rastreamentoProdutoDireto.codigo.codigoInseridoComprador == null}">
			 				    		<input type="text" name="codigoCompradorDesistencia_${desistenciaProdutoDiretoDto.rastreamentoProdutoDireto.infosGeraisProdutoUsuario.id}" class="form-control" />
			 				    		<button class="btn btn-default" id="enviar_cod_comprador_desistencia_${desistenciaProdutoDiretoDto.rastreamentoProdutoDireto.infosGeraisProdutoUsuario.id}" onclick="enviarCodigoCompradorDesistencia(${desistenciaProdutoDiretoDto.rastreamentoProdutoDireto.infosGeraisProdutoUsuario.id})">
			 				    			<span class="glyphicon glyphicon-send"></span> Enviar código do comprador
			 				    		</button>
			 				    	</c:when>
			 				    	<c:otherwise>
			 				    		<input type="text" class="form-control" value="Código já incluído: ${desistenciaProdutoDiretoDto.rastreamentoProdutoDireto.codigo.codigoInseridoComprador}" disabled />
			 				    	</c:otherwise>
			 				    </c:choose>
			 				</div>
						</div>
					</div>
					
					<hr />
				</c:if>	
				<c:if test="${desistenciaProdutoDiretoDto != null && desistenciaProdutoDiretoDto.codigoCompradorNoSistema == false}">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
							<p><b>Até o atual momento não existe interação por parte do vendedor. Caso não haja em até 3 dias apartir da data da desistência, o dinheiro retornará automaticamente para sua conta.</b></p>
						</div>
					</div>
					
					<hr />
				</c:if>
				
			</c:otherwise>
		</c:choose>
		
	</div>
</div>



<div id="envio_sucesso" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Você cadastrou o código de vendedor com sucesso! Com isso podemos finalizar os trâmites de pagamento, o Mercado de Música agradedece.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_generico" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="informacao_a_ser_escrita">
				Ocorreu um erro com o envio da informação, por favor entre em contato conosco.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>