<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<form:form action="${pageContext.request.contextPath}/usuario/alteracao_cadastro_usuario_pj" method="post" modelAttribute="usuarioDto" id="cadastro_novo_usuario_pj">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="hidden" name="id" value="${usuarioDto.id}" />
				
				<input type="hidden" name="ativo" value="${usuarioDto.ativo}" />
				<input type="hidden" name="tipoUsuario" value="${usuarioDto.tipoUsuario}" />
				<input name="email" type="hidden" class="form-control" value="${usuarioDto.email}" id="email_pj" />
				
				<section class="row" id="dados_pessoais">
					
					<h4 class="text-center">Dados empresariais</h4>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">	
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 margin-top-2percent formulario-usuario-pj">
					    <label>Nome fantasia:</label>
					    <input type="text" name="nomeFantasia" value="${usuarioDto.nomeFantasia}" class="form-control" />
					  </div>
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 margin-top-2percent formulario-usuario-pj">
					    <label>Razão Social:</label>
					    <input type="text" name="razaoSocial" value="${usuarioDto.razaoSocial}" class="form-control" />
					  </div>
					  <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4 margin-top-2percent formulario-usuario-pj">
					    <label>CNPJ:</label>
					    <input type="text" name="cnpj" value="${usuarioDto.cnpj}" class="form-control" readonly />
					  </div> 
					</div>
				</section>
				
				<hr />
				
				<section class="row">
					<h4 class="text-center">Dados do titular da conta</h4>
					<div class="col-xs-12 col-sm-12 margin-top-2percent">	
							
					  <div class="col-xs-12 col-sm-6 col-md-6 margin-top-2percent formulario-usuario-pj">
					    <label>Nome:</label>
					    <input type="text" name="nomeTitular" value="${usuarioDto.nomeTitular}" class="form-control" placeholder="José" />
					  </div>
					  <div class="col-xs-12 col-sm-6 col-md-6 margin-top-2percent formulario-usuario-pj">
					    <label>Sobrenome:</label>
					    <input type="text" name="sobrenomeTitular" value="${usuarioDto.sobrenomeTitular}" class="form-control" placeholder="da Silva" />
					  </div>
					  <div class="col-xs-12 col-sm-4 col-md-4 margin-top-2percent formulario-usuario-pj">
					    <label>RG:</label>
					    <input type="text" name="rgTitular" value="${usuarioDto.rgTitular}" class="form-control" />
					  </div>
					  <div class="col-xs-12 col-sm-4 col-md-4 margin-top-2percent formulario-usuario-pj">
					    <label>CPF:</label>
					    <input type="text" name="cpfTitular" value="${usuarioDto.cpfTitular}" class="form-control" />
					  </div> 
					  <div class="col-xs-12 col-sm-4 col-md-4 margin-top-2percent formulario-usuario-pj">
					    <label>Data de nascimento:</label>
					    <input name="dataDeNascimentoTitularString" value="${usuarioDto.dataDeNascimentoTitularString}" type="text" class="datepicker form-control" />
					  </div>
					</div>
				</section>
				
				<hr />
				
				<%@include file="formulario_endereco_telefone.jsp" %>
				
									
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
					<button id="enviar_cadastro_usuario_pj" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send"></span> Enviar cadastro pessoa jurídica</button>	
				</div>
			</form:form>
		</div>
	</div>
</div>


<div id="usuario_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Endereço não encontrado</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">O endereço não foi encontrado pelo CEP</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
    </div>
  </div>
</div>
