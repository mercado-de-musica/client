<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>




<section class="row" id="telefones">
	<h4 class="text-center">
		Telefones 
		
		<button id="acrescentar_telefone" type="button" class="btn btn-default" aria-label="Left Align">
		  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		</button>
	
	</h4>
	
	<c:forEach items="${usuarioDto.telefonesList}" var="telefone" varStatus="telefoneStatus">
		<div id="telefone_${telefoneStatus.index}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-1percent padding-1percent borders-all telefone">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
				<button type="button" class="btn btn-default" onclick="excluirTelefone(${telefoneStatus.index});">
				  <span class="glyphicon glyphicon-remove"></span>
				  Excluir telefone
				</button>
			</div>
			
			
			
			<input name="telefonesList[${telefoneStatus.index}].id" value="${telefone.id}" type="hidden" />
			<input name="telefonesList[${telefoneStatus.index}].ddd" value="${telefone.ddd}" type="hidden" class="ddd-input" />
			
			
		  	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-top-2percent formulario-usuario-pf">
		    	<label>Telefone:</label>
		    	<input name="telefonesList[${telefoneStatus.index}].telefone" value="(${telefone.ddd}) ${telefone.telefone}" type="text" class="form-control telefone-input" />
		    
		  	</div>
		  
			  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 margin-top-2percent formulario-usuario-pf">
			    <label>Tipo de telefone:</label>
			    <select name="telefonesList[${telefoneStatus.index}].tipoDeTelefone" class="form-control select-tipo-telefone" style="width:100%;">
			    	<c:forEach items="${tiposTelefone}" var="tipoTelefone">
			    		<c:choose>
							<c:when test="${tipoTelefone == telefone.tipoDeTelefone }">
      								<option value="${tipoTelefone}" selected>${tipoTelefone.descricao}</option>
			    			</c:when>
			    			<c:otherwise>
			        			<option value="${tipoTelefone}">${tipoTelefone.descricao}</option>
			    			</c:otherwise>
			    		</c:choose>	
			    	</c:forEach>
			    </select>
			  </div>
		</div>
	</c:forEach>	
</section>			
<hr />

<section class="row" id="enderecos">
	<h4 class="text-center">
		Endereço 
	</h4>
	
	<c:forEach items="${usuarioDto.enderecosList}" var="endereco" varStatus="enderecoStatus">
		<div id="endereco_${enderecoStatus.index}" class="col-xs-12 col-sm-12 col-md-12 margin-1percent padding-1percent borders-all endereco">
		
			<input name="enderecosList[${enderecoStatus.index}].id" value="${endereco.id}" type="hidden" />
			
			<div class="col-xs-12 col-sm-3 col-md-3 margin-top-2percent formulario-usuario-pf">
				<label>CEP:</label>
			    <input type="text" name="enderecosList[${enderecoStatus.index}].cep" value="${endereco.cep}" class="form-control cep-input" />
		  	</div>
		  	
		  	<div class="col-xs-12 col-sm-6 col-md-6 margin-top-2percent formulario-usuario-pf">
			    <label>Logradouro:</label>
			    <input name="enderecosList[${enderecoStatus.index}].logradouro" value="${endereco.logradouro}" type="text" class="form-control" />
		  	</div>
		  	
		  	<div class="col-xs-12 col-sm-3 col-md-3 margin-top-2percent formulario-usuario-pf">
			    <label>Numero:</label>
			    <input name="enderecosList[${enderecoStatus.index}].numero" value="${endereco.numero}" type="text" class="form-control" placeholder="Seu número" />
		  	</div>
		  	
		  	<div class="col-xs-12 col-sm-4 col-md-4 margin-top-2percent formulario-usuario-pf">
			    <label>Bairro:</label>
			    <input name="enderecosList[${enderecoStatus.index}].bairro" value="${endereco.bairro}" type="text" class="form-control" placeholder="Seu Bairro" />
		  	</div>
		  	
		  	<div class="col-xs-12 col-sm-4 col-md-4 margin-top-2percent formulario-usuario-pf">
			    <label>Complemento:</label>
			    <input name="enderecosList[${enderecoStatus.index}].complemento" value="${endereco.complemento}" type="text" class="form-control" placeholder="Complemento" />
		  	</div>
		  	
		  	<div class="col-xs-12 col-sm-4 col-md-4 margin-top-2percent formulario-usuario-pf">
			    <label>Estado:</label>
			    
			   	<select name="enderecosList[${enderecoStatus.index}].cidade.estado.nome" class="form-control">
			   		<c:forEach items="${estados}" var="estado">
			   			<c:choose>
							<c:when test="${endereco.cidade.estado.nome == estado}">
			        			<option value="${estado}" selected>${estado.descricao}</option>	
			    			</c:when>
			    			<c:otherwise>
			        			<option value="${estado}">${estado.descricao}</option>
			    			</c:otherwise>
			    		</c:choose>
			   	
			   			
			   		</c:forEach>
			   </select>
		  </div>
		  <div class="col-xs-12 col-sm-5 col-md-5 margin-top-2percent formulario-usuario-pf">
			    <label>Cidade:</label>
			    <input name="enderecosList[${enderecoStatus.index}].cidade.nome" value="${endereco.cidade.nome}" type="text" class="form-control" />
		  </div>
		
		  <div class="col-xs-12 col-sm-7 col-md-7 margin-top-2percent formulario-usuario-pf">
			    <label>Proximidade:</label>
			    <input type="text" name="enderecosList[${enderecoStatus.index}].proximidade" value="${endereco.proximidade}" class="form-control" />
		  </div>
		</div>
	</c:forEach>
</section>