<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form:form action="${pageContext.request.contextPath}/usuario/alteracao_cadastro_usuario_pf" method="post" modelAttribute="usuarioDto" id="cadastro_novo_usuario_pf">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="hidden" name="id" value="${usuarioDto.id}" />
				
				<input type="hidden" name="ativo" value="${usuarioDto.ativo}" />
				<input type="hidden" name="tipoUsuario" value="${usuarioDto.tipoUsuario}" />
				<input name="email" type="hidden" class="form-control" value="${usuarioDto.email}" id="email_pf" />
				
				<section class="row" id="dados_pessoais">
					<h4 class="text-center">Dados pessoais</h4>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">	
							
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margin-top-2percent formulario-usuario-pf">
					    <label>Nome:</label>
					    <input type="text" name="nome" value="${usuarioDto.nome}" class="form-control" />
					  </div>
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margin-top-2percent formulario-usuario-pf">
					    <label>Sobrenome:</label>
					    <input type="text" name="sobrenome" value="${usuarioDto.sobrenome}" class="form-control" />
					  </div>
					  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 margin-top-2percent formulario-usuario-pf">
					    <label>RG:</label>
					    <input type="text" name="rg" value="${usuarioDto.rg}" class="form-control" />
					  </div>
					  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-lg-3 margin-top-2percent formulario-usuario-pf">
					    <label>CPF:</label>
					    <input type="text" name="cpf" value="${usuarioDto.cpf}" class="form-control" readonly />
					  </div> 
					  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 margin-top-2percent formulario-usuario-pf">
					    <label>Data de nascimento:</label>
					    <input name="dataDeNascimentoString" value="${usuarioDto.dataDeNascimentoString}" type="text" class="datepicker form-control datepicker-portugues formulario-usuario-pf" />
					  </div>
	   
					  <div class="radio col-xs-12 col-sm-3 col-md-3 col-lg-3 margin-top-2percent formulario-usuario-pf">
						  <p class="text-left" style="font-weight:700;">
						    Sexo:
						  </p>
						  
						  	<c:choose>
								<c:when test="${usuarioDto.sexo == 'F'}">
				        			<label>
										<input type="radio" name="sexo" value="M">
									    Masculino
									</label>
									<label>
									    <input type="radio" name="sexo" value="F" checked="checked">
									    Feminino
									</label>
				    			</c:when>
				    			<c:otherwise>
				        			<label>
										<input type="radio" name="sexo" value="M" checked="checked">
									    Masculino
									</label>
									<label>
									    <input type="radio" name="sexo" value="F">
									    Feminino
									</label>
				    			</c:otherwise>
				    		</c:choose>
				    		
					  </div>
					</div>
				</section>
				
				<hr />
				
				
				<%@include file="formulario_endereco_telefone.jsp" %>
				
									
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
					<button id="enviar_cadastro_usuario_pf" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send"></span> Enviar cadastro pessoa física</button>	
				</div>
			</form:form>
		</div>
	</div>
</div>



<div id="usuario_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Endereço não encontrado</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">O endereço não foi encontrado pelo CEP</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
    </div>
  </div>
</div>	