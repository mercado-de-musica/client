<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:choose>
	<c:when test="${empty listaDeLikes}">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h4 class="text-center">Não existem produtos que foram dados like</h4>	
			</div>
		</div>	
	</c:when>
	<c:otherwise>
		<c:forEach items="${listaDeLikes}" var="likeComInfosGeraisProdutoUsuario">
			<div class="row" id="linha_${likeComInfosGeraisProdutoUsuario.likeProduto.id}">
				<div class="col-xs-6 col-sm-6 col-md-7 col-lg-6 col-lg-offset-1">
					<p>${likeComInfosGeraisProdutoUsuario.infosGeraisProdutoUsuario.infosGeraisProduto.instrumentoAcessorio.nome} ${likeComInfosGeraisProdutoUsuario.infosGeraisProdutoUsuario.infosGeraisProduto.marca.nome} ${likeComInfosGeraisProdutoUsuario.infosGeraisProdutoUsuario.infosGeraisProduto.produto.nome} ${likeComInfosGeraisProdutoUsuario.infosGeraisProdutoUsuario.infosGeraisProduto.modelo.nome}</p>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ver_produto" id="ver_produto_${likeComInfosGeraisProdutoUsuario.infosGeraisProdutoUsuario.id}">
						
						<button type="button" class="btn btn-default lista-de-like-icones-palavras" onclick="verproduto(${likeComInfosGeraisProdutoUsuario.infosGeraisProdutoUsuario.id})" >
						  <span class="glyphicon glyphicon-eye-open"></span> Ver like
						</button>
						<button type="button" class="btn btn-default lista-de-like-icones" onclick="verproduto(${likeComInfosGeraisProdutoUsuario.infosGeraisProdutoUsuario.id})" >
						  <span class="glyphicon glyphicon-eye-open"></span>
						</button>
					
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						
						<button type="button" class="btn btn-default lista-de-like-icones-palavras" onclick="excluirLike(${likeComInfosGeraisProdutoUsuario.likeProduto.id})">
						  <span class="glyphicon glyphicon-remove"></span> Excluir like
						</button>
						<button type="button" class="btn btn-default lista-de-like-icones" onclick="excluirLike(${likeComInfosGeraisProdutoUsuario.likeProduto.id})">
						  <span class="glyphicon glyphicon-remove"></span>
						</button>
					
					</div>
				</div>
			</div>
			
			<hr id="hr_${likeComInfosGeraisProdutoUsuario.likeProduto.id}" />
			
			
		</c:forEach>
	</c:otherwise>
</c:choose>



<div id="erro_generico" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro com nossos servidores. Por favor, entre em contato conosco
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="exclusao_like_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro na exclusão</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Não foi possível excluir o like. Por favor, entre em contato conosco
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="exclusao_like_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				O like foi excluído com sucesso!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>