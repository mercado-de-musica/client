<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="row">
	<div class="col-xs-12">
		<h4 class="text-center">Cadastro de interesses de compra</h4>
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	      <form:form action="${pageContext.request.contextPath}/usuario/interesses_de_compra_cadastrado" method="post" modelAttribute="interessesDeCompra" id="cadastro_interesses_de_compra" class="navbar-form" role="search">
	          <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
			    <label>Instrumento ou acessório</label>
		    	<select class="form-control tipo-de-produto" name="instrumentoAcessorio" id="instrumento_acessorio_escolha" style="width:100%">
		    		<option value="-1">Escolha aqui seu instrumento ou acessório</option>
		    		<c:forEach var="instrumentoAcessorioDto" items="${listaInstrumentoAcessorioDto}">
					    <option value="${instrumentoAcessorioDto.id}">${instrumentoAcessorioDto.nome}</option>
					</c:forEach>
		    	</select>
			  </div>
			  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
			    <label>Marca</label>
		    	<select class="form-control marca" name="marca" id="marca_escolha" style="width:100%">
		    		 <option value="-1">Escolha aqui a marca</option>
		    	</select>
			  </div>
			  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
			    <label>Produto</label>
		    	<select class="form-control produto" name="produto" id="produto_escolha" style="width:100%">
		    		<option value="-1">Escolha aqui o produto</option>
		    	</select>
			  </div>
			  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
			    <label>Modelo</label>
		    	<select class="form-control modelo" name="modelo" id="modelo_escolha" style="width:100%">
		    		<option value="-1">Escolha aqui o modelo</option> 
		    	</select>
			  </div>
			  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
			    <label>Qual a distância máxima caso o produto seja entrega em mãos?</label>
		    	<select class="form-control modelo" name="distancia" id="distancia_escolha" style="width:100%">
		    		<c:forEach items="${listaDistanciaInteresseDeCompraEnum}" var="distanciaInteresseDeCompraEnum">
		    			<option value="${distanciaInteresseDeCompraEnum}">${distanciaInteresseDeCompraEnum.descricao}</option>
		    		</c:forEach>
		    	</select>
			  </div>
			  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 margin-top-3percent">
			    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Enviar</button>
			  </div>
	      </form:form>
	    </div>
			
	</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
		<h4 class="text-center">Seus interesses de compra cadastrados</h4>
		<ul class="list-group">
			<c:forEach var="interessesDeCompraUsuarioDto" items="${listaInteressesDeCompraUsuarioDto}">
				<li class="list-group-item" id="li_${interessesDeCompraUsuarioDto.id}">
				  <button type="button" class="btn btn-default badge"onclick="excluirInteresseDeCompra(${interessesDeCompraUsuarioDto.id})"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Excluir</button>
				  ${interessesDeCompraUsuarioDto.instrumentoAcessorioDTO.nome} ${interessesDeCompraUsuarioDto.marcaDTO.nome} ${interessesDeCompraUsuarioDto.produtoDTO.nome} ${interessesDeCompraUsuarioDto.modeloDTO.nome} - ${interessesDeCompraUsuarioDto.distancia.descricao}
				</li>
			</c:forEach>
		</ul>	
	</div>
</div>




<div id="exclusao_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão de interesse de compra</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning">Você deseja realmente excluir este interesse de compra?</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="exclusao_sim" data-dismiss="modal">Sim</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
		</div>
    </div>
  </div>
</div>

<div id="exclusao_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">Não foi possível excluir o interesse de compra.</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		</div>
    </div>
  </div>
</div>


<div id="exclusao_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">O interesse de compra foi excluído com sucesso!</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		</div>
    </div>
  </div>
</div>
