<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h4 class="text-center">Últimos produtos comprados</h4>
			<c:choose>
				<c:when test="${listaProdutosCompradosDto != null && !empty listaProdutosCompradosDto}">
					<div class="table-responsive">
						<table class="table table-hover">
							<tr class="info">
								<th>Produto</th>
								<th>Data</th>
								<th>Valor</th>
							</tr>
							<c:forEach items="${listaProdutosCompradosDto}" var="produtosCompradoDto">
								<tr class="info">
									<td>${produtosCompradoDto.infosGeraisProduto.instrumentoAcessorio.nome} ${produtosCompradoDto.infosGeraisProduto.marca.nome} ${produtosCompradoDto.infosGeraisProduto.produto.nome} ${produtosCompradoDto.infosGeraisProduto.modelo.nome}</td>
									<td>
										<c:forEach items="${produtosCompradoDto.compra.datasDaCompra}" var="dataDaCompra">
											<c:if test="${dataDaCompra.dataAtualizada == true}">
												<fmt:formatDate value="${dataDaCompra.dataDaCompra.time}" pattern="dd/MM/yyyy" />
											</c:if>
										</c:forEach>
									</td>
									<td>R$ ${produtosCompradoDto.precoString}</td>
								</tr>
							</c:forEach>
						</table>
					</div>		
				</c:when>
				<c:otherwise>
					<p><b>Você não tem produtos comprados</b></p>
				</c:otherwise>
			</c:choose>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h4 class="text-center">Últimos produtos vendidos</h4>
			<c:choose>
				<c:when test="${listaProdutosVendidosDto != null && !empty listaProdutosVendidosDto}">
					<div class="table-responsive">
						<table class="table table-hover">
							<tr class="info">
								<th>Produto</th>
								<th>Data</th>
								<th>Valor</th>
							</tr>
							
							<c:forEach items="${listaProdutosVendidosDto}" var="produtosVendidoDto">
								<tr class="info">
									<td>${produtosVendidoDto.infosGeraisProduto.instrumentoAcessorio.nome} ${produtosVendidoDto.infosGeraisProduto.marca.nome} ${produtosVendidoDto.infosGeraisProduto.produto.nome} ${produtosVendidoDto.infosGeraisProduto.modelo.nome}</td>
									<td>
										<c:forEach items="${produtosVendidoDto.compra.datasDaCompra}" var="dataDaVenda">
											<c:if test="${dataDaVenda.dataAtualizada == true}">
												<fmt:formatDate value="${dataDaVenda.dataDaCompra.time}" pattern="dd/MM/yyyy" />
											</c:if>
										</c:forEach>
									</td>
									<td>R$ ${produtosVendidoDto.precoString}</td>
								</tr>
							</c:forEach>
						</table>
					</div>	
					
				</c:when>
				<c:otherwise>
					<p><b>Você não tem produtos vendidos</b></p>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>