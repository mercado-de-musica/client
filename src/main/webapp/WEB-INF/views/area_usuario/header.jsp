
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div id="gateway_de_pagamento_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sem conta em gateway de pagamento</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">Para poder cadastrar seus produtos, primeiro você deve abrir uma conta no gateway de pagamento (MOIP).</p>
		</div>
    	<div class="modal-footer">
			<a href="${pageContext.servletContext.contextPath}/usuario/cadastro_gateway_de_pagamento">Ok, mande-me para a página de abertura de conta</a>
		</div>
    </div>
  </div>
</div>


<div id="wrapper">
	<header class="container-fluid">
		<!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        Menu
                    </a>
                </li>
                <li><a href="${pageContext.servletContext.contextPath}/usuario/informacoes_pessoais">Informações pessoais</a></li>
                <li><a href="${pageContext.servletContext.contextPath}/usuario/conta_gateway_de_pagamento">Conta corrente</a></li>
                <li><a href="${pageContext.servletContext.contextPath}/usuario/interesses_de_compra">Interesses de compra</a></li>
		        <li><a href="${pageContext.servletContext.contextPath}/usuario/lista_de_likes">Lista de likes</a></li>
		        <li><a href="${pageContext.servletContext.contextPath}/usuario/produtos_venda">Meus produtos à venda</a></li>
		        
		        <li><a href="${pageContext.servletContext.contextPath}/usuario/perguntas_usuarios">Perguntas feitas por usuários</a></li>
		        <li><a href="${pageContext.servletContext.contextPath}/usuario/minhas_perguntas">Minhas perguntas</a></li>
		        <li><a href="${pageContext.servletContext.contextPath}/usuario/produtos_vendidos">Produtos vendidos</a></li>
		        <li><a href="${pageContext.servletContext.contextPath}/usuario/produtos_comprados">Produtos comprados</a></li>
		        <li><a href="${pageContext.servletContext.contextPath}/usuario/compras_nao_finalizadas">Compras não finalizadas</a></li>
		        
		        <security:authorize ifAnyGranted="ROLE_ADMIN">
		        	<li><a href="${pageContext.servletContext.contextPath}/administracao">Administracao</a></li>
		        </security:authorize>
            </ul>
        </div>	
		<div class="col-xs-12 col-sm-12 col-md-12 margin-top-2percent menu-usuario-header" id="menu-ola"><p class="text-right"><b>Olá ${nomeUsuario}</b></p></div>
		<div class="col-xs-12 col-sm-10 col-sm-offset-2 col-md-10 col-md-offset-2 col-lg-9 col-lg-offset-3 margin-top-2percent">
			<figure>
				<a href="${pageContext.servletContext.contextPath}/usuario">
					<img class="img-responsive" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/logo-mercadodemusica.jpg" alt="Mercado de Música - Área de usuário" />
				</a>
			</figure>	
		</div>
		
		<section class="col-xs-12 margin-top-2percent">	
			<div class="col-xs-4 col-sm-4 menu-usuario-header" id="menu-header"><p class="text-center text-header-mercado-de-musica"><a href="#menu-toggle" id="menu-toggle"><span class="glyphicons glyphicons-menu-hamburger" aria-hidden="true"></span> Menu</a></p></div>
			<div class="col-xs-4 col-sm-4 col-md-6 col-lg-6 menu-usuario-header" id="compras-header"><p class="text-center text-header-mercado-de-musica"><a href="${pageContext.servletContext.contextPath}/"><span class="glyphicons glyphicons-shopping-bag" aria-hidden="true"></span> Área de compras</a></p></div>
			<div class="col-xs-4 col-sm-4 col-md-6 col-lg-6 menu-usuario-header" id="logout-header"><p class="text-center text-header-mercado-de-musica"><a href="${pageContext.servletContext.contextPath}/logout"><span class="glyphicons glyphicons-exit" aria-hidden="true"></span> Logout</a></p></div>
		</section>
	</header>

	<div class="container-fluid">
		<h3 class="text-center">${h3}</h3>
	</div>
