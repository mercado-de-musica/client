<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
               
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="noindex" />
	
	
	<link rel="icon" type="image/png" href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/logo-mercadodemusica-favicon.png" />
	
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/css/simple-sidebar.css" rel="stylesheet">
	
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/css/bootstrap-datepicker.css" rel="stylesheet"/>
	<link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/css/uploader/uploadifive.css">
	<link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/guillotine-master/css/jquery.guillotine.css">
	
	<link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/glyphicons/css/glyphicons.css">
	
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/css/area-usuario-util.css" rel="stylesheet"/>
	
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/select2/dist/css/select2.css" rel="stylesheet" />
    <link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/css/hover_image/tooltipster.css">
    <link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/ckeditor/samples/css/samples.css">
	<link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/resources/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
    
	
	<title>Área do usuário ${nomeUsuario} - Mercado de Música</title>
  </head>
  <body>
  	
<%--   <img src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/ajax-loader.gif" id="loading-indicator" style="display:none" /> --%>
  
  
	<%@include file="../area_usuario/header.jsp" %> 
	
	
	
	
	<!-- conteudo dinamico -->
	<div class="container-fluid">
		<jsp:include page="../${pagina}.jsp" />
	</div>	
	<!-- conteudo dinamico -->	
	
		
	<%@include file="../area_usuario/footer.jsp" %>
	
	
	
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/jquery-2.0.0.min.js"></script>
	
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/select2/dist/js/i18n/pt-BR.js" type="text/javascript"></script>
    <script type="text/javascript">
    	
    	$('select').select2({
    		"language": "pt-BR"
    	});
    </script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/bootstrap-datepicker.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/bootstrap-datepicker-pt.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/jquery-validation-1.13.1/dist/jquery.validate.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/jquery-validation-1.13.1/dist/additional-methods.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/jquery-validation-1.13.1/dist/localization/messages_pt_BR.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/masked-input.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/jquery.maskMoney.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/md5/jquery.md5.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/guillotine-master/js/jquery.guillotine.min.js"></script>
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/bootstrap-waitingfor.min.js"></script>
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/hover_image/jquery.tooltipster.min.js"></script>
    
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/ckeditor/ckeditor.js"></script>
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/ckeditor/samples/js/sample.js"></script>
    
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario_main.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/util/enderecos.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/alteracao_usuario.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/produtos/abrir_tags_filhas.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/produtos/interesses_de_compra.js"></script>
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/uploader/jquery.uploadifive.js"></script>
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/produtos/validacao_cadastro_alteracao_produtos.js"></script>
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/perguntas_respostas/envio_de_resposta.js"></script>
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/likes.js"></script>
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/gateway_de_pagamento/gateway_de_pagamento.js"></script>
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/contas_bancarias/contas_bancarias_gateway_de_pagamento.js"></script>
  
  	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/produtos/produtos_comprados_vendidos.js"></script>
  	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/compras_nao_finalizadas/compras_nao_finalizadas.js"></script>
  	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/gateway_de_pagamento/contas_bancarias.js"></script>
  	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/js/usuario/gateway_de_pagamento/transferencia_de_valores.js"></script>
  	
  	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-80581859-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
  </body>
	
	
</html>