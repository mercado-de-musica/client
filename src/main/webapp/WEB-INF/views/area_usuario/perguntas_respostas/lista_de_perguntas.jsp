<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-10percent">			
			<c:choose>
				<c:when test="${listaDePerguntasRepostas != null && !empty listaDePerguntasRepostas}">
					<c:forEach var="perguntaResposta" items="${listaDePerguntasRepostas}">
						<div class="row caixa-row">
							<c:choose>
								<c:when test="${perguntaResposta.perguntaFeita.respondido == true}">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 caixa-atencao">
										<p>
											Perguntas sobre o produto: ${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.instrumentoAcessorio.nome} 
											${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.marca.nome} 
											${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.produto.nome} 
											${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.modelo.nome}
										</p>
									</div>
								</c:when>
								<c:otherwise>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 caixa-atencao">
										<p>
											Perguntas sobre o produto: ${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.instrumentoAcessorio.nome} 
											${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.marca.nome} 
											${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.produto.nome} 
											${perguntaResposta.perguntaFeita.infosGeraisProdutoUsuario.infosGeraisProduto.modelo.nome}
										</p>
									</div>
									<div class="col-xs-9 col-xs-offset-3 col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3 col-lg-9 col-lg-offset-3 caixa-atencao-alert">
										<c:choose>
											<c:when test="${perguntaResposta.respostas[fn:length(perguntaResposta.respostas) - 1].usuarioInteressado.id == usuarioDto.id}">
												<p>
													Esta pergunta não foi respondida pela outra parte
												</p>
											</c:when>
											<c:otherwise>
												<p>
													Esta pergunta não foi respondida por você
												</p>
											</c:otherwise>
										</c:choose>
									</div>	
								</c:otherwise>
							</c:choose>
						</div>
						<div class="row caixa-normal caixa-row"> 
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<p>${perguntaResposta.perguntaFeita.pergunta}</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<c:choose>
									<c:when test="${perguntaResposta.perguntaFeita.usuarioInteressado.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
										<c:choose>
											<c:when test="${perguntaResposta.perguntaFeita.usuarioInteressado.nomeFantasia != null}">
												<p>Em ${perguntaResposta.perguntaFeita.dataPerguntaString} por: <b>${perguntaResposta.perguntaFeita.usuarioInteressado.nomeFantasia}</b></p>
											</c:when>
											<c:otherwise>
												<p>Em ${perguntaResposta.perguntaFeita.dataPerguntaString} por: <b>${perguntaResposta.perguntaFeita.usuarioInteressado.razaoSocial}</b></p>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<p>Em ${perguntaResposta.perguntaFeita.dataPerguntaString} por: <b>${perguntaResposta.perguntaFeita.usuarioInteressado.nome} ${perguntaResposta.perguntaFeita.usuarioInteressado.sobrenome}</b></p>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						
						<c:forEach var="resposta" items="${perguntaResposta.respostas}">
		   					<div id="linha_resposta_${resposta.id}" class="row caixa-normal caixa-row"> 
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<p>${resposta.pergunta}</p>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<c:choose>
										<c:when test="${resposta.usuarioInteressado.class.simpleName == 'UsuarioPessoaJuridicaDTO'}">
											<c:choose>
												<c:when test="${resposta.usuarioInteressado.nomeFantasia != null}">
													<p>Em ${resposta.dataPerguntaString} por: <b>${resposta.usuarioInteressado.nomeFantasia}</b></p>
												</c:when>
												<c:otherwise>
													<p>Em ${resposta.dataPerguntaString} por: <b>${resposta.usuarioInteressado.razaoSocial}</b></p>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											<p>Em ${resposta.dataPerguntaString} por: <b>${resposta.usuarioInteressado.nome} ${resposta.usuarioInteressado.sobrenome}</b></p>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
		   				</c:forEach>
			    				
			    		<div class="row caixa-normal caixa-row">
			    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input class="input-control" name="idPergunta_${perguntaResposta.perguntaFeita.id}" type="hidden" value="${perguntaResposta.perguntaFeita.id}" />
								<div class="form-group">
			 				    	<label for="reposta_${perguntaResposta.perguntaFeita.id}">Resposta</label>
			 				    	<textarea id="reposta_${perguntaResposta.perguntaFeita.id}" class="btn btn-default form-control" name="reposta_${perguntaResposta.perguntaFeita.id}" rows="3"></textarea>
			 				  	</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkbox">
			 				  	<div class="form-group">
			 				    	<label>
			 				    		<input class="btn btn-default botaoRadio" name="encerraDiscussao_${perguntaResposta.perguntaFeita.id}" type="checkbox" />
			 				    		<span class="glyphicons glyphicons-user-ban"></span>
			 				    		<b>Encerra conversa</b>
			 				    	</label>
			 				  	</div>
			 				</div>
			 				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			 				  	<div class="form-group">
			 				    	<button class="btn btn-default enviarResposta" name="enviarResposta_${perguntaResposta.perguntaFeita.id}">
										<span class="glyphicon glyphicon-send"></span> Enviar resposta
									</button>
			 				  	</div>
			 				</div>	
			    		</div>		
						<hr />
						
					</c:forEach>
				</c:when>
				<c:otherwise>
					<h4 class="text-center">Você não tem pergunta recebida</h4>
				</c:otherwise>
			</c:choose>
				
		</div>
	</div>
</div>



<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ocorreu um erro com a página. Por favor, tente novamente</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro com a página. Por favor, tente novamente
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="ok_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success">
				Sua resposta foi enviada com sucesso. Aguarde a interação do outro usuário.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>	