<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@include file="menu_gateway_de_pagamento.jsp" %> 

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
			<h4 class="text-center">Suas contas bancárias</h4>
			<c:choose>
				<c:when test="${listaDeContasBancarias != null && !empty listaDeContasBancarias}">
					
					<div class="table-responsive">
						<table class="table table-hover">
							<tr class="info">
								<th>Nome da conta</th>
								<th>Agência</th>
								<th>Conta</th>
							</tr>
							
							<c:forEach items="${listaDeContasBancarias}" var="contaBancaria">
								<tr class="info">
									<td>${contaBancaria.nomeDaConta}</td>
									<td>${contaBancaria.numeroDoBanco.numero} ${contaBancaria.numeroDoBanco.descricao}</td>
									<td>${contaBancaria.numeroDaConta} - ${contaBancaria.digitoDaConta}</td>				        
			    				</tr>
							</c:forEach>
						</table>
					</div>		
					
				</c:when>
				<c:otherwise>
					<p class="text-center">Você não tem contas bancárias cadastradas, <a href="${pageContext.servletContext.contextPath}/usuario/cadastro_nova_conta_bancaria">cadastre aqui</a></p>
				</c:otherwise>
			</c:choose>
			
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent">
			
			<h4 class="text-center">Produtos liberados para pagamento</h4>		
			<c:choose>
				<c:when test="${listaDeProdutosLiberados != null && !empty listaDeProdutosLiberados}">
					<div class="table-responsive">
						<table class="table table-hover">
							<tr class="info">
								<th>Produto</th>
								<th>Preço bruto</th>
							</tr>
							<c:forEach var="produtoLiberado" items="${listaDeProdutosLiberados}">
								<tr class="info">
				        			<td>${produtoLiberado.infosGeraisProduto.instrumentoAcessorio.nome} ${produtoLiberado.infosGeraisProduto.marca.nome} ${produtoLiberado.infosGeraisProduto.produto.nome} ${produtoLiberado.infosGeraisProduto.modelo.nome}</td>
				    				<td>${produtoLiberado.precoString}</td>
			    				</tr>
							</c:forEach>
						</table>
					</div>
				</c:when>
				<c:otherwise>
					<p class="text-center">Você não tem produtos liberados para pagamento</p>
				</c:otherwise>	
			</c:choose>
		</div>
		
	</div>
</div>	