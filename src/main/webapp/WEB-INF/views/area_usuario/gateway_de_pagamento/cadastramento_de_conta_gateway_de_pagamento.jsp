<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="row">
	
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-7 col-lg-offset-3 margin-top-2percent">
		<h4>Se você já tem uma conta no Paypal, insrira escolha entre a inserção do seu e-mail ou a escolha do e-mail cadastrado no Mercado de Música</h4>
		<div class="row">
			<div class="col-lg-12 margin-top-2percent">
				
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-7 col-lg-offset-3 margin-top-2percent">
		<h4>Caso você não tenha uma conta no Paypal, clique abaixo para cadastrar uma conta com seus dados do sistema</h4>
		<div class="row">
			<div class="col-lg-12 margin-top-2percent">
				<div class="form-group">
					<input class="input-control" name="cadastro_de_conta_gateway_de_pagamento" type="button" value="Cadastre minha conta no Paypal">
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="row" id="aceite_moip" style="display: none"> -->
<!-- 	<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-7 col-lg-offset-3 margin-top-2percent"> -->
<!-- 		<h4>Aceite do MOIP para continuarmos seu cadastramento</h4> -->
<!-- 		<div class="row"> -->
<!-- 			<div class="col-lg-12 margin-top-2percent" id="aceite_moip_html"> -->
				
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </div> -->



<div id="cadastramento_de_conta_gateway_de_pagamento_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Sua conta no MOIP foi cadastrada com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">
				Sua conta foi cadastrada com sucesso. Caso você deseje cadastrar outras informações como números de cartões de crédito, entre em 
				<a href="${pageContext.servletContext.contextPath}/usuario/conta_gateway_de_pagamento">minha conta corrente</a> pelo menu do usuario.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>

<div id="cadastramento_de_conta_gateway_de_pagamento_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Ocorreu um erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">
				Ocorreu um erro no cadastramento de sua conta, por favor entre em contato conosco para a resolução do problema. 
			</p>
			
			<hr />
			<p class="text-center" id="erro_ocorrido">
			</p>
			
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>