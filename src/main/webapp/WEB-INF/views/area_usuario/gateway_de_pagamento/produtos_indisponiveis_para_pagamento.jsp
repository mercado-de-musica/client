<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<%@include file="menu_gateway_de_pagamento.jsp" %> 


<div class="container-fluid">
	<c:choose>
		<c:when test="${listaDeProdutosIndisponiveis != null && !empty listaDeProdutosIndisponiveis}">
			
			<div class="row">
				<h4 class="text-center">Lista de produtos indisponíveis para pagamento</h4>
			</div>
			
			<div class="row">
				<div class="table-responsive">
					<table class="table table-hover">
						<tr class="info">
							<th>Produto</th>
							<th>Porcentagem Utilizada</th>
							<th>Valor bruto</th>
							<th>Valor líquido</th>
							<th>Dias faltantes para a liberação do pagamento</th>
						</tr>
						<c:forEach var="produtosParaVenda" items="${listaDeProdutosIndisponiveis}">
							<tr id="linha_${produtosParaVenda.id}" class="info">
			        			<td>${produtosParaVenda.infosGeraisProduto.instrumentoAcessorio.nome} ${produtosParaVenda.infosGeraisProduto.marca.nome} ${produtosParaVenda.infosGeraisProduto.produto.nome} ${produtosParaVenda.infosGeraisProduto.modelo.nome}</td>
			    				<td>${produtosParaVenda.porcentagem}</td>
			    				<td>${produtosParaVenda.precoString}</td>
			    				<td>${produtosParaVenda.precoLiquidoString}</td>
			    				<td>${produtosParaVenda.diasParaLiberacaoDePagamento}</td>
		    				</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			
			
			
		</c:when>
		<c:otherwise>
			<div class="row">
				<h4 class="text-center">Não existem produtos nesta lista</h4>
			</div>
		</c:otherwise>
	</c:choose>
	
</div>






<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ocorreu um erro com a página. Por favor, tente novamente</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro com a página. Por favor, tente novamente
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>