<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<%@include file="menu_gateway_de_pagamento.jsp" %> 

<div class="row">
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 margin-top-2percent">
		<c:choose>
			<c:when test="${empty listaDeContas}">
				<h4 class="text-center">Você não tem conta bancária cadastrada</h4>
			</c:when>
			<c:otherwise>
				<div class="table-responsive">
					<table class="table table-hover">
						<tbody>
							<tr class="info">
								<th>Nome da conta</th>
								<th>Número do banco</th>
								<th>Agencia</th>
								<th>Conta</th>
								<th>Dígito</th>
								<th>Editar</th>
								<th>Excluir</th>
							</tr>
							<c:forEach items="${listaDeContas}" var="contaBancaria">
								<tr class="info">
									<td>${contaBancaria.nomeDaConta}</td>
									<td>${contaBancaria.numeroDoBanco.numero} - ${contaBancaria.numeroDoBanco.descricao}</td>
									<td>${contaBancaria.numeroDaAgencia}</td>
									<td>${contaBancaria.numeroDaConta}</td>
									<td>${contaBancaria.digitoDaConta}</td>
									<td><button type="button" class="btn btn-default btn-default" onclick="editarConta(${contaBancaria.id})"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>
									<td><button type="button" class="btn btn-default btn-default" onclick="modalExclusaoConta(${contaBancaria.id})"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
								</tr>
							</c:forEach>	
						</tbody>
					</table>
				</div>		
			</c:otherwise>
		</c:choose>
	</div>
</div>

<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ocorreu um erro com a página. Por favor, tente novamente</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro com a página. Por favor, tente novamente
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="certeza_exclusao" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão de conta</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Você tem certeza que deseja excluir essa conta bancária?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirmacao_exclusao" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">cancelar</button>
		</div>
    </div>
  </div>
</div>

<div id="exclusao_ok_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-success">
				Conta excluída com sucesso!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>


<div id="update_conta_bancaria" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<form:form action="${pageContext.request.contextPath}/usuario/cadastrar_nova_conta_bancaria" method="post" modelAttribute="contaBancariaGatewayDePagamentoDto" id="cadastrar_nova_conta_bancaria">
	    	<input type="hidden" name="id" class="form-control" />
	    	
	    	<div class="modal-header">
				<h3 class="text-center text-mercado-de-musica">Atualização de conta bancária</h3>
			</div>
	    	<div class="modal-body">
				<div class="form-group">
					<label>Nome da conta (opcional)</label>
	 				<input type="text" name="nomeDaConta" class="form-control" />
				</div>
				
				<div class="form-group">
					<label>Banco</label>
				    <select id="numero_do_banco_id" class="form-control produto" name="numeroDoBanco" style="width:100%">
				    	<c:forEach items="${listaDeBancos}" var="banco">
				    		<option value="${banco}">${banco.numero} - ${banco.descricao}</option>
				    	</c:forEach>
				    </select>
				</div>
				
				<div class="form-group">
					<label>Agência</label>
	 				<input type="text" name="numeroDaAgencia" class="form-control" />
				</div>
				
				<div class="form-group">
					<label>Dígito da agência</label>
	 				<input type="text" name="digitoDaAgencia" class="form-control" />
				</div>
				
				<div class="form-group">
					<label>Número da conta</label>
	 				<input type="text" name="numeroDaConta" class="form-control" />
				</div>
				
				<div class="form-group">
					<label>Dígito da conta</label>
	 				<input type="text" name="digitoDaConta" class="form-control" />
				</div>
			</div>
	    	<div class="modal-footer">
				<div class="form-group">
				    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</button>
				    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Enviar</button>
				</div>
			</div>
		</form:form>	
    </div>
  </div>
</div>