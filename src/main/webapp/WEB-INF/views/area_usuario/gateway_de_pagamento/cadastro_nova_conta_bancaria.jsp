<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<%@include file="menu_gateway_de_pagamento.jsp" %> 

<div class="row">
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 margin-top-2percent">
		<form:form action="${pageContext.request.contextPath}/usuario/cadastrar_nova_conta_bancaria" method="post" modelAttribute="contaBancariaGatewayDePagamentoDto" id="cadastrar_nova_conta_bancaria">
			<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<label>Nome da conta (opcional)</label>
 				<input type="text" name="nomeDaConta" class="form-control" />
			</div>
			
			<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<label>Banco</label>
			    <select class="form-control produto" name="numeroDoBanco">
			    	<c:forEach items="${listaDeBancos}" var="banco">
			    		<option value="${banco}">${banco.numero} - ${banco.descricao}</option>
			    	</c:forEach>
			    </select>
			</div>
			
			<div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<label>Agência</label>
 				<input type="text" name="numeroDaAgencia" class="form-control" />
			</div>
			
			<div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<label>Dígito da agência</label>
 				<input type="text" name="digitoDaAgencia" class="form-control" />
			</div>
			
			<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<label>Número da conta</label>
 				<input type="text" name="numeroDaConta" class="form-control" />
			</div>
			
			<div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<label>Dígito da conta</label>
 				<input type="text" name="digitoDaConta" class="form-control" />
			</div>
			
			<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    <button type="submit" class="btn btn-default">
			    <span class="glyphicon glyphicon-send"></span> Enviar conta</button>
			</div>	
		</form:form>	
	</div>
	
</div>