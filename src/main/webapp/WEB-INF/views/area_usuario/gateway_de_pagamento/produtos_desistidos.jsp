<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@include file="menu_gateway_de_pagamento.jsp" %> 

<div class="container-fluid">
	<c:choose>
		<c:when test="${listaDeDesistencias != null && !empty listaDeDesistencias}">
			<div class="row">
				<h4 class="text-center">Lista de produtos liberados para pagamento</h4>
			</div>
			
			<div class="row">
				<div class="table-responsive">
					<table class="table table-hover">
						<tr class="info">
							<th>Produto</th>
							<th>Porcentagem Mercado de Música</th>
							<th>Preço bruto</th>
							<th>Incluir na transferência</th>
							<th>Excluir da transferência</th>
						</tr>
						<c:forEach var="desistencia" items="${listaDeDesistencias}">
							<tr id="linha_${desistencia.id}" class="info">
			        			<td>${desistencia.rastreamento.infosGeraisProdutoUsuario.infosGeraisProduto.instrumentoAcessorio.nome} ${desistencia.rastreamento.infosGeraisProdutoUsuario.infosGeraisProduto.marca.nome} ${desistencia.rastreamento.infosGeraisProdutoUsuario.infosGeraisProduto.produto.nome} ${desistencia.rastreamento.infosGeraisProdutoUsuario.infosGeraisProduto.modelo.nome}</td>
			    				<td>0.00 %</td>
			    				<td id="preco_bruto">${desistencia.rastreamento.infosGeraisProdutoUsuario.precoString}</td>
								<c:choose>
									<c:when test="${desistencia.rastreamento.infosGeraisProdutoUsuario.arquivoDeTransferencia}">
										<td>&nbsp;</td>
										<td><button type="button" class="btn btn-default" onclick="excluirCarrinhoDeTransferencia(${desistencia.rastreamento.infosGeraisProdutoUsuario.id}, false)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>		
									</c:when>
									<c:otherwise>
										<td><button type="button" class="btn btn-default" onclick="incluirCarrinhoDeTransferencia(${desistencia.rastreamento.infosGeraisProdutoUsuario.id}, false)"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button></td>
										<td>&nbsp;</td>
									</c:otherwise>
								</c:choose>
		    				</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			
			<c:if test="${transferenciAtiva != null && transferenciAtiva == true}">
				<div  class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent margin-bottom-2percent">
						<p><button class="btn btn-default" type="button" id="fechar_transferencia">
							<span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> Fechar transferência
						</button>
						</p>
					</div>	
				</div>
			</c:if>
		</c:when>
		<c:otherwise>
			<div class="row">
				<h4 class="text-center">Não existem produtos nesta lista</h4>
			</div>
		</c:otherwise>
	</c:choose>
	
</div>





<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ocorreu um erro com a página. Por favor, tente novamente</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro com a página. Por favor, tente novamente
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>



<div id="erro_nao_existe_conta" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sem conta para recebimento</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Não encontramos nenhuma conta cadastrada para recebimento dos valores, por favor vá em 
				<a href="${pageContext.servletContext.contextPath}/usuario/cadastro_nova_conta_bancaria">cadastro de conta bancária</a>
				e cadastre uma conta.
			</p>
		</div>
    	<div class="modal-footer">
		</div>
    </div>
  </div>
</div>


<div id="modal_escolha_de_conta" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Escolha sua conta para recebimento</h3>
		</div>
    	<div class="modal-body">
			<div class="table-responsive">
				<table class="table table-hover">
					<tbody id="inserir_informacoes_de_conta">
						<tr class="info" id="cabecalho">
							<th>Nome da conta</th>
							<th>Número do banco</th>
							<th>Agencia</th>
							<th>Conta</th>
							<th>Dígito</th>
							<th>&nbsp;</th>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
    	<div class="modal-footer">
		</div>
    </div>
  </div>
</div>


<div id="inclusao_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Transferência incluída com sucesso</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Sua transferência foi inserida com sucesso. Para finalizar vá em finalizar transferência
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="exclusao_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão com sucesso</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Seu produto foi retirado da transferência!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="fechar_transferencia_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Fechar transferência</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning">
				Você deseja fechar a transferência?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
			<button type="button" class="btn btn-default" onclick="fecharTransferencia(true)" data-dismiss="modal">Sim</button>
		</div>
    </div>
  </div>
</div>


<div id="fechamento_de_transferencia_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Transferência concluída</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				A transferência foi realizada. Dentro de alguns dias ela estará disponível na sua conta corrente.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>