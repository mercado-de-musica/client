<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<nav class="navbar navbar-default">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>
	
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav nav-tabs">
			  <li role="presentation" onclick="inserirActive(this)" class="menu_gateway_de_pagamento"><a href="${pageContext.servletContext.contextPath}/usuario/produtos_liberados_para_pagamento">Produtos já liberados para pagamento</a></li>
			  <li role="presentation" onclick="inserirActive(this)" class="menu_gateway_de_pagamento"><a href="${pageContext.servletContext.contextPath}/usuario/produtos_indisponiveis_para_pagamento">Produtos indisponíveis para pagamento</a></li>
			  <li role="presentation" onclick="inserirActive(this)" class="menu_gateway_de_pagamento"><a href="${pageContext.servletContext.contextPath}/usuario/produtos_pagos">Produtos pagos</a></li>
			  <li role="presentation" onclick="inserirActive(this)" class="menu_gateway_de_pagamento"><a href="${pageContext.servletContext.contextPath}/usuario/conta_bancaria">Conta Bancárias</a></li>
			  <li role="presentation" onclick="inserirActive(this)" class="menu_gateway_de_pagamento"><a href="${pageContext.servletContext.contextPath}/usuario/cadastro_nova_conta_bancaria">Cadastrar nova conta bancária</a></li>	
			  <li role="presentation" onclick="inserirActive(this)" class="menu_gateway_de_pagamento"><a href="${pageContext.servletContext.contextPath}/usuario/produtos_desistidos">Produtos desistidos com transferencia disponível</a></li>	
			</ul>
	    </div><!-- /.navbar-collapse -->
	</nav>
</div>
