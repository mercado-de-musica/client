<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@include file="menu_gateway_de_pagamento.jsp" %> 


<div class="container-fluid">
	
	<c:choose>
		<c:when test="${hashProdutosPagosDto != null && hashProdutosPagosDto.lista != null && !empty hashProdutosPagosDto.lista}">
			<div class="row">
				<h4 class="text-center">Lista de produtos pagos</h4>
			</div>
			
			<div class="row">
				<div class="table-responsive">
					<table class="table table-hover">
						<tr class="info">
							<th>Produto</th>
							<th>Preço bruto</th>
							<th>Preço líquido</th>
							<th>Porcentagem</th>
							<th>Situação depósito</th>
							<th>Data</th>
							<th>Tipo de transferência</th>
						</tr>
						
						<c:forEach var="transferenciaDto" items="${hashProdutosPagosDto.lista}">
							<c:forEach var="infosGeraisProdutoUsuarioDto" items="${transferenciaDto.listaInfosGeraisProdutoUsuario}">
								
								<tr id="linha_${transferenciaDto.id}" class="info">
				        			<td>${infosGeraisProdutoUsuarioDto.infosGeraisProduto.instrumentoAcessorio.nome} ${infosGeraisProdutoUsuarioDto.infosGeraisProduto.marca.nome} ${infosGeraisProdutoUsuarioDto.infosGeraisProduto.produto.nome} ${infosGeraisProdutoUsuarioDto.infosGeraisProduto.modelo.nome}</td>
				    				<td>${infosGeraisProdutoUsuarioDto.precoString}</td>
				    				<td>${infosGeraisProdutoUsuarioDto.precoLiquidoString}</td>
				    				<td>${transferenciaDto.porcentagem}</td>
				    				<c:forEach items="${transferenciaDto.listaStatusTransferenciaDeValor}" var="statusTransferenciaValor">
				    					<c:if test="${statusTransferenciaValor.ativo}">
				    						<td>${statusTransferenciaValor.statusTransferenciaDeValoresGatewayDePagamento.descricao}</td>
				    						<td><fmt:formatDate value="${statusTransferenciaValor.dataTransferencia.time}" pattern="dd/MM/yyyy"/></td>
				    					</c:if>
				    				</c:forEach>
				    				<c:choose>
				    					<c:when test="${transferenciaDto.desistencia == true}">
				    						<td>Desistência de produto</td>
				    					</c:when>
				    					<c:otherwise>
				    						<td>Normal</td>
				    					</c:otherwise>
				    				</c:choose>
			    				</tr>
			    			</c:forEach>	
						</c:forEach>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					Total pago: ${hashProdutosPagosDto.preco}
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="row">
				<h4 class="text-center">Não existem produtos nesta lista</h4>
			</div>
		</c:otherwise>
	</c:choose>
</div>



<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ocorreu um erro com a página. Por favor, tente novamente</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Ocorreu um erro com a página. Por favor, tente novamente
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="certeza_exclusao" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão de produto!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Você tem certeza que deseja excluir esse produto?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirmacao_exclusao" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">cancelar</button>
		</div>
    </div>
  </div>
</div>