<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="row margin-top-3percent">
	<h4 class="text-center"><span class="glyphicon glyphicon-exclamation-sign"></span> É necessário ter pelo menos uma foto para que seu produto apareça em nossas pesquisas e o máximo de uploads é de 5 fotos por vez.</h4>
</div>


<div class="container-fluid">
	<div class="row margin-top-10percent">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="form-group">
				<input id="csrf" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input id="infosGeraisProdutoUsuarioDto" type="hidden" name="infosGeraisProdutoUsuarioDto" value="${infosGeraisProdutoUsuarioDto.id}" />
				<label>Envio de fotos e/ou áudio</label>
			    <input id="file_upload" type="file" name="files" class="form-control" />
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<label>Envio de vídeos</label>
			<div class="input-group">
		      <input type="text" class="form-control" name="link_de_video_upload" placeholder="link do youtube">
		      <span class="input-group-btn">
		        <button class="btn btn-default" onclick="subidaDeVideoLink()" type="button"><span class="glyphicon glyphicon-facetime-video"></span> Enviar vídeo</button>
		      </span>
		    </div>
		</div>
	</div>
	
	<div class="row margin-top-10percent">
		<h4 class="text-center">Lista de fotos/vídeos/áudos cadatrados</h4>
	</div>
	
	<div class="row margin-top-10percent fotos">	
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fotos_filhos">
			<c:forEach var="apresentacao" items="${listaDeApresentacao}">
				<div class="row">
					<div class="col-xs-9 col-sm-7 col-md-7 col-lg-5">
						<c:if test="${apresentacao.tipoApresentacao == 'FOTO'}">
							<img class="img-responsive img-thumbnail" src="${apresentacao.endereco}${apresentacao.nomeDoArquivo}" style="width: 400px; height: auto;" />
						</c:if>
						<c:if test="${apresentacao.tipoApresentacao == 'VIDEO'}">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="img-responsive img-thumbnail" width="${apresentacao.width * 100 / 17}" height="${apresentacao.height * 300 / 17}" src="${apresentacao.endereco}" frameborder="0" allowfullscreen></iframe>
							</div>	
						</c:if>
						<c:if test="${apresentacao.tipoApresentacao == 'AUDIO'}">
							<audio controls>
							  <source src="${apresentacao.endereco}${apresentacao.nomeDoArquivo}" type="audio/${apresentacao.tipoArquivo}">
							</audio>
						</c:if>
					</div>
					<div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
						<button type="button" class="btn btn-default btn-default" onclick="modalExclusaoFotos(${apresentacao.id})"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>		
					</div>
					<c:if test="${apresentacao.tipoApresentacao == 'FOTO' && apresentacao.capa == true}">
						<div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 radio margin-celular-capa-foto">
							<label><input type="radio" name="capa" value="${apresentacao.id}" checked>Capa</label>
						</div>
					</c:if>
					<c:if test="${apresentacao.tipoApresentacao == 'FOTO' && apresentacao.capa == false}">
						<div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 radio margin-celular-capa-foto">
							<label><input type="radio" name="capa" value="${apresentacao.id}">Capa</label>
						</div>
					</c:if>
				</div>
				
				<hr />		
			</c:forEach>
			
		</div>
	</div>
	
	<div class="row">	
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">			
			<div class="input-group">
		    	<button class="btn btn-default" onclick="retornarDadosDoProdutoCadastro(${infosGeraisProdutoUsuarioDto.id})" type="button"><span class="glyphicon glyphicon-chevron-left"></span> Voltar para produto</button>
		    </div>		
		</div>
	</div>	
	
</div>




<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Ocorreu um erro com a subida das fotos e/ou audios. Por favor, tente novamente
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="exclusao_apresentacao_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Não foi possível excluir a foto/video/áudio do produto.
			</p>
			
			<p class="text-center" id="texto_de_erro">
				
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="fotos_modal" class="modal fade bs-example-modal-lg" tabindex="-1" data-keyboard="false" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ajuste de fotos</h3>
		</div>
    	<div class="modal-body">
			<div class="container-fluid">
          		<div id="row_excluir" class="row" style="display:none;">
          			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0 auto;">
						<img class="fotografia"  />
					</div>	
					<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 controles'>
				    	<button class='zoom_out'     onclick="zoomOut(this)" name="zoom_out" type='button' title='Zoom out'> - </button>
				    	<button class='fit'          onclick="fit(this)" name="fit" type='button' title='Fit image'> [ ]  </button>
				    	<button class='zoom_in'      onclick="zoomIn(this)" name="zoom_in" type='button' title='Zoom in'> + </button>
				    </div>
          		</div>
          	</div>	
			
		</div>
    	<div class="modal-footer">
			<button type="button" id="subida_redimensionamento" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="certeza_exclusao" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão de apresentação do produto!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Você tem certeza que deseja excluir esta foto/video/áudio do produto?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirmacao_exclusao_apresentacao" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		</div>
    </div>
  </div>
</div>


<div id="apresentacao_excluida" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Foto/video/áudio do produto excluído com sucesso!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="paginaDeFotos(${infosGeraisProdutoUsuarioDto.id})" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>