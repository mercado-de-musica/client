<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="container-fluid">
	<div class="row">
		<h4 class="text-center">Lista de produtos para venda</h4>
	</div>
	
	<div class="row">
		<div class="col-xs-12 aviso-mover-tabela margin-top-10percent">
			<p><span class="glyphicons glyphicons-hand-right"></span> Arraste para mover a tabela</p>
		</div> 
		
		<div class="table-responsive">
			<table class="table table-hover">
				<tr class="info">
					<th>Produto</th>
					<th>&nbsp;</th>
					<th>Preço</th>
					<th>Editar</th>
					<th>Excluir</th>
				</tr>
				<c:forEach var="produtosParaVenda" items="${listaDeProdutosParaVenda}">
					<c:choose>
						<c:when test="${produtosParaVenda.perguntaFeita}">
							<tr id="linha_${produtosParaVenda.id}" class="danger">
								<td><a href="${pageContext.servletContext.contextPath}/usuario/perguntas_usuarios" class="anchor-escuro">${produtosParaVenda.infosGeraisProduto.instrumentoAcessorio.nome} ${produtosParaVenda.infosGeraisProduto.marca.nome} ${produtosParaVenda.infosGeraisProduto.produto.nome} ${produtosParaVenda.infosGeraisProduto.modelo.nome}</a></td>				        
			    				<td><a href="${pageContext.servletContext.contextPath}/usuario/perguntas_usuarios"><b>Existem perguntas para este produto</b></a></td>
			    				<td id="preco_para_venda">R$ ${produtosParaVenda.precoString}</td>
								<td><button type="button" class="btn btn-default btn-default" onclick="editarProduto(${produtosParaVenda.id})"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>
								<td><button type="button" class="btn btn-default btn-default" onclick="modalExclusao(${produtosParaVenda.id})"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
		    				</tr>
		    			</c:when>
		    			<c:otherwise>
		        			<tr id="linha_${produtosParaVenda.id}" class="info">
			        			<td>${produtosParaVenda.infosGeraisProduto.instrumentoAcessorio.nome} ${produtosParaVenda.infosGeraisProduto.marca.nome} ${produtosParaVenda.infosGeraisProduto.produto.nome} ${produtosParaVenda.infosGeraisProduto.modelo.nome}</td>
			    				<td></td>
			    				<td id="preco_para_venda">R$ ${produtosParaVenda.precoString}</td>
								<td><button type="button" class="btn btn-default btn-default" onclick="editarProduto(${produtosParaVenda.id})"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>
								<td><button type="button" class="btn btn-default btn-default" onclick="modalExclusao(${produtosParaVenda.id})"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
		    				</tr>
		    			</c:otherwise>
		    		</c:choose>	
					
				</c:forEach>
			</table>
		</div>
	</div>
	
	<div id="titulo-de-cadastro" class="row margin-top-10percent">
		<h4 class="text-center" id="cadastro_edicao">Cadastro de produtos para venda (campos com * são obrigatórios)</h4>
		
		<p class="text-center margin-top-3percent">
			<a href="#" onclick="$('#categorias_novas').modal('show');" data-toggle="tooltip" data-placement="top" title="Não encontrou o produto que você quer cadastrar? Clique aqui"><span class="glyphicon glyphicon-question-sign"></span>
				Não encontrou o produto que você quer cadastrar? Clique aqui
			</a>
		</p>
		
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<form:form action="${pageContext.request.contextPath}/usuario/cadastro_de_fotos" method="post" modelAttribute="infosGeraisProdutoUsuarioDto" id="cadastro_alteracao_produtos_venda">
				<input id="csrf" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				
				<input type="hidden" name="id" value="null" />
				<input type="hidden" name="dataInsercaoD" value="null" />
				<input type="hidden" name="termosDePesoAltura" value="false" />
				<input type="hidden" name="termosDeEnvio" value="false" />
				
 				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
 				    <label>*Instrumento ou acessório</label>
 			    	<select class="form-control tipo-de-produto" name="infosGeraisProduto.instrumentoAcessorio.id" id="instrumento_acessorio_escolha">
 			    		<option value="0">Escolha aqui seu instrumento ou acessório</option>
 			    		<c:forEach var="instrumentoAcessorioDto" items="${listaInstrumentoAcessorioDto}">
 						    <option value="${instrumentoAcessorioDto.id}">${instrumentoAcessorioDto.nome}</option>
 						</c:forEach>
 			    	</select>
 				</div>
 				  <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
 				    <label>*Marca</label>
 			    	<select class="form-control marca" name="infosGeraisProduto.marca.id" id="marca_escolha">
 			    		 <option value="0">Escolha aqui a marca</option>
 			    	</select>
 				  </div>
 				  <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
 				    <label>*Produto</label>
 			    	<select class="form-control produto" name="infosGeraisProduto.produto.id" id="produto_escolha">
 			    		<option value="0">Escolha aqui o produto</option>
 			    	</select>
 				  </div>
 				  <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
 				    <label>*Modelo</label>
 			    	<select class="form-control modelo" name="infosGeraisProduto.modelo.id" id="modelo_escolha">
 			    		<option value="0">Escolha aqui o modelo</option> 
 			    	</select>
 				  </div>
 				  
 				  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
 				    <label>Descrição</label>
 				    <textarea class="form-control" name="descricao" rows="3"></textarea>
 				  </div>
				  	
				  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
 				  	<p>
						<a href="#" onclick="$('#medir_produto').modal('show');"><span class="glyphicon glyphicon-question-sign"></span>
							Não sabe como medir seu produto, clique aqui.
						</a>
					</p>		
 				  </div>	
				  	
 				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 				    <label>*Altura (em cm)</label>
 				    <input type="text" name="alturaString" class="form-control" />
 				  </div>
				  
 				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 				    <label>*Largura (em cm)</label>
 				    <input type="text" name="larguraString" class="form-control" />
 				  </div>
 				  
 				  
 				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 				    <label>*Comprimento (em cm)</label>
 				    <input type="text" name="comprimentoString" class="form-control" />
 				  </div>
				  
 				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 				    <label>*Peso (em kg)</label>
 				    <input type="text" name="pesoString" class="form-control" />
 				  </div>
 				  
				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 				    <label>*Preço</label>
 				    <input type="text" name="precoString" class="form-control" />
 				  </div>
 				  
 				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 				    <label>Ano de fabricação</label>
 				    <input type="text" name="anoDeFabricacao" class="form-control" />
 				  </div>
				  
 				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 				    <label>Número de série</label>
 				    <input type="text" name="numeroDeSerie" class="form-control" />
 				  </div>
 				  
 				  
				  <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
				  	<label>País de fabricação</label>
						<select class="form-control tipo-de-produto" name="paisDeFabricacao">
				    		<option value="">Escolha o país de fabricação</option>
				    		<c:forEach var="paisDeFabricacao" items="${paises}">
							    <option value="${paisDeFabricacao}">${paisDeFabricacao.descricao}</option>
							</c:forEach>
				    	</select>	
				  </div>
				  
 				<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
 					<label>*Tipo de envio</label>
 				    <select class="form-control" name="tipoEnvio" id="tipo_de_envio">
 			    		<c:forEach var="envioEnum" items="${listaDeEnvioEnum}">
 							<option value="${envioEnum}" id="${envioEnum}">${envioEnum.descricao}</option>
 						</c:forEach>
 			    	</select>
 				</div>
				  
			  	<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span> Avançar</button>
				</div>
			</form:form>
		</div>
	</div>
</div>



<div id="medir_produto" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Como medir o seu produto</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-muted">
				Para medir o seu produto siga a imagem abaixo. Pedimos que meça ele já na caixa para que caso ele seja enviado pelos Correios, 
				o Mercado de Música envie corretamente as informações. 
			</p>
			<p class="text-center" >
				<img class="img-responsive" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/caixa_medicao.png" alt="Exemplo de medição" />
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="categorias_novas" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ajude-nos com os novos produtos</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-muted">
				Ajude-nos a aumentar nossas categorias para cadastramento de produtos e procura. Escreva abaixo qual sugestão de produto você quer cadastrar, 
				nós colocaremos ele no ar e enviaremos um e-mail para você dentro de 4 horas para você continuar seu cadastro.
			</p>
			<p class="text-center" >
				<input type="text" name="nova-categoria-input" id="nova-categoria-input" class="form-control" placeholder="escreva aqui seu produto" />
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="enviarNovaCategoriaInput()" data-dismiss="modal">OK</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		</div>
    </div>
  </div>
</div>

<div id="novo-produto-envio-ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sugestão enviada!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Muito obrigado por enviar sua sugestão. O Mercado de Música agradece por nos ajudar a melhorar nosso cadastro de produtos.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
		</div>
    </div>
  </div>
</div>


<div id="erro_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Ocorreu um erro com a página. Por favor, tente novamente</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Ocorreu um erro com a página. Por favor, tente novamente
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="erro_selects_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger">
				Deve-se escolher se o produto é um instrumento ou acessório, sua marca, produto e modelo antes de enviar os dados;
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="validacao_tamanhos_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sobre peso e tamanho</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-muted">
				É necessário que o tamanho e o peso do produto seja colocado corretamente. 
				Caso não seja, poderá ocorrer diferenças nos valores pagos pelo comprador em relaçào ao frete.
			</p>
			
			<p class="text-center text-success">
				Ao clicar em "Li e estou ciente da possível diferença de frete", a informação sera enviada ao Mercado de Música afim de evitar possíveis conflitos futuros
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="estou_ciente" data-dismiss="modal">Li e estou ciente da possível diferença de frete</button>
		</div>
    </div>
  </div>
</div>

<div id="retiradaem_maos_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Alerta sobre entrega de produtos!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning">
				Os correios tem uma política de não transportar produtos de tamanho acima de 105 cm ou acima de 30 kg. 
				Para vender este produto você deverá negociar a entrega diretamente com o comprador.
			</p>
			
			<p class="text-left text-muted">
				Mas não se preocupe, nós trabalhamos para encontrar um comprador próximo ao seu endereço.
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Li e estou ciente do modo de entrega em mãos pelo fato do produto estar acima do permitido para envio pelos Correios.</button>
		</div>
    </div>
  </div>
</div>

<div id="certeza_exclusao" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão de produto!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-warning">
				Você tem certeza que deseja excluir esse produto?
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" id="confirmacao_exclusao" data-dismiss="modal">Ok</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">cancelar</button>
		</div>
    </div>
  </div>
</div>

<div id="exclusao_ok_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Exclusão com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Produto excluído com sucesso!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" onclick="reloadPage()">Ok</button>
		</div>
    </div>
  </div>
</div>