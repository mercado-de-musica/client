<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<input id="csrf" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input id="infosGeraisProdutoUsuarioDto" type="hidden" name="infosGeraisProdutoUsuarioDto" value="${infosGeraisProdutoUsuarioDto.id}" />

<div id="fotos_modal" class="row margin-top-10percent margin-bottom-10percent">
   	<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
		<div class="row">
        	<c:forEach items="${listaDeApresentacoesDto}" var="apresentacao">
        		<c:if test="${apresentacao.tipoApresentacao == 'FOTO'}">
        			<div class="row borda-ajuste-foto margin-2percent">
	         			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0 auto;">
							<img class="fotografia" src="${apresentacao.endereco}${apresentacao.nomeDoArquivo}" id="_${apresentacao.id}"  />
						</div>	
						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6 controles'>
							<button class='zoom_out'     onclick="zoomOut(this)" name="zoom_out" type='button' title='Zoom out'> - </button>
				   			<button class='fit'          onclick="fit(this)" name="fit" type='button' title='Fit image'> [ ]  </button>
				    		<button class='zoom_in'      onclick="zoomIn(this)" name="zoom_in" type='button' title='Zoom in'> + </button>
						</div>
	         			
	         			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 radio">
							<p class="text-right"><label><input type="radio" name="capa" value="${apresentacao.id}">Capa</label></p>
						</div>
	         			
	         		</div>
	         		
        		</c:if>
        	</c:forEach>
        	
        </div>	
		
	</div>
   	<div class="col-xs-12 col-sm-2 col-sm-offset-8 col-md-2 col-md-offset-8 col-lg-2 col-lg-offset-8">
		<button type="button" id="subida_redimensionamento" class="btn btn-default" data-dismiss="modal">Enviar imagens redimensionadas</button>
	</div>
</div>



<div id="ok_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-success">
				Os arquivos subiram corretamente. Dentro de alguns instantes seu anúncio estará em nossas procuras!
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" id="ok_modal_pagina_inicial" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>