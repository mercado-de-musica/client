<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
		
		<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">	
			<p>Ajude-nos a melhorar o Mercado de Música. Envie-nos sua sugestão para que a cada dia o Mercado de Música esteja mais próximo as 
			suas expectativas.</p> 
			
			
			<form:form action="${pageContext.request.contextPath}/fale_conosco_ok" method="post" modelAttribute="faleConoscoDto" id="fale-conosco">
				<input type="hidden" name="tipoDeMensagem" value="ajude_melhorar" class="form-control" />
				
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margin-top-2percent formulario-usuario-pf">
				    <label>Nome:</label>
				    <input type="text" name="nome" value="${usuarioDto.nome}" class="form-control" />
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margin-top-2percent formulario-usuario-pf">
					<label>Sobrenome:</label>
				    <input type="text" name="sobrenome" value="${usuarioDto.sobrenome}" class="form-control" />
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-2percent formulario-usuario-pf">
					<label>E-mail:</label>
				    <input name="email" type="email" class="form-control" placeholder="seu email" id="email_pf" />
				</div>
				
				<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
 				    <label>Mensagem:</label>
 				    <textarea class="form-control" name="mensagem" rows="6"></textarea>
 				</div>
 				
 				<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send"></span> Enviar</button>
				</div>
				
			</form:form>
			
			
		</section>
	</div>
</div>