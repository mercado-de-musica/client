<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
			<h4 class="text-center">Sua mensagem foi enviada!</h4>
			<p class="text-center">Muito obrigado. Em breve responderemos</p>
			<p class="text-center"><img alt="Obrigado" class="img-responsive" src="/image/thank-you.png"></p>
		</div>
	</div>
</div>