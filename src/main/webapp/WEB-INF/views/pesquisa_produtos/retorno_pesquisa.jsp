<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>		
		
		<meta name="_csrf" content="${_csrf.token}"/>
		<meta name="_csrf_header" content="${_csrf.headerName}"/>
		
		<c:choose>
			<c:when test="${pesquisaDeProdutos != null}">
				
				<c:if test="${pesquisaDeProdutos.listaDeSubcategoriasEscolhidas != null && !empty pesquisaDeProdutos.listaDeSubcategoriasEscolhidas && pesquisaDeProdutosDto.produtosEncontrados == true}">
					<c:forEach items="${pesquisaDeProdutos.listaDeSubcategoriasEscolhidas}" var="subcategoriaEscolhida">
						<c:choose>
							<c:when test="${subcategoriaEscolhida.id != null}">
								<p style="margin:0;">${subcategoriaEscolhida.categoria.nome} -> ${subcategoriaEscolhida.nome}</p>
							</c:when>
							<c:otherwise>
								<p style="margin:0;">${subcategoriaEscolhida.categoria.nome}</p>
							</c:otherwise>
						</c:choose>
						
						
					</c:forEach>
				</c:if>
				
				<section class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
					<nav class="navbar navbar-mercado-de-musica">
						<div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a class="navbar-brand" href="#"></a>
					    </div>
					    
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse">
					        <div class="form-group col-sm-12 col-md-12 col-lg-12">
							    <h4 class="categoria">Categorias</h4>
							    
							    
							    
							    
							    
								<ul class="list-group">
									<c:forEach items="${pesquisaDeProdutos.listaDeCategoriasDto}" var="categoriaDto">
							    		 <li class="list-group-item">
								    		<div class="checkbox">
											  <label><input type="checkbox" name="categoriaEscolhida[]" value="${categoriaDto.idCrypt}" onclick="buscarSubcategoriaPorCategoria('${categoriaDto.idCrypt}', this)">${categoriaDto.nome}</label>
											</div>
										</li>	
							    	</c:forEach>								  
								</ul>
						  	</div>
						  	
						  	<div class="form-group col-sm-12 col-md-12 col-lg-12">
						  		<h4 class="categoria">Pesquisa por preço</h4>
						  		<p><b>R$ ${pesquisaDeProdutos.menorValorDosProdutosString} - R$ ${pesquisaDeProdutos.maiorValorDosProdutosString}</b></p>						  		
						  		<p><input name="pesquisa-preco" onchange="mudarpagina()" data-provide="slider" value="" data-slider-min="${pesquisaDeProdutos.menorValorDosProdutos}" data-slider-max="${pesquisaDeProdutos.maiorValorDosProdutos}" data-slider-step="5" data-slider-value="[${pesquisaDeProdutos.primeiroValorDosProdutos},${pesquisaDeProdutos.ultimoValorDosProdutos}]" data-slider-tooltip="show" /></p>
						  	</div>
						  	
						  	
						  	<div class="form-group col-sm-12 col-md-12 col-lg-12">
						  		<button class="btn btn-primary" type="button" onclick="pesquisar();">
							        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							        <span class="top-main-mercado-de-musica texto-branco">Refinar pesquisa</span>
							    </button>
						  	</div>
						  			      
					    </div>
					    
					    
					    
					</nav>	
				</section>
				
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-4 col-lg-2 col-lg-offset-4 margin-top-2percent margin-bottom-2percent text-center">
					<select class="form-control" style="width:100%" id="tipodePesquisa"onchange="escolhaTipoPesquisa(this)">
			    		<c:forEach var="tipoDePesquisa" items="${listaDeTipoPesquisa}">
			    			<c:choose>
			    				<c:when test="${tipoDePesquisa == pesquisaDeProdutos.tipoPesquisa}">
			    					<option value="${tipoDePesquisa}" selected>${tipoDePesquisa.descricao}</option>		
			    				</c:when>
			    				<c:otherwise>
			    					<option value="${tipoDePesquisa}">${tipoDePesquisa.descricao}</option>
			    				</c:otherwise>
			    			</c:choose>
			    			
			    		</c:forEach>
			    	</select>
				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-4 col-lg-3 col-lg-offset-0 margin-bottom-2percent text-center">	
					<nav style="background-color: #FFF;">
					  <ul class="pagination">
					    <c:choose>
					    	<c:when test="${pesquisaDeProdutos.paginas == 1}">
					    		<li class="page-item disabled">
							      <a class="page-link">${pesquisaDeProdutos.paginaAtiva}</a>
							    </li>
					    	</c:when>
					    	<c:otherwise>
					    		
					    		
					    		<li class="page-item">
							      <a class="page-link" onclick="buscaResultadosPorPagina(1)">Primeiro</a>
							    </li>
							    
							    <c:if test="${pesquisaDeProdutos.paginaAtiva == 1 && pesquisaDeProdutos.paginas == 2}">
							    	<c:forEach begin="${pesquisaDeProdutos.paginaAtiva}" end="${pesquisaDeProdutos.paginaAtiva + 1}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
							    
							    <c:if test="${pesquisaDeProdutos.paginaAtiva == 1 && pesquisaDeProdutos.paginas != 2}">
							    	<c:forEach begin="${pesquisaDeProdutos.paginaAtiva}" end="${pesquisaDeProdutos.paginaAtiva + 2}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
						    	<c:if test="${pesquisaDeProdutos.paginaAtiva > 1 && pesquisaDeProdutos.paginaAtiva < pesquisaDeProdutos.paginas}">
						    		<c:forEach begin="${pesquisaDeProdutos.paginaAtiva - 1}" end="${pesquisaDeProdutos.paginaAtiva + 1}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
						    	
						    	<c:if test="${pesquisaDeProdutos.paginas == 2 && pesquisaDeProdutos.paginaAtiva == pesquisaDeProdutos.paginas}">
						    		<c:forEach begin="${pesquisaDeProdutos.paginas - 1}" end="${pesquisaDeProdutos.paginas}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
						    	
						    	<c:if test="${pesquisaDeProdutos.paginas != 2 && pesquisaDeProdutos.paginaAtiva == pesquisaDeProdutos.paginas}">
						    		<c:forEach begin="${pesquisaDeProdutos.paginas - 2}" end="${pesquisaDeProdutos.paginas}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
							    
							    <li class="page-item">
							      <a class="page-link" onclick="buscaResultadosPorPagina(${pesquisaDeProdutos.paginas})">Último</a>
							    </li>
					    		
					    		
					    		
					    		
					    				
					    	</c:otherwise>
					    </c:choose>
					    
					  </ul>
					</nav>	
				</div>
				
				
				<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
					<c:forEach var="apresentacaoProduto" items="${pesquisaDeProdutos.listaApresentacaoProduto}" varStatus="contagem">
						<c:choose>
							<c:when test="${contagem.index % 2 == 0}">
								<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-5 col-md-offset-0 col-lg-3 col-lg-offset-1 apresentacao-inicial margin-4percent">		
							</c:when>
							<c:otherwise>
								<div id="contagem_${contagem.index}" class="col-xs-12 col-sm-4 col-sm-offset-2 col-md-5 col-md-offset-2 col-lg-3 col-lg-offset-1 apresentacao-inicial margin-4percent">
							</c:otherwise>
						</c:choose>
					
							<div class="row margin-1percent">
								<a href="${pageContext.servletContext.contextPath}/detalhamento_produto/${apresentacaoProduto.produto.idCrypt}">
									<img src="${apresentacaoProduto.endereco}" class="img-responsive img-thumbnail" alt="${apresentacaoProduto.endereco}" />
								</a>
							</div>
							<div class="row margin-1percent">
								<c:choose>
									<c:when test="${apresentacaoProduto.produto.preco > 0}">
										<div class="col-xs-2">
											<h4 class="preco-apresentacao-produto-h">R$</h4>
										</div>
										<div class="col-xs-10">
											<h4><a onclick="inserirPesquisaDetalhe('${apresentacaoProduto.produto.idCrypt}')" class="preco-apresentacao-produto margin-1percent" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${apresentacaoProduto.produto.idCrypt}"><b>${apresentacaoProduto.produto.precoString}</b></a></h4>
										</div>
									</c:when>
									<c:otherwise>
										<h4 class="preco-apresentacao-produto-h"><a class="preco-apresentacao-produto margin-1percent" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${apresentacaoProduto.produto.idCrypt}">Indisponível</a></h4>
									</c:otherwise>	
								</c:choose>
								
								<p> 
									<a onclick="inserirPesquisaDetalhe('${apresentacaoProduto.produto.idCrypt}')" class="nome-apresentacao-produto" href="${pageContext.servletContext.contextPath}/detalhamento_produto/${apresentacaoProduto.produto.idCrypt}">
										${apresentacaoProduto.produto.nome} 
									</a>
								</p>
								
							</div>
							<div class="row margin-1percent margin-bottom-10percent">
								<p class="margin-top-3percent nome-apresentacao-usuario"><b>${apresentacaoProduto.produto.usuario.nome}</b></p>							
							</div>
									
<%-- 							<c:forEach var="endereco" items="${apresentacaoProduto.produto.usuario.nome}"> --%>
<%-- 								<c:if test="${endereco.enderecoDefault}"> --%>
<!-- 									<div class="row margin-bottom-10percent"> -->
<%-- 										<p class="margin-top-3percent nome-apresentacao-produto"><b>${endereco.cidade.nome} - ${endereco.cidade.estado.nome}</b></p>							 --%>
<!-- 									</div> -->
<%-- 								</c:if> --%>
<%-- 							</c:forEach> --%>
						</div>
					</c:forEach>	
				</section>
				
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-4 col-lg-3 col-lg-offset-9 margin-bottom-2percent text-center">	
					<nav style="background-color: #FFF;">
					  <ul class="pagination">
					    <c:choose>
					    	<c:when test="${pesquisaDeProdutos.paginas == 1}">
					    		<li class="page-item disabled">
							      <a class="page-link">${pesquisaDeProdutos.paginaAtiva}</a>
							    </li>
					    	</c:when>
					    	<c:otherwise>
					    		
					    		
					    		<li class="page-item">
							      <a class="page-link" onclick="buscaResultadosPorPagina(1)">Primeiro</a>
							    </li>
							    
							    <c:if test="${pesquisaDeProdutos.paginaAtiva == 1 && pesquisaDeProdutos.paginas == 2}">
							    	<c:forEach begin="${pesquisaDeProdutos.paginaAtiva}" end="${pesquisaDeProdutos.paginaAtiva + 1}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
							    
							    <c:if test="${pesquisaDeProdutos.paginaAtiva == 1 && pesquisaDeProdutos.paginas != 2}">
							    	<c:forEach begin="${pesquisaDeProdutos.paginaAtiva}" end="${pesquisaDeProdutos.paginaAtiva + 2}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
						    	<c:if test="${pesquisaDeProdutos.paginaAtiva > 1 && pesquisaDeProdutos.paginaAtiva < pesquisaDeProdutos.paginas}">
						    		<c:forEach begin="${pesquisaDeProdutos.paginaAtiva - 1}" end="${pesquisaDeProdutos.paginaAtiva + 1}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
						    	
						    	<c:if test="${pesquisaDeProdutos.paginas == 2 && pesquisaDeProdutos.paginaAtiva == pesquisaDeProdutos.paginas}">
						    		<c:forEach begin="${pesquisaDeProdutos.paginas - 1}" end="${pesquisaDeProdutos.paginas}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
						    	
						    	<c:if test="${pesquisaDeProdutos.paginas != 2 && pesquisaDeProdutos.paginaAtiva == pesquisaDeProdutos.paginas}">
						    		<c:forEach begin="${pesquisaDeProdutos.paginas - 2}" end="${pesquisaDeProdutos.paginas}" varStatus="loop">
										<c:choose>
											<c:when test="${loop.index == pesquisaDeProdutos.paginaAtiva}">
												<li class="page-item disabled">
											      <a class="page-link">${loop.index}</a>
											    </li>
											</c:when>
											<c:otherwise>
												<li class="page-item">
											      <a class="page-link" onclick="buscaResultadosPorPagina(${loop.index})">${loop.index}</a>
											    </li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
						    	</c:if>
							    
							    <li class="page-item">
							      <a class="page-link" onclick="buscaResultadosPorPagina(${pesquisaDeProdutos.paginas})">Último</a>
							    </li>
					    		
					    		
					    		
					    		
					    				
					    	</c:otherwise>
					    </c:choose>
					    
					  </ul>
					</nav>	
				</div>
					
			</c:when>
			<c:otherwise>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
					<h3 class="text-center">Não existe nenhum resultado que satisfaça sua pesquisa</h3>
				</div>
			</c:otherwise>
		</c:choose>
		
	</div>		
</div>	





<div id="modalDeErroPesquisa" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center">Faltam dados para realizar a pesquisa.</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center">É necessário você colocar sua pesquisa no campo ou escolher uma categoria</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		</div>
    </div>
  </div>
</div>
