<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<footer class="container-fluid margin-top-5percent">
	<nav class="nav nav-footer">
		<section class="col-xs-12 col-sm-3 col-md-3 col-lg-3 logo-footer margin-footer">
			<img id="logo-mercadodemusica-footer" class="img-responsive" src="<c:out value="${pageContext.servletContext.contextPath}" />/image/logo_sem_nome_negativo.png" alt="Mercado de Música footer" />
		</section>
		
		<section class="col-xs-12 col-sm-3 col-md-3 col-lg-3 links-footer margin-footer">
			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/nossa_proposta">Nossa proposta</a></p>
<%-- 			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/como_comprar_pelo_mercado_de_musica">Como comprar pelo Mercado de Música?</a></p> --%>
<%-- 			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/como_vender">Como vender?</a></p> --%>
<%-- 			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/politica_de_compra_e_venda">Política de compra e venda</a></p> --%>
			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/politica_de_privacidade">Política de privacidade</a></p>
		</section>
		
		<section class="col-xs-12 col-sm-3 col-md-3 col-lg-3 fale-conosco-footer margin-footer">
<%-- 			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/lojas_online/10/1">Nossas lojas virtuais</a></p> --%>
			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/fale_conosco">Fale conosco</a></p>
<%-- 			<p class="texto-footer"><a href="${pageContext.servletContext.contextPath}/ajude_a_melhorar">Ajude a melhorar o Mercado de Música</a></p> --%>
			<p class="acerto-tamanho-height-footer">&nbsp;</p>
			<p class="acerto-tamanho-height-footer acerto-tamanho-height-footer-lg">&nbsp;</p>
			<p class="acerto-tamanho-height-footer acerto-tamanho-height-footer-md">&nbsp;</p>
		</section>
		
		<section class="col-xs-12 col-sm-3 col-md-3 col-lg-3 redes-sociais-footer margin-footer">
<!-- 			<div class="col-xs-12"><p class="texto-footer">Siga-nos nas redes sociais</p></div> -->
			
<!-- 			<div class="col-xs-4"> -->
<%-- 				<a href="http://facebook.com/mercadodemusicabrasil" target="_blank"><img src="<c:out value="${pageContext.servletContext.contextPath}" />/image/facebook-logo.png" class="img-responsive" alt="Facebook" /></a> --%>
<!-- 			</div>	 -->
			
<!-- 			<div class="col-xs-4"> -->
<%-- 				<a href="http://twitter.com/mercadodemusica" target="_blank"><img src="<c:out value="${pageContext.servletContext.contextPath}" />/image/twitter-logo.png" class="img-responsive" alt="Twitter" /></a> --%>
<!-- 			</div> -->
			
<!-- 			<div class="col-xs-4"> -->
<%-- 				<a href="http://instagram.com/mercadodemusica" target="_blank"><img src="<c:out value="${pageContext.servletContext.contextPath}" />/image/instagram-logo.png" class="img-responsive" alt="Instagram" /></a> --%>
<!-- 			</div> -->
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-lg-offset-1 margin-footer img-responsive" style="padding: 2px">
				<span id="siteseal"><script defer type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=AoT2UjRUaEnZR7d3ElnwZexIGo8zQExzbT7oeck4CWJ9scnBmsZ9Va2RGFzR"></script></span>
				<script defer type="text/javascript">
					function setImgResponsive() {
						window.setTimeout(function () {
							$('#siteseal').find('img').each(function(){
								if(!$(this).hasClass('img-responsive')) {
									$(this).addClass('img-responsive');
								} else {
									setImgResponsive();
								}
							});
							
						}, 1000);		
					}
					
					setImgResponsive();
					
				</script>
			</div>
				
		</section>
		
		<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-footer">
			<p class="text-center"><a href="${pageContext.servletContext.contextPath}/termos_de_uso">© copyright 2017 Mercado de Música - Termos de uso</a></p>
		</section>
	</nav>
</footer>



<div id="exclusao_like_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro ao dar o like no produto</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Não foi possível dar o like. Por favor, entre em contato conosco
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>

<div id="ir-loja-virtual" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro ao ir para a loja virtual</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Você precisa escolher uma loja para visitar
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="insercao_like_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Seu like foi dado com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-muted">
				Apartir de agora, acompanhe este produto clicando na sua área de usuário - lista de likes
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>



<div id="insercao_carrinho_de_compras_erro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Erro ao inserir produto no carrinho de compras</h3>
		</div>
    	<div class="modal-body">
			<p class="text-center text-danger" id="texto_selecionado_erro">
				Não foi possível inserir o produto no seu carrinho de compras. Por favor entre em contato conosco.
			</p>
			<p class="text-center" id="texto_ser_escrito"></p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>


<div id="insercao_carrinho_de_compras_ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
			<h3 class="text-center text-mercado-de-musica">Produto inserido com sucesso!</h3>
		</div>
    	<div class="modal-body">
			<p class="text-left text-muted">
				Seu produto foi inserido no carrinho de compras com sucesso! Clique no botão carrinho de compras para visualizar seus produtos
			</p>
		</div>
    	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		</div>
    </div>
  </div>
</div>