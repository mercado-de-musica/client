<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
               
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:description" content="Veja esse e outros produtos em mercadodemusica.com.br">
	
	<c:if test="${h3 != null}">
		<meta property="og:title" content="${h3}">
	</c:if>
	
	    
	<link rel="icon" type="image/png" href="<c:out value="${pageContext.servletContext.contextPath}" />/image/logo-mercadodemusica-favicon.png" />
	
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/glyphicons/css/glyphicons.css">
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/css/bootstrap-datepicker.css" rel="stylesheet"/>
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/select2/dist/css/select2.css" rel="stylesheet" />
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="<c:out value="${pageContext.servletContext.contextPath}" />/slick/slick.css" rel="stylesheet"/>
    <link href="<c:out value="${pageContext.servletContext.contextPath}" />/slick/slick-theme.css" rel="stylesheet"/>
    <link href="<c:out value="${pageContext.servletContext.contextPath}" />/bootstrap-slider/css/bootstrap-slider.css" rel="stylesheet"/>
    <link href="<c:out value="${pageContext.servletContext.contextPath}" />/css/util.css" rel="stylesheet"/>
	<link href="<c:out value="${pageContext.servletContext.contextPath}" />/css/usuario.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/font-awesome-4.7.0/css/font-awesome.min.css">
   	<link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/OwlCarousel2-2.2.1/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="<c:out value="${pageContext.servletContext.contextPath}" />/OwlCarousel2-2.2.1/dist/assets/owl.theme.default.min.css">
    
    
	<title>Mercado de Música ${titulo}</title>
  </head>
  <body>	
	<%@include file="../main/header.jsp" %> 
	
	
	
	
	<!-- conteudo dinamico -->
	<div class="container-fluid">
		<jsp:include page="../${pagina}.jsp" />
	</div>	
	<!-- conteudo dinamico -->	
	
		
	<%@include file="../main/footer.jsp" %>
	
	
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/jquery-2.0.0.min.js"></script>
	
	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/select2/dist/js/i18n/pt-BR.js" type="text/javascript"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/bootstrap-datepicker.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/bootstrap-datepicker-pt.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/bootstrap-waitingfor.min.js"></script>
 	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/bootstrap-star-rating/js/star-rating.js" type="text/javascript"></script>
 	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/main.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/OwlCarousel2-2.2.1/dist/owl.carousel.min.js"></script>
    
    
    <script type="text/javascript">
    	$('select').select2({
    		"language": "pt-BR"
    	});
    	
    	$('.rating').rating({displayOnly: true, step: 0.5, rtl: true});
    	
    	$(".owl-carousel").owlCarousel({
    		responsive:{
    	        0:{
    	            items:1//,
//     	            nav:true
    	        },
    	        577:{
    	            items:3,
//     	            nav:false
    	        },
    	        768:{
    	            items:4,
//     	            nav:true,
//     	            loop:false
    	        },
    	        992:{
    	            items:6,
//     	            nav:true,
//     	            loop:false
    	        },
    	        1200:{
    	            items:8,
//     	            nav:true,
//     	            loop:false
    	        }
    	    }
    	});
    	
    </script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/jquery-validation-1.13.1/dist/jquery.validate.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/jquery-validation-1.13.1/dist/additional-methods.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/jquery-validation-1.13.1/dist/localization/messages_pt_BR.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/bootstrap-slider/bootstrap-slider.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/masked-input.js"></script>
    
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/usuario/produtos/categoria.js"></script>
   	<script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/usuario/produtos/pesquisa_produtos.js"></script>

    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/usuario/cadastro_usuario.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/usuario/login_usuario.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/usuario/recuperacao_usuario_senha.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/md5/jquery.md5.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/detalhamento_produto/perguntas.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/detalhamento_produto/parcelamento.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/detalhamento_produto/apresentacao.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/usuario/likes.js"></script>
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/moip-validator.min.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/slick/slick.min.js"></script>
    
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/carrinho_de_compras/carrinho.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/lojas_virtuais/lojas_virtuais.js"></script>
    <script src="<c:out value="${pageContext.servletContext.contextPath}" />/js/fale_conosco_ajude_a_melhorar.js"></script>
    
    
    
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-80581859-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
  </body>
	
	
</html>