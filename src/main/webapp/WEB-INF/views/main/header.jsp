<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<header class="container-fluid">
	<nav class="row padding-top-1percent">
		<div class="col-xs-2 col-sm-2 col-md-3 col-lg-2 col-lg-offset-1">
			<p><span class="fa fa-shield"></span> <span class="top-main-mercado-de-musica">Vendedores certificados</span></p>
		</div>
		<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
			<p class=""><span class="fa fa-lock"></span> <span class="top-main-mercado-de-musica">Compra segura</span></p>
		</div>
		
<%-- 		<security:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')"> --%>
<!-- 			<div class="col-xs-2 col-sm-2 col-md-3 col-lg-2"> -->
<%-- 				<a href="${pageContext.servletContext.contextPath}/usuario"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="top-main-mercado-de-musica">${nomeUsuario}</span></a> --%>
<!-- 				<p><span class="glyphicon glyphicon-user"></span> <span class="top-main-mercado-de-musica">Erik Scaranello</span></p> -->
<!-- 			</div> -->
<%-- 		</security:authorize> --%>
		
<%-- 		<security:authorize access="!hasRole('ROLE_ADMIN') and !hasRole('ROLE_USER')"> --%>
<!-- 			<div class="col-xs-2 col-sm-2 col-md-3 col-lg-2"> -->
<%-- 				<a href="${pageContext.servletContext.contextPath}/usuario"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="top-main-mercado-de-musica">Área do usuário</span></a> --%>
<!-- 				<p><span class="glyphicon glyphicon-user"></span> <span class="top-main-mercado-de-musica">Erik Scaranello</span></p> -->
<!-- 			</div> -->
<%-- 		</security:authorize> --%>
		
		
		
		<div class="col-xs-2 col-sm-2 col-md-1 col-lg-2">
			<p><a href="http://facebook.com/mercadodemusicabrasil"><span class="fa fa-facebook-square"></span> <span class="top-main-mercado-de-musica midias-sociais">Facebook</span></a></p>
		</div>
		<div class="col-xs-2 col-sm-2 col-md-1 col-lg-2">
			<p><a href="http://twitter.com/mercadodemusica"><span class="fa fa-twitter-square"></span> <span class="top-main-mercado-de-musica midias-sociais">Twitter</span></a></p>
		</div>
		<div class="col-xs-2 col-sm-2 col-md-1 col-lg-2">
			<p><a href="http://instagram.com/mercadodemusica"><span class="fa fa-instagram"></span> <span class="top-main-mercado-de-musica midias-sociais">Instagram</span></a></p>
		</div>
	</nav>
	
	<div class="row">
		<figure class="col-xs-12 col-sm-4 col-md-4 col-lg-3 margin-top-2percent">
			<a href="${pageContext.servletContext.contextPath}/">
				<img id="logo-mercadodemusica" class="img-responsive" src="<c:out value="${pageContext.servletContext.contextPath}" />/image/logo-mercadodemusica.jpg" alt="Mercado de Música" />
			</a>
		</figure>
		
		<section class="col-xs-9 col-sm-6 col-md-6 col-lg-7 margin-top-2percent">
			<input type="hidden" name="produtosPorPagina" value="18" />
			<input type="hidden" name="pagina" value="1" />
			<input type="hidden" name="tipoPesquisa" value="PRECO" />
			
			<div class="input-group">
				<c:choose>
					<c:when test="${produtoPesquisado == null || produtoPesquisado == '' || produtoPesquisado == 'null'}">
						<input type="text" value="" class="form-control" name="pesquisa" placeholder="Produto e/ou acessório musical">
					</c:when>
					<c:otherwise>
						<input type="text" value="${produtoPesquisado}" class="form-control" name="pesquisa" placeholder="Produto e/ou acessório musical">
					</c:otherwise>
				</c:choose>
				
				<span class="input-group-btn">
			    <button class="btn btn-primary" onclick="pesquisar()">
			        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			    </button>
			  	</span>
			</div>
		</section>
		
		<div class="col-xs-2 col-sm-2 col-md-2 col-lg-1 margin-top-2percent">
			<security:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')">
				<button class="btn btn-primary" type="button">
			        <span class="fa fa-usd" aria-hidden="true"></span>
			        <span class="top-main-mercado-de-musica texto-branco">Vender</span>
			    </button>
			</security:authorize>  
			<security:authorize access="!hasRole('ROLE_ADMIN') and !hasRole('ROLE_USER')">
				<!-- onclick="window.location = '${pageContext.servletContext.contextPath}/cadastre_se'"  -->
				<button class="btn btn-primary" type="button">
			        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
			        <span class="top-main-mercado-de-musica texto-branco">Cadastre-se</span>
			    </button>
			</security:authorize>	  
		</div>
	</div>
<!-- 	<div class="row"> -->
<!-- 		<p class="categorias">GUITARRAS | BAIXOS | BATERIAS | MICROFONES | AMPLIFICADORES | EFEITOS | ACESSÓRIOS | ÁUDIO | VIDEOAULAS | BLOG | BANDAS</p> -->
<!-- 	</div> -->
	
<!-- 	<div class="row"> -->
<!-- 		<p class="publicidade">----------------------</p> -->
<!-- 	</div> -->
	
</header>

<div class="container-fluid">
	<div class="row">
		
		<c:if test="${voltarPesquisa != null}">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
				<p><a href="${voltarPesquisa}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Voltar</a></p>
			</div>
		</c:if>
	
		<c:if test="${h3 != null}">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-1percent">
				<h3 class="text-h3 text-center">
					${h3}
				</h3>
			</div>
		</c:if>
		