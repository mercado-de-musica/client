<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<section class="col-xs-12 col-sm-12 col-md-8 col-lg-12 margin-top-1percent">
			<p>O Mercado de música é um portal que oferece um espaço para músicos e pessoas que gostem de produtos musicais para comprar e vender os mesmos. 
				A leitura e aceitação dos termos abaixo são obrigatórias para o cadastro e uso dos serviços oferecidos pelo Mercado de Música 
			 (Erik Scaranello Eireli - ME - CNPJ 18.252.603/0001-46)</p>
			 
			<p>Ao realizar cadastro como usuário, comprar e/ou vender algum item, o mesmo declara que concorda com as condições apresentadas no presente Termo de Uso.</p> 
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Serviço</h4>
			<p>O Mercado de Música é um serviço de compra e venda de equipamentos musicais na internet.</p>
			<p>Apenas permitido a venda equipamentos musicais, equipamentos eletrônicos que sirvam à música. O Mercado de Música se reserva a vetar, desativar e/ou denunciar quaisquer anúncios de produtos que não respeitem a veracidade e procedência formais.</p>
		
			<h4 class="text-mercado-de-musica margin-top-3percent">Comissão</h4>
			<c:if test="${porcentagemAtivaDto != null}">
				<p>A comissão do Mercado de Música é de <b>${porcentagemAtivaDto.porcentagemString}%</b> sobre cada produto vendido</p>
			</c:if>
			
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Frete de produtos</h4>
			<p>O Mercado de Música se responsabiliza pelo cálculo do frete de acordo com os valores oferecidos pelos Correios</p>
			<p>O mercado de Música não se responsabiliza por inserções de informações erradas quanto ao peso e/ou tamanho dos produtos. Causando diferenças no frete</p> 
			
			
			
			<%@include file="equipe_mercado_de_musica.jsp" %>
				
		</section>
	</div>
</div>	
		