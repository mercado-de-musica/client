<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<section class="col-xs-12 col-sm-12 col-md-8 col-lg-12 margin-top-1percent">
			
			<h4 class="text-mercado-de-musica">Cadastramento de anúncios</h4>
			<p>O Mercado de Música, pela natureza de seus serviços, em alguns casos 
			revela informações coletadas de seus usuários relativos aos dados cadastrais para terceiros, tais como 
			empresas integrantes de seu grupo econômico, parceiros comerciais, autoridades e pessoas físicas ou jurídicas que aleguem ter sido lesadas por usuários cadastrados.</p>
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Armazenamento de dados pessoais dos usuários</h4>
			<p>O primeiro passo para utilizar os serviços do Mercado de Música é cadastrar e informar seus dados pessoais ou empresariais, completos e 
			exatos (número de telefone, e-mail, endereço física, etc). O Mercado de Música poderá confirmar os dados pessoais informados consultando 
			entidades públicas, companhias especializadas ou bancos de dados e está desde já expressamente autorizado a fazê-lo.</p>
			
			<p>A qualquer momento o Usuário cadastrado no Mercado de Música poderá solicitar a exclusão definitiva de seus dados ou o 
			cancelamento de seu cadastro.</p>
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Segurança da informação</h4>
			<p>O Mercado de Música está obrigado a observar todas as normas aplicáveis em medidas de segurança de Informação. 
			Além disso, empregamos padrões avançados em matéria de proteção das informações incluindo, entre outras medidas, firewalls e Secure Socket Layers ("SSL"). </p>
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Senha pessoal</h4>
			<p>Para acesso dos serviços reservados aos Usuários devidamente Cadastrados, estes irão dispor de uma senha. 
			Com ela poderão comprar e vender produtos dentro do Mercado de Música. Esta senha, que é escolhida pelo próprio Usuário deve ser mantida sob absoluta 
			confidencialidade e em nenhum caso, deverá ser revelada ou compartilhada com outras pessoas.</p>
			
			
			<%@include file="equipe_mercado_de_musica.jsp" %>
			
			
		</section>
	</div>
</div>		