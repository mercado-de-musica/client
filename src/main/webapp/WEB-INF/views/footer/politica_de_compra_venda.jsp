<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
			
			<h4 class="text-mercado-de-musica">Cadastramento de anúncios</h4>
			<p>Mediante o cadastro de um produto, o usuário aceita as políticas aqui contidas.</p>
			<p>Destacam-se o produtos proibidos e políticas para cadastramento de produtos algumas das regras estabelecidas para o cadastramento de anúncios. Também 
			as tarifas associadas aos produtos.</p>
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Anúncios encerrados pelo Mercado de Música</h4>
			<p>Alguns anúncios podem ser encerrados pelo Mercado de Música sem aviso prévio ao usuário anunciante.</p>
			
			<p>Caso o anúncio tenha sido encerrado, pode ser porque ele violou alguma regra, como:</p>
			<p>
				<ul>
					<li>Produto proibido por lei.</li>
					<li>Produto identificado como infrator dos direitos de Propriedade Intelectual de um terceiro.</li>	
				</ul>
			</p>
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Descrição do anúncio</h4>
			<p>Os anúncios podem ser compostos por textos, descrições e fotos, vídeos e áudios do produto oferecido. 
				O produto oferecido pelo Usuário Vendedor deve ser descrito corretamente, de acordo com o seu estado de conservação, 
				tamanho, marca, modelo, cor, material e outras características que sejam relevantes. No caso da inclusão de novas fotografias, áudios e/ou 
				vídeos, as mesmas devem corresponder especificamente ao produto anunciado.</p>
			
			<p>É proibido o uso de códigos e/ou palavras nas descrições que realizem:</p>
			<p>
				<ul>
					<li>Solicitar ou obter informações de outros usuários</li>
					<li>Enviar os usuários para páginas fora do Mercado de Música</li>
					<li>Enviar suas próprias informações para outros usuários</li>
				</ul>	
			</p>		
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Inclusão de dados pessoais</h4>
			<p>É terminantemente proibida a inserção de informações pessoais ou qualquer forma de contato com o usuário na descrição dos anúncios, nas perguntas ao vendedor e respostas aos compradores.</p>
			<p>Considera-se informação de contato entre outras: endereço de e-mail, endereço residencial ou comercial, caixa postal, número(s) de telefone e/ou fax, assim como URL ou endereço de outros sites.</p>
			<p>Os dados pessoais dos negociantes será disponibilizado pelo Mercado de Música após a finalização da negociação. 
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Links no anúncio</h4>
			<p>A descrição do produto cadastrado deve ser utilizada para descrever, explicar, promover, oferecer e 
			facilitar a venda do produto. Neste campo não é permitido citar ou promover o seu próprio site, incluindo link de outro site pertencente ao 
			vendedor ou a terceiros.</p>
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Cadastro do anúncio na categoria adequada</h4>
			<p>O usuário deverá anunciar somente aquilo que realmente deseja vender e colocá-lo na categoria e subcategoria correspondentes, de acordo com o tipo de produto.</p>
			
			
			<h4 class="text-mercado-de-musica margin-top-3percent">Técnicas para subverter o sistema de cobrança</h4>
			<p>É proibido o emprego de qualquer mecanismo ou técnica destinada a evitar o pagamento das tarifas. Por exemplo:</p>
			<p>
				<ul>
					<li>Oferecer na descrição ou perguntas e respostas concluir uma transação sem realizar a compra pelo Mercado de Música ou fora da plataforma</li>
					<li>Oferecer um produto a um preço diferente do expresso na descrição ou em perguntas e respostas</li>
					<li>Incluir dados pessoais nas descrições, títulos ou respostas</li>
					<li>Oferecer um produto de forma gratuita ou a um valor simbólico ou preço significativamente menor do que o valor de mercado, quando disso se possa inferir que haja intenção de vender outra quantidade, 
					ou a outro preço para evitar pagar pelos serviços do Mercado de Música</li>
					<li>Anunciar produtos para serem trocados</li>
				</ul>
			</p>
			<p>As práticas que visam iludir a cobrança dos serviços do Mercado de Música são 
			terminantemente proibidas e o infrator poderá ser sancionado com a 
			suspensão ou cancelamento do anúncio, bem como do seu cadastro como Usuário.</p>
			
			
			
			<%@include file="equipe_mercado_de_musica.jsp" %>
			
		</section>
	</div>
</div>		