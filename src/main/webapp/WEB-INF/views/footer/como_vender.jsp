<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
			
			
			<p class="margin-bottom-10percent">Ao vender no Mercado de Música, nós garantimos negociações mais próximas entre você e o comprador, 
			além de transações financeiras mais transparentes entre o Mercado de Música e você.</p>
			
			
			<p>No Mercado de Música o processo de venda de um produto já começa quando você vai colocá-lo no site. Ao cadastrá-lo você deve escolher 
			como será o tipo de envio dele. Podemos trocar nossos produtos com o comprador usando os Correios ou a entrega em mãos. Você deve entrar em 
			<b>Área de usuários > Meus produtos à venda</b> para cadastrá-lo conforme a figura abaixo:</p> 
			
			
			<p><img class="img-responsive img-thumbnail" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/cadastramento_de_produtos.jpg" alt="Cadastramento de produtos" /></p>
			
			<p class="error"><span class="glyphicon glyphicon-exclamation-sign"></span> Caso o produto não esteja de acordo com as especificações do 
			<a href="https://www.correios.com.br/para-voce/precisa-de-ajuda/limites-de-dimensoes-e-de-peso" target="_blank">site do Correios</a>,  
			dentro dos limites de peso <b>(30kg)</b> e tamanho <b>(1,05m)</b>, o Mercado de Música fará com que sua entrega seja feita em mãos 
			automaticamente.</p>
			
			
			
			<h4 class="text-mercado-de-musica margin-top-10percent">Venda com envio de produtos pelos Correios</h4>
			
			<p>Ao escolher o frete pelos Correios, quem paga o frete é o comprador pelo próprio Mercado de Música. É ele quem decide se o produto será 
			postado via SEDEX 10, SEDEX ou outro produto dos Correios.</p>
			
			
			<p>Após finalizado processo de compra, você receberá um e-mail avisando que seu produto foi vendido. Você deve entrar em 
			<b>Área de usuários > Produtos vendidos > Produto que você vendeu > Rastreamento do produto > Gerar chancela dos Correios para impressão</b> 
			para imprimir a chancela dos Correios e colocar na embalagem do seu produto</p>
			
			
			<p class="error margin-top-3percent margin-bottom-3percent"><span class="glyphicon glyphicon-exclamation-sign"></span> O Mercado de Música pede que seja respeitado os tamanhos e pesos dos produtos para que não exista 
			problemas/travamentos no envio pelos Correios.</p>
			
			
			
			<p>Após o envio do produto pelos Correios, entre em <b>Área de usuários > Produtos vendidos > Produto que você vendeu > Rastreamento do produto</b> 
			para verificar como anda o rastreamento do pedido que você enviou.</p>			
			
			
<!-- 			envio pessoal  -->
			<h4 class="text-mercado-de-musica margin-top-10percent">Venda com envio de produtos em mãos</h4>
			
			<p>Quando escolhemos a entrega de produtos em mãos, são gerados dois códigos, o <b>código de comprador</b> e o <b>código de vendedor</b>. 
			Esses códigos devem ser trocados pelos mesmos.</p>
			
			<p class="margin-top-3percent margin-bottom-3percent"><b>Como devo usar esses códigos?</b></p>
			
			<p>O Mercado de Música disponibiliza dados como Nome, telefones e e-mail do comprador logo após a 
			finalização da compra em <b>Área de usuários > Produtos vendidos > Seu produto vendido > Informações do comprador</b> conforme figura abaixo. Com esses dados em mãos, os envolvidos na negociação devem tratar entre si onde será feito a troca do produto.</p>
			
			
			<p><img class="img-responsive img-thumbnail" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/produtos_vendidos.jpg" alt="Área de produtos vendidos" /></p>
			
			
			
			<p class="margin-top-10percent">Após a troca do produto, o comprador deverá fornecer o código dele a você. Este código deverá ser postado em 
			<b>Área de usuários > Produtos vendidos > Seu produto > Código de compra dado pelo comprador (entrega em mãos)</b> para que a negociação seja 
			concluída.</p>
			
			<p>Você deve imprimir seu código de vendedor para entregá-lo ao comprador do produto, o link de impressão está em 
			<b>Área de usuários > Produtos vendidos > Seu produto > Seu código de venda de produto (entrega em mãos) >  Geração de pdf do código para a entrega ao comprador</b>
			
			<p>A imagem a seguir demonstra onde deverá ser colocado o código recebido pelo comprador e também onde está seu código para impressão:</p>
			
			
			<p><img class="img-responsive img-thumbnail" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/codigos_vendedor.jpg" alt="Códigos da área do comprador" /></p>
			
			
			<p class="error"><span class="glyphicon glyphicon-exclamation-sign"></span> Aconselhamos você vendedor a colocar o código fornecido pelo comprador já no momento da troca pelo seu celular. 
			Para evitar fraudes no processo de troca de produtos.</p>
			
			
			
			<h4 class="text-mercado-de-musica margin-top-10percent">Transferência de valores</h4>
			
			<p>
				De acordo com a lei <b>
				<a href="http://www.jusbrasil.com.br/topicos/10601327/artigo-49-da-lei-n-8078-de-11-de-setembro-de-1990" target="_blank">
					Art. 49 do Código de Defesa do Consumidor - Lei 8078/90
				</a></b> o comprador tem até 7 dias após o recebimento do produto para desistir do negócio sem que haja encargos para ele. 
				Tendo isso em mente você deve saber que apenas após este prazo o Mercado de Música poderá liberar o valor na sua conta corrente.
			</p>			

			<p class="margin-top-3percent margin-bottom-3percent"><b>Como devo fazer para retirar meu dinheiro?</b></p>
			
			<p class="margin-top-3percent margin-bottom-3percent">Dentro da <b>Área de usuários > Conta corrente</b> estão as informações sobre sua conta corrente 
			com o Mercado de Música conforme figura abaixo:</p>
			
			<p><img class="img-responsive img-thumbnail" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/conta_corrente.jpg" alt="Códigos da área do comprador" /></p>
			
			<p class="margin-top-3percent margin-bottom-3percent">Para sacar o dinheiro, primeiro é necessário cadastrar uma conta corrente para o recebimento. 
			Após o cadastramento, devemos ir em <b>Área de usuários > Conta corrente > Produtos já liberados para pagamento</b> e incluir o produto vendido na nossa transferência.</p>
			
			<p>Após o fechamento da transferência, o Mercado de Música enviará o dinheiro para você.</p>
			
			<%@include file="equipe_mercado_de_musica.jsp" %>
			
		</section>
		
	</div>
</div>