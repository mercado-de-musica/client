<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<section class="col-xs-12 col-sm-12 col-md-8 col-lg-12 margin-top-1percent">
			
			<h3 class="text-center">Nossa proposta</h3>
				
			<br />
			
			<h4 class="text-center text-mercado-de-musica">Ajudar a construir um mundo mais musical e com pessoas mais leves</h4>
		
			<br />
			
			<div class="col-xs-12 col-sm-9 col-md-8 col-lg-9 margin-top-1percent">
				
				<p>A música é uma das mais apaixonantes expressões artísticas. Atua diretamente no nosso cérebro, estado de espírito, 
				expressa a cultura e nos diverte.</p>
				
				<p>Ela influencia o nosso desempenho no dia a dia e traz impactos positivos em todas as esferas de nossa vida. Profissional, 
				pessoal e familiar.</p>
				
				<p>E a nossa proposta é proporcionar as melhores escolhas para quem se interessa por fazer música.</p>
				
				<p>É apresentar os produtos certos e os melhores preços.</p> 
				
				
				<%@include file="equipe_mercado_de_musica.jsp" %>
				
			</div>
			
			<div class="col-sm-3 col-md-4 col-lg-3 margin-top-1percent microfone-nossa-proposta">
				<p class="text-right"><img src="<c:out value="${pageContext.servletContext.contextPath}" />/image/microfone_mercadodemusica.jpg" alt="microfone" /></p>
			</div>
			
		</section>
		
	</div>
</div>