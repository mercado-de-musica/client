<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9 margin-top-1percent">
			
			
			<p class="margin-bottom-10percent">Comprar no Mercado de Música é muito rápido e fácil! Aqui vamos colocar os pontos mais importantes 
			para ocorrer uma compra de produtos e troca segura, sem prejuízos.</p>
			
			
			
			<h4 class="text-mercado-de-musica">Para facilitar sua vida</h4>
			
			<p>No Mercado de Música você pode colocar 1 ou mais produtos dentro de uma compra. 
			Com isso você terá a facilidade de comprar tudo o que você precisa de uma vez só. Sem perder tempo!</p>
			
			
			<p>Caiu a conexão? Não se preocupe, o Mercado de Música reserva o produto que você deseja 
			comprar até o final do dia.</p>
			
			<p>Não foi possível finalizar a compra tão desejada? Na sua área de usuário existe um lugar com todas 
			as suas compras não finalizadas e caso os produtos estejam disponíveis ainda, você pode finalizar a compra quando você quiser. Vá em 
			<b>Área de usuários > Compras não finalizadas</b></p> 
			
			
			
			
			
			
			
			<h4 class="text-mercado-de-musica margin-top-10percent">Compra com envio de produtos pelos Correios</h4>
			
			<p>Ao cadastrar um produto, o vendedor pode escolher pelo tipo de entrega que será realizado. No caso da entrega pelos Correios, 
			o produto deve estar com tamanho e peso dentro das especificações.</p>
			
			<p>Porém quem paga pelo frete do produto é você comprador. Mas fique tranquilo, o Mercado de Música calcula o valor que você vai 
			pagar pelo frete e te dá a opção do frete que mais convém. Por exemplo <b>SEDEX 10</b> ou <b>SEDEX.</b></p>
			
			<p>Na tela do carrinho de compra, além do preço do produto já aparecerá a escolha de frete oferecida pelo Mercado de Música conforme figura a seguir:</p>
			
			<p><img class="img-responsive img-thumbnail" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/carrinho_compras_correios.jpg" alt="carrinho de compras correios" /></p>
			
			
			<p class="margin-top-10percent">Depois de finalizado o processo de compra dos produtos, é só acessar <b>Área de usuários > Produtos comprados > Seu ptoduto comprado > Rastreamento do produto</b> 
			para verificar como está o status dos Correios de cada um dos produtos comprados.</p>
			
			<p>Dentro de <b>Área de usuários > Produtos comprados > Seu produto comprado > Informações do vendedor</b> o Mercado de Música disponibiliza os dados do vendedor de cada produto comprado por você. Conforme figura abaixo:</p>
			
			
			<p><img class="img-responsive img-thumbnail" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/produtos_comprados.jpg" alt="Área de produtos comprados" /></p>
			
			
			
<!-- 			envio pessoal  -->
			<h4 class="text-mercado-de-musica margin-top-10percent">Compra com envio de produtos em mãos</h4>
			
			<p>Muitos dos produtos comercializados pelo Mercado de Música não podem ser enviados pelos Correios, de acordo com o 
			<a href="https://www.correios.com.br/para-voce/precisa-de-ajuda/limites-de-dimensoes-e-de-peso" target="_blank">site do Correios</a> 
			os limites de envio de produtos são: <b>30kg</b> e <b>1,05m</b> de tamanho.</p>
			
			<p class="margin-top-3percent margin-bottom-3percent"><b>Como resolver esse problema?</b></p>
			
			<p>A primeira coisa que nós do Mercado de Música pensamos foi em trazer os produtos dos vendedores que estão mais próximos do comprador. 
			Por exemplo: se o comprador procura uma bateria de marca X, trazemos os vendedores desta bateria de marca X que estão mais próximos nos primeiros resultados da pesquisa.</p>
			
			<p>O segundo ponto é que a cada entrega de produtos em mãos geramos dois códigos, o <b>código de comprador</b> e o <b>código de vendedor</b>. 
			Esses códigos devem ser trocados pelos mesmos.</p>
			
			<p class="margin-top-3percent margin-bottom-3percent"><b>Como devo usar esses códigos?</b></p>
			
			<p>Como já dito acima, o Mercado de Música disponibiliza dados como Nome, telefones e e-mail do comprador e do vendedor logo após a 
			finalização da compra. Com esses dados em mãos, os envolvidos na negociação devem tratar entre si onde será feito a troca do produto.</p>
			
			<p>Após a troca do produto, o vendedor deverá fornecer o código dele a você comprador. Este código deverá ser postado em 
			<b>Área de usuários > Produtos comprados > Seu produto > Código de venda dado pelo vendedor (entrega em mãos)</b> para que a negociação seja 
			concluída.</p>
			
			<p>Você deve imprimir seu código de comprador para entregá-lo ao vendedor do produto, o link de impressão está em 
			<b>Área de usuários > Produtos comprados > Seu produto > Seu código de compra de produto (entrega em mãos) > Geração de pdf do código para a entrega ao vendedor</b>
			
			<p>A imagem a seguir demonstra onde deverá ser colocado o código recebido pelo vendedor e também onde está seu código para impressão:</p>
			
			
			<p><img class="img-responsive img-thumbnail" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/image/codigos_comprador.jpg" alt="Códigos da área do comprador" /></p>
			
			
			<p class="error"><span class="glyphicon glyphicon-exclamation-sign"></span> Aconselhamos você comprador a colocar o código fornecido pelo vendedor já no momento da troca pelo seu celular. 
			Para evitar fraudes no processo de troca de produtos.</p>
			
			
			<%@include file="equipe_mercado_de_musica.jsp" %>
			
		</section>
		
	</div>
</div>